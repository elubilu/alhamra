﻿<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Super_model extends CI_Model {

	public function my_info($user_id){
        $q=$this->db
            ->select('*')
            ->from('admin_info')
            ->where('adminID',$user_id)
            ->where('adminStatus', 1)
            ->get();
        if($q->num_rows()==1){
			return $q->row();
		}
        else{
            return FALSE;
        }
    }

	public function update_contact($admin_id,$data){
		return $this->db
			        ->where('adminID',$admin_id)
				    ->update('admin_info',$data);
	}
	
	public function change_password($data,$admin_id){
		return $this->db
			        ->where('adminID',$admin_id)
				    ->update('admin_info',$data);
	}

	public function add_product_group($data)
	{
		return $this->db->insert('product_group_info', $data );
	}
	
	public function all_product_group($limit,$offset)
	{
		
		$q=$this->db
				->select('*')
				->from('product_group_info')
				->limit($limit,$offset)
				->get();
				
		if($q->num_rows()){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	public function all_expense_field($limit,$offset)
	{
		
		$q=$this->db
				->select('*')
				->from('expense_field_info')
				->limit($limit,$offset)
				->get();
				
		if($q->num_rows()){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	
	public function add_expense_field($data)
	{
		return $this->db->insert('expense_field_info', $data );
	}
	
	public function all_shop($limit,$offset)
	{
		
		$q=$this->db
				->select('*')
				->from('shop_info')
				->limit($limit,$offset)
				->get();
				
		if($q->num_rows()){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	
	public function add_shop($data)
	{
		return $this->db->insert('shop_info', $data );
	}
	
	public function get_all_shop_id_name()
	{
		
		$q=$this->db
				->select('*')
				->from('shop_info')
				->where('shopStatus',1)
				->get();
				
		if($q->num_rows()){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	public function check_admin_id($user_id)
	{
		
		$q=$this->db
				->select('adminUserID')
				->from('admin_info')
				->where('adminUserID',$user_id)
				->get();
				
		if($q->num_rows()){
			return False;
		}
		else{
			return true;
		}
	}
	
	public function add_user($data)
	{
		return $this->db->insert('admin_info', $data );
	}
	
	public function all_user($limit,$offset)
	{
		
		$q=$this->db
				->select('*')
				->from('admin_info')
				->where('admin_role_roleID>',1)
				->limit($limit,$offset)
				->get();
				
		if($q->num_rows()){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	/*function multiexplode ($delimiters,$string) {

        $ready = str_replace($delimiters, $delimiters[0], $string);
        $launch = explode($delimiters[0], $ready);
        return  $launch;
    }
	public function my_info($user_id){
        $q=$this->db
            ->select('*')
            ->from('admin_info')
			->join('admin_role','admin_role.role_id=admin_info.admin_role_role_id')
            ->where('admin_id',$user_id)
            ->where('admin_status', 1)
            ->get();
        if($q->num_rows()==1){
			return $q->row();
		}
        else{
            return FALSE;
        }
    }

    public function admin_info()
	{
		
		$q=$this->db
				->select('*')
				->from('admin_info')
				->join('admin_role','admin_role.role_id=admin_info.admin_role_role_id')
				->where('admin_role_role_id!=',1)
				->limit(500)
				->get();
				
		if($q->num_rows()){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}


	public function update_contact($admin_id,$data){
		return $this->db
			        ->where('admin_id',$admin_id)
				    ->update('admin_info',$data);
	}
	
	public function change_password($data,$admin_id){
		return $this->db
			        ->where('admin_id',$admin_id)
				    ->update('admin_info',$data);
	}
	
	public function get_all_status(){
		$q=$this->db
				->select('status_id,status_name')
				->from('status')
				->get();

		if($q->num_rows()){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}

	public function get_category_parent()
	{

		$q=$this->db
				->select('category_id,category_name')
				->from('category')
				->where('category_status',1)
				->get();

		if($q->num_rows()){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	public function get_parent_depth($parent)
	{

		$q=$this->db
				->select('depth')
				->from('category')
				->where('category_id',$parent)
				->get();

		if($q->num_rows()==1){
			return $q->row()->depth;
		}
		else{
			return FALSE;
		}
	}
	public function add_category($data)
	{
		return $this->db->insert('category', $data );
	}
	
	public function categories_info($limit,$offset){
        $q=$this->db
            ->select('*')
            ->from('category')
			->limit($limit,$offset)
            ->get();
        if($q->num_rows()){
			return $q->result();
		}
        else{
            return FALSE;
        }
    }
	
	public function get_category($category_id){
        $q=$this->db
            ->select('category.*,admin_info.name')
            ->from('category')
			->join('admin_info','admin_info.admin_id=category.admin_id')
			->where('category_id',$category_id)
            ->get();
        if($q->num_rows()){
			return $q->row();
		}
        else{
            return FALSE;
        }
    }
	public function get_category_child(){
		$q=$this->db
				->select('category_id,category_name')
				->from('category')
				->where('category.depth!=',1)
				->get();

		if($q->num_rows()){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	public function get_category_parent_by_id($parent_id){
        $q=$this->db
            ->select('category_name')
            ->from('category')
			->where('category_id',$parent_id)
            ->get();
        if($q->num_rows()){
			return $q->row()->category_name;
		}
        else{
            return FALSE;
        }
    }
	public function update_category($data,$category_id){
		return $this->db
			        ->where('category_id',$category_id)
				    ->update('category',$data);
	}
	
	public function get_unit_name()
	{

		$q=$this->db
				->select('unit_id,unit_name')
				->from('product_unit')
				->get();

		if($q->num_rows()){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	public function get_related_product_name()
	{

		$q=$this->db
				->select('product_id,product_name')
				->from('products')
				->get();

		if($q->num_rows()){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	
	public function get_parent_product_name()
	{

		$q=$this->db
				->select('parent_id,parent_product_name')
				->from('parent_product')
				->where('product_position',1)
				->get();

		if($q->num_rows()){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	public function get_related_product_by_id($product_id){
		$q=$this->db
				->select('*,products.product_name')
				->from('related_product_info')
				->join('products','related_product_info.related_product_id=products.product_id')
				->where('products_product_id',$product_id)
				->get();

		if($q->num_rows()){
			$q= $q->result();
			$s="";
			foreach($q as $q){
				$s=$s.$q->product_name.", ";
			}
			$s=rtrim($s,',');
			return $s;
			//return $q->result();
		}
		else{
			return FALSE;
		}
	}
	
	function bn2enNumber ($number){
		$search_array= array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০");
		$replace_array= array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
		$en_number = str_replace($search_array, $replace_array, $number);

		return $en_number;
	}
	
	public function store_produuct_parent($data)
	{
		$this->db->insert('parent_product', $data );
		return $this->db->insert_id();
	}
	public function add_product($data,$related_products)
	{
		$this->db->insert('products', $data );
        $product_id = $this->db->insert_id();
		$tags =  $this-> multiexplode(array(",","#",".","|",":"),$data['tags']);

        foreach ($tags as $tag) {

            if($tag==null){
                contiune;
            }

            $data=array(
                'tag_name'=>$tag,
                'products_product_id'=>$product_id

            );
           // print_r($data);

            $this->db->insert('tags', $data );
        }
		if($related_products){
			foreach ($related_products['related_product'] as $products){
				$data=array(
					'related_product_id'=>$products,
					'products_product_id'=>$product_id

				);
			   // print_r($data);
				$this->db->insert('related_product_info', $data );
			}

		}
		
		return true;
		
	}
	
	public function updateProductQuantity($data){
			return $this->db
			     ->where('product_id',$data['product_id'])
				 ->set('product_quantity', 'product_quantity + ' . (int) $data['product_quantity'], FALSE)
				 ->update('products');
	}
	
	
	public function get_all_products($limit,$offset){
        $q=$this->db
            ->select('*')
            ->from('products')
			->join('parent_product','products.parent_product_parent_id=parent_product.parent_id')
            ->limit($limit,$offset)
			->get();
        if($q->num_rows()){
			return $q->result();
		}
        else{
            return FALSE;
        }
    }
	
	function eng2bangla ($number){
		
		$search_array= array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
		$replace_array= array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০");
		$en_number = str_replace($search_array, $replace_array, $number);
		return $en_number;
	}
	
	public function get_product_infos_by_id($product_id){
        $q=$this->db
            ->select('*')
            ->from('products')
			->join('parent_product','products.parent_product_parent_id=parent_product.parent_id')
			->join('category','parent_product.category_category_id=category.category_id')
			->join('admin_info','products.admin_info_admin_id=admin_info.admin_id')
			->join('product_unit','products.product_unit_unit_id=product_unit.unit_id')
			->join('discount_info','discount_info.discount_id=products.discount_info_discount_id')
			->where('product_id',$product_id)
            ->get();
        if($q->num_rows()){
			return $q->row();
		}
        else{
            return FALSE;
        }
    }
	
	public function update_produuct_parent($data,$parent_id){
		return $this->db
			        ->where('parent_id',$parent_id)
				    ->update('parent_product',$data);
	}
	public function remove_related_product($product_id){
		return $this->db
			        ->where('products_product_id',$product_id)
				    ->delete('related_product_info');
	}
	public function remove_tags($product_id){
		return $this->db
			        ->where('products_product_id',$product_id)
				    ->delete('tags
					
					');
	}
	public function update_product($data,$related_products,$product_id){
		$this->db->where('product_id',$product_id)
				 ->update('products',$data);
		if($data['tags']){
			$tags =$this-> multiexplode(array(",",".","|",":"),$data['tags']);

			foreach ($tags as $tag) {

				if($tag==null){
					contiune;
				}

				$data=array(
					'tag_name'=>$tag,
					'products_product_id'=>$product_id

				);
			   // print_r($data);

				$this->db->insert('tags', $data );
			}
		}
		if($related_products){
			foreach ($related_products['related_product'] as $products){
				$data=array(
					'related_product_id'=>$products,
					'products_product_id'=>$product_id

				);
			   // print_r($data);
				$this->db->insert('related_product_info', $data );
			}

		}
		
		return true;
		
	}
	public function store_discount($data){	
		$discount_percent = $data['discount_percent'];
		$discount_amount = $data['discount_amount'];
		$product_price = $data['product_price'];
		unset($data['product_price']);
		
		
		
		if($discount_amount == null || ($discount_amount<0)){
			$discount_amount = floor ($product_price * ($discount_percent/100));
		}

		if($discount_percent == null || ($discount_percent<=0)){
			if($product_price <= 0){
				$discount_percent = 0;
			}else{
				$discount_percent = ceil(($discount_amount * 100 )/$product_price);
			}
		}

		$data['discount_percent'] = ceil($discount_percent);
		$data['discount_amount'] = floor($discount_amount);

		$response =  $this->db->insert('discount_info', $data );
		
		if($response){
			$discount_info_discount_id = $this->db->insert_id();
			$products_product_id = $data['products_product_id'];
			$temp = $this->update_discount_on_product($products_product_id,$discount_info_discount_id);
			
		}		
		
		return $response;
	}

	public function update_discount_on_product($products_product_id,$discount_info_discount_id){

		$data=array('discount_info_discount_id'=>$discount_info_discount_id);
		$this->db->where('product_id',$products_product_id);
		return $this->db->update('products',$data);
	}
	public function update_order_status($data,$order_id){

		return $this->db->where('order_id',$order_id)
						->update('order_info',$data);
	}
	public function changeOrderInfoAddress($data,$order_id){

		return $this->db->where('order_id',$order_id)
						->update('order_info',$data);
	}

	public function updateOrderDetails($data){

		return $this->db->where('order_details_id',$data['order_details_id'])
						->update('order_details',$data);
	}

	public function refreshOrderInfo($order_info_order_id){
		$order_details = $this->get_order_details_by_id($order_info_order_id);			
		$total_price = 0;
		$total_discount = 0;
		// print_r($order_details);
		foreach ($order_details as $order) {
			$total_price += $order->quantity * $order->sale_price;
			$total_discount += $order->quantity * ($order->main_price-$order->sale_price);
		}
		$orderDetailsData['total_price'] = $total_price;
		$orderDetailsData['total_discount'] = $total_discount;
		return $this->updateOrderInfo($orderDetailsData,$order_info_order_id);
		//print_r($orderDetailsData);
	}

	public function updateOrderInfo($data,$order_id){

		return $this->db->where('order_id',$order_id)
						->update('order_info',$data);
	}


	public function get_order_details_by_id($order_id){
		$q=$this->db->select('order_details.*,products.*')
					->from('order_details')
					->join('products','products.product_id=order_details.product_product_id')
					->where('order_details.order_info_order_id',$order_id)
					->get();
		if($q->num_rows()>0){			
			return $q->result();
		}else{
			return FALSE;
		}
	}

	public function product_quantity_minus_for_order($order_id,$admin_id){
		$order_info = $this->get_order_info_details_by_id($order_id);
		if($order_info->confirmed_by){
			return false;
		}
		$order_details = $this->get_order_details_by_id($order_id);
		
		$isAvailable = true;
		$isComplete = true;
		
		// echo $isAvailable;
		// echo "<pre>";
		// print_r($order_info);
		// echo "</pre>";
		// exit();
		foreach ($order_details as $order) {
			if($order->quantity > $order->product_quantity){
				$isAvailable = false;
			}
		}

		if($isAvailable===true){
			foreach ($order_details as $order) {
				$result = $this-> minus_product_quantity($order->product_product_id,$order->quantity);
				print_r($result);
				if(!$result){
					$isComplete = false;
					break;
				}
			}
		}
		if($isComplete===true && $isAvailable===true){
			$data['confirmed_by'] = $admin_id;
			$result = $this->updateOrderInfo($data,$order_id);
			$result = $this->refreshOrderInfo($order_id);
			return $result;
		}
	}

	public function minus_product_quantity($product_id,$number){
		return $this->db
		    ->where('product_id',$product_id)
		    ->where('product_quantity >=',$number)
			->set('product_quantity', 'product_quantity - ' . $number, FALSE)
			->update('products');
	}
	

	public function deleteOrderDetails($order_details_id){

		return $this->db
			        ->where('order_details_id',$order_details_id)
				    ->delete('order_details');
	}

	public function get_all_orders($limit,$offset){
		$q=$this->db->select('order_id,total_price,order_date,status_status_id,status_name,type_name')
					->from('order_info')
					->join('status','order_info.status_status_id=status.status_id')
					->join('order_type','order_info.order_type=order_type.type_id')
					//->where('status_status_id',1)	
					->order_by("order_date", "desc")
					->limit($limit,$offset)					
					->get();
		if($q->num_rows()>0){			
			return $q->result();
		}else{
			return FALSE;
		}
	}
	
	public function get_order_info_details_by_id($order_id){
		$q=$this->db->select('order_info.*,order_type.type_name,status.status_name,user_info.user_name,user_info.user_email')
					->from('order_info')
					->join('status','order_info.status_status_id=status.status_id')
					->join('order_type','order_info.order_type=order_type.type_id')
					->join('user_info','order_info.user_info_user_id=user_info.user_id')
					//->join('admin_info','order_info.confirmed_by=admin_info.admin_id')
					->where('order_info.order_id',$order_id)			 
					->get();
		if($q->num_rows()==1){			
			return $q->row();
		}else{
			return FALSE;
		}
	}
	public function get_invoice_info_by_order_id($order_id){
		$q=$this->db->select('*')
					->from('order_info')
					//->join('invoice_info','invoice_info.order_info_order_id=order_info.order_id')
					->join('order_type','order_info.order_type=order_type.type_id')
					->join('user_info','order_info.user_info_user_id=user_info.user_id')
					->join('status','order_info.status_status_id=status.status_id')
					->where('order_info.order_id',$order_id)			 
					->get();
		if($q->num_rows()==1){			
			return $q->row();
		}else{
			return FALSE;
		}
	}
	public function get_invoice($order_id){
		$q=$this->db->select('*')
					->from('invoice_info')
					->where('order_info_order_id',$order_id)			 
					->get();
		if($q->num_rows()==1){			
			return true;
		}else{
			return FALSE;
		}
	}
	public function get_billing_address_by_id($order_id){
		$q=$this->db->select('order_info.order_id,address_info.address_title,address_info.address,address_info.contact,')
					->from('order_info')
					->join('address_info','order_info.billing_address_id=address_info.adr_id')
					->where('order_id',$order_id)			 
					->get();
		if($q->num_rows()==1){			
			return $q->row();
		}else{
			return FALSE;
		}
	}
	public function get_shipping_address_by_id($order_id){
		$q=$this->db->select('order_info.order_id,address_info.address_title,address_info.address,address_info.contact,')
					->from('order_info')
					->join('address_info','order_info.shipping_address_id=address_info.adr_id')
					->where('order_id',$order_id)			 
					->get();
		if($q->num_rows()==1){			
			return $q->row();
		}else{
			return FALSE;
		}
	}
	public function get_user_all_orders($user_id,$limit,$offset){
		$q=$this->db->select('*')
					->from('order_info')
					->join('order_type','order_info.order_type=order_type.type_id')
					->where('user_info_user_id',$user_id)	
					->order_by('order_date','desc')
					->limit($limit,$offset)
					->get();
		if($q->num_rows()>0){			
			return $q->result();
		}else{
			return FALSE;
		}
	}

	public function create_invoice($data)
	{
		$this->db->insert('invoice_info', $data );
		return $this->db->insert_id();
	}
	
	public function store_user_info($data,$address){
		$this->db->insert('user_info', $data );
		$address['user_info_user_id']= $this->db->insert_id();
		$this->db->insert('address_info',$address);
		return $address['user_info_user_id'];
	}
	
	function get_user_info_by_email($email,$user_type){
	
	  $q=$this->db->select('user_info.user_email')
			   ->from('user_info')
               ->where('user_email',$email)
			   //->where('user_info.user_type',$user_type)
			   ->get();
		if($q->num_rows()>0){			
			return false;
		}else{
			return true;
		}
	}
	function get_user_info_by_contact($contact,$user_type){
	
	  $q=$this->db->select('user_info.main_contact')
			   ->from('user_info')
               ->where('main_contact',$contact)
			   //->where('user_info.user_type',$user_type)
			   ->get();
		if($q->num_rows()>0){			
			return false;
		}else{
			return true;
		}
	}
	function get_all_user_info(){
	
	  $q=$this->db->select('user_info.user_id,user_info.user_name,user_info.user_email,user_info.user_type,user_info.main_contact')
			   ->from('user_info')
			   //->join('address_info','user_info.user_id=address_info.user_info_user_id')
               ->where('user_info.user_id>',0)
			   ->get();
		if($q->num_rows()>0){			
			return $q->result();
		}else{
			return false;
		}
	}
	
	function get_user_details_by_id($user_id){
	
	  $q=$this->db->select('user_info.user_id,user_info.user_name,user_info.user_email,user_info.main_contact,user_info.user_wallet,user_info.user_type,address_info.adr_id,address_info.address_title,address_info.address,address_info.contact')
			   ->from('user_info')
			   ->join('address_info','user_info.user_id=address_info.user_info_user_id')
               ->where('user_info.user_id',$user_id)
               ->where('address_info.default_status',1)
			   ->get();
		if($q->num_rows()==1){			
			return $q->row();
		}else{
			return false;
		}
	}
	
	public function updateCustomerContact($user_id,$data){
		return $this->db
			        ->where('user_id',$user_id)
				    ->update('user_info',$data);
	}
	
	public function updateCustomerPassword($user_id,$data){
		return $this->db
			        ->where('user_id',$user_id)
				    ->update('user_info',$data);
	}
	
	function get_other_address_by_id($user_id){
	
	  $q=$this->db->select('address_info.user_name,address_info.adr_id,address_info.address_title,address_info.address,address_info.contact,')
			   ->from('address_info')
               ->where('user_info_user_id',$user_id)
               ->where('address_info.default_status',0)
               ->where('address_info.address_status',1)
			   ->get();
		if($q->num_rows()>0){			
			return $q->result();
		}else{
			return false;
		}
	}
	
	function get_user_id_by_address_id($adr_id){
	
	  $q=$this->db->select('address_info.user_info_user_id')
			   ->from('address_info')
               ->where('address_info.adr_id',$adr_id)
			   ->get();
		if($q->num_rows()==1){			
			return $q->row();
		}else{
			return false;
		}
	}
	
	function get_product_id_by_order_id($order_id){
		$q=$this->db->select('order_details.quantity,order_details.	product_product_id')
			   ->from('order_details')
               ->where('order_info_order_id',$order_id)
			   ->get();
		if($q->num_rows()>0){			
			return $q->result();
		}else{
			return false;
		}
	}
	
	function get_all_user(){
	
	  $q=$this->db->select('user_info.user_id,user_info.user_name,address_info.contact')
			   ->from('user_info')
			   ->join('address_info','user_info.user_id=address_info.user_info_user_id')
               ->where('address_info.default_status',1)
			   ->get();
		if($q->num_rows()>0){			
			return $q->result();
		}else{
			return false;
		}
	}

	public function update_payment($data){
		return $this->db
			        ->where('order_id',$data['order_id'])
				    ->update('order_info',$data);
	}
	
	public function get_all_expenses($limit,$offset){
		$q=$this->db
				->select('payment_id,payment_details,payment_amount,payment_date,pay_to')
				//->select('payment_id,payment_details,payment_amount,payment_date,pay_to,	transaction_id')
				->from('payment_history')
				//->join('transaction_history',' transaction_history.reference=payment_history.payment_id')
				->where('payment_history.bill_type<',2)
				->order_by("payment_date", "desc")
				->limit($limit,$offset)
				->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	public function get_expense_details_by_id($payment_id){
		
				$this->db->select('payment_history.*,admin_info.name,user_info.user_name,user_info.user_id')
				->from('payment_history')
				->where('payment_history.payment_id',$payment_id)
				->join('admin_info','admin_info.admin_id=payment_history.entry_by')
				->join('user_info','user_info.user_id=payment_history.pay_to');
				
			$q=$this->db->get();

		if($q->num_rows()==1){
			return $q->row();
		}
		else{
			return FALSE;
		}
	}
	public function get_all_suppiler_id(){
		$q=$this->db
				->select('user_id,user_name')
				->from('user_info')
				->where('user_info.user_type',2)
				->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	public function add_expense($data){
		
		$this->db->insert('payment_history', $data );
		$expense_id= $this->db->insert_id();
		$transection['transaction_amount']=$data['payment_amount'];
		$transection['transaction_type']=6;
		$transection['reference']=$expense_id;
		$transection['transaction_details']=$data['payment_details'];
		$transection['transaction_by']=$data['entry_by'];
		//$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
		//$transection['transaction_date']=$dt->format('Y-m-d H:i:s');
		$transection['transaction_date']=$data['payment_date'];
		if($data['pay_to']>0){
			$this->db
			    ->where('user_id',$data['pay_to'])
				->set('user_wallet', 'user_wallet - ' . $data['payment_amount'], FALSE)
				->update('user_info');
		}
		return $this->db->insert('transaction_history', $transection );
	}
	
	public function add_due_bill($data){
		
		if($data['pay_to']>0){
			$this->db
			    ->where('user_id',$data['pay_to'])
				->set('user_wallet', 'user_wallet + ' . $data['payment_amount'], FALSE)
				->update('user_info');
		}
		return $this->db->insert('payment_history', $data );
	}
	
	public function get_all_clients_id(){
		$q=$this->db
				->select('user_id,user_name')
				->from('user_info')
				->where('user_info.user_type',1)
				->where('user_info.user_id>',0)
				->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	public function get_all_customers_number(){
		$q=$this->db
				->select('user_id,main_contact,user_name')
				->from('user_info')
				->where('user_info.user_type',1)
				->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	public function get_all_products_id(){
		$q=$this->db
				->select('product_id,product_name')
				->from('products')
				//->where('user_info.user_type',1)
				->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	
	public function add_cashReceipt($data){
		if($data['transaction_type']==3){
			$this->db
			    ->where('user_id',$data['reference'])
				->set('user_wallet', 'user_wallet + ' . (int) $data['transaction_amount'], FALSE)
				->update('user_info');
			$transection['wallet_amount']=$data['transaction_amount'];
			$transection['wallet_transaction_type']=1;
			$transection['user_info_user_id']=$data['reference'];
			$transection['wallet_details']=$data['transaction_details'];
			$transection['admin_info_admin_id']=$data['transaction_by'];
			$transection['wallet_date']=$data['transaction_date'];
			$this->db->insert('wallet_history',$transection );
		}
		else if($data['transaction_type']==4){
			$this->db
			    ->where('user_id',$data['reference'])
				->set('user_wallet', 'user_wallet - ' . (int) $data['transaction_amount'], FALSE)
				->update('user_info');
			$transection['wallet_amount']=$data['transaction_amount'];
			$transection['wallet_transaction_type']=2;
			$transection['user_info_user_id']=$data['reference'];
			$transection['wallet_details']=$data['transaction_details'];
			$transection['admin_info_admin_id']=$data['transaction_by'];
			$transection['wallet_date']=$data['transaction_date'];
			$this->db->insert('wallet_history',$transection);
		}
		return $this->db->insert('transaction_history', $data );
	}

	public function add_transaction_history($data){
		return $this->db->insert('transaction_history', $data);
	}

	public function add_wallet_history($data){
		return $this->db->insert_batch('wallet_history', $data );
	}

	public function minus_user_wallet($user_id,$user_wallet){
		$this->db
		    ->where('user_id',$user_id)
			->set('user_wallet', 'user_wallet - ' . $user_wallet, FALSE)
			->update('user_info');
	}

	public function get_all_transactions($limit,$offset){
		$q=$this->db
				->select('transaction_date,	transaction_id,transaction_amount,transaction_type,reference,transaction_details,transaction_type_name')
				->from('transaction_history')
				->join('transaction_type','transaction_type.transaction_type_id=transaction_history.transaction_type')
				->order_by("transaction_id", "desc")
				->where('transaction_type<',8)
				->limit($limit,$offset)
				->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	
	public function get_transaction_details_by_id($transaction_id,$reference,$type){
		
				$this->db->select('transaction_history.*,admin_info.name,transaction_type.transaction_type_name')
				->from('transaction_history')
				->where('transaction_history.transaction_id',$transaction_id)
				->join('transaction_type','transaction_type.transaction_type_id=transaction_history.transaction_type')
				->join('admin_info','admin_info.admin_id=transaction_history.transaction_by');
				if($type==3 || $type==4){
					$this->db->select('user_info.user_name,user_info.user_id')
						->join('user_info','user_info.user_id=transaction_history.reference');
				}
				else if($type==5){
					$this->db->select('order_info.order_id')
						->join('order_info','order_info.order_id=transaction_history.reference');
				}
				else if($type==6){
					$this->db->select('payment_history.payment_id')
						->join('payment_history','payment_history.payment_id=transaction_history.reference');
				}
				else if($type==7){
					$this->db->select('return_product_history.return_history_id')
						->join('return_product_history','return_product_history.return_history_id=transaction_history.reference');
				}
			$q=$this->db->get();

		if($q->num_rows()==1){
			return $q->row();
		}
		else{
			return FALSE;
		}
	}
	
	public function get_user_wallet_in_by_id($user_id,$user_type){
		$q;
		if($user_type==1){
			$q=$this->db->select('wallet_history.wallet_date as in_date,wallet_history.wallet_history_id as in_id,wallet_history.wallet_amount as in_amount,wallet_history.wallet_details as in_details')
					->from('wallet_history')
					->where('wallet_history.user_info_user_id',$user_id)
					->where('wallet_history.wallet_transaction_type=1 or wallet_history.wallet_transaction_type=4')
					->get();
		}
		else{
			$q=$this->db->select('payment_history.payment_date as in_date,payment_history.payment_id as in_id,payment_history.payment_amount as in_amount,payment_history.payment_details as in_details')
					->from('payment_history')
					->where('payment_history.pay_to',$user_id)
					->where('payment_history.payment_type=1 and payment_history.bill_type=2')
					->get();
		}

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	public function get_user_wallet_out_by_id($user_id,$user_type){
		
		$q;
		if($user_type==1){
			$q=$this->db->select('wallet_history.wallet_date as out_date,wallet_history.wallet_history_id as out_id,wallet_history.wallet_amount as out_amount,wallet_history.wallet_details as out_details')
					->from('wallet_history')
					->where('wallet_history.user_info_user_id',$user_id)
					->where('wallet_history.wallet_transaction_type=2 or wallet_history.wallet_transaction_type=3')
					->get();
		}
		else{
			$q=$this->db->select('payment_history.payment_date as out_date,payment_history.payment_id as out_id,payment_history.payment_amount as out_amount,payment_history.payment_details as out_details')
					->from('payment_history')
					->where('payment_history.pay_to',$user_id)
					->where('payment_history.payment_type=1 and payment_history.bill_type=1')
					->get();
		}

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	public function get_receivable_users($limit,$offset){
		
		$this->db->select('user_info.user_id,user_info.user_name,user_info.user_wallet')
				->from('user_info')
				->where('user_info.user_wallet >',0)
				->where('user_info.user_id >',0)
				->limit($limit,$offset)	;
		$q=$this->db->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	public function get_payable_users($limit,$offset){
		
		$this->db->select('user_info.user_id,user_info.user_name,user_info.user_wallet')
				->from('user_info')
				->where('user_info.user_wallet <',0)
				->where('user_info.user_id >',0)
				->limit($limit,$offset)	;
		$q=$this->db->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	public function get_all_sales($limit,$offset){
		$q=$this->db->select('order_id,total_price,order_date,status_name,user_name')
					->from('order_info')
					->join('status','order_info.status_status_id=status.status_id')
					->join('user_info','order_info.user_info_user_id=user_info.user_id')
					->where('status_status_id>',5)	
					->order_by("order_date", "desc")
					->limit($limit,$offset)					
					->get();
		if($q->num_rows()>0){			
			return $q->result();
		}else{
			return FALSE;
		}
	}
	function get_customer_name_by_id($user_id){
	
	  $q=$this->db->select('user_info.user_id,user_info.user_name')
			   ->from('user_info')
               ->where('user_info.user_id',$user_id)
			   ->get();
		if($q->num_rows()==1){			
			return $q->row();
		}else{
			return false;
		}
	}
	
	public function store_pos_order($data,$cart){
		//print_r($data['total_discount']);exit;
		$data['total_discount']=$data['total_discount']+$data['discount_amount'];
		$data['total_price']=$data['total_price']-$data['discount_amount'];
		//print_r($data['total_discount']);exit;
		unset($data['discount_amount']);
		$this->db->insert('order_info', $data );
		$order_id= $this->db->insert_id();
		foreach ($cart as $product) {

            //if($product==null){
              //  contiune;
            //}

            $products=array(
                'product_product_id'=>$product['id'],
                'quantity'=>$product['qty'],
                'sale_price'=>$product['price']-$product['discount'],
                'main_price'=>$product['price'],
                'order_info_order_id'=>$order_id

            );
            //print_r($data); exit;
			$this->db
			    ->where('product_id',$products['product_product_id'])
				->set('product_quantity', 'product_quantity - ' . (int) $products['quantity'], FALSE)
				->update('products');
            $this->db->insert('order_details', $products );
        }
		if($data['order_payment_type']==2){
				$transection['wallet_amount']=$data['total_price'];
				$transection['wallet_transaction_type']=3;
				$transection['user_info_user_id']=$data['user_info_user_id'];
				$transection['wallet_details']="Payment for order";
				$transection['admin_info_admin_id']=$data['confirmed_by'];
				$transection['wallet_date']=$data['order_date'];
				$this->db
					->where('user_id',$data['user_info_user_id'])
					->set('user_wallet', 'user_wallet - ' . $data['total_price'], FALSE)
					->update('user_info');
				$this->db->insert('wallet_history', $transection );
			}
		else if($data['total_price']<=$data['receved_total'] ){
			$transection['transaction_amount']=$data['total_price'];
			$transection['transaction_type']=5;
			$transection['reference']=$order_id;
			$transection['transaction_details']="Quick Sale";
			$transection['transaction_by']=$data['confirmed_by'];
			$transection['transaction_date']=$data['order_date'];
			
			$this->db->insert('transaction_history', $transection );
		}
		else if($data['total_price']>$data['receved_total']){
			$transection['transaction_amount']=$data['receved_total'];
			$transection['transaction_type']=5;
			$transection['reference']=$order_id;
			$transection['transaction_details']="Partial payment for order";
			$transection['transaction_by']=$data['confirmed_by'];
			$transection['transaction_date']=$data['order_date'];
			
			$this->db->insert('transaction_history', $transection );
			
			$due=$data['total_price']-$data['receved_total'];
			$wallet['wallet_amount']=$due;
			$wallet['wallet_transaction_type']=3;
			$wallet['user_info_user_id']=$data['user_info_user_id'];
			$wallet['wallet_details']="Due entry for partial payment of order";
			$wallet['admin_info_admin_id']=$data['confirmed_by'];
			$wallet['wallet_date']=$data['order_date'];
			
				$this->db
					->where('user_id',$data['user_info_user_id'])
					->set('user_wallet', 'user_wallet - ' . $due, FALSE)
					->update('user_info');
			$this->db->insert('wallet_history', $wallet );
		}
		if($order_id)
		{
			return $order_id;
		}
		else return false;
	}
	
	public function store_take_order($data,$cart){
		
		//$data['total_price']=$this->cart->total();
		//print_r($data['total_discount']);exit;
		unset($data['product_id'],$data['bar_code'],$data['quantity']);
		$this->db->insert('order_info', $data );
		$order_id= $this->db->insert_id();
		$total_price=0;
		$total_discount=0;
		foreach ($cart as $product) {

            //if($product==null){
              //  contiune;
            //}

            $products=array(
                'product_product_id'=>$product['id'],
                'quantity'=>$product['qty'],
                'sale_price'=>$product['price']-$product['discount'],
                'main_price'=>$product['price'],
                'order_info_order_id'=>$order_id

            );
            $total_price=$total_price+($products['sale_price']*$products['quantity']);
            $total_discount=$total_discount+($product['discount']*$products['quantity']);
            $this->db->insert('order_details', $products );
        }
		//print_r($total_discount);exit;
		$price['total_price']=$total_price;
		$price['total_discount']=$total_discount;
		//print_r($price);exit;
		$this->db
			 ->where('order_id',$order_id)
			 ->update('order_info',$price);
		if($order_id)
		{
			return $order_id;
		}
		else return false;
	}
	
	public function add_return($data){
		
		$this->db->insert('return_product_history', $data );
		$return_id= $this->db->insert_id();
		if($data['return_type']==1){
			$transection['transaction_amount']=$data['return_product_amount'];
			$transection['transaction_type']=7;
			$transection['reference']=$return_id;
			$transection['transaction_details']=$data['return_details'];
			$transection['transaction_by']=$data['return_recived_by'];
			$transection['transaction_date']=$data['return_date'];
			return $this->db->insert('transaction_history', $transection );
		}
		else{
			$transection['wallet_amount']=$data['return_product_amount'];
			$transection['wallet_transaction_type']=4;
			$transection['user_info_user_id']=$data['user_info_user_id'];
			$transection['wallet_details']=$data['return_details'];
			$transection['admin_info_admin_id']=$data['return_recived_by'];
			$transection['wallet_date']=$data['return_date'];
			$this->db
			    ->where('user_id',$data['user_info_user_id'])
				->set('user_wallet', 'user_wallet + ' . $data['return_product_amount'], FALSE)
				->update('user_info');
			return $this->db->insert('wallet_history', $transection );
		}
		
	}
	
	public function get_return_details_by_id($return_history_id){
		
				$this->db->select('return_product_history.*,admin_info.name,user_info.user_name,user_info.user_id,products.product_name')
				->from('return_product_history')
				->where('return_product_history.return_history_id',$return_history_id)
				->join('admin_info','admin_info.admin_id=return_product_history.return_recived_by')
				->join('user_info','user_info.user_id=return_product_history.user_info_user_id')
				->join('products','products.product_id=return_product_history.return_product_id');
				//if($supplier_id>0){
					//$this->db->select('user_info.user_name,user_info.user_id')
					//->join('user_info','user_info.user_id=payment_history.pay_to');
				//}
			$q=$this->db->get();

		if($q->num_rows()==1){
			return $q->row();
		}
		else{
			return FALSE;
		}
	}
	
	public function get_wallet_details_by_id($wallet_history_id){
		
			$q=$this->db->select('wallet_history.*,admin_info.name,user_info.user_name,user_info.user_id')
				->from('wallet_history')
				->where('wallet_history.wallet_history_id',$wallet_history_id)
				->join('admin_info','admin_info.admin_id=wallet_history.admin_info_admin_id')
				->join('user_info','user_info.user_id=wallet_history.user_info_user_id')
				->get();

		if($q->num_rows()==1){
			return $q->row();
		}
		else{
			return FALSE;
		}
	}
	public function get_cash_amount(){
		$this->db->select_sum('transaction_amount');
		$this->db->from('transaction_history');
		$this->db->where('transaction_type=1 or transaction_type=3 or transaction_type=5');
		$query = $this->db->get();
		$income= $query->row()->transaction_amount;
		$this->db->select_sum('transaction_amount');
		$this->db->from('transaction_history');
		$this->db->where('transaction_type=2 or transaction_type=4 or transaction_type=6 or transaction_type=7');
		$query = $this->db->get();
		$expense= $query->row()->transaction_amount;
		return $income-$expense;
	}
	
	public function get_all_suppiler_bills($limit,$offset){
		$q=$this->db
				->select('payment_id,payment_details,payment_amount,payment_date,pay_to')
				->from('payment_history')
				->where('payment_history.bill_type',2)
				->order_by("payment_date", "desc")
				->limit($limit,$offset)
				->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	
	public function get_all_wallet_history($limit,$offset){
		$q=$this->db
				->select('wallet_history_id,wallet_details,wallet_amount,wallet_date')
				->from('wallet_history')
				//->where('payment_history.bill_type',2)
				->order_by("wallet_date", "desc")
				->limit($limit,$offset)
				->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	
	public function get_all_return_porduct_history($limit,$offset){
		$q=$this->db
				->select('return_history_id,return_details,return_product_amount,return_date')
				->from('return_product_history')
				//->where('payment_history.bill_type',2)
				->order_by("return_date", "desc")
				->limit($limit,$offset)
				->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	
	public function add_more_product_in_order($data,$order_id){
		//print_r($data);exit;
		
		$price=($data->product_price-$data->discount_amount)*$data->product_quantity;
		//print_r($total_price);
		$discount=$data->discount_amount*$data->product_quantity;
		$order['quantity']=$data->product_quantity;
		$order['sale_price']=$data->product_price-$data->discount_amount;
		$order['main_price']=$data->product_price;
		$order['product_product_id']=$data->product_id;
		$order['order_info_order_id']=$order_id;
		//print_r($order);exit;
			$this->db->where('order_id',$order_id)
					->set('total_price', 'total_price + ' . $price, FALSE)
					->set('total_discount', 'total_discount + ' . $discount, FALSE)
					->update('order_info');
		return $this->db->insert('order_details', $order );
			
		
	}
	
	/*public function deleteTransaction($transaction_id){
		
		return $this->db
			        ->where('transaction_id',$transaction_id)
				    ->delete('transaction_history');
	}*/
}
