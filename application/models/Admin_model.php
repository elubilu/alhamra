<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_Model {
	/*Start Company User*/ 
	public function get_all_user()
	{
		
		$q=$this->db
		->select('admin_info.adminID,admin_info.adminName,admin_info.adminContact,admin_info.adminUserID,admin_role.roleName')
		->from('admin_info')
		->join('admin_role','admin_role.roleID=admin_info.admin_role_roleID')
		->where('admin_role_roleID>',1)
				//->limit($limit,$offset)
		->get();
		
		if($q->num_rows()){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	public function get_user_details_by_id($admin_id)
	{
		$q=$this->db
		->select('*,admin_role.roleName')
		->from('admin_info')
		->join('admin_role','admin_role.roleID=admin_info.admin_role_roleID')
		->where('admin_role_roleID>',1)
		->where('adminID',$admin_id)
		->get();
		
		if($q->num_rows()==1){
			return $q->row();
		}
		else{
			return FALSE;
		}
	}
	public function check_admin_id($user_id)
	{
		
		$q=$this->db
		->select('adminUserID')
		->from('admin_info')
		->where('adminUserID',$user_id)
		->get();
		
		if($q->num_rows()){
			return False;
		}
		else{
			return true;
		}
	}
	
	public function add_user($data)
	{
		return $this->db->insert('admin_info', $data );
	}
	public function updateCompanyUser($data,$companyUser_Id){
		return $this->db
		->where('adminID',$companyUser_Id)
		->update('admin_info',$data);
	}

	/* Start customers */
	public function add_customer($data)
	{
		return $this->db->insert('customer_info', $data );
	}
	public function add_customer_billing_account($data)
	{
		$this->db->trans_start();
		$services=$data['charges'];
		unset($data['charges']);
		$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
		$date= $dt->format('Y-m-d H:i:s');
		$data['previousReadingDate']=$date;
		$this->db->insert('meter_info', $data );
		$meter_id=$this->db->insert_id();
		$this->db
		->where('customerId',$data['customer_info_customerId'])
		->set('billDue', 'billDue + ' .  $data['billDue'], FALSE)
		->update('customer_info');
		
		foreach($services as $service){
			$charge['services_serviceId']=$service;
			$charge['customer_info_customerId']=$data['customer_info_customerId'];
			$charge['meter_info_meterId']=$meter_id;
			$this->db->insert('customer_services', $charge );
		}
		$cart=$this->cart->contents();
		foreach($cart as $item){
			/*$add_services['serviceName']=$item['name'];
			$add_services['serviceAmount']=$item['price'];
			$add_services['serviceType']=2;
			$add_services['serviceStatus']=1;
			$this->db->insert('services', $add_services );
			$service_id=$this->db->insert_id();*/
			$add_charge['services_serviceId']=$item['id'];
			$add_charge['customer_info_customerId']=$data['customer_info_customerId'];
			$add_charge['meter_info_meterId']=$meter_id;
			$add_charge['customerServiceAmount']=$item['price'];
			$this->db->insert('customer_services', $add_charge );
		}
		
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE){
			return false;
		}	
		else return true;
	}

	
	public function update_customer_billing_account($data)
	{
		$this->db->trans_start();
		$meter_id=$data['meterId'];
		$customer_info_customerId=$data['customer_info_customerId'];
		unset($data['meterId'],$data['customer_info_customerId']);
		//print_r($data);exit;
		if(array_key_exists("charges",$data)){
			$services=$data['charges'];
			unset($data['charges']);
			foreach($services as $service){
				$charge['services_serviceId']=$service;
				$charge['customer_info_customerId']=$customer_info_customerId;
				$charge['meter_info_meterId']=$meter_id;
				$this->db->insert('customer_services', $charge );
			}
		}
		$this->db
		->where('meterId',$meter_id)
		->update('meter_info',$data);
		if($this->cart->contents()){
			$cart=$this->cart->contents();
			foreach($cart as $item){
				$add_services['serviceName']=$item['name'];
				$add_services['serviceAmount']=$item['price'];
				$add_services['serviceType']=2;
				$add_services['serviceStatus']=1;
				$this->db->insert('services', $add_services );
				$service_id=$this->db->insert_id();
				$add_charge['services_serviceId']=$service_id;
				$add_charge['customer_info_customerId']=$customer_info_customerId;
				$add_charge['meter_info_meterId']=$meter_id;

				/*$add_charge['services_serviceId']=$item['id'];
				$add_charge['customer_info_customerId']=$data['customer_info_customerId'];
				$add_charge['meter_info_meterId']=$meter_id;
				$add_charge['customerServiceAmount']=$item['price'];*/
				$this->db->insert('customer_services', $add_charge );
			}
		}
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE){
			return false;
		}	
		else return true;
	}

	public function get_all_organization_name_id()
	{
		$q=$this->db
		->select('customer_info.customerId,customer_info.organizationName,customer_info.spaceTitle')
		->from('customer_info')
		->where('customerStatus',1)
		->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}


	public function get_organization_name_by_id($customer_id)
	{
		$q=$this->db
		->select('customer_info.organizationName')
		->from('customer_info')
		->where('customerStatus',1)
		->where('customerId',$customer_id)
		->get();

		if($q->num_rows()==1){
			return $q->row()->organizationName;
		}
		else{
			return FALSE;
		}
	}

	public function remove_service_charge($chargeId)
	{
		return $this->db
		->where('customerServiceId',$chargeId)
		->delete('customer_services');
	}

	public function remove_additional_service_charge($chargeId,$serviceId)
	{
		return $this->db
					->where('customerServiceId',$chargeId)
					->delete('customer_services');
			/*return $this->db
			->where('serviceId',$serviceId)
			->delete('services');*/
		
	}

	public function remove_billing_account($meter_id)
	{	//print_r($meter_id);exit;
		return $this->db
		->where('meterId',$meter_id)
		->set('meterStatus',0, FALSE)
		->update('meter_info');
	}

	public function get_customer_all_rent_bills_by_id($customer_id)
	{
		$q=$this->db
		->select('*')
		->from('transection_info')
		->where('customer_info_customerId',$customer_id)
		->where("(TransectionType='1' OR TransectionType='3')", NULL, FALSE)
				//->order_by('transectionDate','desc')
		->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}

	public function get_customer_all_bill_bills_by_id($customer_id)
	{
		$q=$this->db
		->select('*')
		->from('transection_info')
		->where('customer_info_customerId',$customer_id)
		->where("(TransectionType='2' OR TransectionType='4')", NULL, FALSE)
				//->order_by('transectionDate','desc')
		->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}

	public function get_all_customer_details()
	{
		$q=$this->db
		->select('*')
		->from('customer_info')
		->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	
	public function get_customer_details_by_customer_id($id)
	{
		$q=$this->db
		->select('*')
		->from('customer_info')
		->where('customerId',$id)
		->get();

		if($q->num_rows()==1){
			return $q->row();
		}
		else{
			return FALSE;
		}
	}
	
	public function updateCustomerAccount($data){
		return $this->db
				->where('customerId',$data['customerId'])
				->update('customer_info',$data);
	}

	public function update_customer_bill_deu($total_bill,$customerId){
		return $this->db
		->where('customerId',$customerId)
		->set('billDue', 'billDue + ' .  $total_bill, FALSE)
		->update('customer_info');
	}

	public function get_space_title_by_id($customerId)
	{
		$q=$this->db
		->select('customer_info.spaceTitle')
		->from('customer_info')
		->where('customerId',$customerId)
		->where('customerStatus',1)
		->get();
		
		if($q->num_rows()==1){
			return $q->row()->spaceTitle;
		}
		else{
			return FALSE;
		}
	}
	
	public function get_customer_rent_due_by_id($customerId)
	{
		$q=$this->db
		->select('customer_info.rentDue')
		->from('customer_info')
		->where('customerId',$customerId)
		->get();
		
		if($q->num_rows()==1){
			return $q->row()->rentDue;
		}
		else{
			return 0;
		}
	}
	
	public function get_customer_bill_due_by_id($customerId)
	{
		$q=$this->db
		->select('customer_info.billDue')
		->from('customer_info')
		->where('customerId',$customerId)
		->get();
		
		if($q->num_rows()==1){
			return $q->row()->billDue;
		}
		else{
			return 0;
		}
	}
	
	public function get_all_customer_info_for_shop()
	{
		$q=$this->db
		->select('customerId,rentAmount,rentDue')
		->from('customer_info')
		->where('spaceType',1)
		->where('customerStatus',1)
		->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	public function get_all_customer_info_for_office()
	{
		$q=$this->db
		->select('customerId,rentAmount,rentDue')
		->from('customer_info')
		->where('spaceType',2)
		->where('customerStatus',1)
		->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	/* End customers */
	
	/* Start Services */
	public function add_common_services($data)
	{
		return $this->db->insert('services', $data );
	}
	public function add_additional_charge($data)
	{
		$service['customer_info_customerId']=$data['customerId'];
		unset($data['customerId']);
		$this->db->insert('services', $data );
		$service['services_serviceId']=$this->db->insert_id();
		return $this->db->insert('customer_services', $service);
	}
	
	
	public function ajax_update_services_charges($data,$serviceId)
	{
		return $this->db
		->where('serviceId',$serviceId)
		->update('services',$data);
	}
	
	

	public function get_all_common_service_name_id()
	{
		$q=$this->db
		->select('services.serviceId,services.serviceName')
		->from('services')
		->where('serviceId>',0)
		->where('serviceStatus',1)
		->where('serviceType',1)
		->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}


	public function get_all_additional_service_name_id()
	{
		$q=$this->db
		->select('services.serviceId,services.serviceName')
		->from('services')
		->where('serviceId>',0)
		->where('serviceStatus',1)
		->where('serviceType',2)
		->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}

	public function get_additional_service_charge_name_by_id($service_id)
	{
		$q=$this->db
		->select('services.serviceName')
		->from('services')
		->where('serviceId',$service_id)
		->where('serviceStatus',1)
		->where('serviceType',2)
		->get();

		if($q->num_rows()){
			return $q->row()->serviceName;
		}
		else{
			return FALSE;
		}
	}

	public function get_meter_all_common_services_by_id($meter_id)
	{
		$q=$this->db
		->select('*')
		->from('customer_services')
		->join('services','customer_services.services_serviceId=services.serviceId')
		->where('meter_info_meterId',$meter_id)
		->where('services.serviceStatus',1)
		->where('serviceType',1)
		->get();
		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
		
	}

	
	public function get_meter_all_common_services_id_by_id($meter_id)
	{
		$q=$this->db
		->select('*')
		->from('customer_services')
		->join('services','customer_services.services_serviceId=services.serviceId')
		->where('meter_info_meterId',$meter_id)
		->where('services.serviceStatus',1)
		->where('serviceType',1)
		->get();
		$serviceCharge = array();
		if($q->num_rows()){
			$q= $q->result();
			foreach($q as $q){
				array_push($serviceCharge,$q->services_serviceId);
			}
			return $serviceCharge;
		}
		else{
			return FALSE;
		}
		
		
	}

	public function get_meter_all_additional_services_id_by_id($meter_id)
	{
		$q=$this->db
		->select('*')
		->from('customer_services')
		->join('services','customer_services.services_serviceId=services.serviceId')
		->where('meter_info_meterId',$meter_id)
		->where('services.serviceStatus',1)
		->where('serviceType',2)
		->get();
		$serviceCharge = array();
		if($q->num_rows()){
			$q= $q->result();
			foreach($q as $q){
				array_push($serviceCharge,$q->services_serviceId);
			}
			return $serviceCharge;
		}
		else{
			return FALSE;
		}
		
		
	}

	
	public function get_meter_all_additional_services_by_id($meter_id)
	{
		$q=$this->db
		->select('*')
		->from('customer_services')
		->join('services','customer_services.services_serviceId=services.serviceId')
		->where('meter_info_meterId',$meter_id)
		->where('services.serviceStatus',1)
		->where('serviceType',2)
		->get();
		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}

	public function get_all_common_services()
	{
		$q=$this->db
		->select('*')
		->from('services')
		->where('serviceType',1)
        ->where('serviceId>',0)
		->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	public function get_all_services()
	{
		$q=$this->db
		->select('*')
		->from('services')
		->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	
	public function get_all_services_for_details()
	{
		$q=$this->db
		->select('*')
		->from('services')
		->where('serviceId>',0)
		->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	
	public function get_active_common_services()
	{
		$q=$this->db
		->select('*')
		->from('services')
		->where('serviceType',1)
		->where('serviceStatus',1)
		->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	
	public function get_last_fifteen_service_payment()
	{
		$q=$this->db
		->select('service_charge_payment_info.serviceChargePaymentAmount,service_charge_payment_info.serviceChargePaidAmount,services.serviceName')
		->from('service_charge_payment_info')
		->join('services','service_charge_payment_info.service_info_serviceId=services.serviceId')
		->limit(15)
		->order_by('serviceChargePaymentDate','desc')
		->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	
	public function get_service_payment_history_by_id($service_id)
	{
		$q=$this->db
		->select('service_charge_payment_info.serviceChargePaymentAmount,service_charge_payment_info.serviceChargePaidAmount,service_charge_payment_info.serviceChargePaymentDate,customer_info.organizationName')
		->from('service_charge_payment_info')
		->join('customer_info',' service_charge_payment_info.client_info_clientId=customer_info.customerId')
		->where('service_charge_payment_info.service_info_serviceId',$service_id)
		->limit(15)
		->order_by('serviceChargePaymentDate','desc')
		->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	
	public function get_service_payment_history_by_id_dates($data,$service_id)
	{
		$startDate=date('Y-m-d 00:00:00', strtotime($data['startDate']));
		$endDate=date('Y-m-d 23:59:59', strtotime($data['endDate']));

		$q=$this->db
		->select('service_charge_payment_info.serviceChargePaymentAmount,service_charge_payment_info.serviceChargePaidAmount,service_charge_payment_info.serviceChargePaymentDate,customer_info.organizationName')
		->from('service_charge_payment_info')
		->join('customer_info',' service_charge_payment_info.client_info_clientId=customer_info.customerId')
		->where('service_charge_payment_info.service_info_serviceId',$service_id);
		if($data['startDate']){
			$this->db->where('serviceChargePaymentDate>=',$startDate);
		}
		if($data['endDate']){
			$this->db->where('serviceChargePaymentDate<=',$endDate);
		}
		if($data['client_id']){
			$this->db->where('client_info_clientId',$data['client_id']);
		}
		if($data['meter_id']){
			$this->db->where('meter_info_meterId',$data['meter_id']);
		}
		$this->db->order_by('serviceChargePaymentDate','desc');
		$q=$this->db->get();
		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	
	public function get_service_charged_history_by_id_dates($data,$service_id)
	{
		$startDate=date('Y-m-d 00:00:00', strtotime($data['startDate']));
		$endDate=date('Y-m-d 23:59:59', strtotime($data['endDate']));
		$this->db
		->select('services_bill_info.serviceAmount,services_bill_info.serviceBillAddedDate,customer_info.organizationName,current_bill_info.meter_info_meterId')
		->from('services_bill_info')
		->join('customer_info',' services_bill_info.customer_info_service_customerId=customer_info.customerId')
		->join('current_bill_info',' services_bill_info.current_bill_info_currentBillId=current_bill_info.currentBillId')
		->where('services_bill_info.services_serviceId',$service_id);
		if($data['startDate']){
			$this->db->where('serviceBillAddedDate>=',$startDate);
		}
		if($data['endDate']){
			$this->db->where('serviceBillAddedDate<=',$endDate);
		}
		if($data['client_id']){
			$this->db->where('customer_info_service_customerId',$data['client_id']);
		}
		if($data['meter_id']){
			$this->db->where('current_bill_info.meter_info_meterId',$data['meter_id']);
		}
		$this->db->order_by('serviceBillAddedDate','desc');
		$q=$this->db->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	
	
	public function get_service_charged_history_by_id($service_id)
	{
		$q=$this->db
		->select('services_bill_info.serviceAmount,services_bill_info.serviceBillAddedDate,customer_info.organizationName')
		->from('services_bill_info')
		->join('customer_info',' services_bill_info.customer_info_service_customerId=customer_info.customerId')
		->where('services_bill_info.services_serviceId',$service_id)
		->limit(15)
		->order_by('serviceBillAddedDate','desc')
		->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	
	
	public function get_charged_energy_charge_history()
	{
		$q=$this->db
		->select('current_bill_info.totalEnergyCharge,current_bill_info.currentBillAddedDate,customer_info.organizationName')
		->from('current_bill_info')
		->join('customer_info',' current_bill_info.custome_info_current_customerId=customer_info.customerId')
		->limit(15)
		->order_by('currentBillAddedDate','desc')
		->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	
	public function get_charged_energy_charge_history_by_dates($data)
	{
		$startDate=date('Y-m-d 00:00:00', strtotime($data['startDate']));
		$endDate=date('Y-m-d 23:59:59', strtotime($data['endDate']));
		$this->db
		->select('current_bill_info.totalEnergyCharge,current_bill_info.currentBillAddedDate,customer_info.organizationName')
		->from('current_bill_info')
		->join('customer_info',' current_bill_info.custome_info_current_customerId=customer_info.customerId');
		if($data['startDate']){
			$this->db->where('currentBillAddedDate>=',$startDate);
		}
		if($data['endDate']){
			$this->db->where('currentBillAddedDate<=',$endDate);
		}
		if($data['client_id']){
			$this->db->where('custome_info_current_customerId',$data['client_id']);
		}
		if($data['meter_id']){
			$this->db->where('current_bill_info.meter_info_meterId',$data['meter_id']);
		}
		$this->db->order_by('currentBillAddedDate','desc');
		$q=$this->db->get();
		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	
	
	
	public function get_service_payment_by_dates($data)
	{
		$starDate=date('Y-m-d 00:00:00', strtotime($data['startDate']));
		$endDate=date('Y-m-d 23:59:59', strtotime($data['endDate']));
		$q=$this->db
		->select('service_charge_payment_info.serviceChargePaymentAmount,service_charge_payment_info.serviceChargePaidAmount,services.serviceName')
		->from('service_charge_payment_info')
		->join('services','service_charge_payment_info.service_info_serviceId=services.serviceId')
		->where('serviceChargePaymentDate>=',$starDate)
		->where('serviceChargePaymentDate<=',$endDate)
		->order_by('serviceChargePaymentDate','desc')
		->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	
	public function get_all_services_by_customer_id_and_meter_id($customerId,$meter_id)
	{
		$q=$this->db
		->select('customer_services.*')
		->from('customer_services')
		//->join('services','customer_services.services_serviceId=services.serviceId')
		->where('customer_info_customerId',$customerId)
		->where('meter_info_meterId',$meter_id)
		//->where('services.serviceStatus',1)
		->get();
		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	public function get_all_service_id_by_customer_id_and_meter_id($customerId,$meter_id)
	{
		$q=$this->db
		->select('customer_services.services_serviceId,customer_services.serviceTotalAmount')
		->from('customer_services')
		->where('customer_info_customerId',$customerId)
		->where('meter_info_meterId',$meter_id)
		->get();
		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	public function ajax_get_client_all_service_infos($data)
	{
		$q=$this->db
		->select('customer_services.serviceTotalAmount,customer_services.services_serviceId,services.serviceName')
		->from('customer_services')
		->join('services','customer_services.services_serviceId=services.serviceId')
		->where('customer_info_customerId',$data['client_id'])
		->where('meter_info_meterId',$data['meter_id'])
		->where('services.serviceStatus',1)
		->get();
		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	public function get_all_services_bill_by_id($bill_id)
	{
		$q=$this->db
		->select('services_bill_info.serviceBillId,services_bill_info.services_serviceId,services_bill_info.serviceAmount,services.serviceName')
		->from('services_bill_info')
		->join('services','services_bill_info.services_serviceId=services.serviceId')
		->where('services_bill_info.monthly_bill_info_mothlyBillInfoId',$bill_id)
		->where('services.serviceType',1)
		->group_by('services_serviceId')
		->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	
	public function updateServiceCharges($data)
	{
		$this->db->trans_start();
		$bill_id=$data['bill_id'];
		unset($data['bill_id']);
		$services=$this->admin_model->get_all_services_bill_by_id($bill_id);
		foreach($services as $service){
			$serviceAmount['serviceAmount']=$data[$service->services_serviceId];
			$this->db
			->where('monthly_bill_info_mothlyBillInfoId',$bill_id)
			->where('services_serviceId',$service->services_serviceId)
			->update('services_bill_info',$serviceAmount);
		}
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE){
			return false;
		}		
		
		else return true;
	}
	
	public function get_services_by_id_for_office($bill_id){
		$services=$this->db
		->select('services_bill_info.services_serviceId,services_bill_info.serviceAmount,services.serviceName')
		->from('services_bill_info')
		->join('services','services_bill_info.services_serviceId=services.serviceId')
		->where('services_bill_info.current_bill_info_currentBillId',$bill_id)
		->where('services.serviceType',1)
		->get();
		
		if($services->num_rows()>0){
			return $services->result();
		}
		else{
			return FALSE;
		}
	}
	
	public function get_print_survice_bill_info_by_id($bill_id,$current_bill){
		$services=$this->db
		->select('services_bill_info.services_serviceId,services_bill_info.serviceAmount,services.serviceName')
		->from('services_bill_info')
		->join('services','services_bill_info.services_serviceId=services.serviceId')
		->where('services_bill_info.current_bill_info_currentBillId',$current_bill)
		->where('services_bill_info.monthly_bill_info_mothlyBillInfoId',$bill_id)
		->where('services_bill_info.services_serviceId>',2)
		->get();
		
		if($services->num_rows()>0){
			return $services->result();
		}
		else{
			return FALSE;
		}
	}
	
	public function get_print_common_bill_info_by_id($bill_id,$current_bill){
		$services=$this->db
		->select('services_bill_info.services_serviceId,services_bill_info.serviceAmount,services.serviceName')
		->from('services_bill_info')
		->join('services','services_bill_info.services_serviceId=services.serviceId')
		->where('services_bill_info.current_bill_info_currentBillId',$current_bill)
		->where('services_bill_info.monthly_bill_info_mothlyBillInfoId',$bill_id)
		->where('services_bill_info.services_serviceId',1)
		->get();
		
		if($services->num_rows()==1){
			return $services->row();
		}
		else{
			return FALSE;
		}
	}
	
	public function get_print_govt_service_info_by_id($bill_id,$current_bill){
		$services=$this->db
		->select('services_bill_info.services_serviceId,services_bill_info.serviceAmount,services.serviceName')
		->from('services_bill_info')
		->join('services','services_bill_info.services_serviceId=services.serviceId')
		->where('services_bill_info.current_bill_info_currentBillId',$current_bill)
		->where('services_bill_info.monthly_bill_info_mothlyBillInfoId',$bill_id)
		->where('services_bill_info.services_serviceId',2)
		->get();
		
		if($services->num_rows()==1){
			return $services->row();
		}
		else{
			return FALSE;
		}
	}
	
	/* End Services */

	/* Start Payment Type */
	public function get_all_payment_types()
	{
		$q=$this->db
		->select('*')
		->from('payment_types')
		->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	
	/*Start Meter*/
	public function get_all_meters_by_customer_id($customerId)
	{
		$q=$this->db
		->select('*')
		->from('meter_info')
		->where('customer_info_customerId',$customerId)
		->where('meter_info.meterStatus',1)
		->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	public function ajax_get_client_meter_id_name($customerId)
	{
		$q=$this->db
		->select('meter_info.meterNo,meter_info.meterId')
		->from('meter_info')
		->where('customer_info_customerId',$customerId)
		->where('meter_info.meterStatus',1)
		->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	
	public function ajax_get_client_energyCharge($data)
	{
		$q=$this->db
		->select('billDue')
		->from('meter_info')
		->where('customer_info_customerId',$data['client_id'])
		->where('meterId',$data['meter_id'])
		->where('meterStatus',1)
		->get();
		if($q->num_rows()==1){
			return $q->row()->billDue;
		}
		else{
			return FALSE;
		}
	}
	public function get_all_customer_meter_info()
	{
		$q=$this->db
		->select('customer_info.organizationName,customer_info.customerName,customer_info.spaceType,customer_info.spaceTitle,customer_info.customerId,meter_info.meterId,meter_info.meterNo')
		->from('customer_info')
		->join('meter_info','customer_info.customerId=meter_info.customer_info_customerId')
		->where('customer_info.customerStatus',1)
		->where('meter_info.meterStatus',1)
		->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	
	public function get_meter_info_by_id($meter_id)
	{
		$q=$this->db
		->select('meter_info.*,customer_info.organizationName,customer_info.spaceType,customer_info.spaceTitle,customer_info.customerId')
		->from('meter_info')
		->join('customer_info','customer_info.customerId=meter_info.customer_info_customerId')
		->where('meter_info.meterId',$meter_id)
		->get();

		if($q->num_rows()==1){
			return $q->row();
		}
		else{
			return FALSE;
		}
	}
	
	/*End Meter*/
	/*Start Current Bills*/
	public function get_all_current_bill_by_id($bill_id)
	{
		$q=$this->db
		->select('current_bill_info.currentBillPrevReading,current_bill_info.currentBillCurrentReading,current_bill_info.currentBillId,current_bill_info.meter_info_meterId,meter_info.meterNo,customer_info.organizationName,customer_info.spaceTitle')
		->from('current_bill_info')
		->join('meter_info','current_bill_info.meter_info_meterId=meter_info.meterId')
		->join('customer_info','meter_info.customer_info_customerId=customer_info.customerId')
		->where('current_bill_info.monthly_bill_info_mothlyBillInfoId',$bill_id)
		->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	public function get_print_current_bill_info_by_id($current_bill)
	{
		$q=$this->db
		->select('*')
		->from('current_bill_info')
		->join('meter_info','current_bill_info.meter_info_meterId=meter_info.meterId')
		->join('customer_info','meter_info.customer_info_customerId=customer_info.customerId')
		->join('monthly_bill_info','current_bill_info.monthly_bill_info_mothlyBillInfoId=monthly_bill_info.mothlyBillInfoId')
		->where('current_bill_info.currentBillId',$current_bill)
		->get();

		if($q->num_rows()==1){
			return $q->row();
		}
		else{
			return FALSE;
		}
	}
	
	public function get_all_current_bill_by_id_for_office($bill_id)
	{
		$q=$this->db
		->select('current_bill_info.currentBillPrevReading,current_bill_info.currentBillCurrentReading,current_bill_info.currentBillId,current_bill_info.meter_info_meterId,current_bill_info.bill_info_billId,meter_info.meterNo,customer_info.organizationName,customer_info.spaceTitle')
		->from('current_bill_info')
		->join('meter_info','current_bill_info.meter_info_meterId=meter_info.meterId')
		->join('customer_info','meter_info.customer_info_customerId=customer_info.customerId')
		->where('current_bill_info.monthly_bill_info_mothlyBillInfoId',$bill_id)
		->get();

		if($q->num_rows()>0){
			$currentBills= $q->result();
			
			foreach($currentBills as $currentBill){
				//print_r($currentBills);exit;
				$services=$this->db
				->select('services_bill_info.*,services.serviceName')
				->from('services_bill_info')
				->join('services','services_bill_info.services_serviceId=services.serviceId')
				->where('services_bill_info.current_bill_info_currentBillId',$currentBill->currentBillId)
				->where('services.serviceType',1)
				->get();
				
				if($services->num_rows()>0){
					$currentBill->services=$services->result();
				}
			}
			return $currentBills;
		}
		else{
			return FALSE;
		}
	}
	
	public function get_all_current_bill_id_for_office_by_id($bill_id)
	{
		$q=$this->db
		->select('current_bill_info.currentBillId')
		->from('current_bill_info')
		->where('current_bill_info.monthly_bill_info_mothlyBillInfoId',$bill_id)
		->get();

		if($q->num_rows()>0){
			
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	
	/*End Current Bills */
	
	/* Start Shop Billing */
	
	public function create_shop_bill($data){
		$this->db->trans_start();
		$monthlyBill['monthlyBillingStatus']=$data['monthlyBillingStatus'];
		$monthlyBill['monthlyBillTypeFor']=$data['monthlyBillTypeFor'];
		$monthlyBill['billingMonthYear']=$data['billingMonthYear'];
		$monthlyBill['monthlyBillAddedDate']=$data['monthlyBillAddedDate'];
		$unit=$data['unitCharge'];
		$this->db->insert('monthly_bill_info', $monthlyBill );
		$monthly_bill_info_mothlyBillInfoId=$this->db->insert_id();
		$bill['monthly_bill_info_mothlyBillInfoId']=$monthly_bill_info_mothlyBillInfoId;
		$bill['billingMonth']=$data['billingMonthYear'];
		$bill['billAddedDate']=$data['monthlyBillAddedDate'];
		$bill['billStatus']=1;
		unset($data['monthlyBillingStatus'],$data['monthlyBillTypeFor'],$data['billingMonthYear'],$data['monthlyBillAddedDate'],$data['unitCharge']);
		
		$customers=$this->admin_model->get_all_customer_info_for_shop();
		//print_r($customers);exit;
		foreach($customers as $customer){
			$transbill['transectionAmount']=$customer->rentAmount;
			$transbill['customer_info_customerId']=$customer->customerId;
			$transbill['transectionDetails']="Monthly Rent";
			$transbill['transectionDate']=$bill['billAddedDate'];
			$transbill['TransectionType']=1;
			$transbill['transectionPrevDue']=$customer->rentDue;
			$this->db->insert('transection_info',$transbill);
			$referrenceId=$this->db->insert_id();
			
			$bill['customer_info_customerId']=$customer->customerId;
			$bill['customer_info_rentAmount']=$customer->rentAmount;
			$bill['customer_info_rentDue']=$customer->rentDue;
			$bill['referrenceId']=$referrenceId;
			$this->db->insert('bill_info', $bill);
			$bill_id=$this->db->insert_id();
			$this->db
			->where('customerId',$customer->customerId)
			->set('rentDue', 'rentDue + ' .  $customer->rentAmount, FALSE)
			->update('customer_info');
			$meters=$this->admin_model->get_all_meters_by_customer_id($customer->customerId);
			
			if($meters){
				//print_r($meters);exit;
				$current['monthly_bill_info_mothlyBillInfoId']=$monthly_bill_info_mothlyBillInfoId;
				$current['bill_info_billId']=$bill_id;
				$current['perUnitCharge']=$unit;
				$current['currentBillAddedDate']=$bill['billAddedDate'];
				foreach($meters as $meter){
					$current['meter_info_meterId']=$meter->meterId;
					$current['custome_info_current_customerId']=$customer->customerId;
					$current['currentBillPrevReading']=$meter->previousReading;
					$current['currentBillPrevReadingDate']=$meter->previousReadingDate;
					$current['currentBillCurrentReading']=0;
					$current['currentBillCurrentReadingDate']=$data['currentReadingDate'];
					$current['currentBillLastDate']=$data['lastPaymentDate'];
					$current['currentBillDemandCharge']=$meter->demandCharge;
					$current['currentBillServiceCharge']=$meter->meterServiceCharge;
					$current['meter_info_billDue']=$meter->billDue;
					$this->db->insert('current_bill_info', $current);
					$meter_id=$this->db->insert_id();
					
					$serviceList=$this->admin_model->get_all_services_by_customer_id_and_meter_id($customer->customerId,$meter->meterId);
					//print_r($serviceList);//exit;
					if($serviceList){
						$serviceBill['bill_info_billId']=$bill_id;
						$serviceBill['monthly_bill_info_mothlyBillInfoId']=$monthly_bill_info_mothlyBillInfoId;
						$serviceBill['serviceBillingMonth']=$bill['billingMonth'];
						$serviceBill['serviceBillAddedDate']=$bill['billAddedDate'];
						$serviceBill['current_bill_info_currentBillId']=$meter_id;
						$serviceBill['customer_info_service_customerId']=$customer->customerId;
						foreach($serviceList as $service){
							//print_r($service);
							$serviceBill['services_serviceId']=$service->services_serviceId;
							$serviceBill['serviceAmount']=$service->customerServiceAmount;
							if($service->customerServiceAmount==0 && $data[$service->services_serviceId]){
								$serviceBill['serviceAmount']=$data[$service->services_serviceId]; 
							}
							$this->db->insert('services_bill_info',$serviceBill);
						}
						//exit;
					}
					$meter_info['previousReadingDate']=$data['currentReadingDate'];
					$this->db
						->where('meterId',$meter->meterId)
						->update('meter_info',$meter_info);
				}
			}
		}
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE){
			return false;
		}		
		
		else return true;
	}
	public function get_bill_info_by_id($bill_id){
		$q=$this->db
		->select('monthly_bill_info.mothlyBillInfoId,monthly_bill_info.monthlyBillingStatus,monthly_bill_info.billingMonthYear')
		->from('monthly_bill_info')
		->where('mothlyBillInfoId',$bill_id)
		->get();

		if($q->num_rows()==1){
			return $q->row();
		}
		else{
			return FALSE;
		}
	}
	public function get_all_shop_bills(){
		$q=$this->db
		->select('*')
		->from('monthly_bill_info')
		->where('monthlyBillTypeFor',1)
		->order_by('mothlyBillInfoId','desc')
		->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	public function updateCurrentReading($data){
		//$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
		//$date= $dt->format('Y-m-d H:i:s');
		//$data['currentBillCurrentReadingDate']=$date;
		$currentBillId=$data['currentBillId'];
		$meter_info_meterId=$data['meter_info_meterId'];
		unset($data['currentBillId'],$data['meter_info_meterId']);
		if($this->db->where('currentBillId',$currentBillId)->update('current_bill_info',$data)){
			//$meter['previousReadingDate']=$date;
			$meter['previousReading']=$data['currentBillCurrentReading'];
			return $this->db
			->where('meterId',$meter_info_meterId)
			->update('meter_info',$meter);
		}
	}
	public function make_bills_final($bill_id){
		$this->db->trans_start();
		$q=$this->db
		->select('current_bill_info.currentBillPrevReading,current_bill_info.currentBillCurrentReading,current_bill_info.currentBillId,current_bill_info.perUnitCharge,current_bill_info.currentBillDemandCharge,current_bill_info.meter_info_meterId,current_bill_info.currentBillServiceCharge,meter_info.customer_info_customerId')
		->from('current_bill_info')
		->join('meter_info','current_bill_info.meter_info_meterId=meter_info.meterId')
		->where('current_bill_info.monthly_bill_info_mothlyBillInfoId',$bill_id)
		->get();

		if($q->num_rows()>0){
			$currentBills= $q->result();
			
			foreach($currentBills as $currentBill){
					//print_r($currentBill->currentBillCurrentReading);exit;
				$reading=$currentBill->currentBillCurrentReading-$currentBill->currentBillPrevReading;
				if($reading<46)
				$reading=46;
				$currentBillTotal=$reading*$currentBill->perUnitCharge;
				$totalAmount=$currentBill->currentBillServiceCharge+$currentBill->currentBillDemandCharge+$currentBillTotal;
				$totalEnergyCharge=$totalAmount;
				$this->db
					->where('meterId',$currentBill->meter_info_meterId)
					->set('billDue', 'billDue + ' .  $totalAmount, FALSE)
					->update('meter_info');
				$services=$this->db
				->select('services_bill_info.services_serviceId,services_bill_info.serviceAmount')
				->from('services_bill_info')
				->where('services_bill_info.current_bill_info_currentBillId',$currentBill->currentBillId)
				->where('services_bill_info.monthly_bill_info_mothlyBillInfoId',$bill_id)
				->get();
				
				if($services->num_rows()>0){
					foreach($services->result() as $service){
							//print_r($service);exit;
						$this->db
							->where('meter_info_meterId',$currentBill->meter_info_meterId)
							->where('services_serviceId',$service->services_serviceId)
							->set('serviceTotalAmount', 'serviceTotalAmount + ' .  $service->serviceAmount, FALSE)
							->update('customer_services');
						$totalAmount=$totalAmount+$service->serviceAmount;
					}
				}
				
				$transbill['transectionAmount']=$totalAmount;
				$transbill['customer_info_customerId']=$currentBill->customer_info_customerId;
				$transbill['transectionDetails']="Monthly Bill";
				$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
				$date= $dt->format('Y-m-d H:i:s');
				$transbill['transectionDate']=$date;
				$transbill['TransectionType']=2;
				$transbill['transectionPrevDue']=$this->admin_model->get_customer_bill_due_by_id($currentBill->customer_info_customerId);
				$this->db->insert('transection_info',$transbill);
				$referrenceId=$this->db->insert_id();
				
				$billInfo['totalCurrentBill']=$totalAmount;
				$billInfo['totalEnergyCharge']=$totalEnergyCharge;
				$billInfo['referrenceId']=$referrenceId;
				$this->db
				->where('currentBillId',$currentBill->currentBillId)
				->update('current_bill_info',$billInfo);
				$this->db
				->where('customerId',$currentBill->customer_info_customerId)
				->set('billDue', 'billDue + ' .  $totalAmount, FALSE)
				->update('customer_info');
			}
				//return $currentBills;
		}
		$data['monthlyBillingStatus']=2;
		$this->db
		->where('mothlyBillInfoId',$bill_id)
		->update('monthly_bill_info',$data);
		
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE){
			return false;
		}	
		else return true;
	}
	
	
	
	/* End Shop Billing */
	/* Start Office Billing */
	public function get_all_bills(){
		$q=$this->db
		->select('*')
		->from('monthly_bill_info')
		->order_by('mothlyBillInfoId','desc')
		->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	public function get_all_office_bills(){
		$q=$this->db
		->select('*')
		->from('monthly_bill_info')
		->where('monthlyBillTypeFor',2)
		->order_by('mothlyBillInfoId','desc')
		->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	public function get_office_billing_month_by_date($monthStart,$monthEnd){
		$q=$this->db
		->select('billingMonthYear')
		->from('monthly_bill_info')
		->where('monthlyBillTypeFor',2)
		->where('billingMonthYear>=',$monthStart)
		->where('billingMonthYear<=',$monthEnd)
		->get();

		if($q->num_rows()>0){
			return True;
		}
		else{
			return FALSE;
		}
	}
	
	public function get_shop_billing_month_by_date($monthStart,$monthEnd){
		$q=$this->db
		->select('billingMonthYear')
		->from('monthly_bill_info')
		->where('monthlyBillTypeFor',1)
		->where('billingMonthYear>=',$monthStart)
		->where('billingMonthYear<=',$monthEnd)
		->get();

		if($q->num_rows()>0){
			return True;
		}
		else{
			return FALSE;
		}
	}
	
	public function create_office_bill($data){
		$this->db->trans_start();
		$monthlyBill['monthlyBillingStatus']=$data['monthlyBillingStatus'];
		$monthlyBill['monthlyBillTypeFor']=$data['monthlyBillTypeFor'];
		$monthlyBill['billingMonthYear']=$data['billingMonthYear'];
		//print_r($monthlyBill);exit;
		$monthlyBill['monthlyBillAddedDate']=$data['monthlyBillAddedDate'];
		$unit=$data['unitCharge'];
		$this->db->insert('monthly_bill_info', $monthlyBill );
		$monthly_bill_info_mothlyBillInfoId=$this->db->insert_id();
		$bill['monthly_bill_info_mothlyBillInfoId']=$monthly_bill_info_mothlyBillInfoId;
		$bill['billingMonth']=$data['billingMonthYear'];
		$bill['billAddedDate']=$data['monthlyBillAddedDate'];
		$bill['billStatus']=1;
		unset($data['monthlyBillingStatus'],$data['monthlyBillTypeFor'],$data['billingMonthYear'],$data['monthlyBillAddedDate'],$data['unitCharge']);
		
		$customers=$this->admin_model->get_all_customer_info_for_office();
			//print_r($customers);exit;
		foreach($customers as $customer){
			$transbill['transectionAmount']=$customer->rentAmount;
			$transbill['customer_info_customerId']=$customer->customerId;
			$transbill['transectionDetails']="Monthly Rent";
			$transbill['transectionDate']=$bill['billAddedDate'];
			$transbill['TransectionType']=1;
			$transbill['transectionPrevDue']=$customer->rentDue;
			$this->db->insert('transection_info',$transbill);
			$referrenceId=$this->db->insert_id();
			
			$bill['referrenceId']=$referrenceId;
			$bill['customer_info_customerId']=$customer->customerId;
			$bill['customer_info_rentAmount']=$customer->rentAmount;
			$bill['customer_info_rentDue']=$customer->rentDue;
			$this->db->insert('bill_info', $bill);
			$bill_id=$this->db->insert_id();
			$this->db
			->where('customerId',$customer->customerId)
			->set('rentDue', 'rentDue + ' .  $customer->rentAmount, FALSE)
			->update('customer_info');
			$meters=$this->admin_model->get_all_meters_by_customer_id($customer->customerId);
			
			if($meters){
					//print_r($meters);exit;
				$current['monthly_bill_info_mothlyBillInfoId']=$monthly_bill_info_mothlyBillInfoId;
				$current['bill_info_billId']=$bill_id;
				$current['perUnitCharge']=$unit;
				$current['currentBillAddedDate']=$bill['billAddedDate'];
				foreach($meters as $meter){
					$current['meter_info_meterId']=$meter->meterId;
					$current['custome_info_current_customerId']=$customer->customerId;
					$current['currentBillPrevReading']=$meter->previousReading;
					$current['currentBillPrevReadingDate']=$meter->previousReadingDate;
					$current['currentBillCurrentReading']=0;
					$current['currentBillCurrentReadingDate']=$data['currentReadingDate'];
					$current['currentBillLastDate']=$data['lastPaymentDate'];
					$current['currentBillDemandCharge']=$meter->demandCharge;
					$current['currentBillServiceCharge']=$meter->meterServiceCharge;
					$current['meter_info_billDue']=$meter->billDue;
					$this->db->insert('current_bill_info', $current);
					$meter_id=$this->db->insert_id();
					
					$serviceList=$this->admin_model->get_all_services_by_customer_id_and_meter_id($customer->customerId,$meter->meterId);
						//print_r($serviceList);exit;
					if($serviceList){
						$serviceBill['bill_info_billId']=$bill_id;
						$serviceBill['monthly_bill_info_mothlyBillInfoId']=$monthly_bill_info_mothlyBillInfoId;
						$serviceBill['serviceBillingMonth']=$bill['billingMonth'];
						$serviceBill['serviceBillAddedDate']=$bill['billAddedDate'];
						$serviceBill['current_bill_info_currentBillId']=$meter_id;
						$serviceBill['customer_info_service_customerId']=$customer->customerId;
						foreach($serviceList as $service){
								//print_r($service);
							$serviceBill['services_serviceId']=$service->services_serviceId;
							$serviceBill['serviceAmount']=$service->customerServiceAmount;
							$this->db->insert('services_bill_info',$serviceBill);
						}
					}
					$meter_info['previousReadingDate']=$data['currentReadingDate'];
					$this->db
						->where('meterId',$meter->meterId)
						->update('meter_info',$meter_info);
				}
			}
		}
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE){
			return false;
		}		
		
		else return true;
	}
	
	public function update_office_bill($data){
		$this->db->trans_start();
		//$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
		//$date= $dt->format('Y-m-d H:i:s');
		//$currentBill['currentBillCurrentReadingDate']=$date;
		$currentBill['currentBillCurrentReading']=$data['currentReading'];
		$currentBillId=$data['bill_id'];
		$meter_info_meterId=$data['meter_info_meterId'];
		unset($data['bill_id'],$data['meter_info_meterId'],$data['currentReading'],$data['close'],$data['submit']);
		if($this->db->where('currentBillId',$currentBillId)->update('current_bill_info',$currentBill)){
			//$meter['previousReadingDate']=$date;
			$meter['previousReading']=$currentBill['currentBillCurrentReading'];
			$this->db
			->where('meterId',$meter_info_meterId)
			->update('meter_info',$meter);
			$charges=$this->admin_model->get_services_by_id_for_office($currentBillId);
			if($charges){
				foreach($charges as $charge):
						//$serviceBill['services_serviceId']=$charge->services_serviceId;
				$serviceBill['serviceAmount']=$data[$charge->services_serviceId];
				$this->db
				->where('current_bill_info_currentBillId',$currentBillId)
				->where('services_serviceId',$charge->services_serviceId)
				->update('services_bill_info',$serviceBill);
				endforeach;
			}
		}
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE){
			return false;
		}	
		else return true;
	}
	
	/* End office Billing */
	
	/* Start received payment */
	public function store_received_payment($data){
		$this->db->trans_start();
		$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
		$date= $dt->format('Y-m-d H:i:s');
		$paymentHistory['paymentDate']=$date;
		//print_r($data);exit;
		$bill['transectionAmount']=$data['paymentAmount'];
		$bill['customer_info_customerId']=$data['customer_info_customerId'];
		$bill['transectionDetails']=$data['paymentDetails'];
		$bill['transectionDate']=$date;
		if($data['payment_types_paymentTypeId']==2){
			$bill['TransectionType']=3;
			$bill['transectionPrevDue']=$this->admin_model->get_customer_rent_due_by_id($data['customer_info_customerId']);
		}
		else if($data['payment_types_paymentTypeId']==1){
			$bill['TransectionType']=4;
			$bill['transectionPrevDue']=$this->admin_model->get_customer_bill_due_by_id($data['customer_info_customerId']);
		}
		$this->db->insert('transection_info',$bill);	
		
		$paymentHistory['referrenceId']=$this->db->insert_id();
		$paymentHistory['customer_info_customerId']=$data['customer_info_customerId'];
		$paymentHistory['paymentAmount']=$data['paymentAmount'];
		$paymentHistory['payment_types_paymentTypeId']=$data['payment_types_paymentTypeId'];
		$paymentHistory['paymentDetails']=$data['paymentDetails'];
		$payment=$this->db->insert('payment_history',$paymentHistory);
		$payment_id=$this->db->insert_id();
		if($payment){
			if($data['payment_types_paymentTypeId']==2){
				$this->db
					->where('customerId',$data['customer_info_customerId'])
					->set('rentDue', 'rentDue - ' .  $data['paymentAmount'], FALSE)
					->update('customer_info');
				$cashInhand['cashPurpose']=1;
				$transaction['transactionType']=1;
			}
			else if($data['payment_types_paymentTypeId']==1){
				$this->db
					->where('customerId',$data['customer_info_customerId'])
					->set('billDue', 'billDue - ' .  $data['paymentAmount'], FALSE)
					->update('customer_info');
				$cashInhand['cashPurpose']=2;
				//$transaction['cashPurpose']=2;
				$transaction['transactionType']=2;
				if($data['enargyCharge']){
					$info['client_id']=$data['customer_info_customerId'];
					$info['meter_id']=$data['meter_id'];
					$energyCharge['serviceChargePaymentAmount']=$this->admin_model->ajax_get_client_energyCharge($info);
					$energyCharge['serviceChargePaidAmount']=$data['enargyCharge'];
					$energyCharge['payment_history_paymentID']=$payment_id;
					$energyCharge['service_info_serviceId']=0;
					$energyCharge['meter_info_meterId']=$data['meter_id'];
					$energyCharge['client_info_clientId']=$data['customer_info_customerId'];
					$energyCharge['serviceChargePaymentDate']=$paymentHistory['paymentDate'];
					$this->db->insert('service_charge_payment_info',$energyCharge);
					$this->db
						->where('meterId',$data['meter_id'])
						->set('billDue', 'billDue - ' . $data['enargyCharge'], FALSE)
						->update('meter_info');
				}
				$serviceList=$this->admin_model->get_all_service_id_by_customer_id_and_meter_id($data['customer_info_customerId'],$data['meter_id']);
				foreach($serviceList as $service){
					if($data[$service->services_serviceId]){
						$serviceCharge['serviceChargePaidAmount']=$data[$service->services_serviceId];
						$serviceCharge['serviceChargePaymentAmount']=$service->serviceTotalAmount;
						$serviceCharge['payment_history_paymentID']=$payment_id;
						$serviceCharge['service_info_serviceId']=$service->services_serviceId; 
						$serviceCharge['meter_info_meterId']=$data['meter_id'];
						$serviceCharge['client_info_clientId']=$data['customer_info_customerId'];
						$serviceCharge['serviceChargePaymentDate']=$paymentHistory['paymentDate'];
						$this->db->insert('service_charge_payment_info',$serviceCharge);
						$this->db
						->where('meter_info_meterId',$data['meter_id'])
						->where('services_serviceId',$service->services_serviceId)
						->set('serviceTotalAmount', 'serviceTotalAmount - ' .  $data[$service->services_serviceId], FALSE)
						->update('customer_services');
					}
					
				}
			}
			/* insert in Cash in hand table */
			$cashInhand['cashDate']=$date;
			$cashInhand['cashEntryDate']=$date;
			$cashInhand['cashTo']=$this->admin_model->get_organization_name_by_id($data['customer_info_customerId']);
			$cashInhand['cashInType']=1;
			$cashInhand['cashAmount']=$data['paymentAmount'];
			$cashInhand['cashReferenceId']=$payment_id;
			$this->db->insert('cash_in_hand',$cashInhand);
			
			/* insert in All Transaction table */
			$transaction['transactionDate']=$date;
			$transaction['transactionEntryDate']=$date;
			$transaction['transactionFrom']=$cashInhand['cashTo'];
			$transaction['transactionTo']="Cash In Hand";
			$transaction['transactionDetails']=$data['paymentDetails'];
			$transaction['transactionAmount']=$data['paymentAmount'];
			$transaction['transactionReferenceId']=$payment_id;
			$this->db->insert('all_transaction_info',$transaction);
		}
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE){
			return false;
		}	
		else return true;
	}
	
	public function get_all_received_payment_info(){
		$q=$this->db
		->select('payment_history.*,customer_info.organizationName,payment_types.paymentTypeName')
		->from('payment_history')
		->join('customer_info','payment_history.customer_info_customerId=customer_info.customerId')
		->join('payment_types','payment_history.payment_types_paymentTypeId=payment_types.paymentTypeId')
		->order_by('paymentId','desc')
		->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	
	public function get_received_payment_info_by_id($paymentId){
		$q=$this->db
		->select('payment_history.*,customer_info.organizationName,payment_types.paymentTypeName')
		->from('payment_history')
		->join('customer_info','payment_history.customer_info_customerId=customer_info.customerId')
		->join('payment_types','payment_history.payment_types_paymentTypeId=payment_types.paymentTypeId')
		->where('paymentId',$paymentId)
		->get();

		if($q->num_rows()==1){
			return $q->row();
		}
		else{
			return FALSE;
		}
	}
	
	/* Start Cash Transactions*/
	/* Cash In Hand*/	
	public function get_cash_in_hand_infos(){
		$q=$this->db
		->select('cash_in_hand.*,transaction_type_info.transactionName,transaction_type_info.transactionDetailslink')
		->from('cash_in_hand')
		->join('transaction_type_info','cash_in_hand.cashPurpose=transaction_type_info.transactionId')
		->order_by('cashEntryDate','desc')
		->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	public function get_cash_in_hand_amount(){
		
		$q=$this->db
		->select('sum(`cashAmount`) as cash')
		->from('cash_in_hand')
		->where('cashInType',1)
		->get();
		$q2=$this->db
		->select('sum(`cashAmount`) as cash')
		->from('cash_in_hand')
		->where('cashInType',2)
		->get();
		$cash=$q->row()->cash-$q2->row()->cash;
		if($cash){
			return $cash;
		}
		else{
			return 0;
		}
	}
	/* petty Cash */	
	public function get_petty_cash_infos(){
		$q=$this->db
		->select('petty_cash.*,transaction_type_info.transactionName,transaction_type_info.transactionDetailslink')
		->from('petty_cash')
		->join('transaction_type_info','petty_cash.pettyCashPurpose=transaction_type_info.transactionId')
		->order_by('pettyCashEntryDate','desc')
		->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	public function get_petty_cash_amount(){
		
		$q=$this->db
		->select('sum(`pettyCashAmount`) as cash')
		->from('petty_cash')
		->where('pettyCashType',1)
		->get();
		$q2=$this->db
		->select('sum(`pettyCashAmount`) as cash')
		->from('petty_cash')
		->where('pettyCashType',2)
		->get();
		$cash=$q->row()->cash-$q2->row()->cash;
		if($cash){
			return $cash;
		}
		else{
			return 0;
		}
	}
	
	/* Common Transactions */	
	public function get_all_transection_infos(){
		$q=$this->db
		->select('all_transaction_info.*,transaction_type_info.transactionName,transaction_type_info.transactionDetailslink')
		->from('all_transaction_info')
		->join('transaction_type_info','all_transaction_info.transactionType=transaction_type_info.transactionId')
		->order_by('transactionEntryDate','desc')
		->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	public function store_cash_transfer($data){
		$this->db->trans_start();
		$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
		$date= $dt->format('Y-m-d H:i:s');
		$data['cashTransferEntryDate']=$date;
		$data['cashTransferDate']=date('Y-m-d', strtotime($data['cashTransferDate']));
		//print_r($data);exit;
		$this->db->insert('cash_transfer',$data);
		$cashTransfer_id=$this->db->insert_id();
		if($cashTransfer_id){
			if($data['cashWithdrawnFrom']<1){
				if($data['cashWithdrawnFrom']==-1){
					$transaction['transactionFrom']="Cash In Hand";
					/* insert in Cash in hand table */
					$cashInhand['cashDate']=$data['cashTransferDate'];
					$cashInhand['cashEntryDate']=$date;
					if($data['cashTransferredTo']==0){
						$cashInhand['cashTo']="Petty Cash";
					}
					else{
						$cashInhand['cashTo']=$this->admin_model->get_bank_account_name_by_id($data['cashTransferredTo']);
					}
					$cashInhand['cashInType']=2;
					$cashInhand['cashPurpose']=5;
					$cashInhand['cashAmount']=$data['cashTransferAmount'];
					$cashInhand['cashReferenceId']=$cashTransfer_id;
					$this->db->insert('cash_in_hand',$cashInhand);
				}
				else if($data['cashWithdrawnFrom']==0){
					$transaction['transactionFrom']="Petty Cash";
					/* insert in Petty Cash table */
					$pettyCash['pettyCashDate']=$data['cashTransferDate'];
					$pettyCash['pettyCashEntryDate']=$date;
					if($data['cashTransferredTo']==-1){
						$pettyCash['pettyCashto']="Cash In Hand";
					}
					else{
						$pettyCash['pettyCashto']=$this->admin_model->get_bank_account_name_by_id($data['cashTransferredTo']);
					}
					$pettyCash['pettyCashType']=2;
					$pettyCash['pettyCashPurpose']=5;
					$pettyCash['pettyCashAmount']=$data['cashTransferAmount'];
					$pettyCash['pettyCashReferenceId']=$cashTransfer_id;
					$this->db->insert('petty_cash',$pettyCash);
				}
			}
			else{
				$transaction['transactionFrom']=$this->admin_model->get_bank_account_name_by_id($data['cashWithdrawnFrom']);
				$this->db
				->where('bankAccountId',$data['cashWithdrawnFrom'])
				->set('bankAccountLastBalance', 'bankAccountLastBalance - ' .  $data['cashTransferAmount'], FALSE)
				->update('bank_account_info');
				$bankOut['bank_account_infoBankId']=$data['cashWithdrawnFrom'];
				$bankOut['bankDetailsReferrenceId']=$cashTransfer_id;
				$bankOut['bankDetailsEntryDate']=$date;
				$bankOut['bankDetailsTransactionDate']=$data['cashTransferDate'];
				$bankOut['bankDetailsNote']=$data['cashTransferNote'];
				$bankOut['bankDetailsDeposit']=$data['cashWithdrawnFrom'];
				$bankOut['bankDetailsBalance']=$data['cashTransferAmount'];
				$bankOut['bankDetailsPurpose']=5;
				$bankOut['bankDetailsType']=2;
				$this->db->insert('bank_account_details',$bankOut);
				
			}
			if($data['cashTransferredTo']<1){
				if($data['cashTransferredTo']==-1){
					$transaction['transactionTo']="Cash In Hand";
					/* insert in Cash in hand table */
					$cashInhand['cashDate']=$data['cashTransferDate'];
					$cashInhand['cashEntryDate']=$date;
					if($data['cashWithdrawnFrom']==0){
						$cashInhand['cashTo']="Petty Cash";
					}
					else{
						$cashInhand['cashTo']=$this->admin_model->get_bank_account_name_by_id($data['cashWithdrawnFrom']);
					}
					$cashInhand['cashInType']=1;
					$cashInhand['cashPurpose']=5;
					$cashInhand['cashAmount']=$data['cashTransferAmount'];
					$cashInhand['cashReferenceId']=$cashTransfer_id;
					$this->db->insert('cash_in_hand',$cashInhand);
				}
				if($data['cashTransferredTo']==0){
					$transaction['transactionTo']="Petty Cash";
					/* insert in Petty Cash table */
					$pettyCash['pettyCashDate']=$data['cashTransferDate'];
					$pettyCash['pettyCashEntryDate']=$date;
					if($data['cashWithdrawnFrom']==-1){
						$pettyCash['pettyCashto']="Cash In Hand";
					}
					else{
						$pettyCash['pettyCashto']=$this->admin_model->get_bank_account_name_by_id($data['cashWithdrawnFrom']);
					}
					$pettyCash['pettyCashType']=1;
					$pettyCash['pettyCashPurpose']=5;
					$pettyCash['pettyCashAmount']=$data['cashTransferAmount'];
					$pettyCash['pettyCashReferenceId']=$cashTransfer_id;
					$this->db->insert('petty_cash',$pettyCash);
				}
			}
			else{
				$transaction['transactionTo']=$this->admin_model->get_bank_account_name_by_id($data['cashTransferredTo']);
				$this->db
				->where('bankAccountId',$data['cashTransferredTo'])
				->set('bankAccountLastBalance', 'bankAccountLastBalance + ' .  $data['cashTransferAmount'], FALSE)
				->update('bank_account_info');
				$bankIn['bank_account_infoBankId']=$data['cashTransferredTo'];
				$bankIn['bankDetailsReferrenceId']=$cashTransfer_id;
				$bankIn['bankDetailsEntryDate']=$date;
				$bankIn['bankDetailsTransactionDate']=$data['cashTransferDate'];
				$bankIn['bankDetailsNote']=$data['cashTransferNote'];
				$bankIn['bankDetailsDeposit']=$data['cashTransferredTo'];
				$bankIn['bankDetailsBalance']=$data['cashTransferAmount'];
				$bankIn['bankDetailsPurpose']=5;
				$bankIn['bankDetailsType']=1;
				$this->db->insert('bank_account_details',$bankIn);
					
			}
			
			/* insert in All Transaction table */
			$transaction['transactionType']=5;
			$transaction['transactionDate']=$data['cashTransferDate'];
			$transaction['transactionEntryDate']=$date;
			$transaction['transactionDetails']=$data['cashTransferNote'];
			$transaction['transactionAmount']=$data['cashTransferAmount'];
			$transaction['transactionReferenceId']=$cashTransfer_id;
			$this->db->insert('all_transaction_info',$transaction);
		}
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE){
			return false;
		}	
		else return true;
	}
	
	
	public function get_transfer_details_by_id($transfer_id,$type){
		$q=$this->db
		->select('cash_transfer.*,all_transaction_info.transactionFrom,all_transaction_info.transactionTo')
		->from('cash_transfer')
		->join('all_transaction_info','cash_transfer.cashTransferId=all_transaction_info.transactionReferenceId')
		->where('cashTransferId',$transfer_id)
		->where('transactionType',$type)
		->get();

		if($q->num_rows()==1){
			return $q->row();
		}
		else{
			return FALSE;
		}
	}
	
	/* End Cash Transactions*/
	
	/* Start Bank Account*/
	public function store_bank_account_infos($data)
	{
		$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
		$date= $dt->format('Y-m-d H:i:s');
		$data['bankAccountAddedDate']=$date;
		return $this->db->insert('bank_account_info', $data );
	}
	public function store_bank_adjustment($data)
	{
		$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
		$date= $dt->format('Y-m-d H:i:s');
		$data['accountAdjustmentDate']=$date;
		$data['accountAdjustmentBy']=$this->session->userdata('admin_id');
		print_r($data);exit;
		return $this->db->insert('bank_account_info', $data );
	}
	
	public function get_all_bank_account_infos(){
		$q=$this->db
		->select('*')
		->from('bank_account_info')
		->where('bankAccountStatus',1)
		->order_by('bankAccountAddedDate','desc')
		->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}		
	
	public function get_bank_account_details_by_id($account_id){
		$q=$this->db
		->select('*')
		->from('bank_account_info')
		->where('bankAccountId',$account_id)
		->get();

		if($q->num_rows()==1){
			return $q->row();
		}
		else{
			return FALSE;
		}
	}	
	public function get_bank_account_transection_by_id($account_id){
		$q=$this->db
		->select('*')
		->from('bank_account_details')
		->where('bank_account_infoBankId',$account_id)
		->order_by('bankDetailsEntryDate','desc')
		->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}		
	public function get_bank_account_name_by_id($account_id){
		$q=$this->db
		->select('bank_account_info.bankAccountTitle')
		->from('bank_account_info')
		->where('bankAccountId',$account_id)
		->get();

		if($q->num_rows()==1){
			return $q->row()->bankAccountTitle;
		}
		else{
			return FALSE;
		}
	}			
	public function get_bank_account_balance_by_id($account_id){
		$q=$this->db
		->select('bank_account_info.bankAccountLastBalance')
		->from('bank_account_info')
		->where('bankAccountId',$account_id)
		->get();

		if($q->num_rows()==1){
			return $q->row()->bankAccountLastBalance;
		}
		else{
			return FALSE;
		}
	}	
	public function update_bank_account_infos($data,$bankAccountId){
		return $this->db
		->where('bankAccountId',$bankAccountId)
		->update('bank_account_info',$data);
	}
	
	public function get_all_bank_account_id_name(){
		$q=$this->db
		->select('bank_account_info.bankAccountId,bank_account_info.bankAccountTitle')
		->from('bank_account_info')
		->where('bankAccountStatus',1)
		->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	
	/* End Bank Account*/

	/* Start Fixed Asset*/ 
	public function storeAsset($data)
	{
		return $this->db->insert('asset_info', $data);
	}
	
	public function get_all_asset()
	{
		
		$q=$this->db
				->select('asset_info.assetId,asset_info.assetTitle,asset_info.assetTrackingId,asset_info.assetAccruingDate,asset_info.assetCurrentValue')
				->from('asset_info')
				//->where('assetStatus',1)
				->get();
		if($q->num_rows()){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	public function get_asset_details_by_id($assetId)
	{
		$q=$this->db
				->select('*')
				->from('asset_info')
				//->where('assetStatus',1)
				->where('assetId',$assetId)
				->get();
		if($q->num_rows()==1){
			return $q->row();
		}
		else{
			return FALSE;
		}
	}
	public function updateAsset($data,$asset_Id){
		//print_r($data);exit();
        return $this->db
					->where('assetId',$asset_Id)
					->update('asset_info',$data);
    }
	
	/* Particular */
	
	public function get_all_particular_name_id(){
		$q=$this->db
			->select('particular_info.particularId,particular_info.particularTitle,particular_info.particularType')
			->from('particular_info')
			->where('particularStatus',1)
			->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}

	public function get_all_income_particular_name_id(){
		$q=$this->db
			->select('particular_info.particularId,particular_info.particularTitle,particular_info.particularType')
			->from('particular_info')
			->where('particularStatus',1)
			->where('particularType',1)
			->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}

	public function get_all_expence_particular_name_id(){
		$q=$this->db
			->select('particular_info.particularId,particular_info.particularTitle,particular_info.particularType')
			->from('particular_info')
			->where('particularStatus',1)
			->where('particularType',2)
			->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}

	public function get_all_particular_details(){
		$q=$this->db
			->select('*')
			->from('particular_info')
			->where('particularStatus',1)
			->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}

	public function store_particular($data)
	{
		$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
		$date= $dt->format('Y-m-d H:i:s');
		$data['particularAddedDate']=$date;
		return $this->db->insert('particular_info', $data );
	}
	public function get_all_info_by_particularId($particularid){
		$q=$this->db
			->select('*')
			->from('particular_info')
			->where('particularId',$particularid)
			->get();

		if($q->num_rows()>0){
			return $q->row();
		}
		else{
			return FALSE;
		}
	}
	public function update_particular($data){
	//	print_r($data);exit();
		$particularid=$data['particularId'];
		return $this->db
		->where('particularId',$particularid)
		->update('particular_info',$data);
	}
	
	/* Start Ledgers*/
	public function store_ledger_info($data)
	{
		$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
		$date= $dt->format('Y-m-d H:i:s');
		$data['ledgerAddedDate']=$date;
		return $this->db->insert('ledger_info', $data );
	}
	public function get_all_ledger_info(){
		$q=$this->db
			->select('ledger_info.*,particular_info.particularTitle')
			->from('ledger_info')
			->join('particular_info','ledger_info.particular_info_particularid=particular_info.particularid')
			->order_by('ledgerStatus','desc')
			->order_by('ledgerAddedDate','desc')
			->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	public function get_ledger_entry_infos_by_id($ledgerId){
		$q=$this->db
			->select('ledger_details_info.*')
			->from('ledger_details_info')
			->order_by('ledgerDetailsAddedDate','desc')
			->where('ledger_info_ledgerId',$ledgerId)
			->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	public function get_ledger_entry_details_by_id($ledger_id, $type){
		$q=$this->db
			->select('ledger_details_info.*,all_transaction_info.transactionFrom,all_transaction_info.transactionTo')
			->from('ledger_details_info')
			->join('all_transaction_info','ledger_details_info.ledgerDetailsId=all_transaction_info.transactionReferenceId')
			->where('ledgerDetailsId',$ledger_id)
			->where('transactionType',$type)
			->get();
		if($q->num_rows()==1){
			return $q->row();
		}
		else{
			return FALSE;
		}
	}
	public function get_all_ledger_name_id(){
		$q=$this->db
			->select('ledger_info.ledgerId,ledger_info.ledgerTitle,ledger_info.ledgerType')
			->from('ledger_info')
			->where('ledgerStatus',1)
			->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}	
	public function get_all_income_ledger_name_id(){
		$q=$this->db
			->select('ledger_info.ledgerId,ledger_info.ledgerTitle,ledger_info.ledgerType')
			->from('ledger_info')
			->where('ledgerStatus',1)
			->where('ledgerType',1)
			->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}	
	public function get_all_expence_ledger_name_id(){
		$q=$this->db
			->select('ledger_info.ledgerId,ledger_info.ledgerTitle,ledger_info.ledgerType')
			->from('ledger_info')
			->where('ledgerStatus',1)
			->where('ledgerType',2)
			->get();

		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}	
	public function get_ledger_details_by_id($ledgerId){
		$q=$this->db
			->select('ledger_info.*,particular_info.particularTitle')
			->from('ledger_info')
			->join('particular_info','ledger_info.particular_info_particularid=particular_info.particularid')
			->where('ledgerId',$ledgerId)
			->get();

		if($q->num_rows()==1){
			return $q->row();
		}
		else{
			return FALSE;
		}
	}	
	public function get_ledger_title_by_id($ledger_id){
		$q=$this->db
		->select('ledger_info.ledgerTitle')
		->from('ledger_info')
		->where('ledgerId',$ledger_id)
		->get();

		if($q->num_rows()==1){
			return $q->row()->ledgerTitle;
		}
		else{
			return FALSE;
		}
	}	
	public function store_ledger_entry($data){
		$this->db->trans_start();
		$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
		$date= $dt->format('Y-m-d H:i:s');
		$data['ledgerDetailsAddedDate']=$date;
		$type=$data['ledgerDetailsType'];
		$source=$data['ledgerDepositeTo'];
		$this->db->insert('ledger_details_info', $data );
		$ledger_details=$this->db->insert_id();
		$ledgerTitle=$this->admin_model->get_ledger_title_by_id($data['ledger_info_ledgerId']);
		if($ledger_details){
			if($type==1){
				if($source==0){
					/* insert in Petty Cash table */
					$pettyCash['pettyCashDate']=$data['ledgerDetailsAddedDate'];
					$pettyCash['pettyCashEntryDate']=$data['ledgerDetailsAddedDate'];
					$pettyCash['pettyCashto']=$ledgerTitle;
					$pettyCash['pettyCashType']=1;
					$pettyCash['pettyCashPurpose']=3;
					$pettyCash['pettyCashAmount']=$data['ledgerDetailsAmount'];
					$pettyCash['pettyCashReferenceId']=$ledger_details;
					$this->db->insert('petty_cash',$pettyCash);
					$transaction['transactionTo']="Petty Cash";
				}
				else{
					$this->db
						->where('bankAccountId',$source)
						->set('bankAccountLastBalance', 'bankAccountLastBalance + ' .  $data['ledgerDetailsAmount'], FALSE)
						->update('bank_account_info');
					$bankIn['bank_account_infoBankId']=$source;
					$bankIn['bankDetailsReferrenceId']=$ledger_details;
					$bankIn['bankDetailsEntryDate']=$data['ledgerDetailsAddedDate'];
					$bankIn['bankDetailsTransactionDate']=$data['ledgerDetailsAddedDate'];
					$bankIn['bankDetailsNote']=$data['ledgerDetails'];
					$bankIn['bankDetailsDeposit']=$data['ledger_info_ledgerId'];
					$bankIn['bankDetailsBalance']=$data['ledgerDetailsAmount'];
					$bankIn['bankDetailsPurpose']=3;
					$bankIn['bankDetailsType']=1;
					$this->db->insert('bank_account_details',$bankIn);
					$transaction['transactionTo']=$this->admin_model->get_bank_account_name_by_id($source);
				}
				/* insert in All Transaction table */
				
				$transaction['transactionFrom']=$ledgerTitle;
				$transaction['transactionType']=3;
				$transaction['transactionDate']=$data['ledgerDetailsAddedDate'];
				$transaction['transactionEntryDate']=$data['ledgerDetailsAddedDate'];
				$transaction['transactionDetails']=$data['ledgerDetails'];
				$transaction['transactionAmount']=$data['ledgerDetailsAmount'];
				$transaction['transactionReferenceId']=$ledger_details;
				$this->db->insert('all_transaction_info',$transaction);
			}
			else{
				if($source==0){
					/* insert in Petty Cash table */
					$pettyCash['pettyCashDate']=$data['ledgerDetailsAddedDate'];
					$pettyCash['pettyCashEntryDate']=$data['ledgerDetailsAddedDate'];
					$pettyCash['pettyCashto']=$ledgerTitle;
					/*else{
						$pettyCash['pettyCashto']=$this->admin_model->get_bank_account_name_by_id($data['cashWithdrawnFrom']);
					}*/
					$pettyCash['pettyCashType']=2;
					$pettyCash['pettyCashPurpose']=4;
					$pettyCash['pettyCashAmount']=$data['ledgerDetailsAmount'];
					$pettyCash['pettyCashReferenceId']=$ledger_details;
					$this->db->insert('petty_cash',$pettyCash);
					$transaction['transactionFrom']="Petty Cash";
				}
				else{
					$this->db
						->where('bankAccountId',$source)
						->set('bankAccountLastBalance', 'bankAccountLastBalance - ' .  $data['ledgerDetailsAmount'], FALSE)
						->update('bank_account_info');
					$bankIn['bank_account_infoBankId']=$source;
					$bankIn['bankDetailsReferrenceId']=$ledger_details;
					$bankIn['bankDetailsEntryDate']=$data['ledgerDetailsAddedDate'];
					$bankIn['bankDetailsTransactionDate']=$data['ledgerDetailsAddedDate'];
					$bankIn['bankDetailsNote']=$data['ledgerDetails'];
					$bankIn['bankDetailsDeposit']=$data['ledger_info_ledgerId'];
					$bankIn['bankDetailsBalance']=$data['ledgerDetailsAmount'];
					$bankIn['bankDetailsPurpose']=4;
					$bankIn['bankDetailsType']=2;
					$this->db->insert('bank_account_details',$bankIn);
					$transaction['transactionFrom']=$this->admin_model->get_bank_account_name_by_id($source);
					
				}
				/* insert in All Transaction table */
				
				$transaction['transactionTo']=$ledgerTitle;
				$transaction['transactionType']=4;
				$transaction['transactionDate']=$data['ledgerDetailsAddedDate'];
				$transaction['transactionEntryDate']=$data['ledgerDetailsAddedDate'];
				$transaction['transactionDetails']=$data['ledgerDetails'];
				$transaction['transactionAmount']=$data['ledgerDetailsAmount'];
				$transaction['transactionReferenceId']=$ledger_details;
				$this->db->insert('all_transaction_info',$transaction);
			}
		}
		
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE){
			return false;
		}	
		else return true;
	}
}