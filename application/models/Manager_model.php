﻿<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manager_model extends CI_Model {

	
	public function my_info($user_id){
        $q=$this->db
            ->select('*')
            ->from('admin_info')
            ->where('adminID',$user_id)
            ->where('adminStatus', 1)
            ->get();
        if($q->num_rows()==1){
			return $q->row();
		}
        else{
            return FALSE;
        }
    }

	public function update_contact($admin_id,$data){
		return $this->db
			        ->where('adminID',$admin_id)
				    ->update('admin_info',$data);
	}
	
	public function change_password($data,$admin_id){
		return $this->db
			        ->where('adminID',$admin_id)
				    ->update('admin_info',$data);
	}
	

	public function get_view_product_info($limit,$offset)
	{
		$q=$this->db
				->select('product_info.productID,product_info.productBarcode,product_info.productName,product_info.productQuantity,product_info.productSaleCounter,product_info.productAddedDate,product_group_info.groupName,supplier_info.supplierCompanyName')
				->from('product_info')
				->join('product_group_info','product_info.group_info_productGroupID=product_group_info.groupID')
				->join('supplier_info','product_info.supplier_info_productSupplierID=supplier_info.supplierID')
				->order_by("productID","desc")
				->limit($limit,$offset)
				->get();
				
		if($q->num_rows()){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	
	public function get_product_details_by_id($product_id)
	{
		$q=$this->db
				->select('product_info.*,product_group_info.groupName,supplier_info.supplierCompanyName')
				->from('product_info')
				->join('product_group_info','product_info.group_info_productGroupID=product_group_info.groupID')
				->join('supplier_info','product_info.supplier_info_productSupplierID=supplier_info.supplierID')
				->where('productID',$product_id)
				->get();
				
		if($q->num_rows()==1){
			return $q->row();
		}
		else{
			return FALSE;
		}
	}
	
	public function get_product_details_by_barcode($barcode)
	{
		$q=$this->db
				->select('product_info.productID,product_info.productName,product_info.productBarcode,product_info.productSalePrice,product_info.productQuantity')
				->from('product_info')
				->where('productBarcode',$barcode)
				->get();
				
		if($q->num_rows()==1){
			return $q->row();
		}
		else{
			return FALSE;
		}
	}
	public function get_all_salesman_id_name()
	{
		$q=$this->db
				->select('salesman_info.salesmanID,salesman_info.salesmanName')
				->from('salesman_info')
				->where('salesmanStatus',1)
				->get();
				
		if($q->num_rows()){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	
	public function store_bill($data,$cart)
	{
		
		$this->db->insert('sale_info', $data );
		$sale_id= $this->db->insert_id();
		foreach ($cart as $product) {

            /*if($product==null){
                contiune;
            }*/

            $products=array(
                'product_info_saleProductID'=>$product['id'],
                'saleProductQuantity'=>$product['qty'],
                'salePrice'=>$product['price'],
                'sale_info_saleID'=>$sale_id,
                'shop_info_shopID'=>$data['shop_info_saleShopID']

            );
            //print_r( $products); exit;
			$this->db
			    ->where('productID',$products['product_info_saleProductID'])
				->set('productQuantity', 'productQuantity - ' . (int) $products['saleProductQuantity'], FALSE)
				->set('productSaleCounter', 'productSaleCounter + ' .(int) $products['saleProductQuantity'], FALSE)
				->update('product_info');
            $this->db->insert('sale_details', $products );
        }
			$transection['transectionType']=1;
			$transection['transectionReferenceID']=$sale_id;
			$transection['transectionDetails']="Sale";
			$transection['transectionBy']=$data['entryBy'];
			$transection['transectionDate']=$data['saleDate'];
			$transection['shop_info_transectionShopID']=$data['shop_info_saleShopID'];
			$transection['transectionTotalAmount']=$data['saleTotalAmount'];
			$this->db->insert('transection_info', $transection );
		
		if($sale_id)
		{
			return $sale_id;
		}
		else return false;
	}
	
	public function get_sale_info_by_id($sale_id,$saleman)
	{
		$this->db
				//->select('sale_info.*,shop_info.shopTitle,salesman_info.salesmanName')
				->select('sale_info.*,shop_info.shopTitle')
				->from('sale_info')
				->join('shop_info','sale_info.shop_info_saleShopID=shop_info.shopID');
				if($saleman>0){
				$this->db->select('salesman_info.salesmanName')
						->join('salesman_info','sale_info.salesman_info_salesmanID=salesman_info.salesmanID');
				}
				//->group_by('sale_details.product_info_saleProductID')
				//->where('sale_details.product_info_saleProductID',$product_id)
				//->where('sale_details.shop_info_shopID',$shop_id)
				$this->db->where('saleID',$sale_id);
			$q=$this->db->get();
				
		if($q->num_rows()==1){
			return $q->row();
		}
		else{
			return FALSE;
		}
	}
	public function get_sale_info_details_by_id($sale_id)
	{
		$q=$this->db
				//->select('sale_info.*,shop_info.shopTitle,salesman_info.salesmanName')
				->select('sale_details.*,product_info.productName')
				->from('sale_details')
				->join('product_info','sale_details.product_info_saleProductID=product_info.productID')
				->where('sale_info_saleID',$sale_id)
			    ->get();
				
		if($q->num_rows()){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	
	public function get_all_expense_field_id_name()
	{
		$q=$this->db
				->select('expense_field_info.expenseFieldID,expense_field_info.expenseFieldName')
				->from('expense_field_info')
				->where('expenseFieldStatus',1)
				->get();
				
		if($q->num_rows()){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	public function store_expense($data)
	{
		
		if($this->db->insert('expense_info', $data )){
			$expense_id= $this->db->insert_id();
			$transection['transectionType']=2;
			$transection['transectionReferenceID']=$expense_id;
			$transection['transectionDetails']=$data['expenceReference'];
			$transection['transectionBy']=$data['expenceEntryBy'];
			$transection['transectionDate']=$data['expenceDate'];
			$transection['shop_info_transectionShopID']=$data['shop_info_ExpenceShopID'];
			$transection['transectionTotalAmount']=$data['expenceAmount'];
			return $this->db->insert('transection_info', $transection );
		}
		else return false;
	}
	public function get_sale_report($limit,$offset)
	{
		$shop_id=$this->session->userdata('userShop');
		$q=$this->db
				//->select('sale_details.*,sale_info.saleDate,product_info.productName,product_info.productBarcode,SUM(saleProductQuantity) as saleProductQuantityTotal')
				->select('sale_details.*,sale_info.saleDate,product_info.productName,product_info.productBarcode')
				->from('sale_details')
				->join('sale_info','sale_details.sale_info_saleID=sale_info.saleID')
				->join('product_info','product_info.productID=sale_details.product_info_saleProductID')
				//->group_by('sale_details.product_info_saleProductID')
				->where('sale_details.shop_info_shopID',$shop_id)
				->order_by("sale_info.saleDate", "desc")
				->limit($limit,$offset)
				->get();
				
		if($q->num_rows()){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	public function get_sale_details_by_id($product_id)
	{
		$shop_id=$this->session->userdata('userShop');
		$q=$this->db
				//->select('sale_details.*,sale_info.saleDate,product_info.productName,product_info.productBarcode,SUM(saleProductQuantity) as saleProductQuantityTotal')
				->select('sale_details.*,sale_info.saleDate,product_info.productName,product_info.productBarcode')
				->from('sale_details')
				->join('sale_info','sale_details.sale_info_saleID=sale_info.saleID')
				->join('product_info','product_info.productID=sale_details.product_info_saleProductID')
				//->group_by('sale_details.product_info_saleProductID')
				->where('sale_details.product_info_saleProductID',$product_id)
				->where('sale_details.shop_info_shopID',$shop_id)
				->get();
				
		if($q->num_rows()){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	
	public function get_sale_report_by_dates_and_shop_id($startDate,$endDate)
	{
		$shop_id=$this->session->userdata('userShop');
		$q=$this->db
				->select('sale_details.*,sale_info.saleDate,product_info.productName,product_info.productBarcode')
				->from('sale_details')
				->join('sale_info','sale_details.sale_info_saleID=sale_info.saleID')
				->join('product_info','product_info.productID=sale_details.product_info_saleProductID')
				//->group_by('sale_details.product_info_saleProductID')
				->where('sale_details.shop_info_shopID',$shop_id)
				->where('sale_info.saleDate  >=',$startDate)
				->where('sale_info.saleDate <=',$endDate)
				//->limit($limit,$offset)
				->order_by("sale_info.saleDate", "desc")
				->get();
				
		if($q->num_rows()){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	
	public function get_all_transection_by_shop_id($limit,$offset,$shop_id)
	{
		
		$q=$this->db
				->select('*')
				->from('transection_info')
				->where('shop_info_transectionShopID',$shop_id)
				->order_by("transectionDate","desc")
				->limit($limit,$offset)
				->get();
				
		if($q->num_rows()){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	/*public function get_transection_by_dates_and_shop_id($limit,$offset,$startDate,$endDate,$shop_id)*/
	public function get_transection_by_dates_and_shop_id($startDate,$endDate,$shop_id)
	{
		$q=$this->db
				->select('*')
				->from('transection_info')
				->where('transectionDate >=',$startDate)
				->where('transectionDate <=',$endDate)
				->where('shop_info_transectionShopID',$shop_id)
				->order_by("transectionDate","desc")
				//->limit($limit,$offset)
				->get();
				
		if($q->num_rows()){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	public function store_return_info($data)
	{
		$data['returnTotalPrice']=$data['returnProductQuantity']*$data['returnProductPrice'];
		$data['newTotalPrice']=$data['newProductQuantity']*$data['newProductPrice'];
		$data['returnProductID']=$data['returnProductID']-1000;
		$data['newProductID']=$data['newProductID']-1000;
		//print_r($data);exit;
		$this->db->insert('exchange_info', $data );
		$return_id= $this->db->insert_id();
		
		$transection['transectionType']=3;
		$transection['transectionReferenceID']=$return_id;
		$transection['transectionDetails']="Exchange";
		$transection['transectionBy']=$data['returnReceivedBy'];
		$transection['transectionDate']=$data['returnDate'];
		$transection['shop_info_transectionShopID']=$data['shop_info_returnShopID'];
		$transection['transectionTotalAmount']=$data['newTotalPrice']-$data['returnTotalPrice'];
		$this->db->insert('transection_info', $transection );
		
		$sale_info['saleTotalAmount']=$data['newTotalPrice']-$data['returnTotalPrice'];
		$sale_info['saleDate']=$data['returnDate'];
		$sale_info['saleType']=2;
		$sale_info['entryBy']=$data['returnReceivedBy'];
		$sale_info['shop_info_saleShopID']=$data['shop_info_returnShopID'];
		$this->db->insert('sale_info', $sale_info );
		$sale_id= $this->db->insert_id();
		
		$product['sale_info_saleID']=$sale_id;
		$product['shop_info_shopID']=$data['shop_info_returnShopID'];
		$product['product_info_saleProductID']=$data['newProductID'];
		$product['saleProductQuantity']=$data['newProductQuantity'];
		$product['salePrice']=$data['newProductPrice'];
		//$product['']=;
		$this->db
			 ->where('productID',$data['newProductID'])
			 ->set('productQuantity', 'productQuantity - ' . (int) $data['newProductQuantity'], FALSE)
			 ->set('productSaleCounter', 'productSaleCounter + ' .(int) $data['newProductQuantity'], FALSE)
			 ->update('product_info');
        $this->db
			 ->where('productID',$data['returnProductID'])
			 ->set('productQuantity', 'productQuantity + ' . (int) $data['returnProductQuantity'], FALSE)
			 ->set('productSaleCounter', 'productSaleCounter - ' .(int) $data['returnProductQuantity'], FALSE)
			 ->update('product_info');
        return $this->db->insert('sale_details', $product);
		
		
	}
	
	public function get_cash_amount(){
		$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
		$date= $dt->format('Y-m-d 00:00:00');
		$shop_id=$this->session->userdata('userShop');
		$this->db->select_sum('transectionTotalAmount');
		$this->db->from('transection_info');
		$this->db->where('shop_info_transectionShopID',$shop_id);
		$this->db->where('transectionType',1);
		$this->db->where('transectionDate>=',$date);
		$query = $this->db->get();
		$income= $query->row()->transectionTotalAmount;
		//$shop_id=$this->session->userdata('userShop');
		$this->db->select_sum('transectionTotalAmount');
		$this->db->from('transection_info');
		$this->db->where('shop_info_transectionShopID',$shop_id);
		$this->db->where('transectionType',3);
		$this->db->where('transectionDate>=',$date);
		$query = $this->db->get();
		$return= $query->row()->transectionTotalAmount;
		$income=$income+$return;
		$this->db->select_sum('transectionTotalAmount');
		$this->db->from('transection_info');
		$this->db->where('transectionDate >=',$date);
		$this->db->where('shop_info_transectionShopID',$shop_id);
		$this->db->where('transectionType',2);
		$query = $this->db->get();
		$expense= $query->row()->transectionTotalAmount;
		return $income-$expense;
	}
	
	public function total_income(){
		$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
		$date= $dt->format('Y-m-d 00:00:00');
		$shop_id=$this->session->userdata('userShop');
		$this->db->select_sum('transectionTotalAmount');
		$this->db->from('transection_info');
		$this->db->where('shop_info_transectionShopID',$shop_id);
		$this->db->where('transectionDate>=',$date);
		$this->db->where('transectionType',1);
		$query = $this->db->get();
		$income= $query->row()->transectionTotalAmount;
		return $income;
	}
	
	public function total_expense(){
		$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
		$date= $dt->format('Y-m-d 00:00:00');
		$shop_id=$this->session->userdata('userShop');
		$this->db->select_sum('transectionTotalAmount');
		$this->db->from('transection_info');
		$this->db->where('transectionType',2);
		$this->db->where('shop_info_transectionShopID',$shop_id);
		$this->db->where('transectionDate>=',$date);
		$query = $this->db->get();
		$expense= $query->row()->transectionTotalAmount;
		return $expense;
	}
	
	public function total_return(){
		$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
		$date= $dt->format('Y-m-d 00:00:00');
		$shop_id=$this->session->userdata('userShop');
		$this->db->select_sum('transectionTotalAmount');
		$this->db->from('transection_info');
		$this->db->where('transectionType',3);
		$this->db->where('shop_info_transectionShopID',$shop_id);
		$this->db->where('transectionDate>=',$date);
		$query = $this->db->get();
		$return= $query->row()->transectionTotalAmount;
		return $return;
	}
	public function total_income_by_dates($startDate,$endDate){
		$shop_id=$this->session->userdata('userShop');
		$this->db->select_sum('transectionTotalAmount');
		$this->db->from('transection_info');
		$this->db->where('transectionDate >=',$startDate);
		$this->db->where('transectionDate <=',$endDate);
		$this->db->where('shop_info_transectionShopID',$shop_id);
		$this->db->where('transectionType=1');
		$query = $this->db->get();
		$income= $query->row()->transectionTotalAmount;
		return $income;
	}
	
	public function total_expense_by_dates($startDate,$endDate){
		$shop_id=$this->session->userdata('userShop');
		$this->db->select_sum('transectionTotalAmount');
		$this->db->from('transection_info');
		$this->db->where('transectionDate >=',$startDate);
		$this->db->where('transectionDate <=',$endDate);
		$this->db->where('shop_info_transectionShopID',$shop_id);
		$this->db->where('transectionType=2');
		$query = $this->db->get();
		$expense= $query->row()->transectionTotalAmount;
		return $expense;
	}
	
	public function total_return_by_dates($startDate,$endDate){
		$shop_id=$this->session->userdata('userShop');
		$this->db->select_sum('transectionTotalAmount');
		$this->db->from('transection_info');
		$this->db->where('transectionDate >=',$startDate);
		$this->db->where('transectionDate <=',$endDate);
		$this->db->where('shop_info_transectionShopID',$shop_id);
		$this->db->where('transectionType=3');
		$query = $this->db->get();
		$return= $query->row()->transectionTotalAmount;
		return $return;
	}
	
	
	public function get_return_report_by_dates($startDate,$endDate)
	{
		$shop_id=$this->session->userdata('userShop');
		$q=$this->db
				->select('exchange_info.*,product_info.productName')
				->from('exchange_info')
				->join('product_info','exchange_info.returnProductID=product_info.productID')
				->where('returnDate >=',$startDate)
				->where('returnDate <=',$endDate)
				->where('shop_info_returnShopID',$shop_id)
				//->limit($limit,$offset)
				->order_by("returnDate", "desc")
				->get();
				
		if($q->num_rows()){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	public function get_return_report($limit,$offset)
	{
		$shop_id=$this->session->userdata('userShop');
		$q=$this->db
				->select('exchange_info.*,product_info.productName')
				->from('exchange_info')
				->join('product_info','exchange_info.returnProductID=product_info.productID')
				->where('shop_info_returnShopID',$shop_id)
				->limit($limit,$offset)
				->order_by("returnDate", "desc")
				->get();
				
		if($q->num_rows()){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	
	public function get_salesman_report_by_dates($startDate,$endDate)
	{
		$shop_id=$this->session->userdata('userShop');
		$q=$this->db
				->select('sale_info.*,salesman_info.salesmanName')
				->from('sale_info')
				->join('salesman_info','sale_info.salesman_info_salesmanID=salesman_info.salesmanID')
				->where('saleDate >=',$startDate)
				->where('saleDate <=',$endDate)
				->where('shop_info_saleShopID',$shop_id)
				->order_by("sale_info.saleDate", "desc")
				//->limit($limit,$offset)
				->get();
				
		if($q->num_rows()){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	public function get_salesman_report($limit,$offset)
	{
		$shop_id=$this->session->userdata('userShop');
		$q=$this->db
				->select('sale_info.*,salesman_info.salesmanName')
				->from('sale_info')
				->join('salesman_info','sale_info.salesman_info_salesmanID=salesman_info.salesmanID')
				->where('sale_info.salesman_info_salesmanID>',0)
				->where('shop_info_saleShopID',$shop_id)
				->order_by("sale_info.saleDate", "desc")
				->limit($limit,$offset)
				->get();
				
		if($q->num_rows()){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	
	public function get_return_details_by_id($return_id)
	{
		$q=$this->db
				->select('exchange_info.*,product_info.productName,product_info.productBarcode,')
				->from('exchange_info')
				->join('product_info','exchange_info.returnProductID=product_info.productID')
				->where('exchangeID',$return_id)
				->get();
				
		if($q->num_rows()==1){
			return $q->row();
		}
		else{
			return FALSE;
		}
	}
	public function get_return_exchange_details_by_id($return_id)
	{
		$q=$this->db
				->select('exchange_info.*,product_info.productName,product_info.productBarcode,')
				->from('exchange_info')
				->join('product_info','exchange_info.newProductID=product_info.productID')
				->where('exchangeID',$return_id)
				->get();
				
		if($q->num_rows()==1){
			return $q->row();
		}
		else{
			return FALSE;
		}
	}
	public function get_salesman_id_for_invoice_by_sale_id($ref_id)
	{
		$q=$this->db
				->select('sale_info.salesman_info_salesmanID')
				->from('sale_info')
				->where('saleID',$ref_id)
				->get();
				
		if($q->num_rows()==1){
			return $q->row()->salesman_info_salesmanID;
		}
		else{
			return FALSE;
		}
	}
	public function get_expense_details_by_id($expense_id)
	{
		$q=$this->db
				->select('expense_info.*,transection_info.transectionID,admin_info.adminName,expense_field_info.expenseFieldName')
				->from('expense_info')
				->join('transection_info','expense_info.expenseID=transection_info.transectionReferenceID')
				->join('admin_info','expense_info.expenceEntryBy=admin_info.adminID')
				->join('expense_field_info','expense_info.expenceFieldID=expense_field_info.expenseFieldID')
				->where('expenseID',$expense_id)
				->get();
				
		if($q->num_rows()==1){
			return $q->row();
		}
		else{
			return FALSE;
		}
	}
	
	public function get_all_shop_id_name()
	{
		
		$q=$this->db
				->select('*')
				->from('shop_info')
				->where('shopStatus',1)
				->get();
				
		if($q->num_rows()){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	
	public function all_salesman($limit,$offset)
	{
		
		$q=$this->db
				->select('*,shop_info.shopTitle')
				->from('salesman_info')
				->join('shop_info','shop_info.shopID=salesman_info.shop_info_shopID')
				//->where('admin_role_roleID',3)
				->limit($limit,$offset)
				->get();
				
		if($q->num_rows()){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	public function add_sales_man($data)
	{
		return $this->db->insert('salesman_info', $data );
	}
	public function get_selaman_details_by_id($admin_id)
	{
		$q=$this->db
				->select('*,shop_info.shopTitle')
				->from('salesman_info')
				->join('shop_info','salesman_info.shop_info_shopID=shop_info.shopID')
				->where('salesmanID',$admin_id)
				->get();
				
		if($q->num_rows()==1){
			return $q->row();
		}
		else{
			return FALSE;
		}
	}
	public function update_saleman($data,$admin_id)
	{
		return $this->db
					->where('salesmanID',$admin_id)
					->update('salesman_info',$data);
		
	}
}