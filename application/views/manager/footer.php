		
		
		
		</div>
    </div>

    <!-- jQuery -->
	<script src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/bootstrap-datepicker.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/bootstrap-timepicker.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/select2.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/table.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/metisMenu.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
	<script type="text/javascript">
		$('.forselect2').select2();
	</script>
	
	<script type="text/javascript">
		// Ajax post
		$(document).ready(function() {
			$(".submit").click(function(event) {
				event.preventDefault();
				var productBarcode = $("input#productBarcode").val();
				var quantity = $("input#quantity").val();
				//alert(productBarcode);
				if(productBarcode){
					jQuery.ajax({
						type: "POST",
						url: "<?php echo base_url(); ?>" + "autocomplete/ajax_add_cart",
						dataType: 'json',
						data: {
							productBarcode: productBarcode,
							quantity: quantity
						},
						success: function(res) {
							if (res) {
								
								if (res.status === true) {
									//alert("yes");
									$("#tableHeaderID").show();
									document.getElementById("myForm").reset();
									if(res.past_buy === true){
										var product_id = res.product_id;
										var quantity = res.quantity;
										var price = res.price;
										var subtotal = price*quantity;
										var qid="qty"+product_id;
										var subid="subtotal_row"+product_id;
										var old_qty=$("#"+qid).val();
										var old_subtotal=old_qty*price;
										var old_totalQty=$("#totalQuantityFieldID").val();
										old_totalQty=(+old_totalQty)+(+quantity);
										old_totalQty=(+old_totalQty)-(+old_qty);
										
										//* for total quantity *//
										$("#totalQuantityFieldID").val(old_totalQty);
										$("#"+qid).val(quantity);
										$("#"+subid).html(subtotal);
										
										//* for total price *//
										var old_totalPrice=$("#totalPriceFieldID").val();
										old_totalPrice=(+old_totalPrice)-(+old_subtotal);
										old_totalPrice=(+old_totalPrice)+(+subtotal);
										$("#totalPriceFieldID").val(old_totalPrice);
										$("#netPayable").val(old_totalPrice);
										
									}
									else{
										
										var product_id = res.product_id;
										var quantity = res.quantity;
										var total_quantity = res.total_quantity;
										var price = res.price;
										var name = res.name;
										var barcode = res.barcode;
										var rowid = res.rowid;
										var subtotal = price*quantity;
										//alert(barcode);
										var table = document.getElementById("productTable");
										var table_len = (table.rows.length);
										var row = table.insertRow(table_len).outerHTML = "<tr id='row" + product_id + "' class='active'><td id='barcode_row" + table_len + "'>" + barcode + "</td><td id='name_row" + table_len + "'>" + name + "</td><td id='quantity_row" + table_len + "'><div class='quantity clearfix'> <button type='button' class='minus btn btn-sm btn-info' onclick='minusQuantity(\""+ rowid + "\","+product_id+","+price+")'><i class='fa fa-minus'></i></button> <input type='number' name='quantity' value='"+quantity+"' class='quantity' id='qty" + product_id + "' readonly='' /> <button type='button' class='plus btn btn-sm btn-info' onclick='plusQuantity(\""+ rowid + "\","+total_quantity+","+product_id+","+price+")'><i class='fa fa-plus'></i></button></div></td><td id='price_row" + product_id + "'>" + price + "</td><td id='subtotal_row" + product_id + "'>" + subtotal + "</td><td id='remove_row" + table_len + "' style='width:5%' ><a id='my_cart_table_close' onclick='delete_row(\""+ rowid + "\","+product_id+","+price+")' ><i class='fa fa-remove'></i></a></td></tr>";
										//* for total quantity *//
										var old_totalQty=$("#totalQuantityFieldID").val();
										old_totalQty=(+old_totalQty)+(+quantity);
										$("#totalQuantityFieldID").val(old_totalQty);
										//* for total price *//
										var old_totalPrice=$("#totalPriceFieldID").val();
										old_totalPrice=(+old_totalPrice)+(+subtotal);
										$("#totalPriceFieldID").val(old_totalPrice);
										$("#netPayable").val(old_totalPrice);
	
									}
									
								}
								else {
									alert("NO");
									
								}
							}
						}
					});
				}
			});
		});
	</script>
	<script type="text/javascript">// Plus Quantity
			function plusQuantity(row_id,total_qty,len,price){
				//alert(total_qty);
				
				var qid="qty"+len;
				var subid="subtotal_row"+len;
				var input_val = $("#"+qid).val();
				input_val=++input_val;
				var sub_total=input_val*price;
				  //alert(sub_total);
				 // alert(input_val);
				if(total_qty>=input_val){
					//event.preventDefault();
					jQuery.ajax({
						type: "POST",
						url: "<?php echo base_url(); ?>" + "autocomplete/plus_product",
						dataType: 'json',
						data: {
							row_id: row_id,
							qty: input_val,
						},
						success: function(res) {
							if (res) {
								// Show Entered Value
								if (res.status === true) {
									//alert(sub_total);
									$("#"+qid).val(input_val);
									$("#"+subid).html(sub_total);
									var old_totalQty=$("#totalQuantityFieldID").val();
									old_totalQty=++old_totalQty;
									$("#totalQuantityFieldID").val(old_totalQty);
									//* for total price *//
									var old_totalPrice=$("#totalPriceFieldID").val();
									//alert(price);
									old_totalPrice=(+old_totalPrice)+(+price);
									$("#totalPriceFieldID").val(old_totalPrice);
									$("#netPayable").val(old_totalPrice);
									
									
								} else {
									//jQuery("#quantity_errors").show();
									//jQuery("#quantity_errors").html(res.errors);
								}
							}
						}
					});
					
				}
			}
	</script>
	<script type="text/javascript">// Plus Quantity
			function minusQuantity(row_id,len,price){
				//event.preventDefault();
				var qid="qty"+len;
				var subid="subtotal_row"+len;
				var input_val = $("#"+qid).val();
				input_val=--input_val;
				var sub_total=input_val*price;
				if(input_val>0){
					jQuery.ajax({
						type: "POST",
						url: "<?php echo base_url(); ?>" + "autocomplete/minus_product",
						dataType: 'json',
						data: {
							row_id: row_id,
							qty: input_val,
						},
						success: function(res) {
							if (res) {
								// Show Entered Value
								if (res.status === true) {
									$("#"+qid).val(input_val);
									$("#"+subid).html(sub_total);
									var old_totalQty=$("#totalQuantityFieldID").val();
									old_totalQty=--old_totalQty;
									$("#totalQuantityFieldID").val(old_totalQty);
									//* for total price *//
									var old_totalPrice=$("#totalPriceFieldID").val();
									old_totalPrice=(+old_totalPrice)-(+price);
									$("#totalPriceFieldID").val(old_totalPrice);
									$("#netPayable").val(old_totalPrice);
									
								} else {
									//jQuery("#quantity_errors").show();
									//jQuery("#quantity_errors").html(res.errors);
								}
							}
						}
					});
					
				}
			}
	</script>
	<script type="text/javascript">
		function delete_row(row_id,no,price) {
			//event.preventDefault();
			//alert(discount);
			jQuery.ajax({
				type: "POST",
				url: "<?php echo base_url(); ?>" + "autocomplete/remove",
				dataType: 'json',
				data: {
					row_id: row_id,
					//quantity: quantity,
				},
				success: function(res) {
					if (res) {
						// Show Entered Value
						if (res.status === true) {
							
							var qid="qty"+no;
							var input_qty = $("#"+qid).val();
							//alert(input_qty);
							var sub_total=(+input_qty)*price;
							//var totalPrice=$("#totalInputVal").val();
							//totalPrice=(+totalPrice)-(+sub_total);
							//$("#totalInputVal").val(totalPrice);
							//$(".totalAmountShow").html("Total Amount: "+totalPrice+" ৳");
							var old_totalQty=$("#totalQuantityFieldID").val();
							old_totalQty=(+old_totalQty)-(+input_qty);
							$("#totalQuantityFieldID").val(old_totalQty);
							//* for total price *//
							var old_totalPrice=$("#totalPriceFieldID").val();
							old_totalPrice=(+old_totalPrice)-(+sub_total);
							$("#totalPriceFieldID").val(old_totalPrice);
							$("#netPayable").val(old_totalPrice);
							document.getElementById("row" + no + "").outerHTML = "";
							var table = document.getElementById("productTable");
							var table_len = (table.rows.length);
							//alert(table_len);
							  //document.getElementById("productTable").outerHTML = "";
							if(table_len==1){
								$("#tableHeaderID").hide();
								//$(".paymentButtonID").hide();
								//$("#emptyCartID").show();
								
							}
							
						} else {
							//jQuery("#quantity_errors").show();
							//jQuery("#quantity_errors").html(res.errors);
						}
					}
				}
			});
			
		}
	</script>
	<script type="text/javascript">
		function clearCart() {
			//event.preventDefault();
			//alert("hi");
			jQuery.ajax({
				type: "POST",
				url: "<?php echo base_url(); ?>" + "autocomplete/destroy",
				dataType: 'json',
				data: {
					//row_id: row_id,
					//quantity: quantity,
				},
				success: function(res) {
					if (res) {
						// Show Entered Value
						if (res.status === true) {
							//document.getElementById("row" + no + "").outerHTML = "";
							/*document.getElementById("productTable")
							.outerHTML = "";*/
							var table = document.getElementById("productTable");
							var table_len = (table.rows.length)-1;
							$("table tr").slice(-table_len).remove();
							$("#tableHeaderID").hide();
							$("#totalQuantityFieldID").val(0);
							//* for total price *//
							$("#totalPriceFieldID").val(0);
							$("#netPayable").val(0);
							//$(".paymentButtonID").hide();
							//$("#emptyCartID").show();
							//$("#totalDiscountInputVal").val(0);
							//$("#totalInputVal").val(0);
							//$(".totalAmountShow").html("Total Amount: 0 ?");
							
							
						} else {
							//jQuery("#quantity_errors").show();
							//jQuery("#quantity_errors").html(res.errors);
						}
					}
				}
			});			
		}
	</script>
	<script>
		$(document).ready(function(){
			$("#totalDiscount").change(function(event){
				//alert("The text has been changed.");
				var discount=$("#totalDiscount").val();
				var Tprice=$("#totalPriceFieldID").val();
				Tprice=(+Tprice)-(+discount);
				$("#netPayable").val(Tprice);
				
			});
		});
	</script>
	
	<script type="text/javascript">
		// incomeExpenseReport show
		$(document).ready(function() {
			$("#incomeExpenseReport").click(function(event) {
				alert("YEssssssss");
				event.preventDefault();
				var startDate = $("input#incomeStartDate").val();
				var startTime = $("input#incomeStartTime").val();
				var endDate = $("input#incomeEndDate").val();
				var endTime = $("input#incomeEndTime").val();
				//alert(productBarcode);
				alert("yes1");
					jQuery.ajax({
						type: "POST",
						url: "<?php echo base_url(); ?>" + "autocomplete/ajax_incomeExpenseReport",
						dataType: 'json',
						data: {
							startDate: startDate,
							startTime: startTime,
							endDate: endDate,
							endTime: endTime
						},
						success: function(res) {
							if (res) {
								
								if (res.status === true) {
									alert("yes");
									
									
								}
								else {
									alert("NO");
									
								}
							}
						}
					});
			});
		});
	</script>

</body>

</html>
