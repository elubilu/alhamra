<?php include('header.php') ?>
<?php 
		$totalProductQty=0;
		$totalProductPrice=0;
?> 
<div class="row">
   <div class="col-lg-12">
      <h3 class="page-header">Add Bill / Memo</h3>
   </div>
</div>
<!--<div class="row">
   <div class="col-md-12">
      <div class="panel panel-info">
         <div class="panel-body">
            <div class="row">
               <div class="col-lg-6">
                  <div class="form-group">
                     <label>Bill No</label>
                     <input class="form-control" readonly>
                  </div>
               </div>
               <div class="col-lg-6">
                  <div class="form-group">
                     <label>Date</label>
                     <input class="form-control" readonly>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>-->
<div class="row">
   <div class="col-md-12">
      <div class="panel panel-info">
         <div class="panel-body">
			<?php $attributes = array('class' => 'abcd', 'id' =>'myForm', 'name' =>'myForm'); ?>
			<?php echo form_open('',$attributes); ?>	
				<div class="row">
					<div class="col-lg-4">
					  <div class="form-group">
						 <label>Barcode</label>
						 <input type="text" class="form-control" name="productBarcode" id="productBarcode" required="required">
					  </div>
					</div>
					<div class="col-lg-4">
					  <div class="form-group">
						 <label>Quantity</label>
						 <input type="number" class="form-control" name="quantity" id="quantity">
						 <div style="" id="quantity_errors" class="warningSize red-text"></div>
					  </div>
					</div>
					<div class="col-lg-4">
					  <button type="submit" class="btn btn-primary submit" style="margin-top:25px;">Add</button>
					  <a  type="button" style="margin-top:25px;" onClick="clearCart()"  class="btn btn-danger pull-xs-right pull-sm-right pull-md-right pull-lg-right pull-xl-right">Clear Cart</a>
					</div>
				</div>
				
			<?php echo form_close(); ?>
            <!--<div class="row">
               <div class="col-lg-6">
                  <div class="form-group">
                     <label>Price</label>
                     <input class="form-control" readonly>
                  </div>
               </div>
               <div class="col-lg-6">
                  <div class="form-group">
                     <label>Current Stock</label>
                     <input class="form-control" readonly>
                  </div>
               </div>
            </div>-->
            <div class="row m-top-25 m-bottom-25">
               <div class="col-md-12">
                  <!--<table class="table table-condensed table-bordered">
                     <thead>
                        <tr class="info">
                           <th>Barcode</th>
                           <th>Product Name</th>
                           <th>Quantity</th>
                           <th>Unit Price</th>
                           <th>Total Price</th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr class="active">
                           <td>Tiger Nixon</td>
                           <td>System Architect</td>
                           <td>Edinburgh</td>
                           <td>61</td>
                           <td>$320,800</td>
                        </tr>
                        <tr class="active">
                           <td>Garrett Winters</td>
                           <td>Accountant</td>
                           <td>Tokyo</td>
                           <td>63</td>
                           <td>$170,750</td>
                        </tr>
                     </tbody>
                  </table>-->
				
					<table class="table table-condensed table-bordered" id="productTable">
						<thead class="table-custom-thead">
							<tr id="tableHeaderID" class="info" >
								<th style="text-align:center" >Barcode</th>
								<th style="text-align:center" >Product Name</th>
								<th style="text-align:center" >Quantity </th>
								<th style="text-align:center" >Unit Price</th>
								<th style="text-align:center" >SubTotal</th>
								<th style="text-align:center" >Remove</th>
							</tr>
						</thead>
						<tbody class="table-custom-tbody text-xs-center text-sm-center text-md-center text-lg-center">
						<?php if($cart=$this->cart->contents()){ ?>
							
							<?php foreach($cart as $item): ?>
								<tr id="row<?php echo $item['id'] ?>" class='active'>
									<td id="barcode_row<?php echo $item['id'] ?>"><?php echo $item['barcode'] ?></td>
									<td id="name_row<?php echo $item['id'] ?>"><?php echo $item['name'] ?></td>
									<td id="quantity_row<?php echo $item['id'] ?>">
										<div class='quantity clearfix'>
											<button type='button' class='minus btn btn-sm btn-info' onClick="minusQuantity('<?php echo $item['rowid'] ?>', '<?php echo $item['id'] ?>', '<?php echo $item['price'] ?>')"><i class='fa fa-minus'></i></button> 
											<input type='number' name='quantity' value="<?php echo $item['qty'] ?>" class='quantity' id="qty<?php echo $item['id'] ?>" readonly='' />
											<button type='button' class='plus btn btn-sm btn-info' onClick="plusQuantity('<?php echo $item['rowid'] ?>','<?php echo $item['total_quantity'] ?>', '<?php echo $item['id'] ?>', '<?php echo $item['price'] ?>')"><i class='fa fa-plus'></i></button>
										</div>
									</td>
									<td id="price_row<?php echo $item['id'] ?>"><?php echo $item['price'] ?></td>
									<td id="subtotal_row<?php echo $item['id'] ?>"><?php echo ($item['qty']*$item['price']) ?></td>
									<td id="remove_row<?php echo $item['id'] ?>" style='width:5%' >
										<a id='my_cart_table_close' onClick="delete_row('<?php echo $item['rowid'] ?>', '<?php echo $item['id'] ?>', '<?php echo $item['price'] ?>')" ><i class='fa fa-remove'></i></a>
										
									</td>
								</tr>
								<?php 
									$totalProductQty=$totalProductQty+$item['qty']; 
									$totalProductPrice=$totalProductPrice+($item['qty']*$item['price']); 
								?>
							<?php endforeach ; ?>
							<?php } ?>
						</tbody>
					</table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="row">
   <div class="col-md-12">
      <div class="panel panel-info">
         <div class="panel-body">
			<?php echo form_open('manager/storeBill'); ?>
			<?php 
				$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
				$date= $dt->format('Y-m-d H:i:s');
				$admin_id=$this->session->userdata('admin_id');
				$shop_id=$this->session->userdata('userShop');
				//echo $date;
				echo form_hidden('saleDate',$date); 
				echo form_hidden('entryBy',$admin_id); 
				echo form_hidden('shop_info_saleShopID',$shop_id); 
				//echo form_hidden('saleType',1); 
			?>
            <div class="row">
               <div class="col-lg-4">
                  <div class="form-group">
                     <label>Total Quantity</label>
                     <input class="form-control" id="totalQuantityFieldID" value="<?php echo $totalProductQty; ?>" readonly>
                  </div>
               </div>
               <div class="col-lg-4">
                  <div class="form-group">
                    <label>Sold By</label>
					<select class="form-control forselect2 addProductID" id="select2" name="salesman_info_salesmanID" >
						<option value="0" selected >Sold By</option>
						<?php foreach($infos as $info){ ?>
							<option value="<?php echo $info->salesmanID; ?>"><?php echo $info->salesmanID." => ".$info->salesmanName; ?></option>
						<?php } ?>
					</select>
                  </div>
               </div>
               <div class="col-lg-4">
                  <div class="form-group">
                     <label>Commission</label>
                     <input class="form-control" value="0" name="salesmanTotalCommission">
                  </div>
               </div>
            </div>
            <div class="row">
				<div class="col-lg-4">
					<div class="form-group">
						<label>Gross Amount</label>
						<input class="form-control" id="totalPriceFieldID" value="<?php echo $totalProductPrice; ?>" name="" readonly>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group">
						<label>Discount</label>
						<input class="form-control" id="totalDiscount" value="0" name="saleTotalDiscount" >
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group">
						<label>Net Payable</label>
						<input class="form-control" id="netPayable" value="<?php echo $totalProductPrice; ?>" name="saleTotalAmount" readonly>
					</div>
				</div>
            </div>
        <!--<div class="row">
               <div class="col-lg-6">
                  <div class="form-group">
                     <label>VAT</label>
                     <input class="form-control" readonly>
                  </div>
               </div>
               <div class="col-lg-6">
                  <div class="form-group">
                     <label>Net Payable</label>
                     <input class="form-control" readonly>
                  </div>
               </div>
            </div>-->
         </div>
      </div>
   </div>
</div>
<div class="row">
   <div class="col-lg-12">
      <button type="submit" class="btn btn-primary" >Done</button>
   </div>
</div>
<?php echo form_close(); ?> 
<?php include('footer.php') ?>