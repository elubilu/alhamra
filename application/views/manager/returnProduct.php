<?php include('header.php') ?>
	<div class="row">
		<div class="col-lg-12">
			<h3 class="page-header">Return Product</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<?php if($this->session->flashdata('feedback_successfull'))
					{ ?>
						<div class="alert alert-success alert-dismissible fade in" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">�</span>
								</button>
							<strong>Success!</strong>
							<?php echo $this->session->flashdata('feedback_successfull'); ?>
						</div>
					<?php } 
					if($this->session->flashdata('feedback_failed'))
						{ ?>
							<div class="alert alert-danger alert-dismissible fade in" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">�</span>
									</button>
								<strong>Oops!</strong>
								<?php echo $this->session->flashdata('feedback_failed'); ?>
							</div>
			<?php   } ?>
		</div>
	</div>
	<?php echo form_open('manager/storeExchange'); ?>
	<?php 
		$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
		$date= $dt->format('Y-m-d H:i:s');
		$admin_id=$this->session->userdata('admin_id');
		$shop_id=$this->session->userdata('userShop');
		//echo $date;
		echo form_hidden('returnDate',$date); 
		echo form_hidden('returnReceivedBy',$admin_id); 
		echo form_hidden('shop_info_returnShopID',$shop_id); 
		//echo form_hidden('saleType',1); 
	?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="row">
		<div class="col-lg-6">
			<div class="form-group">
                <label>Date</label>
                <input type="text" size="16" class="form-control span2" id="date" placeholder="<?php echo $date; ?>" readonly />
            </div>
		</div>
		<div class="col-lg-6">
			<div class="form-group">
                <label>Bill No</label>
                <input class="form-control" name="sale_info_saleID" required >
            </div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6">
			<div class="form-group">
                <label>Return Product Barcode</label>
                <input class="form-control" name="returnProductID" required >
            </div>
		</div>
		<div class="col-lg-3">
			<div class="form-group">
                <label>Return Product Quantity</label>
                <input class="form-control" name="returnProductQuantity" required >
            </div>
		</div>
		<div class="col-lg-3">
			<div class="form-group">
                <label>Return Product Price</label>
                <input class="form-control" name="returnProductPrice" required >
            </div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6">
			<div class="form-group">
                <label>New Product Barcode</label>
                <input class="form-control" name="newProductID" required >
            </div>
		</div>
		<div class="col-lg-3">
			<div class="form-group">
                <label>New Product Quantity</label>
                <input class="form-control" name="newProductQuantity" required >
            </div>
		</div>
		<div class="col-lg-3">
			<div class="form-group">
                <label>New Product Price</label>
                <input class="form-control" name="newProductPrice" required >
            </div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-lg-12">
			<button type="submit" class="btn btn-primary">Done</button>
		</div>
	</div>
	<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
	
<?php include('footer.php') ?>