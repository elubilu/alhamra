<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <title>Sale No. 4740</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" />
    <script src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
    <style type="text/css" media="all">
       
        body{
			max-width: 300px;
			margin: 0 auto;
			text-align: center;
			color: #000;
			font-size: 10px;
		}
        #wrapper {
            min-width: 260px;
            margin: 0 auto;
        }
        h3,
        p {
            ~margin: 5px 0;
			font-size:12px;
			~font-weight:bold;
        }
        
        .left {
            width: 60%;
            float: left;
            text-align: left;
            margin-bottom: 3px;
        }
        
        .right {
            width: 40%;
            float: right;
            text-align: right;
            margin-bottom: 3px;
        }
        
        .table,
        .totals {
            width: 280px;
            margin: 0px 0;
        }        
        .table th {
            border-bottom: 1px solid #000;
			vertical-align: top;
        }
        .table td {
            padding: 0;
			vertical-align: top;
        }        
        .totals td {
            width: 24%;
            padding: 0;
			vertical-align: top;
        }        
        .table td:nth-child(2) {
            overflow: hidden;
			vertical-align: top;
        }       
		.table>thead>tr>th{
			border-bottom: 1px solid #000;
		}		
        @media print{
            body{	
				font-size: 16px;
            }
			p{
				font-size:16px;
			}
            #nonPrintSection{
                display: none;
            }
            #wrapper{
                width: 280px;
				margin: 0 5px;
                font-size: 16px;
            }
        }
    </style>
</head>
<?php
	// echo"<pre>";
	 //print_r($info);
	 //echo "</pre>"; 
	 //print_r($details);
?>
<body>
    <div id="wrapper">
        <!--<img src="http://demo.tecdiary.my/spos/assets/images/logo1.png" alt="Simple POS" />-->
        <h4><strong>Excellent Shoes</strong></h4>
        <p class="text-center;border-bottom:1px solid #000;"><?php echo $info->shopTitle; ?><br />
			Invoice No :<?php echo $info->saleID; ?> <br />
			Date:<?php echo $info->saleDate; ?> <br />
			<?php if($info->salesman_info_salesmanID>0){  ?>
			Served By :<?php echo $info->salesmanName; ?>
			<?php } ?>
		</p>
        <div style="clear:both;"></div>

        <table class="table" cellspacing="0" border="0" style="width:100%;">
            <thead>
                <tr>
                    <th style="text-align:left;font-size:12px;">Product Name</th>
                    <th style="text-align:left;font-size:12px;">Rate</th>
                    <th style="text-align:left;font-size:12px;">Qty</th>
                    <th style="text-align:right;font-size:12px;">Total</th>
                </tr>
            </thead>
			<?php $totalqty=0; $totalAmount=0;?>
            <tbody>
				<?php foreach($details as $infod):  //print_r($info);?>
				<?php //$info->supplierAddedDate= date('d,M y h:ia', strtotime($info->supplierAddedDate)); ?>
				<tr id="">
					<td style="text-align:left; width:60%;"><p><span><?php echo $infod->productName; ?></span></p></td>
					<td style="text-align:center; width:10%;;"><p><span><?php echo $infod->salePrice; ?></span></p></td>
					<td style="text-align:center; width:10%;"><p><span><?php echo $infod->saleProductQuantity; ?></span></p></td>
					<td style="text-align:right; width:20%; "><p><span><?php echo ($infod->saleProductQuantity*$infod->salePrice); ?></span></p></td>
					
				</tr>
				<?php $totalqty=$totalqty+$infod->saleProductQuantity; ?>
				<?php //$totalAmount=$totalAmount+($info->saleProductQuantity*$info->salePrice); ?>
				<?php endforeach; ?>
            </tbody>
        </table>

        <table class="totals" cellspacing="0" border="0" style="margin-bottom:5px;width:100%;">
            <tbody>
                <tr>
                    <td style="text-align:left;font-size:13px;">Total Items</td>
                    <td style="text-align:right; padding-right:1.5%; border-right: 1px solid #000;font-size:13px;font-weight:bold;"><p><?php echo $totalqty; ?></p></td>
                    <td style="text-align:left; padding-left:1.5%;font-size:13px;">Total</td>
                    <td style="text-align:right;font-weight:bold;font-size:13px;"><p><?php echo $info->saleTotalAmount; ?></p></td>
                </tr>
               <!-- <tr>
                    <td colspan="2" style="text-align:left; font-weight:bold; border-top:1px solid #000; padding-top:5px;font-size:13px;">Grand Total</td>
                    <td colspan="2" style="border-top:1px solid #000; padding-top:5px; text-align:right; font-weight:bold;"><p><?php echo $info->saleTotalAmount; ?></p></td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align:left; font-weight:bold; padding-top:5px;font-size:13px;">Paid</td>
                    <td colspan="2" style="padding-top:5px; text-align:right; font-weight:bold;"><p>1100</p></td>
                </tr>
				<tr>
					<?php //if(($info['info']->total_price- $info['info']->receved_total)<0){ ?>
					<td colspan="2" style="text-align:left; font-weight:bold; padding-top:5px;font-size:13px;">Change</td>
					<td colspan="2" style="padding-top:5px; text-align:right; font-weight:bold;"><p>0</p></td>
                </tr>-->
            </tbody>
        </table>

        <div style="border-top:1px solid #000; padding-top:10px;">
			
            <p style="border-bottom:1px dotted gray; padding-bottom:5px;">
				<?php if( $info->saleTotalDiscount>0){ ?> You have saved TK <?php echo $info->saleTotalDiscount; ?> <br /> <?php } ?>
                Thank you for your business !
            </p>
			<p style="font-size:14px;font-weight:bold;">
                Developed by StarLab IT (01617827522) <br />
				<span class="text-xs-center text-sm-center text-md-center text-lg-center text-xl-center">www.starlabit.com.bd</span>
            </p>
			
        </div>

        <div id="nonPrintSection" style="padding-top:10px; text-transform:uppercase;">
            <span class=""><button type="button" onClick="window.print();return false;" style="width:100%; cursor:pointer; font-size:12px; background-color:#FFA93C; color:#000; text-align: center; border:1px solid #FFA93C; padding: 10px 1px; font-weight:bold;">Print</button></span>
			
            <div style="clear:both;"></div>
			
            <a href="<?php echo base_url("manager/addBillMemo"); ?>" style="width:95%; display:block; font-size:12px; text-decoration: none; text-align:center; color:#FFF; background-color:#007FFF; border:2px solid #007FFF; padding: 10px 1px; margin: 5px auto 10px auto; font-weight:bold;">Back to POS</a>

            <!--<div style="background:#F5F5F5; padding:10px;">
                <p style="font-weight:bold;">Please don't forget to disble the header and footer in browser print settings.</p>
                <p style="text-transform: capitalize;"><strong>FF:</strong> File > Print Setup > Margin & Header/Footer Make all --blank--</p>
                <p style="text-transform: capitalize;"><strong>chrome:</strong> Menu > Print > Disable Header/Footer in Option & Set Margins to None</p>
            </div>-->
			
            <div style="clear:both;"></div>
			
        </div>
    </div>
    <script type="text/javascript" src="http://demo.tecdiary.my/spos/assets/js/jquery.js"></script>
    <script type="text/javascript">
        /*$(document).ready(function() {
            $('#email').click(function() {
                var email = prompt("Please enter email address", "test@mail.com");
                if (email != null) {
                    $.ajax({
                        type: "post",
                        async: false,
                        url: "index.php?module=pos&view=email_receipt",
                        data: {
                            csrf_pos: "888b39ed7d43ed3d7b79dbf83cf9c89e",
                            email: email,
                            id: 4740
                        },
                        //dataType: "json",
                        success: function(data) {
                            alert(data);
                        },
                        error: function() {
                            alert('Ajax Request Failed');
                            return false;
                        }
                    });
                }
                return false;
            });
        });*/

        $(window).load(function() {
            window.print();
        });
    </script>
</body>

</html>