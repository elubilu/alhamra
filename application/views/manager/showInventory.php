<?php include('header.php') ?>
	<div class="row">
		<div class="col-lg-12">
			<h3 class="page-header">Show Inventory</h3>
		</div>
	</div>
	<!--<div class="row showInventory">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<label>Date</label>
								<input type="text" size="16" class="form-control span2" id="startDate" placeholder="12-02-2012" />
							</div>							
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>Date</label>
								<input type="text" size="16" class="form-control span2" id="endDate" placeholder="12-02-2012" />
							</div>							
						</div>						
					</div>
					<div class="row">
						<div class="col-lg-12">
							<button type="button" class="btn btn-primary">Confirm</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>-->
	<?php if($infos){ ?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<?php if($infos){ ?>
					<div class="row">
						<div class="col-lg-12">
							<table id="showInventory" class="table table-striped table-bordered" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>Barcode</th>
										<th>Product Name</th>
										<th>Group</th>
										<th>Sold Unit</th>
										<th>Stock Unit</th>
										<th>Supplier</th>
										<th>Added Date</th>
										<th>Details</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($infos as $info):  //print_r($info);?>
									<?php $info->productAddedDate= date('d,M y h:ia', strtotime($info->productAddedDate)); ?>
									<tr id="shop<?php echo $info->productID ?>">
										<td><?php echo $info->productBarcode; ?></td>
										<td><?php echo $info->productName; ?></td>
										<td><?php echo $info->groupName; ?></td>
										<td><?php echo $info->productSaleCounter; ?></td>
										<td><?php echo $info->productQuantity; ?></td>
										<td><?php echo $info->supplierCompanyName; ?></td>
										<td><?php echo $info->productAddedDate; ?></td>
										<td>
											<a href="<?php echo base_url("manager/productDetails/{$info->productID}"); ?>" type="button" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Details"><i class="fa fa-info"></i></a>
										</td>
									</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
	<div align="center">
        <ul class="pagination">
            <?php echo $this->pagination->create_links(); ?>
        </ul>
    </div>
	<?php } ?>

<?php include('footer.php') ?>