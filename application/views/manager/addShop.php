<?php include('header.php') ?>
	<div class="row">
		<div class="col-lg-12">
			<h3 class="page-header">Add Shop</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6">
			<div class="form-group">
                <label>ID</label>
                <input class="form-control" readonly>
            </div>
		</div>
		<div class="col-lg-6">
			<div class="form-group">
                <label>Field Name</label>
                <input class="form-control">
            </div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-lg-12">
			<button type="button" class="btn btn-primary">Add Expense Field</button>
		</div>
	</div>
	
	
<?php include('footer.php') ?>