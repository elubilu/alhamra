<?php include('header.php') ?>
<?php //print_r($infos); ?>
	<div class="row">
		<div class="col-lg-12">
			<h3 class="page-header">Salesmem Performance Report</h3>
		</div>
	</div>
	
	<?php echo form_open('manager/salesmanPerformaneceReport'); ?>
	<div class="row salesmanPerformaneceReport">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<label>Date</label>
								<input type="text" size="16" class="form-control span2" id="startDate" name="salemanStartDate" placeholder="Start Date" />
							</div>							
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>Date</label>
								<input type="text" size="16" class="form-control span2" id="endDate" name="salemanEndDate" placeholder="End Date" />
							</div>							
						</div>						
					</div>
					<div class="row">
						<div class="col-lg-12">
							<button type="submit" class="btn btn-primary">Confirm</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php echo form_close(); ?>
	<?php if($infos){  ?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default filterable">
				<div class="panel-heading">
                    <div class="row">
						<div class="col-md-12 col-xs-12" style="margin-top:20px;">
							<div class="pull-right">
								<button id="filter_button" class="btn btn-primary btn-filter" style="color:#fff;"><i class="fa fa-filter"></i> Filter
								</button>
							</div>
						</div>
                    </div>
                </div>
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-12">
							<table class="table table-striped table-bordered" cellspacing="0" width="100%">
								<thead>
									<tr class="filters">											
												<th>
													<input type="text" class="form-control text-left" placeholder="Date " disabled data-toggle="true">
												</th>
												<th>
													<input type="text" class="form-control" placeholder="Salesman" disabled>
												</th>
												<th>
													<input type="text" class="form-control" placeholder="Bill No" disabled>
												</th>
												<th>
													<input type="text" class="form-control" placeholder="Total Sell" disabled>
												</th>
												<th>
													<input type="text" class="form-control" placeholder="Commission" disabled>
												</th>
												<th>
													<span>Details</span>
												</th>
												
											</tr>
								</thead>
								<tbody>
									<?php foreach($infos as $info):  //print_r($info);?>
									<?php $info->saleDate= date('d,M y h:ia', strtotime($info->saleDate)); ?>
									<tr id="productGroup<?php echo $info->saleID ?>" class="active">
										<td>
											<?php echo $info->saleDate; ?>
										</td>
										<td>
											<?php echo $info->salesmanName; ?>
										</td>
										<td>
											<?php echo $info->saleID; ?>
										</td>
										<td>
											<?php echo $info->saleTotalAmount; ?>
										</td>
										<td>
											<?php echo $info->salesmanTotalCommission; ?>
										</td>
										<td>
											<a href="<?php echo base_url("manager/saleDetails/{$info->saleID}/{$info->salesman_info_salesmanID}"); ?>"  type="button" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Details"><i class="fa fa-info"></i></a>
										</td>
									</tr>
									<?php endforeach; ?>
									<!--<tr>
										<td>12-01-2017</td>
										<td>Keshob</td>
										<td>Keshob</td>
										<td>5246574115645</td>
										<td>Slippers</td>
										<td>10</td>
										<td>$320</td>
										<td>$10</td>
									</tr>-->
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div align="center">
        <ul class="pagination">
        <?php
					if($flag['flag']==1){
					echo $this->pagination->create_links(); 
				}
		?>
        </ul>
    </div>
	<!--<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="row m-top-25">
						<div class="col-lg-6">
							<div class="form-group">
								<label>Total Unit Sold</label>
								<input class="form-control" value="500" readonly>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>Total Commision</label>
								<input class="form-control" value="2000" readonly>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>-->
	<?php } ?>
<?php include('footer.php') ?>