<?php include('header.php') ?>
	<div class="row">
		<div class="col-lg-12">
			<h3 class="page-header">Add Expense</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<?php if($this->session->flashdata('feedback_successfull'))
					{ ?>
						<div class="alert alert-success alert-dismissible fade in" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true"><i class="fa fa-times"></i></span>
								</button>
							<strong>Success!</strong>
							<?php echo $this->session->flashdata('feedback_successfull'); ?>
						</div>
					<?php } 
					if($this->session->flashdata('feedback_failed'))
						{ ?>
							<div class="alert alert-danger alert-dismissible fade in" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true"><i class="fa fa-times"></i></span>
									</button>
								<strong>Oops!</strong>
								<?php echo $this->session->flashdata('feedback_failed'); ?>
							</div>
				<?php   } ?>
		</div>
	</div>
	
	<?php echo form_open('manager/storeExpense'); ?>
	<?php 
		$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
		$date= $dt->format('Y-m-d H:i:s');
		$admin_id=$this->session->userdata('admin_id');
		$shop_id=$this->session->userdata('userShop');
		//echo $date;
		echo form_hidden('expenceDate',$date); 
		echo form_hidden('expenceEntryBy',$admin_id); 
		echo form_hidden('shop_info_ExpenceShopID',$shop_id); 
		//echo form_hidden('saleType',1); 
	?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<!--<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<label>Date</label>
								<input class="form-control" value="12-01-2017" readonly>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>Transaction ID</label>
								<input class="form-control" readonly>
							</div>
						</div>
					</div>-->
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<label>Expense Field</label>
								<select class="form-control forselect2 addProductID" id="select2" name="expenceFieldID" required>
									<option value="" selected disabled >Expense Field</option>
									<?php foreach($infos as $info){ ?>
										<option value="<?php echo $info->expenseFieldID; ?>"><?php echo $info->expenseFieldID." => ".$info->expenseFieldName; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>Amount</label>
								<input class="form-control" name="expenceAmount" required>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label>Reference</label>
								<textarea class="form-control" rows="3" name="expenceReference" required></textarea>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<button type="submit" class="btn btn-primary">Add</button>
						</div>
					</div>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>	
	
<?php include('footer.php') ?>