﻿<?php include('header.php') ?>
<?php// echo $infos; ?>
	<div class="row">
		<div class="col-lg-12">
			<h3 class="page-header">Product Details</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-3">
							<div class="form-group">
								<label>Barcode</label>
								<input class="form-control" value="<?php echo $infos->productBarcode ; ?>" readonly>
							</div>
						</div>
						<div class="col-lg-3">
							<div class="form-group">
								<label>Product Name</label>
								<input class="form-control" value="<?php echo $infos->productName; ?>" readonly>
							</div>
						</div>
						
						<div class="col-lg-3">
							<div class="form-group">
								<label>Group</label>
								<input class="form-control" value="<?php echo $infos->groupName;  ?>" readonly>
							</div>
						</div>
						<div class="col-lg-3">
							<div class="form-group">
								<label>Supplier</label>
								<input class="form-control" value="<?php echo $infos->supplierCompanyName; ?>" readonly>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-3">
							<div class="form-group">
								<label>Purchase Price</label>
								<input class="form-control" value="<?php echo $infos->productPurchasePrice; ?>" readonly>
							</div>
						</div>
						<div class="col-lg-3">
							<div class="form-group">
								<label>Sell Price</label>
								<input class="form-control" value="<?php echo $infos->productSalePrice; ?> "readonly>
							</div>
						</div>
						<div class="col-lg-3">
							<div class="form-group">
								<label>Status</label>
								<input class="form-control" value="<?php if($infos->productStatus==1)echo "Active"; else echo "InActive";  ?>" readonly>
							</div>
						</div>
						<div class="col-lg-3">
							<div class="form-group">
								<label>Sold unit</label>
								<input class="form-control" value="<?php echo $infos->productSaleCounter; ?>" readonly>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-4">
							<div class="form-group">
								<label>Stock Unit</label>
								<input class="form-control" value="<?php echo $infos->productQuantity; ?>" readonly>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="form-group">
								<label>Total Purchase price of stock units</label>
								<input class="form-control" value="<?php echo $infos->productQuantity*$infos->productPurchasePrice; ?> "readonly>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="form-group">
								<label>Total Sell price of stock units</label>
								<input class="form-control" value="<?php echo $infos->productQuantity*$infos->productSalePrice; ?>" readonly>
							</div>
						</div>
						<div class="col-lg-3">
							<div class="form-group">
								<label>Product Added Date</label>
								<?php $infos->productAddedDate= date('d,M y h:ia', strtotime($infos->productAddedDate)); ?>
								<input class="form-control" value="<?php echo $infos->productAddedDate; ?>" readonly>
							</div>
						</div>
						<div class="col-lg-3">
							<div class="form-group">
								<label>Product Added By</label>
								<input class="form-control" value="<?php echo $infos->admin_info_productAdminID; ?>" readonly>
							</div>
						</div>
						<div class="col-lg-3">
							<div class="form-group">
								<label>Product Last Updated Date</label>
								<?php $infos->productUpdatedDate= date('d,M y h:ia', strtotime($infos->productUpdatedDate)); ?>
								<input class="form-control" value="<?php echo $infos->productUpdatedDate; ?>" readonly>
							</div>
						</div>
						<div class="col-lg-3">
							<div class="form-group">
								<label>Product Last Updated By</label>
								<input class="form-control" value="<?php echo $infos->productUpdatedBy; ?>" readonly>
							</div>
						</div>
					</div>	
				</div>
			</div>
		</div>
	</div>

	<?php if($sale_infos){ ?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h1 class="panel-title">Sale Info</h1>
				</div>
				
				<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<table class="table table-condensed table-bordered">
									<thead>
										<tr class="info">
											<th>Barcode</th>
											<th>Product Name</th>
											<th>Quantity</th>
											<th>Unit Price</th>
											<th>Total Price</th>
											<th>Date</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach($sale_infos as $info):  //print_r($info);?>
										<?php $info->saleDate= date('d,M y h:ia', strtotime($info->saleDate)); ?>
											<tr id="shop<?php echo $info->saleDetailsID ?>" class="active">
												<td><?php echo $info->productBarcode; ?></td>
												<td><?php echo $info->productName; ?></td>
												<td><?php echo $info->saleProductQuantity;//$info->saleProductQuantityTotal; ?></td>
												<td><?php echo $info->salePrice; ?></td>
												<td><?php echo ($info->salePrice*$info->saleProductQuantity); ?></td>
												<td><?php echo $info->saleDate; ?></td>
												
											</tr>
										<?php endforeach; ?>
									</tbody>
								</table>
							</div>
						</div>
				</div>
			</div>
		</div>
	</div>
	<div align="center">
        <ul class="pagination">
            <?php echo $this->pagination->create_links(); ?>
        </ul>
    </div>
	<?php } ?>
<?php include('footer.php') ?>