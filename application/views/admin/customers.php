<?php include('header.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">Customer Accounts</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url('')?>">Dash Board</a></li>
				<li class="active">Customer Accounts</li>
			</ol>
		</div>
	</div>
	<?php include('messages.php'); ?>
	<div class="row filterable">
		<div class="col-md-12">
			<div class="row m-bottom-10">
				<div class="col-md-12 col-xs-12">
					<div class="pull-right">								
						<a href="<?php echo base_url('admin/addCustomerAccount')?>" style="margin-top:0px;margin-right:5px;" type="button" class="btn btn-default">Add Customer Account</a>
						<button id="filter_button" class="btn btn-default btn-filter"><i class="fa fa-filter"></i> Filter</button>
					</div>
				</div>
			</div>
			<?php if($allCustomer){  ?>
			<div class="row">
				<div class="col-md-12">
					<table class="table table-striped">
						<thead class="dark-header">
							<tr class="filters">
								<th>
									<input type="text" class="form-control text-left" placeholder="Account ID" disabled data-toggle="true">
								</th>
								<th>
									<input type="text" class="form-control text-left" placeholder="Organization" disabled data-toggle="true">
								</th>
								<th>
									<input type="text" class="form-control text-left" placeholder="Owner" disabled data-toggle="true">
								</th>
								<th>
									<input type="text" class="form-control text-left" placeholder="Phone" disabled data-toggle="true">
								</th>
								<th>
									<input type="text" class="form-control text-left" placeholder="Rent Due" disabled data-toggle="true">
								</th>								
								<th>
									<input type="text" class="form-control text-left" placeholder="Bill Due" disabled data-toggle="true">
								</th>								
								<th>
									<span>View</span>
								</th>
							</tr>
						</thead>
						<tbody>
                            <?php foreach ($allCustomer as $customer): ?>
							<tr>
                                <td><?php echo $customer->customerId; ?></td>
								<td><?php echo $customer->organizationName ; ?></td>
								<td><?php echo $customer->customerName; ?></td>
								<td><?php echo $customer->customerPhone ; ?></td>
								<td><?php echo $customer->rentDue ?></td>
								<td><?php echo $customer->billDue; ?></td>
								<td>
                                    <a href="<?php echo base_url("admin/viewCustomerAccount/{$customer->customerId}")?>" type="button"class="btn btn-primary btn-sm" style=""><i class="fa fa-info" ></i></a>
								</td>
							</tr>
                            <?php endforeach;?>
						</tbody>
					</table>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
<?php include('footer.php') ?>