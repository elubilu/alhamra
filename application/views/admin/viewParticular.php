<?php include('header.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">View Particular</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url('')?>">Dash Board</a></li>
				<li class="active">View Particular</li>
			</ol>
		</div>
	</div>
	<?php include('messages.php'); ?>
	
	<div class="row">
		<div class="col-md-12">
			<div class="thumbnail">
				<?php echo form_open('admin/update_particular');
					  echo form_hidden('particularStatus',1);
					  echo form_hidden('particularId',$info->particularId);
				?>
				<div class="caption">
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label>Particular Title</label>
								<input type="text" class="form-control removeDisabled" name="particularTitle" value="<?php echo $info->particularTitle ?>" required disabled/>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>Particular Type</label>
								<select class="form-control removeDisabled" name="particularType" id="" value="" required disabled>
								<option value="<?php echo $info->particularType ?>"><?php if($info->particularType==1)echo "Income"; else echo "Expense"; ?></option>

									<option value="1"> Income</option>
									<option value="2"> Expense</option>
								</select>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>Particular Status</label>
								<select class="form-control removeDisabled" name="particularStatus" id="particularStatus" value="" required disabled>
									<option value="<?php echo $info->particularStatus ?>"><?php if($info->particularStatus==1)echo "Active"; else echo "InActive"; ?>
									<option value="1"> Active</option>
									<option value="2"> Inactive</option>
								</select>
							</div>
						</div>

					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Note</label>
								<textarea type="text" class="form-control removeDisabled" name="particularNote" value="" disabled required><?php echo $info->particularNote ?></textarea>
							</div>
						</div>						
					</div>
					<div class="row">
						<div class="col-md-12">
							<button type="button" class="btn btn-primary" id="removeReadonlyButton"><i class="fa fa-pencil"></i> Edit</button>
							<button type="submit" class="btn btn-success showreadonly" style=""><i class="fa fa-thumbs-up"></i> Save</button>

							<button type="button"class="btn btn-warning showreadonly" id="addReadonlyButton" style=""><i class="fa fa-times" ></i> Cancel</button>
							
							<a class="btn btn-danger" onclick="window.history.back();"> <i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
						</div>
					</div>
				</div>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>

<?php include('footer.php') ?>