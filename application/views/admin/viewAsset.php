<?php include('header.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">View Asset</h3>
		</div>
	</div>
	<?php include('messages.php'); ?>
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url('')?>">Dash Board</a></li>
				<li class="active">View Asset</li>
			</ol>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<div class="thumbnail">
				<?php 
							echo form_open('admin/updateAsset');
							echo form_hidden('assetId',$assetDetails->assetId); 
					?>
				<div class="caption">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Asset Title</label>
								<input type="text" class="form-control removeDisabled" name="assetTitle" value="<?php echo $assetDetails->assetTitle;?>" disabled />
							    <div class="red-text"> <?php  echo form_error('assetTitle'); ?></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label>Tracking ID</label>
								<input type="text" class="form-control removeDisabled" name="assetTrackingId" value="<?php echo $assetDetails->assetTrackingId;?>" disabled />
							    <div class="red-text"> <?php  echo form_error('assetTrackingId'); ?></div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>Original Value</label>
								<input type="text" class="form-control removeDisabled" name="assetOriginalValue" value="<?php echo $assetDetails->assetOriginalValue;?>" disabled />
							    <div class="red-text"> <?php  echo form_error('assetOriginalValue'); ?></div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>Current Value</label>
								<input type="text" class="form-control removeDisabled" name="assetCurrentValue" value="<?php echo $assetDetails->assetCurrentValue;?>" disabled />
							    <div class="red-text"> <?php  echo form_error('assetCurrentValue'); ?></div>
							</div>
						</div>						
					</div>
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label>Accruing date</label>
								<input type="text" class="form-control removeDisabled" name="assetAccruingDate" value="<?php echo $assetDetails->assetAccruingDate;?>" disabled />
							    <div class="red-text"> <?php  echo form_error('assetAccruingDate'); ?></div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>Depreciation rate/year</label>
								<input type="text" class="form-control removeDisabled" name="assetDepreciationRate" value="<?php echo $assetDetails->assetDepreciationRate;?>" disabled />
							    <div class="red-text"> <?php  echo form_error('assetDepreciationRate'); ?></div>
							</div>
						</div>	
						<div class="col-md-4">
							<div class="form-group">
								<label>Status</label>
								<select class="form-control removeDisabled" name="assetStatus" disabled>
									<option value="<?php  echo $assetDetails->assetStatus ?>" selected disabled ><?php if($assetDetails->assetStatus==1)echo "Active"; else echo "InActive"; ?></option>
									<option value="1">Active</option>
									<option value="0">Inactive</option>
								</select>
							</div>
						</div>							
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Note</label>
								<textarea type="text" class="form-control removeDisabled" name="assetNote" value="" disabled><?php echo $assetDetails->assetNote;?></textarea>
							    <div class="red-text"> <?php  echo form_error('assetNote'); ?></div>
							</div>
						</div>						
					</div>
					
					<div class="row">
						<div class="col-md-12">
							<button type="button" class="btn btn-primary" id="removeReadonlyButton"><i class="fa fa-pencil"></i> Edit</button>
							<button type="submit" class="btn btn-success showreadonly" style=""><i class="fa fa-thumbs-up"></i> Save</button>
							<button type="button"class="btn btn-warning showreadonly" id="addReadonlyButton" style=""><i class="fa fa-times" ></i> Cancel</button>
							<a type="button" onclick="window.history.back();" class="btn btn-danger"> <i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
						</div>
					</div>
				</div>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>

<?php include('footer.php') ?>