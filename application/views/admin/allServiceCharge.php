	<?php include('header.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">All Service Charge</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url('')?>">Dash Board</a></li>
				<li class="active">All Service Charge</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="thumbnail">
				<div class="caption">
					<table class="table table condensed table-striped">
						<thead>
							<tr>
								<th>Service</th>
								<th>Details</th>
							</tr>
						</thead>
						<?php if($serviceList){ ?>
						<tbody>
							<?php foreach($serviceList as $service): ?>
							<tr>
								<td><?php echo $service->serviceName; ?></td>
								<td><a href="<?php echo base_url("admin/viewServiceCharge/{$service->serviceId}/{$service->serviceName}");?>" class="btn btn-primary btn-sm"><i class="fa fa-info"></i></a></td>
							</tr>
							<?php endforeach ; ?> 
						</tbody>
						<?php } ?>
					</table>
				</div>
			</div>
		</div>
	</div>
	<?php include('footer.php') ?>