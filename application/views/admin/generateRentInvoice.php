<?php include('header.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">Generate Rent Invoice</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url('')?>">Dash Board</a></li>
				<li class="active">Generate Rent Invoice</li>
			</ol>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<div class="thumbnail">
				<div class="caption">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Billing Month</label>
								<input type="text" class="form-control" name="" value="" />
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-12">
							<button type="submit" class="btn btn-success" style=""><i class="fa fa-thumbs-up"></i> Add</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">Previous Rent Invoice</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<table class="table table-striped">
				<thead class="dark-header">
					<tr>
						<th>Billing Year</th>
						<th>Billing Month</th>
						<th>Status</th>
						<th>View</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>2017</td>
						<td>May</td>
						<td>Pending</td>
						<td><a href="<?php echo base_url('admin/pendingSetupBills');?>" type="button"class="btn btn-primary btn-sm" style=""><i class="fa fa-info" ></i></a></td>
					</tr>	
					<tr>
						<td>2017</td>
						<td>June</td>
						<td>Final</td>
						<td><a href="<?php echo base_url('admin/finalSetupBills');?>" type="button"class="btn btn-primary btn-sm" style=""><i class="fa fa-info" ></i></a></td>
					</tr>	
					
				</tbody>
			</table>
		</div>
	</div>
	<!--<div class="row">
		<div class="col-md-12">
			<div class="panel panel-custom">
				<div class="panel-heading">Transaction History</div>
				<div class="panel-body">
					<table class="table table-hoverable">
						<thead>
							<tr>
								<th>Date & Time of Entry</th>
								<th>Transaction Date</th>
								<th>Details</th>
								<th>Deposit</th>
								<th>Withdrawal</th>
								<th>Balance</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>22-05-2017, 01:55 PM</td>
								<td>22-05-2017</td>
								<td>Details of the trasaction</td>
								<td>50000</td>
								<td>25000</td>
								<td>25000</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>-->
<?php include('footer.php') ?>