	<?php include('header.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">Service Charge Summary</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url('')?>">Dash Board</a></li>
				<li class="active">Service Charge Summary</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="thumbnail">
				<div class="caption">
					<?php echo form_open('admin/serviceChargeSummary'); ?>
					<div class="row">
						<div class="col-md-5">
							<div class="form-group">
								<label>Date From</label>
								<input type="text" class="form-control" name="startDate" value="<?php if($data) echo $data['startDate']; ?> " id="startDate" required>
							</div>
						</div>
						<div class="col-md-5">
							<div class="form-group">
								<label> Date To</label>
								<input type="text" id="endDate" class="form-control" name="endDate" value="<?php if($data) echo $data['endDate']; ?>" required>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group" style="margin-top:25px;">
								<button type="submit" id="" class="btn btn-success"> Search</button>
							</div>
						</div>
					</div>
					
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="thumbnail">
				<div class="caption">
					<table class="table table condensed table-striped">
						<thead>
							<tr>
								<th>Service</th>
								<th>Charged</th>
								<th>Collected</th>
							</tr>
						</thead>
						<?php if($serviceList): ?>
						<tbody>
						<?php foreach($serviceList as $service): ?>
							<tr>
								<td><?php echo $service->serviceName; ?></td>
								<td><?php echo round($service->serviceChargePaymentAmount); ?></td>
								<td><?php echo round($service->serviceChargePaidAmount); ?></td>
							</tr>
						<?php endforeach; ?>
						</tbody>
						<?php endif; ?>
					</table>
				</div>
			</div>
		</div>
	</div>
		
	<?php include('footer.php') ?>