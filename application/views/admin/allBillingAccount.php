﻿<?php include('header.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">All Billing Account</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url('')?>">Dash Board</a></li>
				<li class="active">All Billing Account</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<?php if($this->session->flashdata('feedback_successfull'))
					{ ?>
						<div class="alert alert-success alert-dismissible fade in" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true"><i class="fa fa-times"></i></span>
								</button>
							<strong>Success!</strong>
							<?php echo $this->session->flashdata('feedback_successfull'); ?>
						</div>
					<?php } 
					if($this->session->flashdata('feedback_failed'))
						{ ?>
							<div class="alert alert-danger alert-dismissible fade in" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true"><i class="fa fa-times"></i></span>
									</button>
								<strong>Oops!</strong>
								<?php echo $this->session->flashdata('feedback_failed'); ?>
							</div>
				<?php   } ?>
		</div>
	</div>
	<div class="row filterable">
		<div class="col-md-12">
			<div class="row m-bottom-10">
				<div class="col-md-12 col-xs-12">
					<div class="pull-right">								
						<a href="<?php echo base_url('admin/addBillingAccount')?>" style="margin-top:0px;margin-right:5px;" type="button" class="btn btn-default">Add Billing Account</a>
						<button id="filter_button" class="btn btn-default btn-filter"><i class="fa fa-filter"></i> Filter</button>
					</div>
				</div>
			</div>
			<?php if($infos){  ?>
			<div class="row">
				<div class="col-md-12">
					<table class="table table-striped">
						<thead class="dark-header">
							<tr class="filters">
								<th>
									<input type="text" class="form-control text-left" placeholder="Space Title" disabled data-toggle="true">
								</th>
								<th>
									<input type="text" class="form-control text-left" placeholder="Organization" disabled data-toggle="true">
								</th>
								<th>
									<input type="text" class="form-control text-left" placeholder="Owner" disabled data-toggle="true">
								</th>
								<th>
									<input type="text" class="form-control text-left" placeholder="Meter No" disabled data-toggle="true">
								</th>
								<th>
									<input type="text" class="form-control text-left" placeholder="Space Type" disabled data-toggle="true">
								</th>							
								<th>
									<span>Details</span>
								</th>
							</tr>
						</thead>
						<tbody>
                            <?php foreach ($infos as $customer): ?>
							<tr>
                                <td><?php echo $customer->spaceTitle; ?></td>
								<td><?php echo $customer->organizationName ; ?></td>
								<td><?php echo $customer->customerName; ?></td>
								<td><?php echo $customer->meterNo ; ?></td>
								<td><?php if($customer->spaceType==1) echo "Shop"; else echo "Office"  ; ?></td>
                               
								<td>
                                   <a href="<?php echo base_url("admin/billingAccountDetails/{$customer->meterId}")?>" type="button"class="btn btn-primary btn-sm" style=""><i class="fa fa-info" ></i></a>
								</td>
                                <?php echo form_close();?>
							</tr>
                            <?php endforeach;?>
						</tbody>
					</table>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>

<?php include('footer.php') ?>