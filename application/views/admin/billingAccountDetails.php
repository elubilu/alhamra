﻿<?php include('header.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">View Billing Account</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<?php if($this->session->flashdata('feedback_successfull'))
			{ ?>
				<div class="alert alert-success alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true"><i class="fa fa-times"></i></span>
						</button>
					<strong>Success!</strong>
					<?php echo $this->session->flashdata('feedback_successfull'); ?>
				</div>
			<?php } 
			if($this->session->flashdata('feedback_failed'))
			{ ?>
				<div class="alert alert-danger alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true"><i class="fa fa-times"></i></span>
						</button>
					<strong>Oops!</strong>
					<?php echo $this->session->flashdata('feedback_failed'); ?>
				</div>
			<?php } ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url('admin/')?>">Dash Board</a></li>
				<li class="active">View Billing Account</li>
			</ol>
		</div>
	</div>
	<?php echo form_open('admin/updateCustomerBillingAccount'); 
		  echo form_hidden('meterId',$meterInfo->meterId);
		  echo form_hidden('customer_info_customerId',$meterInfo->customer_info_customerId);
	?>
	<div class="row">
		<div class="col-md-12">
			<div class="thumbnail">
				<div class="caption">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Name of Organization</label>
								<input type="text" class="form-control" name="customer_info_customerId" id="organizationName" value="<?php echo $meterInfo->organizationName ?>" disabled/>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Space Title</label>
								<input type="text" class="form-control" name="" value="<?php echo $meterInfo->spaceTitle ?>" disabled/>
							</div>
						</div>						
					</div>
					<div class="row">
						<div class="col-md-12">
							<a type="button" onclick="window.history.back();" class="btn btn-danger"> <i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="thumbnail">
				<div class="caption">
					<div class="row">
						<div class="col-md-12">
							<table class="table table-striped">
								<thead class="dark-header">
									<tr>
										<th>Meter No.</th>
										<th>Customer ID</th>
										<th>Previous Reading</th>
										<th>Previous Due</th>
										<th>Demand Charge</th>
										<th>Service Charge</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td><input type="text" class="form-control" name="meterNo" value="<?php echo $meterInfo->meterNo ?>" disabled /></td>
										<td><input type="text" class="form-control" name="meterCustomerId" value="<?php echo $meterInfo->meterCustomerId ?>" /></td>
										<td><input type="text" class="form-control" name="previousReading" value="<?php echo $meterInfo->previousReading ?>" disabled /></td>
										<td><input type="text" class="form-control" name="billDue" value="<?php echo $meterInfo->billDue ?>" disabled /></td>
										<td><input type="text" class="form-control" name="demandCharge" value="<?php echo $meterInfo->demandCharge ?>" required /></td>
										<td><input type="text" class="form-control" name="meterServiceCharge" value="<?php echo $meterInfo->meterServiceCharge ?>" required /></td>
									</tr>
								</tbody>						
							</table>
						</div>
					</div>	
					<div class="row m-top-15">
						<div class="col-md-12">
							<div class="form-group">
								<div class="row">
									<div class="col-md-12"><label>Service Charge Setup</label><br></div>
									<div class="col-md-12 text-left">
										<select id="multipleSelect" class="form-control multipleSelect" multiple name="charges[]" multiple="multiple">
										    <?php foreach($services as $service): ?>
											<?php
												if (in_array($service->serviceId, $meterServicesId,true)) { ?>
													<option value="<?php echo $service->serviceId; ?>" selected disabled  ><?php echo $service->serviceName;?></option>
												<?php continue;}
											
											?>
												<option value="<?php echo $service->serviceId ?>" ><?php echo $service->serviceName ?></option>
											<?php endforeach; ?>
										</select>
									</div>
								</div>	
							</div>
						</div>					
					</div>
					
					<h4>Service Charges</h4>
					
					<div class="row">
						<div class="col-md-12">
							<table class="table table-striped addAdditionalChargeTable" id="serviceChargeTable">
								<thead class="dark-header">
									<tr>
										<th>Charge Title</th>
										<th>Remove</th>
									</tr>
								</thead>
								<?php if($meterServices): ?>
								<tbody>
									<?php foreach($meterServices as $meterServic): ?>
										<tr>
											<?php //print_r($meterServic); ?>
											<td><input type="text" class="form-control" name="" value="<?php echo $meterServic->serviceName ?>" readonly /></td>
											<td><!--<button type='button' class='btn btn-danger btn-sm'  ><i class='fa fa-remove'></i></button>-->
											<?php if($meterServic->services_serviceId>2){ ?>
												<a href="<?php echo base_url("admin/removeServiceCharge/{$meterServic->customerServiceId}/{$meterServic->meter_info_meterId}")?>" type="button"class="btn btn-danger btn-sm" style=""><i class="fa fa-remove" ></i></a>
											<?php } ?>
											</td>
										</tr>
									<?php endforeach ; ?>
								</tbody>
								<?php endif; ?>						
							</table>
						</div>
					</div>	
					<h4>Additional Charge</h4>
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label for="">Charge Title</label>	
								<select id="additionalChargeTitle" class="form-control " name="" >
									<option value="0" selected >Select Additional Service Charge</option>
								    <?php foreach($additionalServices as $additionalService): ?>
								    	<?php
											if (in_array($additionalService->serviceId, $additionalMeterServicesId,true)) {
										?>
										<option value="<?php echo $additionalService->serviceId ?>" style="background:s#E8E6E5;" disabled ><?php echo $additionalService->serviceName ?></option>
										
										<?php } 
										else{ ?>
												<option value="<?php echo $additionalService->serviceId ?>"><?php echo $additionalService->serviceName ?></option>
												
										<?php } ?>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="">Amount</label>	
								<input type="text" class="form-control" name="" value="" id="additionalChargeAmount" placeholder="Amount" />	
							</div>							
						</div>
						<div class="col-md-4">
							<button type="button" class="btn btn-success m-top-25" id="additionalChargesID" >Add Additional Charge</button>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<table class="table table-striped addAdditionalChargeTable" id="additionalChargeTable">
								<thead class="dark-header">
									<tr>
										<th>Charge Title</th>
										<th>Amount</th>
										<th>Remove</th>
									</tr>
								</thead>
								<tbody>
								<?php if($meterAdditionalServices){ ?>
									<?php foreach($meterAdditionalServices as $meterAdditionalService): ?>
									<?php //print_r($meterAdditionalService); ?>
										<tr>
											<td><input type="text" class="form-control" name="" value="<?php echo $meterAdditionalService->serviceName ?>" readonly /></td>
											<td><input type="text" class="form-control" name="" value="<?php echo $meterAdditionalService->serviceAmount ?>" readonly /></td>
											<td><!--<button type='button' class='btn btn-danger btn-sm'  ><i class='fa fa-remove'></i></button>-->
												<a href="<?php echo base_url("admin/removeAdditionalServiceCharge/{$meterAdditionalService->customerServiceId}/{$meterAdditionalService->meter_info_meterId}/{$meterAdditionalService->services_serviceId}")?>" type="button"class="btn btn-danger btn-sm" style=""><i class="fa fa-remove" ></i></a>
											</td>
										</tr>
									<?php endforeach ; ?>
								<?php } ?>
								</tbody>						
							</table>
						</div>
					</div>	
					<div class="row m-top-25">
						<div class="col-md-12">
							<button type="submit" class="btn btn-success"><i class="fa fa-thumbs-up"> </i> Done</button>
							<button type="reset" class="btn btn-warning"><i class="fa fa-refresh"> </i> Reset</button>
							<a href="<?php echo base_url("admin/removeBillingAccount/{$meterInfo->meterId}")?>" type="button" class="btn btn-danger"><i class="fa fa-times"> </i> Delete</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php echo form_close(); ?>


	<div class="row">
		<div class="col-md-12">
			<div class="row text-center m-top-25">
				<?php if($meterServices){ ?>
				<?php foreach($meterServices as $commonService): ?>
					<?php 
						echo form_open("admin/viewServiceCharge/{$commonService->services_serviceId}/{$commonService->serviceName}");
						echo form_hidden("client_id",$meterInfo->customerId);
						echo form_hidden("meter_id",$meterInfo->meterId);
						echo form_hidden("startDate");
						echo form_hidden("endDate");
					?>
					<div class="col-md-6">
						<button type="submit" class="btn btn-success btn-block btn-lg  m-bottom-15" >
								<?php echo $commonService->serviceName; ?><br>
								TK. <?php echo $commonService->serviceTotalAmount; ?>
						</button>
					</div>
					<?php echo form_close(); ?>
				<?php endforeach; } ?>
				<?php if($meterAdditionalServices){ ?>
				<?php foreach($meterAdditionalServices as $additionalService): ?>
					<?php 
						echo form_open("admin/viewServiceCharge/{$additionalService->services_serviceId}/{$additionalService->serviceName}");
						echo form_hidden("client_id",$meterInfo->customerId);
						echo form_hidden("meter_id",$meterInfo->meterId);
					?>
					<div class="col-md-6">
						<button type="submit" class="btn btn-success btn-block btn-lg  m-bottom-15" >
							<?php echo $additionalService->serviceName; ?>
							<br>TK. <?php echo $additionalService->serviceTotalAmount; ?>
						</button>
					</div>
					<?php echo form_close(); ?>
				<?php endforeach; } ?>
			</div>
		</div>
	</div>
<?php include('footer.php') ?>