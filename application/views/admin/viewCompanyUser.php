<?php include('header.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">View Company User</h3>
		</div>
	</div>
	<?php include('messages.php'); ?>
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url('')?>">Dash Board</a></li>
				<li class="active">View Company User</li>
			</ol>
		</div>
	</div>
	<?php
	       echo form_open("admin/updateCompanyUser");
	       echo form_hidden("adminId",$companyUserDetails->adminID);
	//print_r($companyUserDetails);
	 ?>
	<div class="row">
		<div class="col-md-12">
			<div class="thumbnail">
				<div class="caption">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Full Name</label>
								<input type="text" class="form-control removeDisabled" name="adminName" value=" <?php echo $companyUserDetails->adminName; ?>" disabled />
							    <div class="red-text"><?php  echo form_error('adminName'); ?></div>
							</div>
						</div>			
					</div>				
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label>User ID</label>
								<input type="text" class="form-control " name="adminUserID" value="<?php echo $companyUserDetails->adminUserID; ?>" disabled />
							    <div class="red-text"><?php  echo form_error('adminUserID'); ?></div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>User Role</label>
								<select class="form-control removeDisabled" name="admin_role_roleID" disabled>
									<option value="<?php echo $companyUserDetails->admin_role_roleID ?>;" selected disabled ><?php echo $companyUserDetails->roleName ?></option>
									<option value="2">Admin</option>
									<!-- <option value="3">Manager</option> -->
									
								</select>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>Status</label>
								<select class="form-control removeDisabled" name="adminStatus" disabled>
									<option value="<?php  echo $companyUserDetails->adminStatus ?>" selected disabled ><?php if($companyUserDetails->adminStatus==1)echo "Active"; else echo "InActive"; ?></option>
									<option value="1">Active</option>
									<option value="0">Inactive</option>
								</select>
							</div>
						</div>						
					</div>				
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Note</label>
								<textarea type="text" class="form-control removeDisabled" name="adminNote" value="<?php echo $companyUserDetails->adminNote;?>" disabled><?php echo $companyUserDetails->adminNote;?></textarea>
							    <div class="red-text"><?php  echo form_error('adminNote'); ?></div>
							</div>
						</div>						
					</div>
					
					<div class="row">
						<div class="col-md-12">
							<button type="button" class="btn btn-primary" id="removeReadonlyButton"><i class="fa fa-pencil"></i> Edit</button>
							<button type="submit" class="btn btn-success showreadonly" style=""><i class="fa fa-thumbs-up"></i> Save</button>
							<button type="button"class="btn btn-warning showreadonly" id="addReadonlyButton" style=""><i class="fa fa-times" ></i> Cancel</button>
							<a class="btn btn-danger" onclick="window.history.back();"> <i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php include('footer.php') ?>