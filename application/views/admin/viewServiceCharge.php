	<?php include('header.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header"><?php echo urldecode($serviceName); ?></h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url('')?>">Dash Board</a></li>
				<li class="active">View Service Charge</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="thumbnail">
				<?php echo form_open("admin/viewServiceCharge/{$service_id}/{$serviceName}"); ?>
				<div class="caption">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Date From</label>
								<input type="text" class="form-control" name="startDate" value="<?php if($inputVal && $inputVal['startDate']) { echo $inputVal['startDate'];} ?>" id="startDate">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label> Date To</label>
								<input type="text" id="endDate" class="form-control" name="endDate" value="<?php if( $inputVal && $inputVal['endDate']) echo $inputVal['endDate']; ?>">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-5">
							<div class="form-group">
								<label> Select Client</label>
								<select class="form-control forselect2" name="client_id" id="organizationID">
									<option value=""> Select an Option</option>
									<?php foreach($organizations as $organization): ?>
									<?php if($inputVal && $inputVal['client_id'] && $organization->customerId==$inputVal['client_id'] ) { ?>
										<option value="<?php echo $organization->customerId ?>" selected ><?php echo $organization->organizationName ?></option>
									<?php } else{ ?>
										<option value="<?php echo $organization->customerId ?>"><?php echo $organization->organizationName ?></option>
									<?php } ?>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<?php if($inputVal && $inputVal['meter_id']) { ?> <input type="hidden" value="<?php echo $inputVal['meter_id']; ?>" id="hiddenMeterID" /> <?php } ?>
						<div class="col-md-5">
							<div class="form-group">
								<label> Select Meter</label>
								<select class="form-control" name="meter_id" id="clientMeterNo">
									<option value=""> Select an Option</option>
									
								</select>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group" style="margin-top:25px;">
								<button type="submit" id="" class="btn btn-success"> Search</button>
							</div>
						</div>
					</div>
				</div>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="thumbnail">
				<div class="caption">
					<div class="row">
						<div class="col-md-6">
							<table class="table table condensed table-striped">
								<thead>
									<tr>
										<th>Date-Time</th>
										<th>Customer</th>
										<th>Charged</th>
									</tr>
								</thead>
								<?php $totalCharged=0; ?>
								<?php if($serviceCharged){  ?>
								<tbody>
									<?php if($service_id==0){ ?>
										<?php foreach($serviceCharged as $charged): ?>
												<tr>
													<td><?php echo date('Y-m-d h:i a', strtotime($charged->currentBillAddedDate)); ?></td>
													<td><?php echo $charged->organizationName; ?></td>
													<td><?php echo round($charged->totalEnergyCharge); ?></td>
												</tr>
												<?php $totalCharged=$totalCharged+$charged->totalEnergyCharge; ?>
										<?php endforeach; ?>
									<?php } else{ ?>
										<?php foreach($serviceCharged as $charged): ?>
												<tr>
													<td><?php echo date('Y-m-d h:i a', strtotime($charged->serviceBillAddedDate)); ?></td>
													<td><?php echo $charged->organizationName; ?></td>
													<td><?php echo round($charged->serviceAmount); ?></td>
												</tr>
												<?php $totalCharged=$totalCharged+$charged->serviceAmount; ?>
										<?php endforeach; ?>
									<?php } ?>
								</tbody>
								<?php } ?>
							</table>
						</div>
						<div class="col-md-6">
							<table class="table table condensed table-striped">
								<thead>
									<tr>
										<th>Date-Time</th>
										<th>Customer</th>
										<th>Collected</th>
									</tr>
								</thead>
								<?php $totalPaid=0;  ?>
								<?php if($servicePayments){  ?>
								<tbody>
									<?php foreach($servicePayments as $info): ?>
									<tr>
										<td><?php echo date('Y-m-d h:i a', strtotime($info->serviceChargePaymentDate)); ?></td>
										<td><?php echo $info->organizationName; ?></td>
										<td><?php echo round($info->serviceChargePaidAmount); ?></td>
									</tr>
									<?php $totalPaid=$totalPaid+$info->serviceChargePaidAmount; ?>
									<?php endforeach; ?>
								</tbody>
								<?php } ?>
							</table>
						</div>
					</div>					
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="thumbnail">
				<div class="caption">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label> Total Charged</label>
								<input type="text" name="" value="<?php echo round($totalCharged); ?>" class="form-control" disabled>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label> Total Collected</label>
								<input type="text" name="" value="<?php echo round($totalPaid); ?>" class="form-control" disabled>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	
	<?php include('footer.php') ?>