﻿<!-- Modal -->
<div class="modal fade" id="officeBillModal<?php echo $currentBill->currentBillId ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Edit Office Bill</h4>
			</div>
			<?php 
				$attributes = array('id' =>$currentBill->currentBillId );
				echo form_open('', $attributes);
				echo form_hidden('bill_id',$currentBill->currentBillId);
				echo form_hidden('meter_info_meterId',$currentBill->meter_info_meterId);
			?>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label>Current Reading</label>
							<input type="text"  class="form-control span2"  name="currentReading" value="<?php echo $currentBill->currentBillCurrentReading ?>" placeholder="" required />
						</div>
					</div>
					<div class="col-md-12">
						<h3 class="page-header">Service Charges</h3>
					</div>
					<?php foreach($currentBill->services as $charge){  ?>
					<div class="col-md-4">
						<div class="form-group">
							<label><?php echo $charge->serviceName ?></label>
							<input type="text"  class="form-control span2"  name="<?php echo $charge->services_serviceId ?>"  value="<?php echo $charge->serviceAmount ?>" placeholder="" required />
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" onClick="updateOfficeBillInfo('<?php echo $currentBill->currentBillId  ?>')" class="btn btn-primary" name="submit" value="button" id="submitOfficeBill" data-dismiss="modal">Save changes</button>
				<button type="button" class="btn btn-default" name="close" data-dismiss="modal">Close</button>
			</div>
			<?php echo form_close();?>
		</div>
	</div>
</div>