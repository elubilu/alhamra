<?php include('header.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">Receive Payment</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<?php if($this->session->flashdata('feedback_successfull'))
			{ ?>
				<div class="alert alert-success alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true"><i class="fa fa-times"></i></span>
						</button>
					<strong>Success!</strong>
					<?php echo $this->session->flashdata('feedback_successfull'); ?>
				</div>
			<?php } 
			if($this->session->flashdata('feedback_failed'))
			{ ?>
				<div class="alert alert-danger alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true"><i class="fa fa-times"></i></span>
						</button>
					<strong>Oops!</strong>
					<?php echo $this->session->flashdata('feedback_failed'); ?>
				</div>
			<?php } ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url('')?>">
				Dash Board</a></li>
				<li class="active">Receive Payment</li>
			</ol>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<div class="thumbnail">
                <?php echo form_open("admin/storeReceivedPayment");?>
				<div class="caption">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Client</label>
								<select class="form-control forselect2" name="customer_info_customerId" id="clientIDValue" value="" required>
                                    <option readonly value="" selected> Select Client</option>
                                    <?php foreach ($organizations as $organization): ?>
								        <option value="<?php echo $organization->customerId; ?>"><?php echo $organization->organizationName." => ".$organization->spaceTitle ; ?></option>
                                    <?php endforeach;?>
								</select>`
							</div>
						</div>	
						<div class="col-md-6">
							<div class="form-group">
								<label>Purpose</label>
								<select class="form-control receivePaymentPurpose" id="receivePaymentPurpose" name="payment_types_paymentTypeId" required>
                                    <option readonly value="0" selected>Select Payment Purpose</option>
                                    <?php foreach ($payment_types as $payment_type): ?>
                                        <option value="<?php echo $payment_type->paymentTypeId; ?>"><?php echo $payment_type->paymentTypeName; ?></option>
                                    <?php endforeach;?>
								</select>
							</div>
						</div>								
					</div>
					<div class="row receivePaymentRestInput">
						<div class="col-md-4">
							<div class="form-group">
								<label>Meter No.</label>
								<select name="meter_id" id="meterNo" class="form-control ">
									<option value="" selected readonly>Select an Option</option>

								</select>
							</div>
						</div>
						<div class="col-md-4" id="energyChargeDiv">
							<div class="form-group">
								<label> Energy Charge</label>
								<input type="text" id="energyChargeValue" onChange="changeTotalAmount()" name="enargyCharge" value="" 
								class="form-control serviceInputClass" />
							</div>
						</div>
						
					</div>
					<input type="hidden" id="indexValue">
					<input type="hidden" class="form-control totalPaymentAmountValue" name="paymentAmount" value="" required />
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Amount</label>
								<input type="text" class="form-control receivePaymentAmount totalPaymentAmountValue" name="paymentAmount" value="" required />
							    <div class="red-text"><?php  echo form_error('paymentAmount'); ?></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Details</label>
								<textarea type="text" class="form-control" name="paymentDetails" value="" required ></textarea>
							</div>
						</div>						
					</div>
					
					<div class="row">
						<div class="col-md-12">
							<button type="submit" class="btn btn-success"><i class="fa fa-thumbs-up"> </i> Done</button>
							<button type="reset" class="btn btn-warning"><i class="fa fa-refresh"></i> Reset</button>
							<button type="button" class="btn btn-danger" onclick="window.history.back();"><i class="fa fa-arrow-left"></i> Back</button>		
						</div>
					</div>
				</div>
			    <?php echo form_close();?>
            </div>
		</div>
	</div>

<?php include('footer.php') ?>