<?php include('header.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">Add New Particular</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url('')?>">Dash Board</a></li>
				<li class="active">Add New Particular</li>
			</ol>
		</div>
	</div>
	<?php include('messages.php'); ?>
	
	<div class="row">
		<div class="col-md-12">
			<div class="thumbnail">
				<?php echo form_open('admin/storeParticular');
					echo form_hidden('particularStatus',1);
				?>
				<div class="caption">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Particular Title</label>
								<input type="text" class="form-control" name="particularTitle" value="" required />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Particular Type</label>
								<select class="form-control" name="particularType" id="" value="" required>>
									<option value="1"> Income</option>
									<option value="2"> Expense</option>
								</select>
							</div>
						</div>						
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Note</label>
								<textarea type="text" class="form-control" name="particularNote" value="" required></textarea>
							</div>
						</div>						
					</div>
					<div class="row">
						<div class="col-md-12">
							<button type="submit" class="btn btn-success"><i class="fa fa-thumbs-up"></i> Done</button>
							<button type="reset" class="btn btn-warning"><i class="fa fa-refresh"></i> Reset</button>
							<button type="button" class="btn btn-danger" onclick="window.history.back();"><i class="fa fa-arrow-left"></i> Back</button>		
						</div>
					</div>
				</div>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
	

<?php include('footer.php') ?>