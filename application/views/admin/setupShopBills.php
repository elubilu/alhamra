<?php include('header.php') ?>
	<div class="row">
		<div class="col-md-6">
			<h3 class="page-header">Setup Shop Bills</h3>
		</div>
		<div class="col-md-6 m-top-50">	
			<div class="pull-right">
				<!--<a href="<?php //echo base_url('admin/additionalServiceCharges');?>" class="btn btn-default">Additional Service Charges</a>-->
				<a href="<?php echo base_url('admin/serviceCharges');?>" class="btn btn-default">Service Charges</a>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<?php if($this->session->flashdata('feedback_successfull'))
					{ ?>
						<div class="alert alert-success alert-dismissible fade in" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true"><i class="fa fa-times"></i></span>
								</button>
							<strong>Success!</strong>
							<?php echo $this->session->flashdata('feedback_successfull'); ?>
						</div>
					<?php } 
					if($this->session->flashdata('feedback_failed'))
						{ ?>
							<div class="alert alert-danger alert-dismissible fade in" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true"><i class="fa fa-times"></i></span>
									</button>
								<strong>Oops!</strong>
								<?php echo $this->session->flashdata('feedback_failed'); ?>
							</div>
				<?php   } ?>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url('')?>">Dash Board</a></li>
				<li class="active">Setup Shop Bills</li>
			</ol>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<div class="thumbnail">
				<?php 
					echo form_open('admin/createShopBill');
					echo form_hidden('monthlyBillTypeFor',1);
					echo form_hidden('monthlyBillingStatus',1);
				?>
				<div class="caption">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Billing Month</label>
								<input type="text" size="16" class="form-control span2" id="date" name="billingMonthYear" placeholder="" required />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Per Unit Charge</label>
								<input type="text"  class="form-control span2"  name="unitCharge" placeholder="" required />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Current Reading Date</label>
								<input type="text" size="16" class="form-control span2" id="startDate" name="currentReadingDate" placeholder="" required />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Last Payment Date</label>
								<input type="text" size="16" class="form-control span2" id="endDate" name="lastPaymentDate" placeholder="" required />
							</div>
						</div>
					</div>

					<div class="row m-bottom-15">
						<div class="col-md-12">
							<h2 class="">Service Charges</h2>
						</div>
					</div>
					<div class="row">
						<?php foreach($charges as $charge){  ?>
						<div class="col-md-4">
							<div class="form-group">
								<label><?php echo $charge->serviceName ?></label>
								<input type="text"  class="form-control span2"  name="<?php echo $charge->serviceId ?>" placeholder="" required />
							</div>
						</div>
						<?php } ?>
					</div>
					
					<div class="row">
						<div class="col-md-12">
							<button type="submit" class="btn btn-success"><i class="fa fa-thumbs-up"></i> Add</button>
							<a type="button" onclick="window.history.back();" class="btn btn-danger"> <i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
						</div>
					</div>
				</div>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
	<?php if($bills){ ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">Previous Bills</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<table class="table table-striped">
				<thead class="dark-header">
					<tr>
						<th>Billing Year</th>
						<th>Billing Month</th>
						<th>Status</th>
						<th>View</th>
						<th>Services</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($bills as $bill){ 
						$billYear= date('Y', strtotime($bill->billingMonthYear));
						$billMonth= date('M', strtotime($bill->billingMonthYear));
					?>
					<tr>
						<td><?php echo $billYear ; ?></td>
						<td><?php echo $billMonth ; ?></td>
						<td><?php if($bill->monthlyBillingStatus==1)echo "Pending"; else echo "Final" ; ?></td>
						<td>
							<a href="<?php echo base_url("admin/pendingShopSetupBills/{$bill->mothlyBillInfoId}")?>" type="button"class="btn btn-primary btn-sm" style=""><i class="fa fa-info" ></i></a>
						</td>
						<td>
							<a href="<?php echo base_url("admin/editServiceCharges/{$bill->mothlyBillInfoId}")?>" type="button"class="btn btn-primary btn-sm" style=""><i class="fa fa-pencil" ></i></a>
						</td>
					</tr>	
					<?php } ?>
					
				</tbody>
			</table>
		</div>
	</div>
	<?php } ?>
	<!--<div class="row">
		<div class="col-md-12">
			<div class="panel panel-custom">
				<div class="panel-heading">Transaction History</div>
				<div class="panel-body">
					<table class="table table-hoverable">
						<thead>
							<tr>
								<th>Date & Time of Entry</th>
								<th>Transaction Date</th>
								<th>Details</th>
								<th>Deposit</th>
								<th>Withdrawal</th>
								<th>Balance</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>22-05-2017, 01:55 PM</td>
								<td>22-05-2017</td>
								<td>Details of the trasaction</td>
								<td>50000</td>
								<td>25000</td>
								<td>25000</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>-->
<?php include('footer.php') ?>