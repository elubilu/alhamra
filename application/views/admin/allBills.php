<?php include('header.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">All Bills</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<?php if($this->session->flashdata('feedback_successfull'))
					{ ?>
						<div class="alert alert-success alert-dismissible fade in" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true"><i class="fa fa-times"></i></span>
								</button>
							<strong>Success!</strong>
							<?php echo $this->session->flashdata('feedback_successfull'); ?>
						</div>
					<?php } 
					if($this->session->flashdata('feedback_failed'))
						{ ?>
							<div class="alert alert-danger alert-dismissible fade in" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true"><i class="fa fa-times"></i></span>
									</button>
								<strong>Oops!</strong>
								<?php echo $this->session->flashdata('feedback_failed'); ?>
							</div>
				<?php   } ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url('')?>">Dash Board</a></li>
				<li class="active">All Bills</li>
			</ol>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<div class="thumbnail">
				<div class="caption">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Date From</label>
								<input type="text" size="16" class="form-control span2" id="startDate" name="startDate" placeholder="" />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Date To</label>
								<input type="text" size="16" class="form-control span2" id="endDate" name="endDate" placeholder="" />
							</div>
						</div>						
					</div>
					<div class="row">
						<div class="col-md-12">
							<button type="submit" class="btn btn-success"><i class="fa fa-thumbs-up"> Done</i></button>
							<button type="reset" class="btn btn-warning"><i class="fa fa-refresh"> Reset</i></button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php if($bills){ ?>
	<div class="row filterable">
		<div class="col-md-12">
			<div class="row m-bottom-10">
				<div class="col-md-12 col-xs-12">
					<div class="pull-right">
						<button id="filter_button" class="btn btn-default btn-filter"><i class="fa fa-filter"></i> Filter</button>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="table table-striped">
						<thead class="dark-header">
							<tr class="filters">
								<th>
									<input type="text" class="form-control text-left" placeholder="Billing Year" disabled data-toggle="true">
								</th>
								<th>
									<input type="text" class="form-control text-left" placeholder="Billing Month" disabled data-toggle="true">
								</th>
								<th>
									<input type="text" class="form-control text-left" placeholder="Status" disabled data-toggle="true">
								</th>
								<th>
									<input type="text" class="form-control text-left" placeholder="Bill Type" disabled data-toggle="true">
								</th>						
								<th>
									<span>View</span>
								</th>						
								<th>
									<span>Print</span>
								</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($bills as $bill){ 
								$billYear= date('Y', strtotime($bill->billingMonthYear));
								$billMonth= date('M', strtotime($bill->billingMonthYear));
							?>
							<tr>
								<td><?php echo $billYear ; ?></td>
								<td><?php echo $billMonth ; ?></td>
								<td><?php if($bill->monthlyBillingStatus==1)echo "Pending"; else echo "Final" ; ?></td>
								<td><?php if($bill->monthlyBillTypeFor==1)echo "Shop Bill"; else echo "Office Bill" ; ?></td>
								<?php if($bill->monthlyBillTypeFor==1){ ?>
								<td>
									<a href="<?php echo base_url("admin/pendingShopSetupBills/{$bill->mothlyBillInfoId}")?>" type="button"class="btn btn-primary btn-sm" style=""><i class="fa fa-info" ></i></a>
								</td>
								<?php } ?> 
								<?php if($bill->monthlyBillTypeFor==2){ ?>
								<td>
									<a href="<?php echo base_url("admin/pendingOfficeSetupBills/{$bill->mothlyBillInfoId}") ?>" type="button"class="btn btn-primary btn-sm" style=""><i class="fa fa-info" ></i></a>
								</td>
								<?php } ?> 
								<?php if($bill->monthlyBillingStatus==1){ ?>
								<td>
									<p>Pending</p>
								</td>
								<?php } else{ ?>
								<td>
									<a href="<?php echo base_url("admin/printAll/{$bill->mothlyBillInfoId}") ?>"type="button" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Print"><i class="fa fa-print"></i></a>
								</td>
								<?php } ?>
							</tr>	
							<?php } ?>
							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<?php } ?>
	<div class="row">
		<div class="col-md-12">
			<button type="button" class="btn btn-danger" onclick="window.history.back();"><i class="fa fa-arrow-left"></i> Back</button>			
		</div>
	</div>
<?php include('footer.php') ?>