<?php include('header.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">Service Charge History</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url('')?>">Dash Board</a></li>
				<li class="active">Service Charge History</li>
			</ol>
		</div>
	</div>	
	<div class="row filterable">
		<div class="col-md-12">
			<div class="row m-bottom-10">
				<div class="col-md-12 col-xs-12">
					<div class="pull-right">
						<button id="filter_button" class="btn btn-default btn-filter"><i class="fa fa-filter"></i> Filter</button>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="table table-striped">
						<thead class="dark-header">
							<tr class="filters">
								<th>
									<input type="text" class="form-control text-left" placeholder="Date" disabled data-toggle="true">
								</th>
								<th>
									<input type="text" class="form-control text-left" placeholder="Details" disabled data-toggle="true">
								</th>
								<th>
									<input type="text" class="form-control text-left" placeholder="Charges" disabled data-toggle="true">
								</th>
								<th>
									<input type="text" class="form-control text-left" placeholder="Payments" disabled data-toggle="true">
								</th>	
								<th>
									<input type="text" class="form-control text-left" placeholder="Due" disabled data-toggle="true">
								</th>						
								<th>
									<span>View</span>
								</th>
							</tr>
						</thead>
						<?php if($bills){ ?>
						<tbody>
							<?php foreach($bills as $bill): ?>
								<tr>
									<td><?php echo $bill->transectionDate ?></td>
									<td><?php echo $bill->transectionDetails ?></td>
									<td><?php if($bill->TransectionType==2) echo $bill->transectionAmount; else echo "__"; ?></td>
									<td><?php if($bill->TransectionType==4) echo $bill->transectionAmount; else echo "__"; ?></td>
									<td><?php if($bill->TransectionType==2) echo $bill->transectionPrevDue+$bill->transectionAmount; else echo $bill->transectionPrevDue-$bill->transectionAmount; ?></td>
									<td><a href="<?php echo base_url('admin/viewLedgerEntry');?>" type="button"class="btn btn-primary btn-sm" style=""><i class="fa fa-info" ></i></a></td>
								</tr>	
							<?php endforeach; ?>
						</tbody>
						<?php } ?>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<button type="button" class="btn btn-danger" onclick="window.history.back();"><i class="fa fa-arrow-left"></i> Back</button>			
		</div>
	</div>
<?php include('footer.php') ?>