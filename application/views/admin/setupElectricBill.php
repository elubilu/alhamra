<?php include('header.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">Setup Electric Bill</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url('')?>">Dash Board</a></li>
				<li class="active">Setup Electric Bill</li>
			</ol>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<div class="thumbnail">
				<div class="caption">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Billing Month</label>
								<select class="form-control">
									<option>January</option>
									<option>March</option>
									<option>April</option>
									<option>May</option>
									<option>June</option>
									<option>July</option>
									<option>August</option>
									<option>September</option>
									<option>October</option>
									<option>November</option>
									<option>December</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<button type="submit" class="btn btn-success"><i class="fa fa-thumbs-up"> Save</i></button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<table class="table table-striped">
				<thead class="dark-header">
					<tr>
						<th>Space ID</th>
						<th>Organization</th>
						<th>Meter No</th>
						<th>Previous Reading</th>
						<th>Current Reading</th>
						<th>Demand Charge</th>
						<th>Service Charge</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>22-05-2017, 01:55 PM</td>
						<td>Details of the trasaction</td>
						<td>5000</td>
						<td>25000</td>
						<td>2500</td>
						<td>2500</td>
						<td>2500</td>
					</tr>					
				</tbody>
			</table>
		</div>
	</div>
	<!--<div class="row">
		<div class="col-md-12">
			<div class="panel panel-custom">
				<div class="panel-heading">Transaction History</div>
				<div class="panel-body">
					<table class="table table-hoverable">
						<thead>
							<tr>
								<th>Date & Time of Entry</th>
								<th>Transaction Date</th>
								<th>Details</th>
								<th>Deposit</th>
								<th>Withdrawal</th>
								<th>Balance</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>22-05-2017, 01:55 PM</td>
								<td>22-05-2017</td>
								<td>Details of the trasaction</td>
								<td>50000</td>
								<td>25000</td>
								<td>25000</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>-->
<?php include('footer.php') ?>