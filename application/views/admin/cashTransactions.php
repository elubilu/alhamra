<?php include('header.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">Cash Transactions</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url('')?>">Dash Board</a></li>
				<li class="active">Cash Transactions</li>
			</ol>
		</div>
	</div>
	<?php include('messages.php'); ?>
	<div class="row filterable">
		<div class="col-md-12">
			<div class="row m-bottom-10">
				<div class="col-md-12 col-xs-12">
					<div class="pull-right">								
						<a href="<?php echo base_url('admin/receivedPayment')?>" style="margin-top:0px;margin-right:5px;" type="button" class="btn btn-default">Receive Payment</a>
						<a href="<?php echo base_url('admin/cashInHand')?>" style="margin-top:0px;margin-right:5px;" type="button" class="btn btn-default">Cash in Hand</a>
						<a href="<?php echo base_url('admin/pettyCash')?>" style="margin-top:0px;margin-right:5px;" type="button" class="btn btn-default">Petty Cash</a>
						<a href="<?php echo base_url('admin/addNewCashTransfer')?>" style="margin-top:0px;margin-right:5px;" type="button" class="btn btn-default">Add New Cash Transfer</a>
						<a href="<?php echo base_url('admin/bankAccounts')?>" style="margin-top:0px;margin-right:5px;" type="button" class="btn btn-default">Bank Accounts</a>
						<button id="filter_button" class="btn btn-default btn-filter"><i class="fa fa-filter"></i> Filter</button>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="table table-striped">
						<thead class="dark-header">
							<tr class="filters">
								<th>
									<input type="text" class="form-control text-left" placeholder="Entry Date-Time" disabled data-toggle="true">
								</th>
								<th>
									<input type="text" class="form-control text-left" placeholder="Date" disabled data-toggle="true">
								</th>
								<th>
									<input type="text" class="form-control text-left" placeholder="Withdrawn From" disabled data-toggle="true">
								</th>
								<th>
									<input type="text" class="form-control text-left" placeholder="Transferred / Deposited to" disabled data-toggle="true">
								</th>
								<th>
									<input type="text" class="form-control text-left" placeholder="Amount" disabled data-toggle="true">
								</th>								
								<th>
									<span>View</span>
								</th>
							</tr>
						</thead>
						<?php if($infos){ ?>
						<tbody>
							<?php foreach($infos as $info): 
								$info->transactionDate=date('d/m/Y', strtotime($info->transactionDate)); 
								$info->transactionEntryDate=date('d/m/Y, h:ia', strtotime($info->transactionEntryDate)); 
							?>
							<tr>
								<td><?php echo $info->transactionEntryDate ?></td>
								<td><?php echo $info->transactionDate ?></td>
								<td><?php echo $info->transactionFrom ?></td>
								<td><?php echo $info->transactionTo ?></td>
								<td><?php echo $info->transactionAmount	 ?></td>
								<td>
									<a href="<?php echo base_url("admin/{$info->transactionDetailslink}/{$info->transactionReferenceId}/{$info->transactionType}")?>" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Details"><i class="fa fa-info"></i></a>
								</td>
							</tr>
							<?php endforeach; ?>
						</tbody>
						<?php } ?>
					</table>
				</div>
			</div>
		</div>
	</div>

<?php include('footer.php') ?>