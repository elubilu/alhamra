<?php include('header.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">All Particular</h3>
		</div>
	</div>
	<?php include('messages.php'); ?>
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url('')?>">Dash Board</a></li>
				<li class="active">All Particular</li>
			</ol>
		</div>
	</div>
	
	<div class="row filterable">
		<div class="col-md-12">
			<div class="row m-bottom-10">
				<div class="col-md-12 col-xs-12">
					<div class="pull-right">
						<a href="<?php echo base_url('admin/addNewParticular')?>" style="margin-top:0px;margin-right:5px;" type="button" class="btn btn-default">Add New Particular</a>
						<button id="filter_button" class="btn btn-default btn-filter"><i class="fa fa-filter"></i> Filter</button>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="table table-striped">
						<thead class="dark-header">
							<tr class="filters">
								<th>
									<input type="text" class="form-control text-left" placeholder="Particular Title" disabled data-toggle="true">
								</th>
								<th>
									<input type="text" class="form-control text-left" placeholder="Particular Type" disabled data-toggle="true">
								</th>
								<th>
									<input type="text" class="form-control text-left" placeholder="Note" disabled data-toggle="true">
								</th>
								<th>
									<input type="text" class="form-control text-left" placeholder="Status" disabled data-toggle="true">
								</th>								
								<th>
									<span>View</span>
								</th>
							</tr>
						</thead>
						<tbody>
							<?php if($infos){
								foreach($infos as $info): ?>
							<tr>
								<td><?php  echo $info->particularTitle ?></td>
								<td><?php   if($info->particularType == 1) echo "Income"; else echo "Expense";?></td>
								<td><?php echo $info->particularNote ?></td>
								<td><?php if($info->particularStatus == 1) echo "Active"; else echo "InActive"; ?></td>
								<td>
									<a href="<?php echo base_url("admin/viewParticular/{$info->particularId}")?>" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Details"><i class="fa fa-info"></i></a>
								</td>
							</tr>
							<?php endforeach; } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<button type="button" class="btn btn-danger" onclick="window.history.back();"><i class="fa fa-arrow-left"></i> Back</button>			
		</div>
	</div>

<?php include('footer.php') ?>