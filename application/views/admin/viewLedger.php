<?php include('header.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">View Ledger</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url('admin/index')?>">Dash Board</a></li>
				<li class="active">View Ledger</li>
			</ol>
		</div>
	</div>
	<?php //print_r($info); ?>
	<div class="row">
		<div class="col-md-12">
			<div class="thumbnail">
				<div class="caption">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Ledger Title</label>
								<input type="text" class="form-control" name="" value="<?php echo $info->ledgerTitle ?>" disabled />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Particular</label>
								<input type="text" class="form-control" name="" value="<?php echo $info->particularTitle ?>" disabled />
							</div>
						</div>	
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Ledger Type</label>
								<input type="text" class="form-control" name="" value="<?php if($info->ledgerType==1) echo "Income"; else echo "Expense"; ?>" disabled />
							</div>
						</div>	
						<div class="col-md-6">
							<div class="form-group">
								<label>Ledger Balance</label>
								<input type="text" class="form-control" name="" value="<?php echo $info->ledgerBalance ?>" disabled />
							</div>
						</div>					
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Note</label>
								<textarea type="text" class="form-control" name="" value="<?php echo $info->ledgerNote ?>" disabled><?php echo $info->ledgerNote ?></textarea>
							</div>
						</div>						
					</div>
					<div class="row">
						<div class="col-md-12">
							<a class="btn btn-danger" onclick="window.history.back();"> <i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row filterable">
		<div class="col-md-12">
			<div class="row m-bottom-10">
				<div class="col-md-6 col-xs-12">							
					<h3>Ledger Entries</h3>
				</div>
				<div class="col-md-6 col-xs-12 m-top-20">
					<div class="pull-right">
						<button id="filter_button" class="btn btn-default btn-filter"><i class="fa fa-filter"></i> Filter</button>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="table table-striped">
						<thead class="dark-header">
							<tr class="filters">
								<th>
									<input type="text" class="form-control text-left" placeholder="Entry Date-Time" disabled data-toggle="true">
								</th>
								<th>
									<input type="text" class="form-control text-left" placeholder="Transaction Date" disabled data-toggle="true">
								</th>
								<th>
									<input type="text" class="form-control text-left" placeholder="Details" disabled data-toggle="true">
								</th>
								<th>
									<input type="text" class="form-control text-left" placeholder="Amount" disabled data-toggle="true">
								</th>	
								<th>
									<input type="text" class="form-control text-left" placeholder="Balance" disabled data-toggle="true">
								</th>	
								<th>
									<span>View</span>
								</th>
							</tr>
						</thead>
						<?php if($ledgers): ?>
						<?php $totalBalence=$info->ledgerBalance; ?>
						<tbody>
							<?php foreach($ledgers as $ledger):
								$ledger->ledgerDetailsAddedDate=date('d/m/Y', strtotime($ledger->ledgerDetailsAddedDate)); 
								$date=date('d/m/Y, h:ia', strtotime($ledger->ledgerDetailsAddedDate)); 
							?>
							<tr>
								<td><?php echo $ledger->ledgerDetailsAddedDate  ?></td>
								<td><?php echo $date  ?></td>
								<td><?php echo $ledger->ledgerDetails  ?></td>
								<td><?php echo $ledger->ledgerDetailsAmount ?></td>
								<td><?php echo $totalBalence ?></td>
								<?php $type=$ledger->ledgerDetailsType+2; ?>
								<td>
									<a href="<?php echo base_url("admin/viewLedgerEntry/{$ledger->ledgerDetailsId}/{$type}")?>" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Details"><i class="fa fa-info"></i></a>
								</td>
							</tr>
							<?php 
								if($totalBalence>=0){
									if($ledger->ledgerDetailsType==1){
									$totalBalence=$totalBalence-$ledger->ledgerDetailsAmount;
									} 
									else{
									$totalBalence=$totalBalence+$ledger->ledgerDetailsAmount;
									} 
								}
								else{
									if($ledger->ledgerDetailsType==1){
									$totalBalence=$totalBalence+$ledger->ledgerDetailsAmount;
									} 
									else{
									$totalBalence=$totalBalence-$ledger->ledgerDetailsAmount;
									} 
								}
							?>
							<?php endforeach; endif; ?>							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
<?php include('footer.php') ?>