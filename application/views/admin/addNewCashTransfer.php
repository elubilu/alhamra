<?php include('header.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">Add New Cash Transfer</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url('')?>">Dash Board</a></li>
				<li class="active">Add New Cash Transfer</li>
			</ol>
		</div>
	</div>
	<?php include('messages.php'); ?>
	<div class="row">
		<div class="col-md-12">
			<div class="thumbnail">
				<?php echo form_open('admin/storeCashTransfer'); ?>
				<div class="caption">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Withdrawn From</label>
								<select class="form-control forselect2 cashTransferFrom" name="cashWithdrawnFrom" value="" required>
									<option value=""  id="fromDisabledOption" selected>Select an Option</option>
									<option value="-1">Cash In Hand</option>
									<option value="0">Petty Cash</option>
									<?php foreach($bank as $withdrawn): ?>
										<option value="<?php echo $withdrawn->bankAccountId;  ?>"><?php echo $withdrawn->bankAccountTitle;  ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Money Transferred / Deposited To</label>
								<select class="form-control forselect2 cashTransferTo" name="cashTransferredTo" value=""required>
									<option value="" id="toDisabledOption"  selected>Select an Option</option>
									<option value="-1">Cash In Hand</option>
									<option value="0">Petty Cash</option>
									<?php foreach($bank as $deposited ): ?>
										<option value="<?php echo $deposited ->bankAccountId;  ?>"><?php echo $deposited ->bankAccountTitle;  ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>						
					</div>
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label>Amount</label>
								<input type="text" class="form-control" name="cashTransferAmount" value="" required />
							    <div class="red-text"><?php  echo form_error('cashTransferAmount'); ?></div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>Cheque/Slip Number</label>
								<input type="text" class="form-control" name="cashSlipNo" value="" required/>
							</div>
						</div>	
						<div class="col-md-4">
							<div class="form-group">
								<label>Date</label>
								<input type="text" size="16" class="form-control span2" id="startDate" name="cashTransferDate" placeholder="" required />
							</div>
						</div>					
					</div>					
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Note</label>
								<textarea type="text" class="form-control" name="cashTransferNote" value="" required></textarea>
							</div>
						</div>						
					</div>
					
					<div class="row">
						<div class="col-md-12">
							<button type="submit" class="btn btn-success"><i class="fa fa-thumbs-up"> </i> Done</button>
							<button type="submit" class="btn btn-warning"><i class="fa fa-refresh"> </i> Reset</button>
							<button type="button" class="btn btn-danger" onclick="window.history.back();"><i class="fa fa-arrow-left"></i> Back</button>
						</div>
					</div>
				</div>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>

<?php include('footer.php') ?>