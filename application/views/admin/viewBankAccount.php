			<?php include('header.php') ?>
				<div class="row">
					<div class="col-md-12">
						<h3 class="page-header">View Bank Account</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<ol class="breadcrumb">
							<li><a href="<?php echo base_url('')?>">Dash Board</a></li>
							<li class="active">View Bank Account</li>
						</ol>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<?php if($this->session->flashdata('feedback_successfull'))
							{ ?>
								<div class="alert alert-success alert-dismissible fade in" role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true"><i class="fa fa-times"></i></span>
										</button>
									<strong>Success!</strong>
									<?php echo $this->session->flashdata('feedback_successfull'); ?>
								</div>
							<?php } 
							if($this->session->flashdata('feedback_failed'))
								{ ?>
									<div class="alert alert-danger alert-dismissible fade in" role="alert">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true"><i class="fa fa-times"></i></span>
											</button>
										<strong>Oops!</strong>
										<?php echo $this->session->flashdata('feedback_failed'); ?>
									</div>
						<?php   } ?>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="thumbnail">
							<?php echo form_open('admin/updateBankAccount');
								echo form_hidden('id',$info->bankAccountId);
							?>
							<div class="caption">
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label>Account Title</label>
											<input type="text" class="form-control removeDisabled" name="bankAccountTitle" value="<?php echo $info->bankAccountTitle ?>" disabled />
										      <div class="red-text"><?php  echo form_error('bankAccountTitle'); ?></div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label>Bank Name</label>
											<input type="text" class="form-control removeDisabled" name="bankName" value="<?php echo $info->bankName ?>" disabled />
										      <div class="red-text"><?php  echo form_error('bankName'); ?></div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label>Branch</label>
											<input type="text" class="form-control removeDisabled" name="bankAccountBranchName" value="<?php echo $info->bankAccountBranchName ?>" disabled />
										    <div class="red-text"><?php  echo form_error('bankAccountBranchName'); ?></div>
										</div>
									</div>						
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label>Account Holder Name</label>
											<input type="text" class="form-control removeDisabled" name="bankAccountHolderName" value="<?php echo $info->bankAccountHolderName ?>" disabled />
										    <div class="red-text"><?php  echo form_error('bankAccountHolderName'); ?></div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label>Account Number</label>
											<input type="text" class="form-control removeDisabled" name="bankAccountNumber" value="<?php echo $info->bankAccountNumber ?>" disabled />
										    <div class="red-text"><?php  echo form_error('bankAccountNumber'); ?></div>
										</div>
									</div>						
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label>Routing Number</label>
											<input type="text" class="form-control removeDisabled" name="bankAccountRoutingNumber" value="<?php echo $info->bankAccountRoutingNumber ?>" disabled />
										    <div class="red-text"><?php  echo form_error('bankAccountRoutingNumber'); ?></div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label>Current Balance</label>
											<input type="text" class="form-control " name="bankAccountLastBalance" value="<?php echo $info->bankAccountLastBalance ?>" disabled />
										    <div class="red-text"><?php  echo form_error('bankAccountLastBalance'); ?></div>
										</div>
									</div>						
								</div>
									<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label>Note</label>
											<textarea type="text" class="form-control removeDisabled" name="bankAccountNote" value="<?php echo $info->bankAccountNote ?>" disabled><?php echo $info->bankAccountNote ?></textarea>
										    <div class="red-text"><?php  echo form_error('bankAccountNote'); ?></div>
										</div>
									</div>						
								</div>
								<div class="row">
									<div class="col-md-12">
										<button type="button" class="btn btn-primary" id="removeReadonlyButton"><i class="fa fa-pencil"></i> Edit</button>

										<button type="submit" class="btn btn-success showreadonly" style=""><i class="fa fa-thumbs-up"></i> Save</button>

										<button type="button"class="btn btn-warning showreadonly" id="addReadonlyButton" style=""><i class="fa fa-times" ></i> Cancel</button>
										
										<a type="button" onclick="window.history.back();" class="btn btn-danger"> <i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
									</div>
								</div>
							</div>
							<?php echo form_close(); ?>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<h3 class="page-header">Transaction History</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<table class="table table-striped">
							<thead class="dark-header">
								<tr>
									<th>Entry Date-Time </th>
									<th>Transaction Date</th>
									<th>Details</th>
									<th>Deposit</th>
									<th>Withdrawal</th>
									<th>Balance</th>
								</tr>
							</thead>
							<?php if($bankTransections): ?>
							<tbody>
								<?php $totalBalence=$info->bankAccountLastBalance; ?>
								<?php foreach($bankTransections as $bankTransection):
									$bankTransection->bankDetailsTransactionDate=date('d-m-Y', strtotime($bankTransection->bankDetailsTransactionDate)); 
									$bankTransection->bankDetailsEntryDate=date('d-m-Y, h:ia', strtotime($bankTransection->bankDetailsEntryDate));
								?>
								<tr>
									<td><?php echo $bankTransection->bankDetailsEntryDate ?></td>
									<td><?php echo $bankTransection->bankDetailsTransactionDate ?></td>
									<td><?php echo $bankTransection->bankDetailsNote ?></td>
									<td><?php if($bankTransection->bankDetailsType==1) echo $bankTransection->bankDetailsBalance; else echo "--"  ?></td>
									<td><?php if($bankTransection->bankDetailsType==2) echo $bankTransection->bankDetailsBalance; else echo "--"  ?></td>
									<td><?php echo $totalBalence ?></td>
								</tr>
								<?php 
								if($totalBalence>=0){
									if($bankTransection->bankDetailsType==1){
									$totalBalence=$totalBalence-$bankTransection->bankDetailsBalance;
									} 
									else{
									$totalBalence=$totalBalence+$bankTransection->bankDetailsBalance;
									} 
								}
								else{
									if($bankTransection->bankDetailsType==1){
									$totalBalence=$totalBalence+$bankTransection->bankDetailsBalance;
									} 
									else{
									$totalBalence=$totalBalence-$bankTransection->bankDetailsBalance;
									} 
								}
							?>
								<?php endforeach; ?>
							</tbody>
							<?php endif; ?>
						</table>
					</div>
				</div>
			<?php include('footer.php') ?>