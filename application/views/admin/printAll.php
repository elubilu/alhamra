<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Al Hamra Shopping City</title>
	<!--<link href="https://fonts.googleapis.com/css?family=Nunito+Sans" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto+Slab:100,300,400,700" rel="stylesheet">-->



    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <style>
		* cyrillic-ext */
		@font-face {
		  font-family: 'Roboto Slab';
		  font-style: normal;
		  font-weight: 400;
		  src: local('Roboto Slab Regular'), local('RobotoSlab-Regular'), 
		  url(<?php echo base_url('assets/back-end/fonts/y7lebkjgREBJK96VQi37ZjTOQ_MqJVwkKsUn0wKzc2I.woff2');?>) format('woff2');
		  unicode-range: U+0460-052F, U+20B4, U+2DE0-2DFF, U+A640-A69F;
		}
		/* cyrillic */
		@font-face {
		  font-family: 'Roboto Slab';
		  font-style: normal;
		  font-weight: 400;
		  src: local('Roboto Slab Regular'), local('RobotoSlab-Regular'),
		   url(<?php echo base_url('assets/back-end/fonts/y7lebkjgREBJK96VQi37ZjUj_cnvWIuuBMVgbX098Mw.woff2');?>) format('woff2');
		  unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
		}
		/* greek-ext */
		@font-face {
		  font-family: 'Roboto Slab';
		  font-style: normal;
		  font-weight: 400;
		  src: local('Roboto Slab Regular'), local('RobotoSlab-Regular'), 
		  url(<?php echo base_url('assets/back-end/fonts/y7lebkjgREBJK96VQi37ZkbcKLIaa1LC45dFaAfauRA.woff2');?>) format('woff2');
		  unicode-range: U+1F00-1FFF;
		}
		/* greek */
		@font-face {
		  font-family: 'Roboto Slab';
		  font-style: normal;
		  font-weight: 400;
		  src: local('Roboto Slab Regular'), local('RobotoSlab-Regular'), 
		  url(<?php echo base_url('assets/back-end/fonts/y7lebkjgREBJK96VQi37Zmo_sUJ8uO4YLWRInS22T3Y.woff2');?>) format('woff2');
		  unicode-range: U+0370-03FF;
		}
		/* vietnamese */
		@font-face {
		  font-family: 'Roboto Slab';
		  font-style: normal;
		  font-weight: 400;
		  src: local('Roboto Slab Regular'), local('RobotoSlab-Regular'), 
		  url(<?php echo base_url('assets/back-end/fonts/y7lebkjgREBJK96VQi37Zr6up8jxqWt8HVA3mDhkV_0.woff2');?>) format('woff2');
		  unicode-range: U+0102-0103, U+1EA0-1EF9, U+20AB;
		}
		/* latin-ext */
		@font-face {
		  font-family: 'Roboto Slab';
		  font-style: normal;
		  font-weight: 400;
		  src: local('Roboto Slab Regular'), local('RobotoSlab-Regular'), 
		  url(<?php echo base_url('assets/back-end/fonts/y7lebkjgREBJK96VQi37ZiYE0-AqJ3nfInTTiDXDjU4.woff2');?>) format('woff2');
		  unicode-range: U+0100-024F, U+1E00-1EFF, U+20A0-20AB, U+20AD-20CF, U+2C60-2C7F, U+A720-A7FF;
		}
		/* latin */
		@font-face {
		  font-family: 'Roboto Slab';
		  font-style: normal;
		  font-weight: 400;
		  src: local('Roboto Slab Regular'), local('RobotoSlab-Regular'), 
		  url(<?php echo base_url('assets/back-end/fonts/y7lebkjgREBJK96VQi37Zo4P5ICox8Kq3LLUNMylGO4.woff2');?>) format('woff2');
		  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215;
		}
		body{
			~font-family: 'Nunito Sans', sans-serif;
			font-family: 'Roboto Slab', serif;
			font-weight:400;
			font-size: 15px;

		}
		.wrapper{
			width:560px;
			margin:0px auto;
			border:1px solid #000;
			color:#000;
		}
		p{
			margin-bottom: 10px;
			font-size: 15px;			
		}
		.print-area{
			margin:0px 8px;
		}
		.parint-header{
			text-align:center;
		}
		.parint-header h4{
			margin-bottom:-15px;
		}
		table {
			border-collapse: collapse;
			width: 100%;
			font-size: 17px;
			color:#000;
		}
		.elec-table td, .elec-table th {
			border: 1px solid #333;
			text-align: left;
			padding:0px 5px
		}
		.signature{
			margin-top:25px;
			text-align:center;
			position:relative;
		}
		.signature ul li{
			list-style:none;
			display:inline-block;
			width:150px;
		}
		.signature h5{
			border-top:1px solid #333;
		}
		.note{
			margin-top:-15px;
			font-size: 15px;
		}
		.developer{
			margin-top:0px;
			text-align:center;  
			font-size: 15px;			
		}
		@media print{
			body{
				font-size: 15px;
			}
			.wrapper{
				border:0px solid #000;
			}
            #nonPrintSection{
                display: none;
            }
            .developer{
            	page-break-after: always;
            }
        }
    </style>

	 <script src="<?php echo base_url('assets/back-end/js/jquery.min.js'); ?>"></script>
	 <script>
	 	var result = 1;
	 </script>
</head>

<body>
	<div id="nonPrintSection" style="text-transform:uppercase;">
            <span class=""><button onclick="window.history.back();" type="button"  style="width:5%; cursor:pointer; font-size:12px; background-color:red; color:#FFF; text-align: center; border:1px solid red; padding: 10px 1px; font-weight:bold; float:right; margin-left:5px;text-transform:none;text-decoration:none;">Back</button></span>
			<span class=""><button type="button" onClick="window.print();return false;" style="width:5%; cursor:pointer; font-size:12px; background-color:#337ab7; color:#FFF; text-align: center; border:1px solid #337ab7; padding: 10px 1px; font-weight:bold; float:right;">Print</button></span>
    </div>
    <?php $i = 1; ?>
	<?php foreach($currentBills as $currentBill){ ?>
	<div class="wrapper" style="margin-bottom: 10px;">	
		<div class="print-area">
			<div class="parint-header">
				<!--<img src="<?php //echo base_url('alhamra logo.pnglogo-extended.png')?>" alt="">-->
				<h2 style="margin-bottom:-15px;">Al Hamra Shopping City</h2>
				<h4>Zinda Bazar. Sylhet. Ph: 719612</h4>
				<p>( Electricity Bill, Service Charge, Phone Bill, Parking Bill and Other )</p>
			</div>
			<div class="print-body">
				<?php //print_r($currentBill); ?>
        <?php //print_r($bill[$currentBill->currentBillId]); ?>
        
        <?php //$lastDate = date('Y-m-d', strtotime($bill[$currentBill->currentBillId]->monthlyBillAddedDate. '+7 days')); 
        	//$lastDate = date('d/m/Y', strtotime($lastDate));
        ?>
        <?php $bill[$currentBill->currentBillId]->billingMonthYear= date('M, Y', strtotime($bill[$currentBill->currentBillId]->billingMonthYear)); ?>
        <?php $bill[$currentBill->currentBillId]->monthlyBillAddedDate= date('d/m/Y', strtotime($bill[$currentBill->currentBillId]->monthlyBillAddedDate)); ?>
				<table>
					<tbody>
						<tr>
							<td style="width:50%;">Bill No.: <?php echo $bill[$currentBill->currentBillId]->monthly_bill_info_mothlyBillInfoId ?></td>
							<td style="width:50%;text-align:right;">Date: <?php echo $bill[$currentBill->currentBillId]->monthlyBillAddedDate ?></td>
						</tr>
						<tr>
							<td style="width:50%;">Client ID: <?php echo $bill[$currentBill->currentBillId]->meterCustomerId ?>
							</td>
							<td style="width:50%;text-align:right;">Shop No.: <?php echo $bill[$currentBill->currentBillId]->spaceTitle ?></td>
						</tr>
						<tr>
							<td style="width:50%;">Organization: <?php echo $bill[$currentBill->currentBillId]->organizationName ?></td>
							<td style="width:50%;text-align:right;">Phone: <?php echo $bill[$currentBill->currentBillId]->customerPhone ?> </td>
						</tr>
						<tr>
							<td style="width:50%;"><b>Billing Month/Year: <?php echo $bill[$currentBill->currentBillId]->billingMonthYear ?></b></td>
							<td style="width:50%;text-align:right;"><b>Last Payment Date : <?php echo date('d/m/Y', strtotime($bill[$currentBill->currentBillId]->currentBillLastDate));?></b></td>
						</tr>
						
					</tbody>
				</table>
				<h4 style="text-align:center;margin-bottom:2px;font-size: 22px;margin-bottom: 10px;">Electricity Bill</h4>
				<?php $bill[$currentBill->currentBillId]->currentBillPrevReadingDate= date('d/m/Y ', strtotime($bill[$currentBill->currentBillId]->currentBillPrevReadingDate)); ?>
				<?php $bill[$currentBill->currentBillId]->currentBillCurrentReadingDate= date('d/m/Y ', strtotime($bill[$currentBill->currentBillId]->currentBillCurrentReadingDate)); ?>
				<table>
					<tbody>
						<tr>
							<td style="width:50%;">Previous Reading: <?php echo $bill[$currentBill->currentBillId]->currentBillPrevReading ?> </td>
							<td style="width:50%;text-align:right;">Date: <?php echo $bill[$currentBill->currentBillId]->currentBillPrevReadingDate ?> </td>
						</tr>
						<tr>
							<td style="width:50%;">Current Reading: <?php echo $bill[$currentBill->currentBillId]->currentBillCurrentReading ?> </td>
							<td style="width:50%;text-align:right;">Date : <?php echo $bill[$currentBill->currentBillId]->currentBillCurrentReadingDate ?></td>
						</tr>
						<tr>
							<td style="width:50%;">Used Unit: <?php echo ($bill[$currentBill->currentBillId]->currentBillCurrentReading-$bill[$currentBill->currentBillId]->currentBillPrevReading); ?></td>


							<td style="width:50%;text-align:right;">Meter No.: <?php //echo $bill[$currentBill->currentBillId]->meterNo ?>  000</td>
						</tr>
						
					</tbody>
				</table>
				<table class="elec-table">
					<?php
					$totalUnit=($bill[$currentBill->currentBillId]->currentBillCurrentReading-$bill[$currentBill->currentBillId]->currentBillPrevReading);
					if($totalUnit<46)
					$totalUnit=46;
					$energyCharge=$totalUnit*$bill[$currentBill->currentBillId]->perUnitCharge ?>
					<thead>
						<tr>
							<th style="width:70%;">Particulars</th>
							<th style="width:30%;text-align:right;">Amount </th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<?php
								$totalUnit=($bill[$currentBill->currentBillId]->currentBillCurrentReading-$bill[$currentBill->currentBillId]->currentBillPrevReading);
								if($totalUnit<46){
									$totalUnit=46; 
									echo "<td style='width:70%;'>Minimum Energy Charge  </td>";
								}
								else{
									echo "<td style='width:70%;'>Energy Charge  </td>";
								}
								$energyCharge = ceil($totalUnit*$bill[$currentBill->currentBillId]->perUnitCharge); 
							?>
							<td style="width:30%;text-align:right;">  <?php echo $energyCharge ?></td>
						</tr>
						<tr>
							<td style="width:70%;">Demand Charge  </td>
							<td style="width:30%;text-align:right;">   <?php echo $bill[$currentBill->currentBillId]->currentBillDemandCharge ?></td>
						</tr>
						<?php if($bill[$currentBill->currentBillId]->currentBillServiceCharge>0): ?>
							<tr>
								<td style="width:70%;">Service Charge  </td>
								<td style="width:30%;text-align:right;">  <?php echo $bill[$currentBill->currentBillId]->currentBillServiceCharge ?></td>
							</tr>
						<?php endif; ?>
						<?php $common_bill_amount=0; ?>
						
						<?php if($common_bill[$currentBill->currentBillId]){ ?>
						<tr>
							<td>
								 <?php echo $common_bill[$currentBill->currentBillId]->serviceName ?>
							</td>
							
							<td style="text-align:right;">
								<?php echo $common_bill[$currentBill->currentBillId]->serviceAmount ?>
							</td>
						</tr>
						<?php $common_bill_amount=$common_bill[$currentBill->currentBillId]->serviceAmount; ?>
						<?php } ?>

						<?php $govt_charge=0; ?>
						<?php if($govt_bill[$currentBill->currentBillId]){ ?>
						<tr>
							<td>
								 <?php echo $govt_bill[$currentBill->currentBillId]->serviceName ?>
							</td>
							
							<td style="text-align:right;">
								<?php echo $govt_bill[$currentBill->currentBillId]->serviceAmount ?>
							</td>
						</tr>
						<?php $govt_charge=$govt_bill[$currentBill->currentBillId]->serviceAmount; ?>
						<?php } ?>
					</tbody>
					<tfoot>
						<tr>
							<th style="width:70%;text-align:right;">Sub Total</th>
							<th style="width:30%;text-align:right;">  <?php echo round($energyCharge+$bill[$currentBill->currentBillId]->currentBillDemandCharge+$bill[$currentBill->currentBillId]->currentBillServiceCharge+$common_bill_amount+$govt_charge); ?></th>
						</tr>
					</tfoot>
				</table>
				<h4 style="text-align:center;margin-bottom:2px;font-size: 22px;margin-bottom: 10px;">Service Charge</h4>
				<table class="elec-table">
					<thead>
						<tr>
							<th style="width:70%;">Particulars</th>
							<th style="width:30%;text-align:right;">Amount </th>
						</tr>
					</thead>
					<tbody>
						<?php $total_service=0; $parking_fee=0; ?>
						<?php if($services[$currentBill->currentBillId]){ ?>
						<?php foreach($services[$currentBill->currentBillId] as $service): ?>
						<?php if($service->services_serviceId==3) $parking_fee=$service->serviceAmount; 
							else{
						?>
							<tr class="item">
								<td>
									<?php echo $service->serviceName ?>
								</td>
								<td style="text-align:right;">
									<?php echo $service->serviceAmount ?>
								</td>
							</tr>
						<?php 
							$total_service= $total_service+$service->serviceAmount;
						} endforeach; ?>
						<?php } ?>
					</tbody>
					<tfoot>
						<tr>
							<th style="width:70%;text-align:right;">Sub Total</th>
							<th style="width:30%;text-align:right;">  <?php  $total_service=round($total_service); echo $total_service;  ?></th>
						</tr>
					</tfoot>
				</table>
				<table class="elec-table" style="margin-top:10px;">
					<tbody>
						<tr>
							<td style="width:70%;text-align:right;">
								<b>Total Bill</b> 
							</td>
							
							<td style="width:30%;text-align:right;font-weight: bold; " id="totalBill<?php echo $i; ?>">
							<?php $totalBillAmount=$energyCharge+$bill[$currentBill->currentBillId]->currentBillDemandCharge+$bill[$currentBill->currentBillId]->currentBillServiceCharge+$common_bill_amount+$govt_charge+$total_service ?>
                    				<?php 
                    				$totalBillAmount=round($totalBillAmount);
                    				echo $totalBillAmount; 
                    				 ?>		
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="print-footer">
				<table style="margin-top:10px; font-weight: bold; text-transform: capitalize;">
					<tbody>
						<tr style="margin-top:40px;">
							<td style="width:20%;">In Words: </td>
							<td style="width:80%;text-align:left;" 
							id="totalBillInWords<?php echo $i; ?>"> </td>
						</tr>
						<?php if($parking_fee>0){ ?>
						<tr style="margin-top:40px;">
							<td style="width:20%;background-color: #000; color:#FFF;"><b>Parking Fee: </b></td>
							<td style="width:80%;text-align:left;background-color: #000; color:#FFF;" id=""> <b><?php echo $parking_fee; ?></b></td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
				<table class="signature" style="margin-top:28px;">
					<tbody>
						<tr>
							<td style="width:33.33%;padding:0px 10px;"><h5>Bill Maker</h5></td>
							<td style="width:33.33%;padding:0px 10px;"><h5>Bill Supervisor</h5></td>
							<td style="width:33.33%;padding:0px 10px;"><h5>Electrical Engineer</h5></td>
						</tr>
					</tbody>
				</table>
				<div class="note">
					<ul>
						<li>For avoiding disconnection, please pay the bill in time.</li>
						<li>After last pay date 5% charge will be added.</li>
					</ul>
				</div>
				<div class="developer">
					<h4>System Developed by StarLab IT - Contact: 01617827522</h4>
				</div>
			</div>
		</div>
	</div>
	

   
 	
	<script src="<?php echo base_url('assets/back-end/js/numberToWords.js');?>"></script>
	<script src="<?php echo base_url('assets/back-end/js/toWords.js'); ?>"></script>
	<script>

	var totalBill = document.getElementById("totalBill"+result).innerHTML;
	var totalBillInWords = numberToWords.toWords(totalBill);

 	document.getElementById("totalBillInWords"+result).innerHTML = totalBillInWords+' Taka Only';
 	result++;
	</script>

   <?php $i++; } ?>
</body>
</html>




































































































