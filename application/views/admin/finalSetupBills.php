<?php include('header.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">Final Bill Setup</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url('')?>">Dash Board</a></li>
				<li class="active">Final Bill Setup</li>
			</ol>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<div class="thumbnail">
				<div class="caption">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Billing Month</label>
								<input type="text" class="form-control" name="" value="" disabled />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">Electric Bill</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<table class="table table-striped">
				<thead class="dark-header">
					<tr>
						<th>Space ID</th>
						<th>Organization</th>
						<th>Meter No</th>
						<th>Previous Reading</th>
						<th>Current Reading</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>AL 20138</td>
						<td>ABCD Store</td>
						<td>2080</td>
						<td>1350</td>
						<td>1350</td>
					</tr>	
					
				</tbody>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">Service Charge</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<table class="table table-striped">
				<thead class="dark-header">
					<tr>
						<th>Details</th>
						<th>Amount</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>ABCD Store</td>
						<td>20000</td>
					</tr>	
					
				</tbody>
			</table>
		</div>
	</div>
<?php include('footer.php') ?>