<?php include('header.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">View Income Statements</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url('')?>">Dash Board</a></li>
				<li class="active">View Income Statements</li>
			</ol>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<div class="thumbnail">
				<div class="caption">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Date From</label>
								<input type="text" size="16" class="form-control span2" id="startDate" name="startDate" placeholder="" />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Date To</label>
								<input type="text" size="16" class="form-control span2" id="endDate" name="endDate" placeholder="" />
							</div>
						</div>						
					</div>
					<div class="row">
						<div class="col-md-12">
							<button type="submit" class="btn btn-success"><i class="fa fa-thumbs-up"> Done</i></button>
							<button type="submit" class="btn btn-warning"><i class="fa fa-refresh"> Reset</i></button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row filterable">
		<div class="col-md-12">
			<div class="row m-bottom-10">
				<div class="col-md-12 col-xs-12">
					<div class="pull-right">
						<button id="filter_button" class="btn btn-default btn-filter"><i class="fa fa-filter"></i> Filter</button>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="table table-striped">
						<thead class="dark-header">
							<tr class="filters">
								<th>
									<input type="text" class="form-control text-left" placeholder="Entry Date-Time" disabled data-toggle="true">
								</th>
								<th>
									<input type="text" class="form-control text-left" placeholder="Date" disabled data-toggle="true">
								</th>
								<th>
									<input type="text" class="form-control text-left" placeholder="Particular" disabled data-toggle="true">
								</th>
								<th>
									<input type="text" class="form-control text-left" placeholder="Ledger" disabled data-toggle="true">
								</th>
								<th>
									<input type="text" class="form-control text-left" placeholder="Details" disabled data-toggle="true">
								</th>
								<th>
									<input type="text" class="form-control text-left" placeholder="Amount" disabled data-toggle="true">
								</th>

								<th>
									<span>View</span>
								</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>AL56987420</td>
								<td>ABCD Store</td>
								<td>25000</td>
								<td>25000</td>
								<td>25000</td>
								<td>25000</td>
								<td>
									<a href="<?php echo base_url('admin/viewLedgerEntry')?>" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Details"><i class="fa fa-info"></i></a>
								</td>
							</tr>							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="row m-top-15">
		<div class="col-md-12">
			<div class="thumbnail">
				<div class="caption">
					<div class="form-group">
						<label>Total Amount</label>
						<input type="text" class="form-control" id="" name="" placeholder="" disabled />
					</div>					
				</div>
			</div>			
		</div>
	</div>

<?php include('footer.php') ?>