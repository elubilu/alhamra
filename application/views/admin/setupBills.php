<?php include('header.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">Setup Bills</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url('')?>">Dash Board</a></li>
				<li class="active">Setup Bills</li>
			</ol>
		</div>
	</div>
	
	<div class="row text-center m-top-25 setupBill">
		<div class="col-md-3">
			<a href="<?php echo base_url('admin/setupOfficeBills') ?>">
				<div class="thumbnail thumbOne">
					<div class="caption">
						<h4>Ofiice Bill</h4>
					</div>
				</div>
			</a>
		</div>
		<div class="col-md-3">
			<a href="<?php echo base_url('admin/setupShopBills') ?>">
				<div class="thumbnail thumbTwo">
					<div class="caption">
						<h4>Shop Bill</h4>
					</div>
				</div>
			</a>
		</div>
	</div>
<?php include('footer.php') ?>