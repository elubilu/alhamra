<?php include('header.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">View Transaction</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url('')?>">Dash Board</a></li>
				<li class="active">View Transaction</li>
			</ol>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<div class="thumbnail">
				<div class="caption">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Withdrawn From</label>
								<input type="text" class="form-control" name="" value="<?php echo $info->transactionFrom ?>" disabled />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Money Transferred / Deposited To</label>
								<input type="text" class="form-control" name="" value="<?php echo $info->transactionTo ?>" disabled />
							</div>
						</div>						
					</div>
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label>Amount</label>
								<input type="text" class="form-control" name="" value="<?php echo $info->cashTransferAmount ?>" disabled />
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>Cheque/Slip Number</label>
								<input type="text" class="form-control" name="" value="<?php echo $info->cashSlipNo ?>" disabled />
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<?php $info->cashTransferDate=date('d/m/Y', strtotime($info->cashTransferDate));  ?>
								<label>Date</label>
								<input type="text" class="form-control" name="" value="<?php echo $info->cashTransferDate; ?>" disabled />
							</div>
						</div>						
					</div>						
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Note</label>
								<textarea type="text" class="form-control" name="" value="<?php echo $info->cashTransferNote; ?>" disabled><?php echo $info->cashTransferNote; ?></textarea>
							</div>
						</div>						
					</div>
					
					<div class="row">
						<div class="col-md-12">
							<a class="btn btn-danger" onclick="window.history.back();"> <i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php include('footer.php') ?>