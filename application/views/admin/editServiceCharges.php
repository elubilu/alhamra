﻿<?php include('header.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">Edit Service Charges</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url('')?>">Dash Board</a></li>
				<li class="active">Edit Service Charges</li>
			</ol>
		</div>
	</div>
	<?php $billInfo->billingMonthYear= date('M, Y', strtotime($billInfo->billingMonthYear)); ?>
	<div class="row">
		<div class="col-md-12">
			<div class="thumbnail">
				<div class="caption">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Billing Month</label>
								<input type="text" class="form-control" name="" value="<?php echo $billInfo->billingMonthYear ?>" disabled/>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">Service Charge</h3>
		</div>
	</div>
	<?php 
		echo form_open('admin/updateServiceCharges');
		//echo form_hidden('serviceBillId',$service->serviceBillId); 
		echo form_hidden('bill_id',$billInfo->mothlyBillInfoId); 
	?>
	<div class="row">
		<div class="col-md-12">
			<table class="table table-striped">
				<thead class="dark-header">
					<tr>
						<th>Name</th>
						<th>Amount</th>
					</tr>
				</thead>
				
				<tbody>
					<?php foreach($services as $service){ ?>
					<tr>
						<td><?php echo $service->serviceName; ?></td>
						<?php if($billInfo->monthlyBillingStatus==1){ ?>
						<td>
							<input type="text" class="form-control input-sm " name="<?php echo $service->services_serviceId; ?>" value="<?php echo $service->serviceAmount ?>" />
						</td>
						<?php } else{ ?>
						<td>
							<input type="text" class="form-control input-sm " name="<?php echo $service->services_serviceId; ?>" value="<?php echo $service->serviceAmount ?>" disabled />
						</td>
						<?php } ?>
					</tr>	
					<?php } ?>
				</tbody>
				
			</table>
		</div>
	</div>
	<div class="row m-top-15">
		<div class="col-md-12">
			<?php if($billInfo->monthlyBillingStatus==1){ ?>
			<button type="submit" class="btn btn-success"><i class="fa fa-thumbs-up"> Save</i></button>
			<?php } ?>
			<button type="button" class="btn btn-danger" onclick="window.history.back();"><i class="fa fa-arrow-left"></i> Back</button>	
		</div>
	</div>
	<?php echo form_close(); ?>
<?php include('footer.php') ?>