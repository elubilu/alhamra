<?php include('header.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">Ledgers</h3>
		</div>
	</div>
	<?php include('messages.php'); ?>
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url('')?>">Dash Board</a></li>
				<li class="active">Ledgers</li>
			</ol>
		</div>
	</div>
	
	<div class="row filterable">
		<div class="col-md-12">
			<div class="row m-bottom-10">
				<div class="col-md-12 col-xs-12">
					<div class="pull-right">								
						<a href="<?php echo base_url('admin/addNewLedger')?>" style="margin-top:0px;margin-right:5px;" type="button" class="btn btn-default">Add New Ledger</a>
						<a href="<?php echo base_url('admin/newLedgerEntry')?>" style="margin-top:0px;margin-right:5px;" type="button" class="btn btn-default">New Ledger Entry</a>
						<a href="<?php echo base_url('admin/allparticular')?>" style="margin-top:0px;margin-right:5px;" type="button" class="btn btn-default">All Particular</a>
						<button id="filter_button" class="btn btn-default btn-filter"><i class="fa fa-filter"></i> Filter</button>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="table table-striped">
						<thead class="dark-header">
							<tr class="filters">
								<th>
									<input type="text" class="form-control text-left" placeholder="Ledger Title" disabled data-toggle="true">
								</th>
								<th>
									<input type="text" class="form-control text-left" placeholder="Particular" disabled data-toggle="true">
								</th>
								<th>
									<input type="text" class="form-control text-left" placeholder="Ledger Type" disabled data-toggle="true">
								</th>
								<th>
									<input type="text" class="form-control text-left" placeholder="Balance" disabled data-toggle="true">
								</th>								
								<th>
									<span>View</span>
								</th>
							</tr>
						</thead>
						<tbody>
							<?php if($infos){
								foreach($infos as $info): ?>
							<tr>
								<td><?php echo $info->ledgerTitle ?></td>
								<td><?php echo $info->particularTitle ?></td>
								<td><?php if($info->ledgerType==1) echo "Income"; else echo "Expense"; ?></td>
								<td><?php echo $info->ledgerBalance ?></td>
								<td>
									<a href="<?php echo base_url("admin/viewLedger/{$info->ledgerId}")?>" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Details"><i class="fa fa-info"></i></a>
								</td>
							</tr>
							<?php endforeach; } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

<?php include('footer.php') ?>