<?php include('header.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">Bill Collections</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url('')?>">Dash Board</a></li>
				<li class="active">Bill Collections</li>
			</ol>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<div class="thumbnail">
				<div class="caption">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Date From</label>
								<input type="text" size="16" class="form-control span2" id="startDate" name="startDate" placeholder="" />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Date To</label>
								<input type="text" size="16" class="form-control span2" id="endDate" name="endDate" placeholder="" />
							</div>
						</div>						
					</div>
					
					<div class="row">
						<div class="col-md-12">
							<button type="submit" class="btn btn-success"><i class="fa fa-thumbs-up"> Done</i></button>
							<button type="submit" class="btn btn-danger"><i class="fa fa-refresh"> Reset</i></button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<table class="table table-striped">
				<thead class="dark-header">
					<tr>
						<th>Entry Date</th>
						<th>Payment Date</th>
						<th>Client</th>
						<th>Amount</th>
						<th>Purpose</th>
						<th>View</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>22-05-2017, 01:55 PM</td>
						<td>22-05-2017</td>
						<td>Details of the trasaction</td>
						<td>50000</td>
						<td>25000</td>
						<td>
							<a href="<?php echo base_url('admin/viewReceivedPayment')?>" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Details"><i class="fa fa-info"></i></a>
						</td>
					</tr>						
				</tbody>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="thumbnail">
				<div class="caption">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Total Bill Collections</label>
								<input type="text" class="form-control" name="" value="" />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Total Amount</label>
								<input type="text" class="form-control" name="" value="" />
							</div>
						</div>								
					</div>
				</div>
			</div>
		</div>
	</div>
<?php include('footer.php') ?>