	<?php include('header.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">View Customer Account</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url('')?>">Dash Board</a></li>
				<li class="active">View Customer Account</li>
			</ol>
		</div>
	</div>


	<?php
	echo form_open("admin/updateCustomerAccount");
	echo form_hidden("customerId",$customerDetails->customerId);
	?>
	<div class="row">
		<div class="col-md-12">
			<div class="thumbnail">
				<div class="caption">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Name of Organization</label>
								<input type="text" class="form-control removeDisabled" name="organizationName" value="<?php echo $customerDetails->organizationName;?>" disabled />
								<div class="red-text"><?php  echo form_error('organizationName'); ?></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Owner / Corresponded Person</label>
								<input type="text" class="form-control removeDisabled" name="customerName" value="<?php echo $customerDetails->customerName;?>" disabled />
								<div class="red-text"><?php  echo form_error('customerName'); ?></div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Contact Number</label>
								<input type="text" class="form-control removeDisabled" name="customerPhone" value="<?php echo $customerDetails->customerPhone;?>" disabled />
								<div class="red-text"><?php  echo form_error('customerPhone'); ?></div>
							</div>
						</div>						
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Space Type</label>
								<select class="form-control removeDisabled" name="spaceType" disabled>
									<option value="1" <?php if($customerDetails->spaceType == 1){echo "selected";} ?>>Shop</option>
									<option value="2" <?php if($customerDetails->spaceType == 2){echo "selected";} ?>>Office</option>
									<option value="3" <?php if($customerDetails->spaceType == 3){echo "selected";} ?>>Other</option>
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Space Title</label>
								<input type="text" class="form-control removeDisabled" name="spaceTitle" value="<?php echo $customerDetails->spaceTitle;?>" disabled />
								<div class="red-text"><?php  echo form_error('spaceTitle'); ?></div>
							</div>
						</div>						
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group" >
								<label>Leasing Method</label>
								<select class="form-control removeDisabled"  name="leasingMethodId" disabled>
									<option value="1" <?php if($customerDetails->leasingMethodId == 1){echo "selected";} ?>>Rent</option>
									<option value="2" <?php if($customerDetails->leasingMethodId == 2){echo "selected";} ?>>Jomidari</option>
									<option value="3" <?php if($customerDetails->leasingMethodId == 3){echo "selected";} ?>>Other</option>
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Monthly Rent/Jomidari Charge</label>
								<input type="text" class="form-control removeDisabled" name="rentAmount" value="<?php echo $customerDetails->rentAmount;?>" disabled />
								<div class="red-text"><?php  echo form_error('rentAmount'); ?></div>
							</div>
						</div>								
					</div>
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label>Rent Due</label>
								<input type="text" class="form-control removeDisabled" name="rentDue" value="<?php echo $customerDetails->rentDue;?>" disabled />
								
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>Bill Due</label>
								<input type="text" class="form-control removeDisabled" name="billDue"  value="<?php echo $customerDetails->billDue;?>" disabled />
							</div>
						</div>
						<?php if($customerDetails->customerStatus == 1){ ?>
						<div class="col-md-4">
							<div class="form-group" >
								<label>Status</label>
								<select class="form-control removeDisabled" name="customerStatus" disabled>
									<option value="1" <?php if($customerDetails->customerStatus == 1){echo "selected";} ?>>Active</option>
									<option value="0" <?php if($customerDetails->customerStatus == 0){echo "selected";} ?>>Inactive</option>
								</select>
							</div>
						</div>
						<?php } else{ ?>
						<div class="col-md-4">
							<div class="form-group">
								<label>Status</label>
								<input type="text" class="form-control "  value="InActive" disabled />
							</div>
						</div>
						
						<?php } ?>						
					</div>
					
					<div class="row">
						<div class="col-md-12">
							<button type="button" class="btn btn-primary" id="removeReadonlyButton"><i class="fa fa-pencil"></i> Edit</button>
							<button type="submit" class="btn btn-success showreadonly" style=""><i class="fa fa-thumbs-up"></i> Save</button>
							<button type="button"class="btn btn-warning showreadonly" id="addReadonlyButton" style=""><i class="fa fa-times" ></i> Cancel</button>
							<a type="button" onclick="window.history.back();" class="btn btn-danger"> <i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php echo form_close(); ?>
	<div class="row text-center m-top-25 viewCustomerAccount">
		<div class="col-md-3">
			<a href="<?php echo base_url("admin/rentHistory/{$customerDetails->customerId}") ?>">
				<div class="thumbnail thumbOne">
					<div class="caption">
						<h4>Rent</h4>
						<p><?php echo $customerDetails->rentDue;?></p>
					</div>
				</div>
			</a>
		</div>
		<div class="col-md-3">
			<a href="<?php echo base_url("admin/serviceChargeHistory/{$customerDetails->customerId}") ?>">
				<div class="thumbnail thumbTwo">
					<div class="caption">
						<h4>Service Charge</h4>
						<p><?php echo $customerDetails->billDue;?></p>
					</div>
				</div>
			</a>
		</div>
	</div>
	<?php include('footer.php') ?>