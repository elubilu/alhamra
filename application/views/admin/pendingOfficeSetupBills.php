﻿<?php include('header.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">Office Bill Setup</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url('')?>">Dash Board</a></li>
				<li class="active">Office Bill Setup</li>
			</ol>
		</div>
	</div>
	<?php $billInfo->billingMonthYear= date('M, Y', strtotime($billInfo->billingMonthYear)); ?>
	<div class="row">
		<div class="col-md-12">
			<div class="thumbnail">
				<div class="caption">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Billing Month</label>
								<input type="text" class="form-control" name="" value="<?php echo $billInfo->billingMonthYear ?>" disabled/>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<a type="button" onclick="window.history.back();" class="btn btn-danger"> <i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>		
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">Electric Bill</h3>
		</div>
	</div>
	<?php if($currentBills){ ?>
	<div class="row">
		<div class="col-md-12">
			<table class="table table-striped">
				<thead class="dark-header">
					<tr>
						<th>Space ID</th>
						<th>Organization</th>
						<th>Meter No</th>
						<th>Previous Reading</th>
						<th>Current Reading</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($currentBills as $currentBill){ ?>
						<tr>
							<td><?php echo $currentBill->spaceTitle; ?></td>
							<td><?php echo $currentBill->organizationName; ?></td>
							<td><?php echo $currentBill->meterNo; ?></td>
							<td><?php echo $currentBill->currentBillPrevReading; ?></td>
							<?php if($billInfo->monthlyBillingStatus==1){ ?>
							<td>
								<div class="col-md-9" id="readingID<?php echo $currentBill->currentBillId ?>">
									<?php echo $currentBill->currentBillCurrentReading ?>
								</div>
								<div class="col-md-3">
									<!--<button type="button" onClick="updateCurrentReading('<?php// echo $currentBill->currentBillId ?>','<?php //echo $currentBill->meter_info_meterId ?>')" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Save"><i class="fa fa-thumbs-up" id="currentReadingButton" ></i></button>-->
									<!-- Button trigger modal -->
									<button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#officeBillModal<?php echo $currentBill->currentBillId ?>"><i class="fa fa-pencil" id="currentReadingButton" ></i></button>
									<?php include('officeBillModal.php') ?>
								</div>
							</td>
							<?php } else{ ?>
							<td>
								<div class="col-md-8">
									<?php echo $currentBill->currentBillCurrentReading ?>
								</div>
								<div class="col-md-4">
									<a href="<?php echo base_url("admin/printBill/{$billInfo->mothlyBillInfoId}/{$currentBill->currentBillId}")?>"type="button" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Print"><i class="fa fa-print"></i></a>
								</div>
							</td>
							
							<?php } ?>
						</tr>	
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
	<?php } ?>
	
	<div class="row m-top-15">
		<div class="col-md-12">
			<!--<button type="submit" class="btn btn-success"><i class="fa fa-thumbs-up"> Save</i></button>-->
			<?php if($billInfo->monthlyBillingStatus==1){ ?>
			<a href="<?php echo base_url("admin/makeOfficeBillsFinal/{$billInfo->mothlyBillInfoId}")?>" type="button" class="btn btn-success"><i class="fa fa-check"> Mark as Final</i></a>
			<?php } ?>
		</div>
	</div>
<?php include('footer.php') ?>