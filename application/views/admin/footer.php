		
    </div>
	<section class="developer">
		<div class="thumbnail">
			<div class="caption">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12 text-right">
							<b>System Developed By >> <a href="http://starlabit.com.bd/">StarLab IT</a></b>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
    <!-- jQuery -->
	<script src="<?php echo base_url('assets/back-end/js/jquery.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/back-end/js/bootstrap.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/back-end/js/select2.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/back-end/js/bootstrap-datepicker.js'); ?>"></script>
	<script src="<?php echo base_url('assets/back-end/js/bootstrap-timepicker.js'); ?>"></script>
	<script src="<?php echo base_url('assets/back-end/js/bootstrap-multiselect.js'); ?>"></script>
	<script src="<?php echo base_url('assets/back-end/js/table.js'); ?>"></script>
	<script src="<?php echo base_url('assets/back-end/js/metisMenu.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/back-end/js/jquery.nicescroll.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/back-end/js/script.js'); ?>"></script>
	
	<script type="text/javascript">
		$(function(){
			var windowHeight = $(window).height();
			var navbarHeight = $('.navbar').height();
			var wrapperHeight = windowHeight - navbarHeight;
			$('#page-wrapper').css('min-height', wrapperHeight);
		});
	</script>

	<script>

		$(function(){ 
			$('.pendingShopBill .currentReading').change(function(){			
				var prev = Number($('.prevReading').html()); 
				//alert(prev);	
				var current = Number($('.currentReading').val()); 
				var newReading = Number(current-prev);
				$('.newReading').val(newReading);
			});
		});
	</script>

	<script>
		$(function(){
			$('.showreadonly').hide();
			$('.cancleButton').hide();
			$('.saveButton').hide();
		});
	</script>

	<script type="text/javascript">
		$(function(){
			$('#removeReadonlyButton').click(function(){
				//alert('hm');
				$('.removeReadonly').removeAttr('readonly', 'readonly');
				$('.removeDisabled').removeAttr('disabled', 'disabled');
				$('#removeReadonlyButton').css('display','none');
				$('.showreadonly').show();
				$('.hidereadonly').hide();
			});
		});
	</script>
	<script type="text/javascript">
		$(function(){
			$('#addReadonlyButton').click(function(){
				//alert('hm');
				$('.removeReadonly').attr('readonly', 'readonly');
				$('.removeDisabled').attr('disabled', 'disabled');
				$('#removeReadonlyButton').show();
				$('.showreadonly').hide();
				$('.hidereadonly').show();
			});
		});
	</script>
    <script type="text/javascript">
       /* $(function(){
            $('#addReadonlyButton').click(function(){
                //alert('hm');
                $('.removeReadonly').attr('readonly', 'readonly');
                $('.removeDisabled').attr('disabled', 'disabled');
                $('#removeReadonlyButton').show();
                $('.showreadonly').hide();
                $('.hidereadonly').show();
            });
        });
        function cancle(no){
            var inputclass="removeReadonly"+no;
        function cancleService(no){
            //var inputclass="removeReadonly"+no;
            var save="save_button"+no;
            var cancle="cancle_button"+no;
            var edit="edit_button"+no;
            //alert(inputclass);
            $("#"+inputclass).attr('readonly', 'readonly');
            $(".removeDisabled"+no).attr('disabled', 'disabled');
            var serviceName=$("#serviceNameOldId"+no).val();
            var serviceNote=$("#serviceNoteOldId"+no).val();
            var serviceStatus=$("#serviceStatusOldId"+no).val();
            $("#serviceNameId"+no).val(serviceName);
            $("#serviceNoteId"+no).val(serviceNote);
            $("#serviceStatusId"+no).val(serviceStatus);
            $("#"+save).hide();
            $("#"+cancle).hide();
            $("#"+edit).show();
        }*/
    </script>

	<script type="text/javascript">
		function cancleService(no){
			//var inputclass="removeReadonly"+no;
			var save="save_button"+no;
			var cancle="cancle_button"+no;
			var edit="edit_button"+no;
			//alert(inputclass);
			$(".removeDisabled"+no).attr('disabled', 'disabled');
			var serviceName=$("#serviceNameOldId"+no).val();
			var serviceNote=$("#serviceNoteOldId"+no).val();
			var serviceStatus=$("#serviceStatusOldId"+no).val();
			$("#serviceNameId"+no).val(serviceName);
			$("#serviceNoteId"+no).val(serviceNote);
			$("#serviceStatusId"+no).val(serviceStatus);
			$("#"+save).hide();
			$("#"+cancle).hide();
			$("#"+edit).show();
		}
	</script>
	<script type="text/javascript">
		function editService_row(no){
			var save="save_button"+no;
			var cancle="cancle_button"+no;
			var edit="edit_button"+no;
			$(".removeDisabled"+no).removeAttr('disabled', 'disabled');
			$("#"+save).show();
			$("#"+cancle).show();
			$("#"+edit).hide();
		}
	</script>
	<script type="text/javascript">
		function saveService_row(no){
			var serviceName=$("#serviceNameId"+no).val();
			var serviceNote=$("#serviceNoteId"+no).val();
			var serviceStatus=$("#serviceStatusId"+no).val();
			var save="save_button"+no;
			var cancle="cancle_button"+no;
			var edit="edit_button"+no;
			//alert(serviceStatus);
			jQuery.ajax({
				type: "POST",
				url: "<?php echo base_url(); ?>" + "admin/ajax_updateServicesCharges/"+no,
				dataType: 'json',
				data: {
				   serviceName:serviceName,
				   serviceNote:serviceNote,
				   serviceStatus:serviceStatus
				},
				success: function(res) {
				if (res) {
						if (res.status === true){
							//alert("YES");
							$(".removeDisabled"+no).attr('disabled', 'disabled');
							$("#"+save).hide();
							$("#"+cancle).hide();
							$("#"+edit).show();
							$("#serviceNameOldId"+no).val(serviceName);
							$("#serviceNoteOldId"+no).val(serviceNote);
							$("#serviceStatusOldId"+no).val(serviceStatus);
						}
						else {
							var oldserviceName=$("#serviceNameOldId"+no).val();
							var oldserviceNote=$("#serviceNoteOldId"+no).val();
							var oldserviceStatus=$("#serviceStatusOldId"+no).val();
							$("#serviceNameId"+no).val(oldserviceName);
							$("#serviceNoteId"+no).val(oldserviceNote);
							$("#serviceStatusId"+no).val(oldserviceStatus);
							
						}
					}
				}
			});
		}
	</script>
	
	<script type="text/javascript">
		$(function(){
			$('.addAdditionalChargeButton').click(function(){
				$(".addAdditionalChargeTable tbody").append("<tr><td><input type='text' class='form-control' name='' value=''/></td><td><input type='text' class='form-control' name='' value=''/></td></tr>");
			});
		});
	</script>
	<script type="text/javascript">
		$(function(){
			$('.addServiceChargeButton').click(function(){
				$(".addServiceChargeTable tbody").append("<tr><td>7</td><td><input type='text' class='form-control removeDisabled' name='' value='Cleaning Charge' disabled></td><td><input type='text' class='form-control removeDisabled' name='' value='Employee cost for cleaning' disabled></td><td><select name='' id='' class='form-control removeDisabled' disabled><option value='1'>Enable</option><option value='2'>Disabled</option></select></td><td><button type='button' class='btn btn-primary btn-sm' id='removeReadonlyButton' data-toggle='tooltip' data-placement='top' title='Edit'><i class='fa fa-pencil'></i></button><button type='submit' class='btn btn-success showreadonly btn-sm' data-toggle='tooltip' data-placement='top' title='Save'><i class='fa fa-thumbs-up'></i></button><button type='button'class='btn btn-warning showreadonly btn-sm' id='addReadonlyButton' data-toggle='tooltip' data-placement='top' title='Cancel'><i class='fa fa-times' ></i> </button></td></tr>");
			});
		});
	</script>
	<script>
		$(function(){
			$("#organizationName").change(function(event){
				//alert("The text has been changed.");
				var customerId=$("#organizationName").val();
				jQuery.ajax({
					type: "POST",
					url: "<?php echo base_url(); ?>" + "admin/ajax_organizationSpaceTitle",
					dataType: 'json',
					data: {
						customerId: customerId
					},
					success: function(res) {
						if (res) {
							
							if (res.status === true) {
								//alert(res.spaceTitle);
								$("#spaceTitle").val(res.spaceTitle);
							}
							else {
								//alert("NO");
								
							}
						}
					}
				});
			});
		});
	</script>
	<script>
		$(function(){
			$("#additionalChargesID").click(function(event){
				var table = document.getElementById("additionalChargeTable");
				var table_len = (table.rows.length);
													
			//function clearCart(){
				//alert("The text has been changed.");
				var serviceId=$("#additionalChargeTitle").val();
				var serviceAmount=$("#additionalChargeAmount").val();
				var customerId=$("#organizationName").val();
				if(customerId && serviceId>0 ){
					jQuery.ajax({
						type: "POST",
						url: "<?php echo base_url(); ?>" + "admin/ajax_addAdditionalCharge",
						dataType: 'json',
						data: {
							serviceId: serviceId,
							serviceAmount: serviceAmount,
							customerId: customerId
						},
						success: function(res) {
							if (res) {
								
								if (res.status === true) {
									if(res.pastBuy==0){
										$("#additionalChargeTitle").val(0);
										$("#additionalChargeAmount").val(" ");
										var row = table.insertRow(table_len).outerHTML = "<tr id='row" + res.rowid + "' class='active'><td><input type='text' class='form-control' disabled name='' value='" + res.serviceName + "' /></td><td><input type='text' disabled class='form-control' name='' value='" + serviceAmount + "' /></td><td><button type='button' class='btn btn-danger btn-sm' onclick='delete_row(\""+ res.rowid +"\")' ><i class='fa fa-remove'></i></button></td></tr>";

									}
									else{
										alert("Already Selected");
									}
									
								}
								else {
									//alert("NO");
									
								}
							}
						}
					});
				}
				else{
					alert("Please Select A CustomerFirst ");
				}
				
			});
		});
	</script>
	<script type="text/javascript">
		function delete_row(row_id) {
			//event.preventDefault();
			//alert("working");
			jQuery.ajax({
				type: "POST",
				url: "<?php echo base_url(); ?>" + "admin/ajaxRemoveAdditional",
				dataType: 'json',
				data: {
					row_id: row_id,
					//quantity: quantity,
				},
				success: function(res) {
					if (res) {
						// Show Entered Value
						if (res.status === true) {
							
							document.getElementById("row" + row_id + "").outerHTML = "";
							
						} else {
							//jQuery("#quantity_errors").show();
							//jQuery("#quantity_errors").html(res.errors);
						}
					}
				}
			});
			
		}
	</script>
	<script type="text/javascript">
		function updateCurrentReading(no,meterId){
			var currentReading=$("#currentReading"+no).val();
			//alert(currentReading);
			if(currentReading>0){
				$("#currentReadingError"+no).html('');
				$("#currentReading"+no).css('background','#fff');
				jQuery.ajax({
					type: "POST",
					url: "<?php echo base_url(); ?>" + "admin/ajax_updateCurrentReading",
					dataType: 'json',
					data: {
					   currentBillCurrentReading:currentReading,
					   currentBillId:no,
					   meter_info_meterId:meterId
					},
					success: function(res) {
					if (res) {
							if (res.status === true){
								//alert("YES");
								$("#currentReading"+no).val(currentReading);
								$("#currentReading"+no).css('background','#5cb85c');
								
							}
							else {
								
							}
						}
					}
				});
			}
			else{
				$("#currentReadingError"+no).html('Please Insert A value');
				$("#currentReading"+no).css('background','#fff');
			}
		}
	</script>
	<script type="text/javascript">
		function updateOfficeBillInfo(no){
				// get all the inputs into an array.
				event.preventDefault();
				var $inputs = $("#"+no+" :input");
				//alert("submiting");
				//alert("$inputs");

				// not sure if you wanted this, but I thought I'd add it.
				// get an associative array of just the values.
				var values = {};
				$inputs.each(function() {
					values[this.name] = $(this).val();
				});
				jQuery.ajax({
					type: "POST",
					url: "<?php echo base_url(); ?>" + "admin/ajax_updateOfficeBill",
					dataType: 'json',
					data: {
					   values:values
					},
					success: function(res) {
					if (res) {
							if (res.status === true){
								//alert("Updated Successfully");
								$("#readingID"+no).html(res.currentReading);
								//$("#currentReading"+no).css('background','#5cb85c');
								
							}
							else {
								alert("Pleast Try Again");
							}
						}
					}
				});
			}
	</script>
	<script>
		$(function(){
			$("#bankAccountId").change(function(event){
				//alert("The text has been changed.");
				$("#error-msg").html("");
				var bankId=$("#bankAccountId").val();
				jQuery.ajax({
					type: "POST",
					url: "<?php echo base_url(); ?>" + "admin/ajax_bankAccountBalance",
					dataType: 'json',
					data: {
						bankId: bankId
					},
					success: function(res) {
						if (res) {
							
							if (res.status === true) {
								//alert(res.spaceTitle);
								$("#bankCurrentBalance").val(res.bankAccountLastBalance);
							}
							else {
								//alert("NO");
								
							}
						}
					}
				});
			});
		});
	</script>
	<script>
		$(function(){
			$("#accountAdjustmentAmount").change(function(event){
				//alert("The text has been changed.");
				var bankId=$("#bankAccountId").val();
				var amount=$("#accountAdjustmentAmount").val();
				var type=$("#accountAdjustmentType").val();
				if(bankId){
					$("#error-msg").html("");
					jQuery.ajax({
						type: "POST",
						url: "<?php echo base_url(); ?>" + "admin/ajax_bankAccountBalance",
						dataType: 'json',
						data: {
							bankId: bankId
						},
						success: function(res) {
							if (res) {
								
								if (res.status === true) {
									//alert(res.spaceTitle);
									if(type==1){
										var amountUpdated=(+res.bankAccountLastBalance)+(+amount);
									}
									else{
										var amountUpdated=(+res.bankAccountLastBalance)-(+amount);
									}
									$("#bankCurrentBalance").val(res.bankAccountLastBalance);
									$("#accountAdjustmentAfterAmount").val(amountUpdated);
								}
								else {
									//alert("NO");
									
								}
							}
						}
					});
				}
				else{
					$("#error-msg").html("Please Select A Bank Account First");
				}
			});
		});
	</script>
	<script>
		$(function(){
			$("#accountAdjustmentType").change(function(event){
				//alert("The text has been changed.");
				var bankId=$("#bankAccountId").val();
				var amount=$("#accountAdjustmentAmount").val();
				var type=$("#accountAdjustmentType").val();
				if(bankId){
					$("#error-msg").html("");
					jQuery.ajax({
						type: "POST",
						url: "<?php echo base_url(); ?>" + "admin/ajax_bankAccountBalance",
						dataType: 'json',
						data: {
							bankId: bankId
						},
						success: function(res) {
							if (res) {
								
								if (res.status === true) {
									//alert(res.spaceTitle);
									if(type==1){
										var amountUpdated=(+res.bankAccountLastBalance)+(+amount);
									}
									else{
										var amountUpdated=(+res.bankAccountLastBalance)-(+amount);
									}
									$("#bankCurrentBalance").val(res.bankAccountLastBalance);
									$("#accountAdjustmentAfterAmount").val(amountUpdated);
								}
								else {
									//alert("NO");
									
								}
							}
						}
					});
				}
				else{
					$("#error-msg").html("Please Select A Bank Account First");
				}
			});
		});
	</script>
	<script>
		$(function(){
			$('#organizationID').change(function(){
				$('.removable').remove();
				var client_id=$('#organizationID').val();
				if(client_id){
					jQuery.ajax({
						type: "POST",
						url: "<?php echo base_url(); ?>" + "admin/ajax_get_client_meter_info",
						dataType: 'json',
						data: {
						   client_id:client_id
						},
						success: function(res) {
						if (res) {
								if (res.status === true){
									var len = res.infos.length;
									for(var i=0;i<len;i++){
										$('#clientMeterNo')
										.append('<option class="removable" value="'+res.infos[i].meterId+'"> '+res.infos[i].meterNo+' </option>');
									}
									
								}
								else {
									alert("Pleast Try Again");
								}
							}
						}
					});
				}
				else{
					//alert("Please Select A Client First");
				}
			});
		});
	</script>
	<script>
	$(function(){ 
		$('.removable').remove();
			var client_id=$('#organizationID').val();
			//alert(client_id);
			if(client_id){
				jQuery.ajax({
					type: "POST",
					url: "<?php echo base_url(); ?>" + "admin/ajax_get_client_meter_info",
					dataType: 'json',
					data: {
					   client_id:client_id
					},
					success: function(res) {
					if (res) {
							if (res.status === true){
								var meter_hidden_val=$('#hiddenMeterID').val();
								//alert(meter_hidden_val);
								var len = res.infos.length;
								for(var i=0;i<len;i++){
									if(meter_hidden_val && res.infos[i].meterId==meter_hidden_val ){
										$('#clientMeterNo')
										.append('<option class="removable" selected="selected" value="'+res.infos[i].meterId+'"> '+res.infos[i].meterNo+' </option>');
									}
									else{
										$('#clientMeterNo')
										.append('<option class="removable" value="'+res.infos[i].meterId+'"> '+res.infos[i].meterNo+' </option>');
									}
								}
								
							}
							else {
								alert("Pleast Try Again");
							}
						}
					}
				});
			}
	});
</script>
	<?php include ('meterJS.php')?>
</body>

</html>
