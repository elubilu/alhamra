<?php include('header.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">Bank Account Fund Adjustments</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url('')?>">Dash Board</a></li>
				<li class="active">Bank Account Fund Adjustments</li>
			</ol>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<div class="thumbnail">
				<?php echo form_open('admin/storeBankAdjustment'); ?>
				<div class="caption">
					<div class="row">
						<div class="col-md-12">
							<p id="error-msg" style="color:red"></p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Select Account</label>
								<select class="form-control forselect2" id="bankAccountId" value="" name="bank_account_info_bankAccountId" required >
								<option value="" selected disabled >Selecte One</option>
								<?php foreach($bank_infos as $bank_info): ?>
									<option value="<?php echo $bank_info->bankAccountId ?>"><?php echo $bank_info->bankAccountTitle ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group" required >
								<label>Current Balance</label>
								<input type="text" class="form-control" id="bankCurrentBalance" name="accountAdjustmentbeforeAmount" value="" disabled  />
							    <div class="red-text"><?php  echo form_error('accountAdjustmentbeforeAmount'); ?></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Adjustment Type</label>
								<select class="form-control forselect2" value="" id="accountAdjustmentType" name="accountAdjustmentType" required >
									<option value="1">Add Amount</option>
									<option value="2">Remove Amount</option>
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Amount</label>
								<input type="text" class="form-control" id="accountAdjustmentAmount" name="accountAdjustmentAmount" value="" required  />
							    <div class="red-text"><?php  echo form_error('accountAdjustmentAmount'); ?></div>
							</div>
						</div>						
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Note</label>
								<textarea type="text" class="form-control" name="accountAdjustmentNote" value="" required ></textarea>
							  
							</div>
						</div>						
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Updated Balance</label>
								<input type="text" class="form-control" id="accountAdjustmentAfterAmount" name="accountAdjustmentAfterAmount" value="" disabled />
							</div>
						</div>						
					</div>
					<div class="row">
						<div class="col-md-12">
							<button type="submit" class="btn btn-success"><i class="fa fa-thumbs-up"></i> Done</button>
							<button type="reset" class="btn btn-warning"><i class="fa fa-refresh"></i> Reset</button>
							<button type="button" class="btn btn-danger" onclick="window.history.back();"><i class="fa fa-arrow-left"></i> Back</button>		
						</div>
					</div>
				</div>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>

<?php include('footer.php') ?>