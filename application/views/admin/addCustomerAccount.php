	<?php include('header.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">Add Customer Account</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<?php if($this->session->flashdata('feedback_successfull'))
			{ ?>
			<div class="alert alert-success alert-dismissible fade in" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true"><i class="fa fa-times"></i></span>
				</button>
				<strong>Success!</strong>
				<?php echo $this->session->flashdata('feedback_successfull'); ?>
			</div>
			<?php } 
			if($this->session->flashdata('feedback_failed'))
				{ ?>
			<div class="alert alert-danger alert-dismissible fade in" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true"><i class="fa fa-times"></i></span>
				</button>
				<strong>Oops!</strong>
				<?php echo $this->session->flashdata('feedback_failed'); ?>
			</div>
			<?php   } ?>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url('')?>">Dash Board</a></li>
				<li class="active">Add Customer Account</li>
			</ol>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<div class="thumbnail">
				<?php 
				echo form_open('admin/storeCustomerAccount');
				echo form_hidden('customerStatus',1); 
				?>
				<div class="caption">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Name of Organization</label>
								<input type="text" class="form-control" name="organizationName" value="" required />
								<div class="red-text"><?php  echo form_error('organizationName'); ?></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Owner / Corresponded Person</label>
								<input type="text" class="form-control" name="customerName" value="" required />
								<div class="red-text"> <?php  echo form_error('customerName'); ?></div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Contact Number</label>
								<input type="text" class="form-control" name="customerPhone" value="" required />
								<div class="red-text"> <?php  echo form_error('customerPhone'); ?></div>
							</div>
						</div>						
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Space Type</label>

								<select class="form-control" name="spaceType" required>
									<option value="1">Shop</option>
									<option value="2">Office</option>
									<option value="3">Other</option>
								</select>

							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Space Title</label>
								<input type="text" class="form-control" name="spaceTitle" value="" required />
								<div class="red-text"> <?php  echo form_error('spaceTitle'); ?></div>
							</div>
						</div>						
					</div>
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label>Leasing Method</label>
								<select class="form-control" name="leasingMethodId" required>
									<option value="1">Rent</option>
									<option value="2">Jomidari</option>
									<option value="3">Other</option>
								</select>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>Monthly Rent/Jomidari Charge</label>
								<input type="text" class="form-control" name="rentAmount" value="" required />
								<p class="red-text"> <?php  echo form_error('rentAmount'); ?></p>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>Previous Due</label>
								
								<input type="text" class="form-control removeDisabled" name="rentDue" value="" required />
								<div class="red-text"> <?php  echo form_error('rentDue'); ?></div>
							</div>
						</div>							
					</div>
					<div class="row">
						<div class="col-md-12">
							<button type="submit" class="btn btn-success"><i class="fa fa-thumbs-up"></i> Add</button>
							<button type="reset" class="btn btn-warning"><i class="fa fa-refresh"> </i> Reset</button>
							<button type="button" class="btn btn-danger" onclick="window.history.back();"><i class="fa fa-arrow-left"></i> Back</button>		
						</div>
					</div>
				</div>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
	<?php include('footer.php') ?>