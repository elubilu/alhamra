	<?php include('header.php') ?>
		<div class="row">
			<div class="col-md-12">
				<h3 class="page-header">Add Bank Account</h3>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url('')?>">Dash Board</a></li>
					<li class="active">Add Bank Account</li>
				</ol>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<?php if($this->session->flashdata('feedback_successfull'))
						{ ?>
							<div class="alert alert-success alert-dismissible fade in" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true"><i class="fa fa-times"></i></span>
									</button>
								<strong>Success!</strong>
								<?php echo $this->session->flashdata('feedback_successfull'); ?>
							</div>
						<?php } 
						if($this->session->flashdata('feedback_failed'))
							{ ?>
								<div class="alert alert-danger alert-dismissible fade in" role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true"><i class="fa fa-times"></i></span>
										</button>
									<strong>Oops!</strong>
									<?php echo $this->session->flashdata('feedback_failed'); ?>
								</div>
					<?php   } ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="thumbnail">
					<?php echo form_open('admin/storeBankAccountInfo') ?>
					<div class="caption">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label>Account Title</label>
									<input type="text" class="form-control" name="bankAccountTitle" value="" />
								    <div class="red-text"><?php  echo form_error('bankAccountTitle'); ?></div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Bank Name</label>
									<input type="text" class="form-control" name="bankName" value="" />
								     <div class="red-text"><?php  echo form_error('bankName'); ?></div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Branch</label>
									<input type="text" class="form-control" name="bankAccountBranchName" value="" />
								     <div class="red-text"><?php  echo form_error('bankAccountBranchName'); ?></div>
								</div>
							</div>						
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Account Holder Name</label>
									<input type="text" class="form-control" name="bankAccountHolderName" value="" />
								     <div class="red-text"><?php  echo form_error('bankAccountHolderName'); ?></div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Account Number</label>
									<input type="text" class="form-control" name="bankAccountNumber" value="" />
								     <div class="red-text"><?php  echo form_error('bankAccountNumber'); ?></div>
								</div>
							</div>						
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Routing Number</label>
									<input type="text" class="form-control" name="bankAccountRoutingNumber" value="" />
								     <div class="red-text"><?php  echo form_error('bankAccountRoutingNumber'); ?></div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Balance as Per Last Account</label>
									<input type="text" class="form-control" name="bankAccountLastBalance" value="" />
								    <div class="red-text"><?php  echo form_error('bankAccountLastBalance'); ?></div>
								</div>
							</div>						
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label>Note</label>
									<textarea type="text" class="form-control" name="bankAccountNote" value=""></textarea>
								     <div class="red-text"><?php  echo form_error('bankAccountNote'); ?></div>
								</div>
							</div>						
						</div>
						<div class="row">
							<div class="col-md-12">
								<button type="submit" class="btn btn-success"><i class="fa fa-thumbs-up"></i> Done</button>
								<button type="reset" class="btn btn-warning"><i class="fa fa-refresh"> </i> Reset</button>
								<button type="button" class="btn btn-danger" onclick="window.history.back();"><i class="fa fa-arrow-left"></i> 
								Back</button>
							</div>
						</div>
					</div>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>

	<?php include('footer.php') ?>]