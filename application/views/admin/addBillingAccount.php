﻿<?php include('header.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">Add Billing Account</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<?php if($this->session->flashdata('feedback_successfull'))
					{ ?>
						<div class="alert alert-success alert-dismissible fade in" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true"><i class="fa fa-times"></i></span>
								</button>
							<strong>Success!</strong>
							<?php echo $this->session->flashdata('feedback_successfull'); ?>
						</div>
					<?php } 
					if($this->session->flashdata('feedback_failed'))
						{ ?>
							<div class="alert alert-danger alert-dismissible fade in" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true"><i class="fa fa-times"></i></span>
									</button>
								<strong>Oops!</strong>
								<?php echo $this->session->flashdata('feedback_failed'); ?>
							</div>
				<?php   } ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url('')?>">Dash Board</a></li>
				<li class="active">Add Billing Account</li>
			</ol>
		</div>
	</div>
	<?php echo form_open('admin/storeCustomerBillingAccount'); ?>
	<div class="row">
		<div class="col-md-12">
			<div class="thumbnail">
				<div class="caption">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Name of Organization</label>
								<select class="form-control forselect2" name="customer_info_customerId" id="organizationName" required >
									<option value="" disabled selected>Select Name of Organization</option>
									<?php foreach($organizations as $organization): ?>
										<option value="<?php echo $organization->customerId ?>"><?php echo $organization->organizationName." => ".$organization->spaceTitle; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Space Title</label>
								<input type="text" class="form-control" name="" value="" id="spaceTitle" disabled/>
							</div>
						</div>						
					</div>
					<div class="row">
						<div class="col-md-12">
							<a type="button" onclick="window.history.back();" class="btn btn-danger"> <i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="thumbnail">
				<div class="caption">
					<div class="row">
						<div class="col-md-12">
							<table class="table table-striped">
								<thead class="dark-header">
									<tr>
										<th>Meter No.</th>
										<th>Customer ID</th>
										<th>Previous Reading</th>
										<th>Previous Due</th>
										<th>Demand Charge</th>
										<th>Service Charge</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td><input type="text" class="form-control" name="meterNo" value="" required /></td>
										<td><input type="text" class="form-control" name="meterCustomerId" value="" required /></td>
										<td><input type="text" class="form-control" name="previousReading" value="" required /></td>
										<td><input type="text" class="form-control" name="billDue" value="" required /></td>
										<td><input type="text" class="form-control" name="demandCharge" value="" required /></td>
										<td><input type="text" class="form-control" name="meterServiceCharge" value="" required /></td>
									</tr>
								</tbody>						
							</table>
						</div>
					</div>	
					<div class="row m-top-15">
						<div class="col-md-12">
							<div class="form-group">
								<div class="row">
									<div class="col-md-12"><label>Service Charge Setup</label><br></div>
									<div class="col-md-12 text-left">
										<select id="multipleSelect" class="form-control multipleSelect" multiple name="charges[]" multiple="multiple">
										    <?php foreach($services as $service): ?>
												<option value="<?php echo $service->serviceId ?>"><?php echo $service->serviceName ?></option>
											<?php endforeach; ?>
										</select>
									</div>
								</div>	
							</div>
						</div>					
					</div>
					<h4>Additional Charge</h4>
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label>Charge Title</label>
								<!--<input type="text" class="form-control" name="" value="" id="additionalChargeTitle" placeholder="Charge Title" />-->
								<select id="additionalChargeTitle" class="form-control " name="" >
									<option value="0" selected >Select Additional Service Charge</option>
								    <?php foreach($additionalServices as $additionalService): ?>
										<option value="<?php echo $additionalService->serviceId ?>"><?php echo $additionalService->serviceName ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>Amount</label>
								<input type="text" class="form-control" name="" value="" id="additionalChargeAmount"  placeholder="Amount" />
							</div>
						</div>
						<div class="col-md-4">
							<button type="button" class="btn btn-success m-top-20" id="additionalChargesID" >Add Additional Charge</button>
							<!--<button type="button" class="btn btn-default m-bottom-10 pull-right addAdditionalChargeButton">Add Additional Charge</button>-->
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<table class="table table-striped addAdditionalChargeTable" id="additionalChargeTable">
								<thead class="dark-header">
									<tr>
										<th>Charge Title</th>
										<th>Amount</th>
										<th>Remove</th>
									</tr>
								</thead>
								<tbody>
									<!--<tr>
										<td><input type="text" class="form-control" name="" value="" /></td>
										<td><input type="text" class="form-control" name="" value="" /></td>
										<td><button type='button' class='btn btn-danger btn-sm'  ><i class='fa fa-remove'></i></button></td>
									</tr>-->
								</tbody>						
							</table>
						</div>
					</div>	
					<div class="row m-top-25">
						<div class="col-md-12">
							<button type="submit" class="btn btn-success"><i class="fa fa-thumbs-up"> </i> Done</button>
							<button type="reset" class="btn btn-warning"><i class="fa fa-refresh"> </i> Reset</button>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php form_close(); ?>
<?php include('footer.php') ?>