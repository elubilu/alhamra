<?php include('header.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">Petty Cash</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url('')?>">Dash Board</a></li>
				<li class="active">Petty Cash</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="thumbnail">
				<div class="caption">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Current Balance</label>
								<input type="text" class="form-control" name="" value="<?php echo $amount; ?>" disabled />
							</div>
						</div>					
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row filterable">
		<div class="col-md-12">
			<div class="row m-bottom-10">
				<div class="col-md-12 col-xs-12">
					<div class="pull-right">
						<a href="<?php echo base_url('admin/addNewCashTransfer')?>" style="margin-top:0px;margin-right:5px;" type="button" class="btn btn-default">Add New Cash Transfer</a>
						<a href="<?php echo base_url('admin/newLedgerEntry')?>" style="margin-top:0px;margin-right:5px;" type="button" class="btn btn-default">New Ledger Entry</a>
						<button id="filter_button" class="btn btn-default btn-filter"><i class="fa fa-filter"></i> Filter</button>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="table table-striped">
						<thead class="dark-header">
							<tr class="filters">
								<th>
									<input type="text" class="form-control text-left" placeholder="Entry Date-Time" disabled data-toggle="true">
								</th>
								<th>
									<input type="text" class="form-control text-left" placeholder="Date" disabled data-toggle="true">
								</th>
								<th>
									<input type="text" class="form-control text-left" placeholder="Ledger Title" disabled data-toggle="true">
								</th>
								<th>
									<input type="text" class="form-control text-left" placeholder="Purpose" disabled data-toggle="true">
								</th>
								<th>
									<input type="text" class="form-control text-left" placeholder="Amount" disabled data-toggle="true">
								</th>	
								<th>
									<input type="text" class="form-control text-left" placeholder="Balance" disabled data-toggle="true">
								</th>									
								<th>
									<span>View</span>
								</th>
							</tr>
						</thead>
						<tbody>
							<?php if($infos){ ?>
							<?php $totalBalence=$amount; ?>
							<?php foreach($infos as $info): 
								$info->pettyCashDate=date('d/m/Y', strtotime($info->pettyCashDate)); 
								$info->pettyCashEntryDate=date('d/m/Y, h:ia', strtotime($info->pettyCashEntryDate));
							?>
								<tr>
									<td><?php echo $info->pettyCashEntryDate ?></td>
									<td><?php echo $info->pettyCashDate ?></td>
									<td><?php echo $info->pettyCashto ?></td>
									<td><?php echo $info->transactionName ?></td>
									<td><?php if($info->pettyCashType==2) echo "(-)";echo $info->pettyCashAmount ?></td>
									<td><?php echo $totalBalence ?></td>
									<td>
										<a href="<?php echo base_url("admin/{$info->transactionDetailslink}/{$info->pettyCashReferenceId}/{$info->pettyCashPurpose}")?>" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Details"><i class="fa fa-info"></i></a>
									</td>
								</tr>	
							<?php 
								if($totalBalence>=0){
									if($info->pettyCashType==1){
									$totalBalence=$totalBalence-$info->pettyCashAmount;
									} 
									else{
									$totalBalence=$totalBalence+$info->pettyCashAmount;
									} 
								}
								else{
									if($info->pettyCashType==1){
									$totalBalence=$totalBalence+$info->pettyCashAmount;
									} 
									else{
									$totalBalence=$totalBalence-$info->pettyCashAmount;
									} 
								}
							?>
							<?php endforeach; } ?>						
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<button type="button" class="btn btn-danger" onclick="window.history.back();"><i class="fa fa-arrow-left"></i> Back</button>			
		</div>
	</div>
<?php include('footer.php') ?>