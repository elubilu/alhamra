<?php include('header.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">Add New Ledger</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url('')?>">Dash Board</a></li>
				<li class="active">Add New Ledger</li>
			</ol>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<div class="thumbnail">
				<?php echo form_open('admin/storeLedger'); ?>
				<div class="caption">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Ledger Title</label>
								<input type="text" class="form-control" name="ledgerTitle" value="" required />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Ledger Type</label>
								<select class="form-control incomeExpenseSelect" name="ledgerType" id="ledgerDetailsType" value="" required >
									<option value="1">Income</option>
									<option value="2">Expense</option>
								</select>
							</div>
						</div>						
					</div>
					<div class="row">
						<div class="col-md-6" id="incomeDiv" >
							<div class="form-group">
								<label>Particular</label>
								<select class="form-control forselect2" name="particular_info_particularid" id="particularSelect">
									<option value="">Select an Option</option>
									<?php foreach($infos['income'] as $info):?>
									<option value="<?php echo $info->particularId ?>" class="particular<?php echo $info->particularType; ?>"><?php echo $info->particularTitle ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="col-md-6" id="expenseDiv">
							<div class="form-group">
								<label>Particular</label>
								<select class="form-control forselect2" name="particular_info_particularid" id="particularSelect">
									<option value="">Select an Option</option>
									<?php foreach($infos['expence'] as $info):?>
									<option value="<?php echo $info->particularId ?>" class="particular<?php echo $info->particularType; ?>"><?php echo $info->particularTitle ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Balance ( As Per Last Account )</label>
								<input type="text" class="form-control" name="ledgerBalance" value=""  />
								<div class="red-text"><?php  echo form_error('ledgerBalance'); ?></div>
							</div>
						</div>						
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Note</label>
								<textarea type="text" class="form-control" name="ledgerNote" value="" required></textarea>
							</div>
						</div>						
					</div>
					<div class="row">
						<div class="col-md-12">
							<button type="submit" class="btn btn-success"><i class="fa fa-thumbs-up"> </i> Done</button>
							<button type="submit" class="btn btn-warning"><i class="fa fa-refresh"></i> Reset</button>
							<button type="button" class="btn btn-danger" onclick="window.history.back();"><i class="fa fa-arrow-left"></i> Back</button>
						</div>
					</div>
				</div>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>

<?php include('footer.php') ?>