<?php include('header.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">Service Charges</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url('')?>">Dash Board</a></li>
				<li class="active">Service Charges</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<?php if($this->session->flashdata('feedback_successfull'))
					{ ?>
						<div class="alert alert-success alert-dismissible fade in" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true"><i class="fa fa-times"></i></span>
								</button>
							<strong>Success!</strong>
							<?php echo $this->session->flashdata('feedback_successfull'); ?>
						</div>
					<?php } 
					if($this->session->flashdata('feedback_failed'))
						{ ?>
							<div class="alert alert-danger alert-dismissible fade in" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true"><i class="fa fa-times"></i></span>
									</button>
								<strong>Oops!</strong>
								<?php echo $this->session->flashdata('feedback_failed'); ?>
							</div>
				<?php   } ?>
		</div>
	</div>
	<?php 
		echo form_open('admin/storeCommonServices');
		echo form_hidden('serviceType',1); 
		echo form_hidden('serviceStatus',1); 
	?>
	<div class="row">
		<div class="col-md-12">
			<div class="thumbnail">
				<div class="caption">
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<input type="text" class="form-control" name="serviceName" value="" placeholder="Service Name" required >
							</div>
						</div>
						<div class="col-md-4">	
							<div class="form-group">
								<input type="text" class="form-control" name="serviceNote" value="" placeholder="Note" >
							</div>							
						</div>

						<div class="col-md-4">	
							<div class="form-group">
								<select name="serviceType" id="" required class="form-control" >
									<option value="" selected disabled>Select A Type </option>
									<option value="1">Common Service Charge</option>
									<option value="2">Additional Service Charge</option>
								</select>
							</div>							
						</div>

						<div class="col-md-4 ">
							<button type="submit" class="btn btn-primary  " >Add New</button>
							<a type="button" onclick="window.history.back();" class="btn btn-danger "> <i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php echo form_close(); ?>
	<div class="row m-top-15">
		<div class="col-md-12">
			<table class="table table-striped addServiceChargeTable">
				<thead class="dark-header">
					<tr>
						<th>ID</th>
						<th>Service Name</th>
						<th>Note</th>
						<th>Status</th>
						<th>Edit</th>
					</tr>
				</thead>
				<tbody>
                    <?php foreach ($serviceList as $service): ?>
					<input type="hidden" value="<?php echo $service->serviceName; ?>" id="serviceNameOldId<?php echo $service->serviceId ?>">
					<input type="hidden" value="<?php echo $service->serviceNote; ?>" id="serviceNoteOldId<?php echo $service->serviceId ?>">
					<input type="hidden" value="<?php echo $service->serviceStatus; ?>" id="serviceStatusOldId<?php echo $service->serviceId ?>">
					<tr>
						<td><?php echo $service->serviceId; ?></td>
						<td>
							<input type="text" class="form-control removeDisabled<?php echo $service->serviceId ?>" name="serviceName" value="<?php echo $service->serviceName; ?>" id="serviceNameId<?php echo $service->serviceId ?>" disabled>
						</td>
						<td>
							<input type="text" class="form-control removeDisabled<?php echo $service->serviceId ?>" name="serviceNote" value="<?php echo $service->serviceNote; ?>" id="serviceNoteId<?php echo $service->serviceId ?>" disabled>
						</td>
						<td>
							<select name="serviceStatus" id="serviceStatusId<?php echo $service->serviceId ?>"  class="form-control removeDisabled<?php echo $service->serviceId ?>" disabled>
								<option value="1" <?php if($service->serviceStatus == 1){echo "selected";} ?>>Enable</option>
								<option value="0" <?php if($service->serviceStatus == 0){echo "selected";} ?>>Disabled</option>
							</select>
						</td>
						<td>
							<!--<button type="button" class="btn btn-primary btn-sm" id="removeReadonlyButton" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil"></i></button>
							<button type="submit" class="btn btn-success showreadonly  btn-sm" data-toggle="tooltip" data-placement="top" title="Save"><i class="fa fa-thumbs-up"></i></button>
							<button type="button"class="btn btn-warning showreadonly btn-sm" id="addReadonlyButton" data-toggle="tooltip" data-placement="top" title="Cancel"><i class="fa fa-times" ></i> </button>-->
							
							<button type="button" class="btn btn-primary btn-sm" id="edit_button<?php echo $service->serviceId  ?>" data-toggle="tooltip" data-placement="top" title="Edit" onclick="editService_row('<?php echo $service->serviceId  ?>')"><i class="fa fa-pencil"></i></button>
							<button type="button" class="btn btn-success showreadonly  btn-sm" data-toggle="tooltip" data-placement="top" title="Save" id="save_button<?php echo $service->serviceId  ?>" onclick="saveService_row('<?php echo $service->serviceId  ?>')"  ><i class="fa fa-thumbs-up" ></i></button>
							<button type="button"class="btn btn-warning showreadonly btn-sm" id="cancle_button<?php echo $service->serviceId  ?>" data-toggle="tooltip" data-placement="top" title="Cancel"  onclick="cancleService('<?php echo $service->serviceId  ?>')"><i class="fa fa-times" ></i> </button>
						</td>
					</tr>
                    <?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>

<?php include('footer.php') ?>