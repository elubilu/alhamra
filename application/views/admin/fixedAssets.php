<?php include('header.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">Fixed Assets</h3>
		</div>
	</div>
	<?php include('messages.php'); ?>
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url('')?>">Dash Board</a></li>
				<li class="active">Fixed Assets</li>
			</ol>
		</div>
	</div>
	
	<div class="row filterable">
		<div class="col-md-12">
			<div class="row m-bottom-10">
				<div class="col-md-12 col-xs-12">
					<div class="pull-right">								
						<a href="<?php echo base_url('admin/addAsset')?>" style="margin-top:0px;margin-right:5px;" type="button" class="btn btn-default">Add Assets</a>
						<button id="filter_button" class="btn btn-default btn-filter"><i class="fa fa-filter"></i> Filter</button>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="table table-striped">
						<thead class="dark-header">
							<tr class="filters">
								<th>
									<input type="text" class="form-control text-left" placeholder="Asset ID" disabled data-toggle="true">
								</th>
								<th>
									<input type="text" class="form-control text-left" placeholder="Asset Title" disabled data-toggle="true">
								</th>
								<th>
									<input type="text" class="form-control text-left" placeholder="Tracking ID" disabled data-toggle="true">
								</th>
								<th>
									<input type="text" class="form-control text-left" placeholder="Accruing Date" disabled data-toggle="true">
								</th>
								<th>
									<input type="text" class="form-control text-left" placeholder="Current Value" disabled data-toggle="true">
								</th>								
								<th>
									<span>View</span>
								</th>
							</tr>
						</thead>
						<tbody>
							<?php if($fixedAssets) { ?>
							<?php foreach ($fixedAssets as $allAssets): ?>
							<tr>
								<td><?php echo $allAssets->assetId; ?></td>
								<td><?php echo $allAssets->assetTitle; ?></td>
								<td><?php echo $allAssets->assetTrackingId; ?></td>
								<td><?php echo $allAssets->assetAccruingDate; ?></td>
								<td><?php echo $allAssets->assetCurrentValue; ?></td>
								<td>
									<a href="<?php echo base_url("admin/viewAsset/{$allAssets->assetId}")?>" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Details"><i class="fa fa-info"></i></a>
								</td>
							</tr>
							<?php endforeach;
							  } ?>								
						</tbody>

					</table>
				</div>
			</div>
		</div>
	</div>

<?php include('footer.php') ?>