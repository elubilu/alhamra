<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Al Hamra International Limited</title>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <style>
    .invoice-box{
        max-width:750px;
        margin:auto;
        padding:30px;
        font-size:14px;
        line-height:24px;
        font-family:'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color:#555;
        background-color: #FFF;
    }
    
    .invoice-box table{
        width:100%;
        line-height:inherit;
        text-align:left;
    }
    
    .invoice-box table td{
        padding-top:3px;
        padding-bottom:3px;
        vertical-align:top;
    }
    
    .invoice-box table tr td:nth-child(2){
        text-align:right;
    }
    .invoice-box table.history tr td:nth-child(2){
        text-align:left;
    }
    .invoice-box table.history tr td:last-child{
        text-align:right;
    }
    .invoice-box table tr.top table td{
        padding-bottom:20px;
    }
    
    .invoice-box table tr.top table td.title{
        font-size:45px;
        line-height:45px;
        color:#333;
    }
    
    .invoice-box table tr.information table td{
        padding-bottom:15px;
    }
    
    .invoice-box table tr.heading td{
        background:#eee;
        border-bottom:1px solid #ddd;
        font-weight:bold;
    }
    
    .invoice-box table tr.details td{
        padding-bottom:20px;
    }
    
    .invoice-box table tr.item td{
        border-bottom:1px solid #eee;
    }
    
    .invoice-box table tr.item.last td{
        border-bottom:none;
    }
    
    .invoice-box table tr.total td:nth-child(2){
        font-weight:bold;
    }
    .header-top{
        text-align:center;
    }
    .header-top p{
        margin-top:-10px;
        ~border-bottom:1px dashed #CCC;
        padding-bottom:5px;
    }   
    .header-center h3{
        margin-bottom:15px;
        margin-top: 5px;
        text-align: center;
    }
    .signature{
        margin-top:50px;
        text-align:center;
        position:relative;
    }
    .signature h5{
        width:200px;
        border-top:1px solid #333;
    }
    .signature h5:nth-child(2){
        right:0px;
        top:-20px;
        position:absolute;
    }
    .note{
        margin-top:25px;
    }
    .developer{
        margin-top:10px;
        text-align:center;      
    }
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td{
            width:100%;
            display:block;
            text-align:center;
        }
        
        .invoice-box table tr.information table td{
            width:100%;
            display:block;
            text-align:center;
        }
    }
    </style>
</head>

<body>
    <div class="invoice-box">
        <div class="header-top">
            <h2>Al-Hamra International Ltd.</h2>
            <p>Zindabazar, Sylhet</p>
            <h3>Client Account Statement</h3>
        </div>
        <table cellpadding="0" cellspacing="0">
            <tr class="information">
                <td colspan="2">
                    <table>
                        <tr>
                            <td class="title">

                                <b>Client ID : </b> AL-1976523 <br />
                                <b>Organization : </b>ABCD Corporation<br>
                                <b>Owner : </b>55451519 <br />
                                <b>Billing Account ID(s) : </b>10/07/2017
                            </td>
                            
                            <td>
                                <b>Statement From : </b> 10-07-2017 <br />
                                <b>Statement To :</b> 25-09-2017 <br />
                                <b>Print Date : </b> 28-09-2017
                                
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <hr />
        <div class="header-center">
            <h3>Charge/Payment History</h3>
        </div>
        <table class="history">            
            <tr class="heading">
                <td>
                    Date
                </td>
                
                <td>
                    Details
                </td>
                <td>
                    Charges
                </td>
                
                <td>
                    Payments
                </td>
                <td>
                    Balance
                </td>
            </tr>
            
            <tr class="item">
                <td>
                    Energy Charge   
                </td>
                
                <td>
                    1250
                </td>
                <td>
                    Energy Charge   
                </td>
                <td>
                    Energy Charge   
                </td>
                
                <td>
                    1250
                </td>
            </tr>
        </table>
        <div class="signature">      
                <h5>Signature 1</h5>
                <h5>Signature 2</h5>
            </tr>
        </div>
        <div class="developer">
            <h4>System Developed by StarLab IT - Contact: 01617827522</h4>
        </div>
    </div>
</body>
</html>
