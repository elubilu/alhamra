<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Al Hamra Shopping City</title>
    <!-- CSS -->
	<link rel="stylesheet" type="text/css" href="" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/back-end/font-awesome/css/font-awesome.min.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/back-end/css/bootstrap.min.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/back-end/css/select2.min.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/back-end/css/datepicker.css'); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/back-end/css/bootstrap-timepicker.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/back-end/css/bootstrap-multiselect.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/back-end/css/style_table.css'); ?>" />	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/back-end/css/metisMenu.min.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/back-end/css/style.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/back-end/css/responsive.css'); ?>" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	<style type="text/css">
		
	</style>
</head>

<body>

    <div id="wrapper">
	
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo base_url('admin/index'); ?>"> <img src="<?php //echo base_url('assets/back-end/images/alhamra <logo class="p"></logo>ng'); ?>" alt="" class="img-responsive" /> Al Hamra Shopping City</a>
            </div>

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        
                        <li><a href="<?php  echo base_url('admin/myAccount'); ?>"><i class="fa fa-user fa-fw"></i> My Account</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="<?php  echo base_url('login/logout'); ?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                </li>
            </ul>

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a class="nav-item" href="<?php echo base_url('admin/index'); ?>"><i class="fa fa-bar-chart fa-fw"></i> Dashboard</a>
                        </li>
						<li>
                            <a href="#"><i class="fa fa-tasks fa-fw"></i> Accounts<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a class="nav-item" href="<?php echo base_url('admin/ledgers'); ?>"> Ledgers</a>
                                    <!--<a class="nav-item" href="<?php //echo base_url('admin/balanceSheet'); ?>"> Balance Sheet</a>-->
                                    <a class="nav-item" href="<?php echo base_url('admin/cashTransactions'); ?>"> Cash Transactions</a>
                                </li>
                            </ul>
                        </li>
						<li>
                            <a href="#"><i class="fa fa-th-list fa-fw"></i> Asset<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a class="nav-item" href="<?php echo base_url('admin/customers'); ?>"> Customer Accounts</a>
                                    <a class="nav-item" href="<?php echo base_url('admin/fixedAssets'); ?>"> Fixed Assets</a>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <a href="#"><i class="fa fa-th-list fa-fw"></i> Billing<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a class="nav-item" href="<?php echo base_url('admin/allBills'); ?>"> All Bills</a>
                                    <a class="nav-item" href="<?php echo base_url('admin/setupBills'); ?>"> Setup Bill</a>
                                    <a class="nav-item" href="<?php echo base_url('admin/allBillingAccount'); ?>">Billing Account</a>
                                    <a class="nav-item" href="<?php echo base_url('admin/rentManagement'); ?>"> Rent Management</a>
                                </li>
                            </ul>
                        </li>
                        
						<li>
                            <a href="#"><i class="fa fa-clone fa-fw"></i> Statements<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a class="nav-item" href="<?php echo base_url('admin/incomeStatements'); ?>"> Income Statements</a>
                                    <a class="nav-item" href="<?php echo base_url('admin/expenseStatements'); ?>"> Expense Statements</a>
                                    <a class="nav-item" href="<?php echo base_url('admin/generateBalanceSheet'); ?>"> Generate Balance Sheet</a>
                                    <a class="nav-item" href="<?php echo base_url('admin/assetList'); ?>"> Asset List</a>
                                    <a class="nav-item" href="<?php echo base_url('admin/allReceivedPayment'); ?>"> All Received Payments</a>
                                    <a class="nav-item" href="<?php echo base_url('admin/allServiceCharge'); ?>"> All Service Charge</a>
                                    <a class="nav-item" href="<?php echo base_url('admin/serviceChargeSummary'); ?>">        Service Charge Summary</a>
                                </li>
                            </ul>
                        </li>
						<li class="last-link">
                            <a href="#"><i class="fa fa-bars fa-fw"></i> Others<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a class="nav-item" href="<?php echo base_url('admin/companyUser'); ?>"> Company User</a>
                                    <a class="nav-item" href="<?php echo base_url('admin/expenseStatements'); ?>"> Backup Database</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div id="page-wrapper">