
<script>

	$(function(){ 
		$('.receivePaymentRestInput').hide();
		$("#addTotalAmountButton").hide();
		/* Client Change */
		/*$("#receivePaymentPurpose").val(function() {
		    alert($(this).attr("value"));
		});*/
		//var meter_id = $('#receivePaymentPurpose').val();
		//alert(meter_id);
		

		$('#clientIDValue').change(function(){
			var meter_id = $('#meterNo').val();
			//alert(meter_id);
			if(meter_id !== '0'){
				meter_id = '0';
			}
			$('.receivePaymentRestInput').hide();
			$('.removable').remove();
			$('.receivePaymentAmount').removeAttr('disabled', 'disabled');				
			$('#meterNo').removeAttr('required', 'required');
			$("#addTotalAmountButton").hide();
			if($('#receivePaymentPurpose').val()=='1'){
			$('.receivePaymentRestInputAppend').remove();
				var client_id=$('#clientIDValue').val();
				if(client_id){
					jQuery.ajax({
						type: "POST",
						url: "<?php echo base_url(); ?>" + "admin/ajax_get_client_meter_info",
						dataType: 'json',
						data: {
						   client_id:client_id
						},
						success: function(res) {
						if (res) {
								if (res.status === true){
									$('.receivePaymentRestInput').show();
									$('#energyChargeDiv').hide();
									$('.receivePaymentAmount').attr('disabled', 'disabled');
									$('#meterNo').attr('required', 'required');
									var len = res.infos.length;
									for(var i=0;i<len;i++){
										$('#meterNo')
										.append('<option class="removable" value="'+res.infos[i].meterId+'"> '+res.infos[i].meterNo+' </option>');
									}
									
								}
								else {
									alert("Pleast Try Again");
								}
							}
						}
					});
				}
				else{
					alert("Please Select A Client First");
				}
			}
		});
		/* purpose Change */
		$('.receivePaymentPurpose').change(function(){
			var meter_id = $('#meterNo').val();
			if(meter_id !== '0'){
				meter_id = '0';
			}
			$('.receivePaymentRestInput').hide();
			$('.removable').remove();
			$('.receivePaymentAmount').removeAttr('disabled', 'disabled');				
			$('#meterNo').removeAttr('required', 'required');
			$('.receivePaymentRestInputAppend').remove();
			$("#addTotalAmountButton").hide();
			if($('#receivePaymentPurpose').val()=='1'){
				var client_id=$('#clientIDValue').val();
				if(client_id){
					jQuery.ajax({
						type: "POST",
						url: "<?php echo base_url(); ?>" + "admin/ajax_get_client_meter_info",
						dataType: 'json',
						data: {
						   client_id:client_id
						},
						success: function(res) {
						if (res) {
								if (res.status === true){

									$('.receivePaymentRestInput').show();
									$('#energyChargeDiv').hide();
									$('.receivePaymentAmount').attr('disabled', 'disabled');
									$('#meterNo').attr('required', 'required');
									var len = res.infos.length;
									for(var i=0;i<len;i++){
										$('#meterNo')
										.append('<option class="removable" value="'+res.infos[i].meterId+'"> '+res.infos[i].meterNo+' </option>');
									}
									
								}
								else {
									alert("Pleast Try Again");
								}
							}
						}
					});
				}
				else{
					alert("Please Select A Client First");
				}
			}
		});
		/* meter number change */
		$('#meterNo').change(function(){
			$('.receivePaymentRestInputAppend').remove();
			$('#energyChargeDiv').show();
			var client_id=$('#clientIDValue').val();
			var meter_id=$('#meterNo').val();
			jQuery.ajax({
				type: "POST",
				url: "<?php echo base_url(); ?>" + "admin/ajax_get_client_services",
				dataType: 'json',
				data: {
				   client_id:client_id,
				   meter_id:meter_id
				},
				success: function(res) {
				if (res) {
						if (res.status === true){
							$("#energyChargeValue").attr('placeholder',res.energyCharge);
							var len = res.services.length;
							//alert(len);
							var txt = "";
							var index=1;
							for(var i=0;i<len;i++,index++){
								/*$('.receivePaymentRestInput').append('<div class="col-md-4 receivePaymentRestInputAppend"><div class="form-group"><label>'+res.services[i].serviceName+'</label><input type="text" class="form-control serviceInputClass" name="'+res.services[i].services_serviceId+'" id="serviceInput'+index+'" placeholder="'+res.services[i].serviceTotalAmount+'" ></div></div>');*/
								txt += "<div class='col-md-4 receivePaymentRestInputAppend'><div class='form-group'><label>"+res.services[i].serviceName+"</label><input type='text' onChange='changeTotalAmount()' class='form-control serviceInputClass' name='"+res.services[i].services_serviceId+"' id='serviceInput"+index+"' placeholder='"+res.services[i].serviceTotalAmount+"' ></div></div>";
								
							}
							if(txt != ""){
										$('.receivePaymentRestInput').append(txt);
									}
							
							
							$("#addTotalAmountButton").show();
							$("#indexValue").val(index);
							
						}
						else {
							alert("Pleast Try Again");
						}
					}
				}
			});
		});
	});
</script>
<script type="text/javascript">
	function changeTotalAmount() {
		if($('#receivePaymentPurpose').val()=='1'){
			var len=$("#indexValue").val();
			var totalAmount=0;
			for(var i=1; i<len; i++){
				totalAmount=(+totalAmount)+(+$("#serviceInput"+i).val());
			}
			totalAmount=(+totalAmount)+(+$("#energyChargeValue").val());
			$('.totalPaymentAmountValue').val(totalAmount);
		}
		
	}
</script>
