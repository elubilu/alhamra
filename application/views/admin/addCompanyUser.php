<?php include('header.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">Add Company User</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url('')?>">Dash Board</a></li>
				<li class="active">Add Company User</li>
			</ol>
		</div>
	</div>
	<?php include('messages.php'); ?>
	<div class="row">
		<div class="col-md-12">
			<div class="thumbnail">
	<?php echo form_open('admin/storeCompanyUserInfo') ?>

				<div class="caption">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Full Name</label>
								<input type="text" class="form-control" name="adminName" value="" required/>
								<div class="red-text"><?php  echo form_error('adminName'); ?></div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>User ID</label>
								<input type="text" class="form-control" name="adminUserID" value="" required />
								<div class="red-text"><?php  echo form_error('adminUserID'); ?></div>
							</div>
						</div>						
					</div>
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label>Password</label>
								<input type="password" class="form-control" name="adminPassword" value="" required />
								<div class="red-text"><?php  echo form_error('adminPassword'); ?></div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>Retype Password</label>
								<input type="password" class="form-control" name="companyUserConfirmPassword" value="" required/>
								<div class="red-text"><?php  echo form_error('companyUserConfirmPassword'); ?></div>
							</div>
						</div>
						
						<div class="col-md-4">
							<div class="form-group">
								<label>User Role</label>
								<select class="form-control" name="admin_role_roleID" >
									<option value="2">Admin</option>
								</select>
							</div>
						</div>						
					</div>					
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Note</label>
								<textarea type="text" class="form-control" name="adminNote" value="" required ></textarea>
								<div class="red-text"><?php  echo form_error('adminNote'); ?></div>
							</div>
						</div>						
					</div>
					
					<div class="row">
						<div class="col-md-12">
							<button type="submit" class="btn btn-success"><i class="fa fa-thumbs-up"> Done</i></button>
							<button type="submit" class="btn btn-warning"><i class="fa fa-refresh"> Reset</i></button>
							
							<button type="button" class="btn btn-danger" onclick="window.history.back();"><i class="fa fa-arrow-left"></i> Back</button>
						</div>
					</div>
				</div>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>

<?php include('footer.php') ?>