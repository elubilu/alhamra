<?php include('header.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">My Account</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url('Admin/index'); ?>">Dash Board</a></li>
				<li class="active">My Account</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<?php if($this->session->flashdata('feedback_successfull'))
			{ ?>
				<div class="alert alert-success alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true"><i class="fa fa-times"></i></span>
						</button>
					<strong>Success!</strong>
					<?php echo $this->session->flashdata('feedback_successfull'); ?>
				</div>
			<?php } 
			if($this->session->flashdata('feedback_failed'))
				{ ?>
					<div class="alert alert-danger alert-dismissible fade in" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true"><i class="fa fa-times"></i></span>
							</button>
						<strong>Oops!</strong>
						<?php echo $this->session->flashdata('feedback_failed'); ?>
					</div>
			<?php   } ?>
		</div>
	</div>
		
	<div class="row">
		<div class="col-md-12">
			<div class="thumbnail">
				<div class="caption">
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">	
								<label>Name</label>
							<input type="" class="form-control removeReadonly" name="" value="" readonly />
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">	
								<label>Used Name</label>
							<input type="" class="form-control removeReadonly" name="" value="" readonly />
							</div>
						</div>	
						<div class="col-md-4">
							<div class="form-group">	
								<label>User Role</label>
								<select class="form-control removeDisabled" disabled >
									<option>Admin</option>
									<option>Manger</option>
									<option>Store Keeper</option>
								</select>
							</div>
						</div>							
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">	
								<label>Password</label>
								<input type="" class="form-control removeReadonly" name="" value="" readonly />
							</div>
						</div>
						<div class="col-md-6 m-top-25">
							<button class="btn btn-default removeDisabled" type="button" data-toggle="collapse" data-target="#changePass" aria-expanded="false" aria-controls="changePass" disabled>
								Change Password
							</button>
						</div>	
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="collapse" id="changePass">
								<div class="well">
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">	
												<label>Old Password</label>
												<input type="" class="form-control removeReadonly" name="" value="" readonly />
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">	
												<label>New Password</label>
												<input type="" class="form-control removeReadonly" name="" value="" readonly />
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">	
												<label>Retype New Password</label>
												<input type="" class="form-control removeReadonly" name="" value="" readonly />
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<button class="btn btn-default removeDisabled" disabled><i class="fa fa-thumbs-up"></i> Save</button>
										</div>
									</div>									
								</div>
							</div>
						</div>
					</div>					
					<div class="row">
						<div class="col-md-6">	
							<button type="submit" class="btn btn-default" id="removeReadonlyButton"><i class="fa fa-pencil"></i> Edit</button>
							<button type="submit" class="btn btn-default showreadonly" style=""><i class="fa fa-thumbs-up"></i> Save</button>
							<button type="button"class="btn btn-danger showreadonly" id="addReadonlyButton" style=""><i class="fa fa-times" ></i> Cancel</button>
						</div>						
					</div>
					
				</div>
			</div>
		</div>
	</div>
	
<?php include('footer.php') ?>