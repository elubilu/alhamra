<?php include('header.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">Rent Management</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url('')?>">Dash Board</a></li>
				<li class="active">Rent Management</li>
			</ol>
		</div>
	</div>
	
	<div class="row filterable">
		<div class="col-md-12">
			<div class="row ">
				<div class="col-md-6 col-xs-12">
					<h3>Monthly Rent</h3>
				</div>
				<div class="col-md-6 col-xs-12" style="margin-top:15px;">
					<div class="pull-right">
						<button id="filter_button" class="btn btn-default btn-filter"><i class="fa fa-filter"></i> Filter</button>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="table table-striped">
						<thead class="dark-header">
							<tr class="filters">
								<th>
									<input type="text" class="form-control text-left" placeholder="Space ID" disabled data-toggle="true">
								</th>
								<th>
									<input type="text" class="form-control text-left" placeholder="Organization" disabled data-toggle="true">
								</th>
								<th>
									<input type="text" class="form-control text-left" placeholder="Amount" disabled data-toggle="true">
								</th>								
								<th>
									<span>Details</span>
								</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>AL56987420</td>
								<td>ABCD Store</td>
								<td>25000</td>
								<td>
									<a href="<?php echo base_url('admin/rentHistory')?>" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Details"><i class="fa fa-info"></i></a>
								</td>
							</tr>							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="row filterable">
		<div class="col-md-12">
			<div class="row ">
				<div class="col-md-6 col-xs-12">
					<h3>Jomidari</h3>
				</div>
				<div class="col-md-6 col-xs-12" style="margin-top:15px;">
					<div class="pull-right">
						<button id="filter_button" class="btn btn-default btn-filter"><i class="fa fa-filter"></i> Filter</button>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="table table-striped">
						<thead class="dark-header">
							<tr class="filters">
								<th>
									<input type="text" class="form-control text-left" placeholder="Space ID" disabled data-toggle="true">
								</th>
								<th>
									<input type="text" class="form-control text-left" placeholder="Organization" disabled data-toggle="true">
								</th>
								<th>
									<input type="text" class="form-control text-left" placeholder="Amount" disabled data-toggle="true">
								</th>								
								<th>
									<span>Details</span>
								</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>AL56987420</td>
								<td>ABCD Store</td>
								<td>25000</td>
								<td>
									<a href="<?php echo base_url('admin/rentHistory')?>" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Details"><i class="fa fa-info"></i></a>
								</td>
							</tr>							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
<?php include('footer.php') ?>