<?php include('header.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">Setup Service Charges</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url('')?>">Dash Board</a></li>
				<li class="active">Setup Service Charges</li>
			</ol>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<div class="thumbnail">
				<div class="caption">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Month</label>
								<input type="text" class="form-control" name="" value="" />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label>Number of Total Units</label>
								<input type="text" class="form-control" name="" value="" />
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>Generator Bill</label>
								<input type="text" class="form-control" name="" value="" />
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>Security Bill</label>
								<input type="text" class="form-control" name="" value="" />
							</div>
						</div>						
					</div>
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label>Cleaning Charge</label>
								<input type="text" class="form-control" name="" value="" />
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>Maintenance (Lift)</label>
								<input type="text" class="form-control" name="" value="" />
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>Air Cutter</label>
								<input type="text" class="form-control" name="" value="" />
							</div>
						</div>						
					</div>
					<div class="row">
						<div class="col-md-12">
							<button type="submit" class="btn btn-success"><i class="fa fa-thumbs-up"> Done</i></button>
							<button type="submit" class="btn btn-danger"><i class="fa fa-refresh"> Reset</i></button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php include('footer.php') ?>