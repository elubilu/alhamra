<?php include('header.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">View Received Payment</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url('')?>">Dash Board</a></li>
				<li class="active">View Received Payment</li>
			</ol>
		</div>
	</div>
	<?php //print_r($info); ?>
	<div class="row">
		<div class="col-md-12">
			<div class="thumbnail">
				<div class="caption">
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label>Payment Date</label>
								<input type="text" class="form-control" name="" value="<?php echo $info->paymentDate ?>" disabled/>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>Client</label>
								<input type="text" class="form-control" name="" value="<?php echo $info->organizationName ?>" disabled />
							</div>
						</div>		
						<div class="col-md-4">
							<div class="form-group">
								<label>Amount</label>
								<input type="text" class="form-control" name="paymentAmount" value="<?php echo $info->paymentAmount ?>" disabled />
							</div>
						</div>						
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Purpose</label>
								<textarea type="text" class="form-control" name="" value="" disabled ><?php echo $info->paymentTypeName ?></textarea>
							</div>
						</div>	
						<div class="col-md-6">
							<div class="form-group">
								<label>Details</label>
								<textarea type="text" class="form-control" name="" value="" disabled ><?php echo $info->paymentDetails ?></textarea>
							</div>
						</div>				
					</div>
					<div class="row">
						<div class="col-md-12">
							<a class="btn btn-danger" onclick="window.history.back();"> <i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php include('footer.php') ?>