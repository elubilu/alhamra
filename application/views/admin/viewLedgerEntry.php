<?php include('header.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">View Ledger Entry</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url('')?>">Dash Board</a></li>
				<li class="active">View Ledger Entry</li>
			</ol>
		</div>
	</div>
	<?php //print_r($info); ?>
	<div class="row">
		<div class="col-md-12">
			<div class="thumbnail">
				<div class="caption">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Entry Type</label>
								<input type="text" class="form-control" name="" value="<?php if($info->ledgerDetailsType==1) echo "Income"; else echo "Expence"; ?>" disabled />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Account Name/Ledger Title</label>
								<input type="text" class="form-control" name="" value="<?php if($info->ledgerDetailsType==1) echo $info->transactionFrom; else echo $info->transactionTo; ?>" disabled />
							</div>
						</div>						
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Details</label>
								<textarea type="text" class="form-control" name="" value="<?php echo $info->ledgerDetails ?>" disabled><?php echo $info->ledgerDetails ?></textarea>
							</div>
						</div>						
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Amount</label>
								<input type="text" class="form-control" name="" value="<?php echo $info->ledgerDetailsAmount ?>" disabled />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Deposited to/Withdrawn from </label>
								<input type="text" class="form-control" name="" value="<?php if($info->ledgerDetailsType==1) echo $info->transactionTo; else echo $info->transactionFrom ; ?>" disabled />
							</div>
						</div>						
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Note</label>
								<textarea type="text" class="form-control" name="" value="<?php echo $info->ledgerDetailsNote ?>" disabled><?php echo $info->ledgerDetailsNote ?></textarea>
							</div>
						</div>						
					</div>
					<div class="row">
						<div class="col-md-12">
							<a class="btn btn-danger" onclick="window.history.back();"> <i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php include('footer.php') ?>