<?php include('header.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">New Ledger Entry</h3>
		</div>
	</div>
	<?php include('messages.php'); ?>
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url('')?>">Dash Board</a></li>
				<li class="active">New Ledger Entry</li>
			</ol>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<div class="thumbnail">
				<?php 
					echo form_open('admin/storeLedgerEntry');
					echo form_hidden('ledgerDetailsStatus',1);
				?>
				<div class="caption">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Entry Type</label>
								<select class="form-control" name="ledgerDetailsType" id="ledgerDetailsType">
									<option value="1">Income</option>
									<option value="2">Expense</option>
								</select>
							</div>
						</div>
						<div class="col-md-6" id="incomeDiv">
							<div class="form-group">
								<label>Account Name/Ledger Title</label>
								<select class="form-control forselect2" name="ledger_info_ledgerId" id="ledger_info_ledgerId">
									<option selected disabled >Select</option>
									<?php foreach($ledgers['income'] as $info):?>
									<option value="<?php echo $info->ledgerId ?>" class="ledger<?php echo $info->ledgerType; ?>"><?php echo $info->ledgerTitle ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="col-md-6" id="expenseDiv">
							<div class="form-group">
								<label>Account Name/Ledger Title</label>
								<select class="form-control forselect2" name="ledger_info_ledgerId" id="ledger_info_ledgerId">
									<option selected disabled >Select</option>
									<?php foreach($ledgers['expence'] as $info):?>
									<option value="<?php echo $info->ledgerId ?>" class="ledger<?php echo $info->ledgerType; ?>"><?php echo $info->ledgerTitle ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>												
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Details</label>
								<textarea type="text" class="form-control" name="ledgerDetails" value="" required></textarea>
							</div>
						</div>						
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Amount</label>
								<input type="text" class="form-control" name="ledgerDetailsAmount" value="" />
								<div class="red-text"><?php  echo form_error('ledgerDetailsAmount'); ?></div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Deposited to/Withdrawn from</label>
								<select class="form-control forselect2" name="ledgerDepositeTo" id="" value="" required>
									<option value="0">Petty Cash</option>
									<?php foreach($banks as $bank):?>
										<option value="<?php echo $bank->bankAccountId ?>"><?php echo $bank->bankAccountTitle ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>						
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Note</label>
								<textarea type="text" class="form-control" name="ledgerDetailsNote" value="" required></textarea>
							</div>
						</div>						
					</div>					
					<div class="row">
						<div class="col-md-12">
							<button type="submit" class="btn btn-success"><i class="fa fa-thumbs-up"></i> Done</button>
							<button type="reset" class="btn btn-warning"><i class="fa fa-refresh"></i> Reset</button>
							<button type="button" class="btn btn-danger" onclick="window.history.back();"><i class="fa fa-arrow-left"></i> Back</button>
						</div>
					</div>
				</div>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>

<?php include('footer.php') ?>