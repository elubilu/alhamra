<?php include('header.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">Add Asset</h3>
		</div>
	</div>
	<?php include('messages.php'); ?>
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url('')?>">Dash Board</a></li>
				<li class="active">Add Asset</li>
			</ol>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<div class="thumbnail">
					<?php 
						echo form_open('admin/storeAsset');
						echo form_hidden('assetStatus',1); 
					?>
				<div class="caption">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Asset Title</label>
								<input type="text" class="form-control" name="assetTitle" value="" />
							     <div class="red-text"> <?php  echo form_error('assetTitle'); ?></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label>Tracking ID</label>
								<input type="text" class="form-control" name="assetTrackingId" value="" />
	             			    <div class="red-text"> <?php  echo form_error('assetTrackingId'); ?></div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>Original Value</label>
								<input type="text" class="form-control" name="assetOriginalValue" value="" />
	             			    <div class="red-text"> <?php  echo form_error('assetOriginalValue'); ?></div>

							</div>
						</div>	
						<div class="col-md-4">
							<div class="form-group">
								<label>Current Value</label>
								<input type="text" class="form-control" name="assetCurrentValue" value="" />
							    <div class="red-text"> <?php  echo form_error('assetCurrentValue'); ?></div>
							</div>
						</div>							
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Accruing date</label>
								<input type="text" size="16" class="form-control span2" id="startDate" name="assetAccruingDate" placeholder="" />
							    <div class="red-text"> <?php  echo form_error('assetAccruingDate'); ?></div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Depreciation rate/year</label>
								<input type="text" class="form-control" name="assetDepreciationRate" value="" />
							    <div class="red-text"> <?php  echo form_error('assetDepreciationRate'); ?></div>
							</div>
						</div>						
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Note</label>
								<textarea type="text" class="form-control" name="assetNote" value=""></textarea>
							    <div class="red-text"> <?php  echo form_error('assetNote'); ?></div>
							</div>
						</div>						
					</div>
					
					<div class="row">
						<div class="col-md-12">
							<button type="submit" class="btn btn-success"><i class="fa fa-thumbs-up"></i> Done</button>
							<button type="submit" class="btn btn-warning"><i class="fa fa-refresh"></i> Reset</button>
							<button type="button" class="btn btn-danger" onclick="window.history.back();"><i class="fa fa-arrow-left"></i> Back</button>		
						</div>
					</div>
				</div>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>

<?php include('footer.php') ?>