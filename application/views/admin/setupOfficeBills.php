<?php include('header.php') ?>
	<div class="row">
		<div class="col-md-6">
			<h3 class="page-header">Setup Office Bills</h3>
		</div>
		<div class="col-md-6 m-top-50">	
			<div class="pull-right">
				<!--<a href="<?php //echo base_url('admin/additionalServiceCharges');?>" class="btn btn-default">Additional Service Charges</a>-->
				<a href="<?php echo base_url('admin/serviceCharges');?>" class="btn btn-default">Service Charges</a>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<?php if($this->session->flashdata('feedback_successfull'))
					{ ?>
						<div class="alert alert-success alert-dismissible fade in" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true"><i class="fa fa-times"></i></span>
								</button>
							<strong>Success!</strong>
							<?php echo $this->session->flashdata('feedback_successfull'); ?>
						</div>
					<?php } 
					if($this->session->flashdata('feedback_failed'))
						{ ?>
							<div class="alert alert-danger alert-dismissible fade in" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true"><i class="fa fa-times"></i></span>
									</button>
								<strong>Sorry! </strong>
								<?php echo $this->session->flashdata('feedback_failed'); ?>
							</div>
				<?php   } ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url('')?>">Dash Board</a></li>
				<li class="active">Setup Office Bills</li>
			</ol>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<div class="thumbnail">
				<div class="caption">
					<?php 
						echo form_open('admin/createOfficeBill');
						echo form_hidden('monthlyBillTypeFor',2);
						echo form_hidden('monthlyBillingStatus',1);
					?>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Billing Month</label>
								<input type="text" size="16" class="form-control span2" id="date" name="billingMonthYear" placeholder="" required />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Per Unit Charge</label>
								<input type="text"  class="form-control span2"  name="unitCharge" placeholder="" required />
							    <div class="red-text"><?php  echo form_error('unitCharge'); ?></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Current Reading Date</label>
								<input type="text" size="16" class="form-control span2" id="startDate" name="currentReadingDate" placeholder="" required />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Last Payment Date</label>
								<input type="text" size="16" class="form-control span2" id="endDate" name="lastPaymentDate" placeholder="" required />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<button type="submit" class="btn btn-success" style=""><i class="fa fa-thumbs-up"></i> Add</button>
							<a type="button" onclick="window.history.back();" class="btn btn-danger"> <i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
						</div>
					</div>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
	<?php if($bills){ ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">Previous Bills</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<table class="table table-striped">
				<thead class="dark-header">
					<tr>
						<th>Billing Year</th>
						<th>Billing Month</th>
						<th>Status</th>
						<th>View</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($bills as $bill){ 
						$billYear= date('Y', strtotime($bill->billingMonthYear));
						$billMonth= date('M', strtotime($bill->billingMonthYear));
					?>
					<tr>
						<td><?php echo $billYear ; ?></td>
						<td><?php echo $billMonth ; ?></td>
						<td><?php if($bill->monthlyBillingStatus==1)echo "Pending"; else echo "Final" ; ?></td>
						<td>
							<a href="<?php echo base_url("admin/pendingOfficeSetupBills/{$bill->mothlyBillInfoId}")?>" type="button"class="btn btn-primary btn-sm" style=""><i class="fa fa-info" ></i></a>
						</td>
					</tr>	
					<?php } ?>
					
				</tbody>
			</table>
		</div>
	</div>
	<?php } ?>
	
<?php include('footer.php') ?>