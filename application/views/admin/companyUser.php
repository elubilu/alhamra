		<?php include('header.php') ?>
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-header">Company User</h3>
				</div>
			</div>
			<?php include('messages.php'); ?>
			<div class="row">
				<div class="col-md-12">
					<ol class="breadcrumb">
						<li><a href="<?php echo base_url('')?>">Dash Board</a></li>
						<li class="active">Company User</li>
					</ol>
				</div>
			</div>
			
			<div class="row filterable">
				<div class="col-md-12">
					<div class="row m-bottom-10">
						<div class="col-md-12 col-xs-12">
							<div class="pull-right">
								<a href="<?php echo base_url('admin/addCompanyUser')?>" style="margin-top:0px;margin-right:5px;" type="button" class="btn btn-default">Add Company User</a>
								<button id="filter_button" class="btn btn-default btn-filter"><i class="fa fa-filter"></i> Filter</button>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<table class="table table-striped">
								<thead class="dark-header">
									<tr class="filters">
										<th>
											<input type="text" class="form-control text-left" placeholder="User" disabled data-toggle="true">
										</th>
										<th>
											<input type="text" class="form-control text-left" placeholder="Username" disabled data-toggle="true">
										</th>
										<th>
											<input type="text" class="form-control text-left" placeholder="User Role" disabled data-toggle="true">
										</th>								
										<th>
											<span>View</span>
										</th>
									</tr>
								</thead>
								<tbody>
									
									<?php foreach ($allcompanyUser as $companyUser): ?>
								    <tr>
		                                <td><?php echo $companyUser->adminName; ?></td>
		                                <td><?php echo $companyUser->adminUserID; ?></td>
		                                <td><?php echo $companyUser->roleName; ?></td>
										<td>
										    <a href=" <?php echo base_url("admin/viewCompanyUser/{$companyUser->adminID}"); ?>" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Details"><i class="fa fa-info"></i></a>
										</td>
									</tr>
									<?php endforeach;?>							
								</tbody>
							</table>
						</div>
					</div>

				</div>
			</div>

		<?php include('footer.php') ?>