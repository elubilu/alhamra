    <?php 
    $config = array(
           'login_form' => array(
                array(
    			   'field' => 'email',
                   'label' => 'User Email',
                   'rules' => 'required' 
                ),
                array(
                   'field' => 'password',
                   'label' => 'Password',
                   'rules' => 'required|md5', 
                )
                   
    		), 
          'addCustomerAccount'=> array(

              array(
                   'field' => 'organizationName',
                   'label' => 'Organization Name',
                   'rules' => 'required' 
                ),
                array(
                   'field' => 'customerName',
                   'label' => 'Customer Name',
                   'rules' => 'required', 
                ),
                 array(
                   'field' => 'customerPhone',
                   'label' => 'customer Phone',
                   'rules' => 'required|regex_match[/^[0-9]{11}$/]', //for matching number
                ),
               
           
                array(
                   'field' => 'rentAmount',
                   'label' => 'Rent Amount',
                   'rules' => 'required|numeric', 
                ),
                   array(
                   'field' => 'rentDue',
                   'label' => 'Rent Due',
                   'rules' => 'required|numeric', 
                ),



           ) , 
        'viewCustomerAccount'=> array(

              array(
                   'field' => 'OrganizationName',
                   'label' => 'organization Name',
                   'rules' => 'required' 
                ),
                array(
                   'field' => 'customerName',
                   'label' => 'Customer Name',
                   'rules' => 'required|md5', 
                ),
                 array(
                   'field' => 'customerPhone',
                   'label' => 'Customer Phone',
                   'rules' => 'required|regex_match[/^[0-9]{11}$/]', //for matching number
                ),
               
           
                array(
                   'field' => 'rentAmount',
                   'label' => 'Rent Amount',
                   'rules' => 'required|numeric', 
                ),
                   array(
                   'field' => 'rentDue',
                   'label' => 'Rent Due',
                   'rules' => 'required|numeric', 
                ),



           ) , 

           'addBankAccount'=> array(

              array(
                   'field' => 'bankAccountNote',
                   'label' => 'Note',
                   'rules' => 'required' 
                ),
                array(
                   'field' => 'bankAccountLastBalance',
                   'label' => 'Balance as Per Last Account',
                   'rules' => 'required|numeric', 
                ),
                 array(
                   'field' => 'bankAccountRoutingNumber',
                   'label' => 'Routing Number',
                   'rules' => 'required', 
                ),
                array(
                   'field' => 'bankAccountNumber',
                   'label' => 'Account Number',
                   'rules' => 'required', 
                ),
                array(
                   'field' => 'bankAccountHolderName',
                   'label' => 'Account Holder Name',
                   'rules' => 'required', 
                ),
                array(
                   'field' => 'bankAccountBranchName',
                   'label' => 'Branch Name',
                   'rules' => 'required', 
                ),
                  array(
                   'field' => 'bankName',
                   'label' => 'Bank Name',
                   'rules' => 'required', 
                ),
                array(
                   'field' => 'bankAccountTitle',
                   'label' => 'AccountTitle',
                   'rules' => 'required', 
                ),



           ) ,       

           'viewBankAccount'=> array(

              array(
                   'field' => 'bankAccountNote',
                   'label' => 'Note',
                   'rules' => 'required' 
                ),
                
                 array(
                   'field' => 'bankAccountRoutingNumber',
                   'label' => 'Routing Number',
                   'rules' => 'required', 
                ),
                array(
                   'field' => 'bankAccountNumber',
                   'label' => 'Account Number',
                   'rules' => 'required', 
                ),
                array(
                   'field' => 'bankAccountHolderName',
                   'label' => 'Account Holder Name',
                   'rules' => 'required', 
                ),
                array(
                   'field' => 'bankAccountBranchName',
                   'label' => 'Branch Name',
                   'rules' => 'required', 
                ),
                  array(
                   'field' => 'bankName',
                   'label' => 'Bank Name',
                   'rules' => 'required', 
                ),
               



           ) , 

           'addCompanyUser'=> array(

              array(
                   'field' => 'adminName',
                   'label' => 'Full Name',
                   'rules' => 'required' 
                ),
                array(
                   'field' => 'adminUserID',
                   'label' => 'User ID',
                   'rules' => 'required|is_unique[admin_info.adminUserID]', 
                ),
                 array(
                   'field' => 'adminPassword',
                   'label' => 'Password',
                   'rules' => 'required|md5', 
                ),
                 array(
                   'field' => 'companyUserConfirmPassword',
                   'label' => 'Confirm Password',
                   'rules' => 'required|md5|matches[adminPassword]', 
                ),
                 array(
                   'field' => 'adminNote',
                   'label' => 'Note',
                   'rules' => 'required', 
                ),
           
               



           ) ,   
            'viewCompanyUser'=> array(

              array(
                   'field' => 'adminName',
                   'label' => 'Name',
                   'rules' => 'required' 
                ),
                

                 array(
                   'field' => 'adminNote',
                   'label' => 'Note',
                   'rules' => 'required', 
                ),
           
               



           ) , 
           'addAsset'=> array(

              array(
                   'field' => 'assetTitle',
                   'label' => 'Asset Title',
                   'rules' => 'required' 
                ),
                

                 array(
                   'field' => 'assetTrackingId',
                   'label' => 'Tracking Id',
                   'rules' => 'required', 
                ),


                 array(
                   'field' => 'assetOriginalValue',
                   'label' => 'Original Value',
                   'rules' => 'required|numeric', 
                ),
           
                 array(
                   'field' => 'assetCurrentValue',
                   'label' => 'Current Value',
                   'rules' => 'required|numeric', 
                ),
                 array(
                   'field' => 'assetAccruingDate',
                   'label' => 'Accruing Date',
                   'rules' => 'required', 
                ),
                 array(
                   'field' => 'assetDepreciationRate',
                   'label' => 'Depreciation Rate',
                   'rules' => 'required', 
                ),
                 array(
                   'field' => 'assetNote',
                   'label' => 'Note',
                   'rules' => 'required', 
                ),         

           ) ,    


           'viewAsset'=> array(

              array(
                   'field' => 'assetTitle',
                   'label' => 'Asset Title',
                   'rules' => 'required' 
                ),
                

                 array(
                   'field' => 'assetTrackingId',
                   'label' => 'Tracking Id',
                   'rules' => 'required', 
                ),


                 array(
                   'field' => 'assetOriginalValue',
                   'label' => 'Original Value',
                   'rules' => 'required|numeric', 
                ),
           
                 array(
                   'field' => 'assetCurrentValue',
                   'label' => 'Current Value',
                   'rules' => 'required|numeric', 
                ),
                 array(
                   'field' => 'assetAccruingDate',
                   'label' => 'Accruing Date',
                   'rules' => 'required', 
                ),
                 array(
                   'field' => 'assetDepreciationRate',
                   'label' => 'Depreciation Rate',
                   'rules' => 'required|numeric', 
                ),
                 array(
                   'field' => 'assetNote',
                   'label' => 'Note',
                   'rules' => 'required', 
                ),

           ) ,    

           'receivedPayment'=>array(

                   array(
                   'field' => 'paymentAmount',
                   'label' => 'Amount',
                   'rules' => 'required|numeric', 
                ),

                   array(
                   'field' => 'payment_types_paymentTypeId',
                   'label' => 'Purpose',
                   'rules' => 'required', 
                ),

                   array(
                   'field' => 'customer_info_customerId',
                   'label' => ' Client',
                   'rules' => 'required', 
                ),

            ) , 
             'addNewLedger'=>array(

                   array(
                   'field' => 'ledgerBalance',
                   'label' => 'Ledger Balance',
                   'rules' => 'required|numeric', 
                ),
            ), 

            'newLedgerEntry'=>array(

                   array(
                   'field' => 'ledgerDetailsAmount',
                   'label' => 'Amount',
                   'rules' => 'required|numeric', 
                ),
            ), 
            'setupOfficeBills'=>array(

                   array(
                   'field' => 'unitCharge',
                   'label' => 'Per Unit Charge',
                   'rules' => 'required|numeric', 
                ),
            ), 
            'addNewCashTransfer'=>array(

                   array(
                   'field' => 'cashTransferAmount',
                   'label' => 'Amount',
                   'rules' => 'required|numeric', 
                ),
            ),  
            'updateCustomerAccount'=>array(

                   array(
                   'field' => 'rentAmount',
                   'label' => 'Monthly Rent/Jomidari Charge',
                   'rules' => 'required|numeric', 
                ),
            ), 
            'bankAccounAdjustment'=>array(

                   array(
                   'field' => 'accountAdjustmentbeforeAmount',
                   'label' => 'Current Balance',
                   'rules' => 'required|numeric', 
                ),
                   array(
                   'field' => 'accountAdjustmentAmount',
                   'label' => 'Amount',
                   'rules' => 'required|numeric', 
                ),
            ),
   	);
    	
    	
    	
    ?>	
    	