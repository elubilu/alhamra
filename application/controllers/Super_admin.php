
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Super_admin extends MY_Controller {	
	public function __construct(){
		parent:: __construct();
		$this->load->model('super_model');
		$this->load->model('home_model');
		$this->load->model("MAutocomplete"); 
		//$data=$this->session->userdata('user_role');
		if($this->session->userdata('user_role')!=2)return redirect('login/');
	}
	
	public function dashBoard(){
		$this->load->view('super_admin/dashBoard');
	}
	public function addBillMemo(){
		$this->load->view('super_admin/addBillMemo');
	}
	public function returnProduct(){
		$this->load->view('super_admin/returnProduct');
	}
	public function showInventory(){
		$this->load->view('super_admin/showInventory');
	}
	public function salesmanPerformaneceReport(){
		$this->load->view('super_admin/salesmanPerformaneceReport');
	}
	public function addExpense(){
		$this->load->view('super_admin/addExpense');
	}
	public function addNewProduct(){
		$this->load->view('super_admin/addNewProduct');
	}
	public function allManager(){
		$this->load->view('super_admin/allManager');
	}
	public function addManager(){
		$this->load->view('super_admin/addManager');
	}
	public function allSalesman(){
		$this->load->view('super_admin/allSalesman');
	}
	public function addSalesman(){
		$this->load->view('super_admin/addSalesman');
	}
	public function allSupplier(){
		$this->load->view('super_admin/allSupplier');
	}
	public function addSupplier(){
		$this->load->view('super_admin/addSupplier');
	}
	public function adjustStock(){
		$this->load->view('super_admin/adjustStock');
	}	
	public function addProductToStock(){
		$this->load->view('super_admin/addProductToStock');
	}
	public function viewProduct(){
		$this->load->view('super_admin/viewProduct');
	}	
	public function productDetails(){
		$this->load->view('super_admin/productDetails');
	}		
	public function incomeExpenseReport(){
		$this->load->view('super_admin/incomeExpenseReport');
	}
	public function productSaleReport(){
		$this->load->view('super_admin/productSaleReport');
	}
	public function returnReport(){
		$this->load->view('super_admin/returnReport');
	}
	public function allProductGroup(){
		$this->load->view('super_admin/allProductGroup');
	}
	public function addProductGroup(){
		$this->load->view('super_admin/addProductGroup');
	}
	public function allExpenseField(){
		$this->load->view('super_admin/allExpenseField');
	}
	public function addExpenseField(){
		$this->load->view('super_admin/addExpenseField');
	}
	public function allShop(){
		$this->load->view('super_admin/allShop');
	}
	public function addShop(){
		$this->load->view('super_admin/addShop');
	}
	
}
