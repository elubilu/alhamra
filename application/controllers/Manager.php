﻿<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manager extends MY_Controller {

	public function __construct(){
		parent:: __construct();
		$this->load->model('manager_model');
		if($this->session->userdata('user_role')!=4)return redirect('login/');
	}

	public function index(){
		$cash=$this->manager_model->get_cash_amount();
		$this->load->view('manager/dashBoard',['cash'=>$cash]);
	}	
	public function myAccount(){
		$admin_id=$this->session->userdata('admin_id');
		$user=$this->manager_model->my_info($admin_id);
		$this->load->view('manager/myAccount',['info'=>$user]);
	}
	public function updateContact(){
		$admin_id=$this->session->userdata('admin_id');
		$user=$this->manager_model->my_info($admin_id);
		if($user){
			//$this->load->view('super_admin/header',['info'=>$user]);
			$data['adminContact']=$this->input->post('contact');
			if($this->manager_model->update_contact($admin_id,$data)){
				$this->session->set_flashdata('feedback_successfull', 'Updated contact successfully');
				return redirect('manager/myAccount');
			}
			else {
				$this->session->set_flashdata('feedback_failed', 'Updating contact failed!');
				return redirect('manager/myAccount');
			}
		}
		else
			return redirect('login/');

	}
	
	public function updatePassword(){

		$admin_id=$this->session->userdata('admin_id');
		$user=$this->manager_model->my_info($admin_id);
		if($user){
			$this->load->library('form_validation');
			if($this->form_validation->run('update_password')){
				$data=$this->input->post();
				//print_r($data); print_r($user); exit;
				if($user->adminPassword==$data['old_pass']){
					$password['adminPassword']=$data['new_pass'];
					//print_r($password); exit;
					if($this->manager_model->change_password($password,$admin_id)){
							//echo "user Add Successful";
							$this->session->set_flashdata('feedback_successfull', 'Changed Password Successfully');
							return redirect('login/logout');
					}
					else{
						$this->session->set_flashdata('feedback_failed', 'Failed to Change Password');
						return redirect('manager/myAccount');
					}

				}
				else{
					$this->session->set_flashdata('feedback_failed', 'Old Password is not Matching ');
					return redirect('manager/myAccount');
				}

			}
			else{
				$this->load->view('manager/my_account',['info'=>$user]);
			}
		}
		else
			return redirect('login/');
	}
	public function addBillMemo(){
		$salesmans=$this->manager_model->get_all_salesman_id_name();
		$this->load->view('manager/addBillMemo',['infos'=>$salesmans]);
	}
	
	
	public function storeBill(){
		$bill=$this->input->post();
		//print_r($bill);exit;
		if($cart=$this->cart->contents()){
			
			$cart=$this->cart->contents();
			$sale_id=$this->manager_model->store_bill($bill,$cart);
			//print_r($sale_id);
			if($sale_id){
				$this->cart->destroy();
				$this->session->set_flashdata('feedback_successfull', 'Transaction successfull');
				//$this->load->view('manager/addBillMemo',['infos'=>$salesmans]);
				//if($bill['salesman_info_salesmanID'])
				$sale_info=$this->manager_model->get_sale_info_by_id($sale_id,$bill['salesman_info_salesmanID']);
				$sale_details=$this->manager_model->get_sale_info_details_by_id($sale_id);
				//print_r($sale_info);
				//print_r($sale_details);exit;
				$this->load->view('manager/printInvoice',['info'=>$sale_info,'details'=>$sale_details]);
			}
			else{
				$this->session->set_flashdata('feedback_failed', 'Please Try Again');
				redirect($this->agent->referrer());
			}
		}
		
	}
	
	public function returnProduct(){
		$this->load->view('manager/returnProduct');
	}
	public function showInventory(){
		$this->load->library('pagination');
		$config['total_rows']=$this->db->get('product_info')->num_rows();
		$config['base_url']=base_url('manager/showInventory');
		$config['per_page']=12;
		$config['num_links']=5;
		$this->pagination->initialize($config);
		$data=$this->manager_model->get_view_product_info($config['per_page'],$this->uri->segment(3));
		$this->load->view('manager/showInventory',['infos'=>$data]);
	}
	public function addExpense(){
		$expenseField=$this->manager_model->get_all_expense_field_id_name();
		$this->load->view('manager/addExpense',['infos'=>$expenseField]);
	}
	public function storeExpense(){
		$data=$this->input->post();
		if($this->manager_model->store_expense($data)){
			$this->session->set_flashdata('feedback_successfull', 'Added New Expense successfully');
			return redirect('manager/incomeExpenseReport');
		}
		else{
				$this->session->set_flashdata('feedback_failed', 'Please Try Again');
				redirect($this->agent->referrer());
		}
	}
	public function viewProduct(){
		$this->load->library('pagination');
		$config['total_rows']=$this->db->get('product_info')->num_rows();
		$config['base_url']=base_url('manager/viewProduct');
		$config['per_page']=12;
		$config['num_links']=5;
		$this->pagination->initialize($config);
		$data=$this->manager_model->get_view_product_info($config['per_page'],$this->uri->segment(3));
		$this->load->view('manager/viewProduct',['infos'=>$data]);
	}	
	public function productDetails($productID){
		$data=$this->manager_model->get_product_details_by_id($productID);
		$sale_info=$this->manager_model->get_sale_details_by_id($productID);
		//print_r($data);exit;
		$this->load->view('manager/productDetails',['infos'=>$data,'sale_infos'=>$sale_info]);
	}
	public function incomeExpenseReport(){
		if($this->input->post()){
			$data=$this->input->post();
			$data['startDate']= date('Y-m-d', strtotime($data['startDate']));
			$data['startTime']= date('H:i:s', strtotime($data['startTime']));
			$startDate=$data['startDate']." ".$data['startTime'] ;
			$data['endDate']= date('Y-m-d', strtotime($data['endDate']));
			$data['endTime']= date('H:i:s', strtotime($data['endTime']));
			$endDate=$data['endDate']." ".$data['endTime'] ;
			$shop_id=$this->session->userdata('userShop');
			/*$this->load->library('pagination');
			$config['total_rows']=$this->db->where('shop_info_transectionShopID',$shop_id)->where('transectionDate >=',$startDate)->where('transectionDate <=',$endDate)->get('transection_info')->num_rows();
			$config['base_url']=base_url('manager/incomeExpenseReport');
			$config['per_page']=15;
			$config['num_links']=5;
			$this->pagination->initialize($config);
			$transection_details=$this->manager_model->get_transection_by_dates_and_shop_id($config['per_page'],$this->uri->segment(3),$startDate,$endDate,$shop_id);*/
			$transection_details=$this->manager_model->get_transection_by_dates_and_shop_id($startDate,$endDate,$shop_id);
			$amount['income']=$this->manager_model->total_income_by_dates($startDate,$endDate);
			$amount['expense']=$this->manager_model->total_expense_by_dates($startDate,$endDate);
			$amount['return']=$this->manager_model->total_return_by_dates($startDate,$endDate);
			$amount['flag']=0;
			$this->load->view('manager/incomeExpenseReport',['infos'=>$transection_details,'amount'=>$amount]);
		}
		else{
			$shop_id=$this->session->userdata('userShop');
			$this->load->library('pagination');
			$config['total_rows']=$this->db->where('shop_info_transectionShopID',$shop_id)->get('transection_info')->num_rows();
			$config['base_url']=base_url('manager/incomeExpenseReport');
			$config['per_page']=15;
			$config['num_links']=5;
			$this->pagination->initialize($config);
			$data=$this->manager_model->get_all_transection_by_shop_id($config['per_page'],$this->uri->segment(3),$shop_id);
			$amount['income']=$this->manager_model->total_income();
			$amount['expense']=$this->manager_model->total_expense();
			$amount['return']=$this->manager_model->total_return();
			$amount['flag']=1;
			$this->load->view('manager/incomeExpenseReport',['infos'=>$data,'amount'=>$amount]);
		}
	}
	
	public function transectionDetails($ref_id,$type){
		//print_r($type);exit;
		if($type==1){
			$salesman_id=$this->manager_model->get_salesman_id_for_invoice_by_sale_id($ref_id);
			//print_r($salesman_id);exit;
			return redirect('manager/saleDetails/'.$ref_id.'/'.$salesman_id);
		}
		else if($type==2){
			 return redirect('manager/expenseDetails/'.$ref_id);
		}
		else if($type==3){
			return redirect('manager/returnReportDetails/'.$ref_id);
		}
		//$transectionDetails=$this->manager_model->get_transection_details_by_ref_id($ref_id,$type);
		//$this->load->view('manager/transectionDetails');
	}
	public function productSaleReport(){
		$shop_id=$this->session->userdata('userShop');
		if($this->input->post()){
			$data=$this->input->post();
			$data['saleStartDate']= date('Y-m-d 00:00:00', strtotime($data['saleStartDate']));
			$data['saleEndDate']= date('Y-m-d 23:59:59', strtotime($data['saleEndDate']));
			$sale_info=$this->manager_model->get_sale_report_by_dates_and_shop_id($data['saleStartDate'],$data['saleEndDate']);
			$flag['flag']=0;
			$this->load->view('manager/productSaleReport',['infos'=>$sale_info,'flag'=>$flag]);
		}
		else{
			$this->load->library('pagination');
			$config['total_rows']=$this->db->where('sale_details.shop_info_shopID',$shop_id)->get('sale_details')->num_rows();
			$config['base_url']=base_url('manager/productSaleReport');
			$config['per_page']=12;
			$config['num_links']=5;
			$this->pagination->initialize($config);
			$sale_info=$this->manager_model->get_sale_report($config['per_page'],$this->uri->segment(3));
			$flag['flag']=1;
			$this->load->view('manager/productSaleReport',['infos'=>$sale_info,'flag'=>$flag]);
		}
		
	}
	public function returnReport(){
		if($this->input->post()){
			$data=$this->input->post();
			$data['returnStartDate']= date('Y-m-d 00:00:00', strtotime($data['returnStartDate']));
			$data['returnEndDate']= date('Y-m-d 23:59:59', strtotime($data['returnEndDate']));
			$shop_id=$this->session->userdata('userShop');
			$data=$this->manager_model->get_return_report_by_dates($data['returnStartDate'],$data['returnEndDate']);
			$flag['flag']=0;
			$this->load->view('manager/returnReport',['infos'=>$data,'flag'=>$flag]);
		}
		else{
			$shop_id=$this->session->userdata('userShop');
			$this->load->library('pagination');
			$config['total_rows']=$this->db->where('shop_info_returnShopID',$shop_id)->get('exchange_info')->num_rows();
			$config['base_url']=base_url('manager/returnReport');
			$config['per_page']=15;
			$config['num_links']=5;
			$this->pagination->initialize($config);
			$data=$this->manager_model->get_return_report($config['per_page'],$this->uri->segment(3));
			$flag['flag']=1;
			$this->load->view('manager/returnReport',['infos'=>$data,'flag'=>$flag]);
		}
	}
	public function returnReportDetails($return_id){
		$data['return']=$this->manager_model->get_return_details_by_id($return_id);
		$data['exchange']=$this->manager_model->get_return_exchange_details_by_id($return_id);
		//print_r($data);exit;
		$this->load->view('manager/returnReportDetails',['infos'=>$data]);
	}	
	public function salesmanPerformaneceReport(){
		if($this->input->post()){
			$data=$this->input->post();
			$data['salemanStartDate']= date('Y-m-d 00:00:00', strtotime($data['salemanStartDate']));
			$data['salemanEndDate']= date('Y-m-d 23:59:59', strtotime($data['salemanEndDate']));
			$shop_id=$this->session->userdata('userShop');
			$data=$this->manager_model->get_salesman_report_by_dates($data['salemanStartDate'],$data['salemanEndDate']);
			$flag['flag']=0;
			$this->load->view('manager/salesmanPerformaneceReport',['infos'=>$data,'flag'=>$flag]);
		}
		else{
			$shop_id=$this->session->userdata('userShop');
			$this->load->library('pagination');
			$config['total_rows']=$this->db->where('shop_info_saleShopID',$shop_id)->where('salesman_info_salesmanID>',0)->get('sale_info')->num_rows();
			$config['base_url']=base_url('manager/salesmanPerformaneceReport');
			$config['per_page']=15;
			$config['num_links']=5;
			$this->pagination->initialize($config);
			$data=$this->manager_model->get_salesman_report($config['per_page'],$this->uri->segment(3));
			$flag['flag']=1;
			$this->load->view('manager/salesmanPerformaneceReport',['infos'=>$data,'flag'=>$flag]);
		}
		//$this->load->view('manager/salesmanPerformaneceReport');
	}
	public function storeExchange(){
		$data=$this->input->post();
		//print_r($data);exit;
		if($this->manager_model->store_return_info($data)){
			$this->session->set_flashdata('feedback_successfull', 'Added New Expense successfully');
			redirect($this->agent->referrer());
		}
		else{
			$this->session->set_flashdata('feedback_failed', 'Please Try Again');
			redirect($this->agent->referrer());
		}
	}
	public function allProductGroup(){
		$this->load->view('manager/allProductGroup');
	}
	public function addProductGroup(){
		$this->load->view('manager/addProductGroup');
	}
	public function allExpenseField(){
		$this->load->view('manager/allExpenseField');
	}
	public function addExpenseField(){
		$this->load->view('manager/addExpenseField');
	}
	public function allShop(){
		$this->load->view('manager/allShop');
	}
	public function addShop(){
		$this->load->view('manager/addShop');
	}
	public function searchProduct(){
		$this->load->view('manager/searchProduct');
	}
	public function saleDetails($sale_id,$salesman_id){
		$sale_info=$this->manager_model->get_sale_info_by_id($sale_id,$salesman_id);
		$sale_details=$this->manager_model->get_sale_info_details_by_id($sale_id);
				//print_r($sale_info);
				//print_r($sale_details);exit;
		$this->load->view('manager/saleDetails',['info'=>$sale_info,'details'=>$sale_details]);
	}	
	public function expenseDetails($expense_id){
		$expense=$this->manager_model->get_expense_details_by_id($expense_id);
		$this->load->view('manager/expenseDetails',['infos'=>$expense]);
	}

	public function allSalesman(){
		$this->load->library('pagination');
		$config['total_rows']=$this->db->get('salesman_info')->num_rows();
		$config['base_url']=base_url('manager/allSalesman');
		$config['per_page']=10;
		$config['num_links']=5;
		$this->pagination->initialize($config);
		$data=$this->manager_model->all_salesman($config['per_page'],$this->uri->segment(3));
		$this->load->view('manager/allSalesman',['infos'=>$data]);
		//$this->load->view('manager/allSalesman');
	}
	public function addSalesman(){
		$shop_info=$this->manager_model->get_all_shop_id_name();
		$this->load->view('manager/addSalesman',['shop_info'=>$shop_info]);
	}
	public function storeSalesman(){
		$this->load->library('form_validation');
		if($this->form_validation->run('add_sales_man')){
			$data=$this->input->post();
			//$userID=$this->manager_model->check_manager_id($data['managerUserID']);
			//print_r($userID);exit;
			//if($userID){
				if($this->manager_model->add_sales_man($data)){
					$this->session->set_flashdata('feedback_successfull', 'Added New Salesman Successfully');
					return redirect('manager/allSalesman');
				}
				else{
					$this->session->set_flashdata('feedback_failed', 'Add Salesman Failed!');
					return redirect('manager/allSalesman');
				}
			/*}
			else{
				$this->session->set_flashdata('feedback_failed', 'Given User ID is Already in Use');
				return redirect('manager/allSalesman');
			}*/
		}
		else{
			$shop_info=$this->manager_model->get_all_shop_id_name();
			$this->load->view('manager/addSalesman',['shop_info'=>$shop_info]);
		}
	}
	public function detailsSaleman($salesman_id){
		$user=$this->manager_model->get_selaman_details_by_id($salesman_id);
		$this->load->view('manager/detailsSaleman',['infos'=>$user]);
	}
	public function editSaleman($salesman_id){
		$user=$this->manager_model->get_selaman_details_by_id($salesman_id);
		$shop_info=$this->manager_model->get_all_shop_id_name();
		$this->load->view('manager/editSaleman',['infos'=>$user,'shop_info'=>$shop_info]);
	}
	public function updateSaleman($admin_id){
		$this->load->library('form_validation');
		//print_r($this->input->post());exit;
		//if($this->form_validation->run('add_sales_man')){
			$data=$this->input->post();
			//print_r($this->input->post());exit;
			if($this->manager_model->update_saleman($data,$admin_id)){
				$this->session->set_flashdata('feedback_successfull', 'Updated Salesman Successfully');
				return redirect('manager/detailsSaleman/'.$admin_id);
			}
			else{
				$this->session->set_flashdata('feedback_failed', 'Update Salesman Failed!');
				return redirect('manager/detailsSaleman/'.$admin_id);
			}
		/*}
		else{
			$user=$this->admin_model->get_selaman_details_by_id($admin_id);
			$shop_info=$this->admin_model->get_all_shop_id_name();
			$this->load->view('admin/editSaleman',['infos'=>$user,'shop_info'=>$shop_info]);
		}*/
	}
}
