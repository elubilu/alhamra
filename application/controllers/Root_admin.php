<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Root_admin extends MY_Controller {

	public function __construct(){
		parent:: __construct();
		$this->load->model('root_model');
		//$this->load->model('supper_model');
		//$data=$this->session->userdata('user_role');
		if($this->session->userdata('user_role')!=1)return redirect('login/');
	}

	
	public function index(){	
		$admin_id=$this->session->userdata('admin_id');
		$user=$this->root_model->my_info($admin_id);
		if($user){
		//$this->load->view('root_admin/header',['info'=>$user]);
		$this->load->view('root_admin/dash_board');
		}
		else
			return redirect('login/');
	}
	
	public function my_account(){
		
		$admin_id=$this->session->userdata('admin_id');
		$data=$this->root_model->my_info($admin_id);
		//$this->load->view('root_admin/header',['info'=>$data]);
		//print_r($data);
		//exit;
		if($data)
		$this->load->view('root_admin/my_account',['info'=>$data]);
		else
			return redirect('login/');
	}
	
	
	public function user(){
		
		$admin_id=$this->session->userdata('admin_id');
		$user=$this->root_model->my_info($admin_id);
		/*if($user)
		$this->load->view('root_admin/header',['info'=>$user]);
		else
			return redirect('login/');*/
		$this->load->library('pagination');
		//$data=$this->admin_model->users_info();
		$config['total_rows']=$this->db->where('admin_role_role_id!=',1)->get('admin_info')->num_rows();
		$config['base_url']=base_url('root_admin/user');
		$config['per_page']=10;
		$config['num_links']=5;
		$this->pagination->initialize($config);

		$data=$this->root_model->users_info($config['per_page'],$this->uri->segment(3));
		$this->load->view('root_admin/user_info',['infos'=>$data]);
	}
	
	public function add_user(){
		$admin_id=$this->session->userdata('admin_id');
		$user=$this->root_model->my_info($admin_id);
		if($user){
			//$this->load->view('root_admin/header',['info'=>$user]);
			$data=$this->root_model->get_user_role();
			$this->load->view('root_admin/add_user',['roles'=>$data]);
		}
		else
			return redirect('login/');
		
	}
	
	public function store_user(){
		$this->load->library('form_validation');
		if($this->form_validation->run('add_user')){
			$data=$this->input->post();
			unset($data['add'],$data['confirm_password']);
			//print_r($data);
			//exit;
			if($this->root_model->add_user($data)){
					//echo "user Add Successful";
					$this->session->set_flashdata('feedback_successfull', 'Added user successfully');
					return redirect('root_admin/add_user');
				}
				else{
					$this->session->set_flashdata('feedback_failed', 'Add user failed!');
					return redirect('root_admin/add_user');
				}
		}
		else{
			$data=$this->root_model->get_user_role();
			$this->load->view('root_admin/add_user',['roles'=>$data]);
		}
	}
	public function profile($user_id){
		$admin_id=$this->session->userdata('admin_id');
		$user=$this->root_model->my_info($admin_id);
		if($user){
			//$this->load->view('root_admin/header',['info'=>$user]);
			$data=$this->root_model->get_user($user_id);
			$this->load->view('root_admin/user_profile',['data'=>$data]);
		}
		else
			return redirect('login/');
		//$id= $this->input->post();
		//$admin_id=$id['userAI'];
		
	}
	public function edit_user($user_id){
		$admin_id=$this->session->userdata('admin_id');
		$user=$this->root_model->my_info($admin_id);
		if($user){
		//$this->load->view('root_admin/header',['info'=>$user]);
			$data=$this->root_model->get_user($user_id);
			$this->load->view('root_admin/edit_user_info',['data'=>$data]);
		}
		else
			return redirect('login/');
		
	}
	public function update_user($admin_id){
		
		$my_id=$this->session->userdata('admin_id');
		$user=$this->root_model->my_info($my_id);
		if($user){
			//$this->load->view('root_admin/header',['info'=>$user]);
			$data = $this->input->post();
			//print_r($data);
			//exit;
			unset($data['edit']);
			if ($this->root_model->update_user($admin_id, $data)) {
				$this->session->set_flashdata('feedback_successfull', 'Updated user info successfully');
				return redirect('root_admin/user');
			} else {
				$this->session->set_flashdata('feedback_failed', 'Updating user info failed!');
				return redirect('root_admin/user');
			}
		}
		else
			return redirect('login/');

		$this->load->library('form_validation');
		//if($this->form_validation->run('add_user')){
		
		//else return redirect('root_admin/user');
	}
	
	public function update_contact(){
		$admin_id=$this->session->userdata('admin_id');
		$user=$this->root_model->my_info($admin_id);
		if($user)
		$this->load->view('root_admin/header',['info'=>$user]);
		else
			return redirect('login/');
		$data['contact']=$this->input->post('contact');
		if($this->root_model->update_contact($admin_id,$data)){
			$this->session->set_flashdata('feedback_successfull', 'Updated contact successfully');
			return redirect('root_admin/my_account');
		}
		else {
			$this->session->set_flashdata('feedback_failed', 'Updating contact failed!');
			return redirect('root_admin/my_account');
		}

	}
	public function change_password(){
		
		$admin_id=$this->session->userdata('admin_id');
		$user=$this->root_model->my_info($admin_id);
		if($user){
			$this->load->library('form_validation');
			if($this->form_validation->run('update_password')){
				$data=$this->input->post();
				//print_r($data); print_r($user); exit;
				if($user->password==$data['old_pass']){
					$password['password']=$data['new_pass'];
					if($this->root_model->change_password($password,$admin_id)){
							//echo "user Add Successful";
							$this->session->set_flashdata('feedback_successfull', 'Changed Password Successfully');
							return redirect('root_admin/my_account');
					}
					else{
						$this->session->set_flashdata('feedback_failed', 'Failed to Change Password');
						return redirect('root_admin/my_account');
					}
					
				}
				else{
					$this->session->set_flashdata('feedback_failed', 'Old Password is not Matching ');
					return redirect('root_admin/my_account');
				}
				
			}
			else{
				$this->load->view('root/my_account',['info'=>$user]);
			}
		}
		else
			return redirect('login/');

	}
	
	public function add_category(){
		$admin_id=$this->session->userdata('admin_id');
		$user=$this->root_model->my_info($admin_id);
		if($user){
			//$this->load->view('root_admin/header',['info'=>$user]);
			$data=$this->root_model->get_category_parent();
			$this->load->view('root_admin/add_category',['parents'=>$data]);
		}
		else
			return redirect('login/');
		
	}
	
	
	public function store_category(){
		$this->load->library('form_validation');
		//if($this->form_validation->run('add_category')){
			$images = array();
			$data = $this->input->post();
			$parent=$this->input->post('parent_id');
			//print_r($parent); exit;
			if($parent){
				$depth_id=$this->root_model->get_parent_depth($parent);
			}
			//print_r($depth_id); exit;
			if($depth_id){
				$data['depth']=$depth_id+1;
			}
			else $data['depth']=1;
			//print_r($parent); exit;
			unset($data['add'], $data['category_pic']);
			if (isset($_FILES['category_pic'])&& $_FILES['category_pic']['size'] > 0) {
				$config['upload_path'] = './images/';
				$config['allowed_types'] = 'gif|jpg|png';
				//echo "no"; exit;
				$this->load->library('upload', $config);
				if (!$this->upload->do_upload('category_pic')) {
					$this->session->set_flashdata('feedback_failed', 'Image upload failed');
					return redirect('root_admin/add_category');
				} 
				else {
						$data1 = $this->upload->data(); //array('upload_data' => $this->upload->data());
						$image = base_url("images/" . $data1['raw_name'] . $data1['file_ext']);
						$data['category_image']=$image;
				}
			}
			//print_r($data); exit;				
			if($this->root_model->add_category($data)){
				$this->session->set_flashdata('feedback_successfull', 'Added Category successfully');
				return redirect('root_admin/add_category');
			}
			else{
					$this->session->set_flashdata('feedback_failed', 'Add Category failed!');
						return redirect('root_admin/add_category');
			}
		/*}
		else{
			$data=$this->root_model->get_category_parent();
			$this->load->view('root_admin/add_category',['parents'=>$data]);
		}*/
	}
	
	public function category(){
		
		$admin_id=$this->session->userdata('admin_id');
		$user=$this->root_model->my_info($admin_id);
		/*if($user)
		$this->load->view('root_admin/header',['info'=>$user]);
		else
			return redirect('login/');*/
		$this->load->library('pagination');
		//$data=$this->admin_model->users_info();
		$config['total_rows']=$this->db->where('admin_status',1)->where('admin_role_role_id!=',1)->get('admin_info')->num_rows();
		$config['base_url']=base_url('root_admin/user');
		$config['per_page']=10;
		$config['num_links']=5;
		$this->pagination->initialize($config);

		$data=$this->root_model->users_info($config['per_page'],$this->uri->segment(3));
		$this->load->view('root_admin/user_info',['infos'=>$data]);
	}
	
}
