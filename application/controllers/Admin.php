<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller {

    public function __construct(){
        parent:: __construct();
        $this->load->model('admin_model');
        if($this->session->userdata('user_role')!=2)return redirect('login/');
    }
    public function index(){
        $this->load->view('admin/dashBoard');
    }

    /*Start service Charges */
    public function serviceCharges(){
        $serviceList  = $this->admin_model->get_all_services_for_details();
        $this->load->view('admin/serviceCharges',["serviceList"=>$serviceList]);
    }
	
    public function allServiceCharge(){
		$serviceList  = $this->admin_model->get_all_services();
        $this->load->view('admin/allServiceCharge',["serviceList"=>$serviceList]);
    }
    public function viewServiceCharge($service_id, $serviceName){
        $organizations= $this->admin_model->get_all_organization_name_id();
        $info=$this->input->post();
        if($info){
            $servicePayments = $this->admin_model->get_service_payment_history_by_id_dates($info,$service_id);
			if($service_id>0)
            $serviceCharged = $this->admin_model->get_service_charged_history_by_id_dates($info,$service_id);
			else
			$serviceCharged=$this->admin_model->get_charged_energy_charge_history_by_dates($info);
        }
        else{
            $servicePayments=$this->admin_model->get_service_payment_history_by_id($service_id);
			if($service_id>0)
            $serviceCharged=$this->admin_model->get_service_charged_history_by_id($service_id);
			else
			$serviceCharged=$this->admin_model->get_charged_energy_charge_history();
        }
        
        $this->load->view('admin/viewServiceCharge',["servicePayments"=>$servicePayments,"serviceCharged"=>$serviceCharged,"serviceName"=>$serviceName,"service_id"=>$service_id,"organizations"=>$organizations,"inputVal"=>$info]);
    }
    public function serviceChargeSummary(){
		$data=$this->input->post();
		if($data){
			$serviceList  = $this->admin_model->get_service_payment_by_dates($data);
		}
		else{
			$serviceList  = $this->admin_model->get_last_fifteen_service_payment();
		}
        $this->load->view('admin/serviceChargeSummary',["serviceList"=>$serviceList,"data"=>$data]);
    }
	public function additionalServiceCharges(){
        $this->load->view('admin/additionalServiceCharges');
    }
    public function storeCommonServices(){
        $data=$this->input->post();
        if($this->admin_model->add_common_services($data)){
            $this->session->set_flashdata('feedback_successfull', 'Added New Service Successfully');
            return redirect('admin/serviceCharges');
        }
        else {
            $this->session->set_flashdata('feedback_failed', 'Add New Service Failed!');
            return redirect('admin/serviceCharges');
        }
    }   
    public function serviceChargeHistory($customer_id){
        $data=$this->admin_model->get_customer_all_bill_bills_by_id($customer_id);
        //print_r($data);exit;
        $this->load->view('admin/serviceChargeHistory',['bills'=>$data,'customerId'=>$customer_id]);
    }
        
    public function editServiceCharges($bill_id){
        $services=$this->admin_model->get_all_services_bill_by_id($bill_id);
        $billInfo=$this->admin_model->get_bill_info_by_id($bill_id);
        $this->load->view('admin/editServiceCharges',['services'=>$services,'billInfo'=>$billInfo]);
    }
        
    public function updateServiceCharges(){
        $data=$this->input->post();
        if($this->admin_model->updateServiceCharges($data)){
            $this->session->set_flashdata('feedback_successfull', 'Updated Services Charges Successfully');
            return redirect('admin/setupShopBills');
        }
        else {
            $this->session->set_flashdata('feedback_failed', 'Update Service Charges Failed!');
            return redirect('admin/setupShopBills');
        }
        
    }
    
    public function ajax_updateServicesCharges($serviceId){
        
            $data=$this->input->post();
            //print_r($product_id);
            //print_r($data);exit;
            if($this->admin_model->ajax_update_services_charges($data,$serviceId)){
                $data = array(
                'status'=>true
                );
                echo json_encode($data);
            }
            else{
                $data = array(
                    'status'=>false
                    );
                echo json_encode($data);
            }
    }
    
    /* End serviceCharges */
    public function setupGeneralServiceCharges(){
        $this->load->view('admin/setupGeneralServiceCharges');
    }
    public function setupElectricBill(){
        $this->load->view('admin/setupElectricBill');
    }   
    public function addAsset(){
        $this->load->view('admin/addAsset');
    }       
    public function myAccount(){
        $this->load->view('admin/myAccount');
    }       
    
    
    /* Ledgers */
    public function ledgers(){
        $data=$this->admin_model->get_all_ledger_info();
        $this->load->view('admin/ledgers',['infos'=>$data]);
    }   
    
    public function addNewLedger(){
        $data['income']=$this->admin_model->get_all_income_particular_name_id();
        $data['expence']=$this->admin_model->get_all_expence_particular_name_id();
        $this->load->view('admin/addNewLedger',['infos'=>$data]);
    }
    public function storeLedger(){
        $data=$this->input->post();
        //print_r($data);exit;
        $this->load->library('form_validation');
        if ($this->form_validation->run('addNewLedger') == FALSE){
            $this->load->view('admin/addNewLedger');
        }
        else{
            if($this->admin_model->store_ledger_info($data)){
                $this->session->set_flashdata('feedback_successfull', 'Adde New Ledger Successfully');
                return redirect('admin/ledgers');
            }
            else{
                $this->session->set_flashdata('feedback_failed', 'please Try Again!');
                return redirect('admin/addNewLedger');
            }
        }
        //$data=$this->admin_model->get_all_particular_name_id();
        //$this->load->view('admin/addNewLedger',['infos'=>$data]);
    }
    public function viewLedger($ledgerId){
        $data=$this->admin_model->get_ledger_details_by_id($ledgerId);
        $ledger=$this->admin_model->get_ledger_entry_infos_by_id($ledgerId);
        $this->load->view('admin/viewLedger',['info'=>$data,'ledgers'=>$ledger]);
    }   
    public function newLedgerEntry(){
        $bank_infos=$this->admin_model->get_all_bank_account_id_name();
        $ledger_infos['income']=$this->admin_model->get_all_income_ledger_name_id();
        $ledger_infos['expence']=$this->admin_model->get_all_expence_ledger_name_id();
        $this->load->view('admin/newLedgerEntry',['banks'=>$bank_infos,'ledgers'=>$ledger_infos]);
    }   
    public function storeLedgerEntry(){
        $data=$this->input->post();
        
         $this->load->library('form_validation');
        if ($this->form_validation->run('newLedgerEntry') == FALSE){
            $this->load->view('admin/newLedgerEntry');
        }
        else{
           
    		if($this->admin_model->store_ledger_entry($data)){
    			$this->session->set_flashdata('feedback_successfull', 'Ledger Entry Has Done Successfully');
                return redirect('admin/ledgers');
            }
            else{
                $this->session->set_flashdata('feedback_failed', 'please Try Again!');
                return redirect('admin/addNewLedger');
            }
       }
        /*$bank_infos=$this->admin_model->get_all_bank_account_id_name();
        $ledger_infos=$this->admin_model->get_all_ledger_name_id();
        $this->load->view('admin/newLedgerEntry',['banks'=>$bank_infos,'ledgers'=>$ledger_infos]);*/
    }
    public function viewLedgerEntry($ledger_id, $type){
		$data=$this->admin_model->get_ledger_entry_details_by_id($ledger_id, $type);
        $this->load->view('admin/viewLedgerEntry',['info'=>$data]);
    }   
    
    /* Balance Sheet */
    public function balanceSheet(){
        $this->load->view('admin/balanceSheet');
    }   
    
    /* Cash Transactions*/
    public function cashTransactions(){
        $data=$this->admin_model->get_all_transection_infos();
        $this->load->view('admin/cashTransactions',['infos'=>$data]);
    }
    public function pettyCash(){
        $data=$this->admin_model->get_petty_cash_infos();
        $amount=$this->admin_model->get_petty_cash_amount();
        $this->load->view('admin/pettyCash',['infos'=>$data,'amount'=>$amount]);
    }
    public function cashInHand(){
        $data=$this->admin_model->get_cash_in_hand_infos();
        $amount=$this->admin_model->get_cash_in_hand_amount();
        $this->load->view('admin/cashInHand',['infos'=>$data,'amount'=>$amount]);
    }   
    public function addNewCashTransfer(){
        $bank_infos=$this->admin_model->get_all_bank_account_id_name();
        $this->load->view('admin/addNewCashTransfer',['bank'=>$bank_infos]);
    }
    public function storeCashTransfer(){
        $data = $this->input->post();
        $this->load->library('form_validation');
        if ($this->form_validation->run('addNewCashTransfer') == FALSE){
            $bank_infos=$this->admin_model->get_all_bank_account_id_name();
            $this->load->view('admin/addNewCashTransfer',['bank'=>$bank_infos]);
        }
        else{
            if($this->admin_model->store_cash_transfer($data)){
                $this->session->set_flashdata('feedback_successfull', 'Cash Transferd Successfully');
                return redirect('admin/cashTransactions');
            }
            else{
                $this->session->set_flashdata('feedback_failed', 'please Try Again!');
                return redirect('admin/cashTransactions');
            }
        }
        /*$bank_infos=$this->admin_model->get_all_bank_account_id_name();
        $this->load->view('admin/addNewCashTransfer',['bank'=>$bank_infos]);*/
    }
    public function viewTransaction($transfer_id,$type){
        $data=$this->admin_model->get_transfer_details_by_id($transfer_id,$type);
        //print_r($data);exit;
        $this->load->view('admin/viewTransaction',['info'=>$data]);
    }
    /* Bank Acount */
    public function bankAccounts(){
        $data=$this->admin_model->get_all_bank_account_infos();
        $this->load->view('admin/bankAccounts',['infos'=>$data]);
    }   
    public function addBankAccount(){
        $this->load->view('admin/addBankAccount');
    }   
    public function storeBankAccountInfo(){
        $data=$this->input->post();
        $this->load->library('form_validation');
        if ($this->form_validation->run('addBankAccount') == FALSE)
            {
               $this->load->view('admin/addBankAccount');
            }
        else{
            if($this->admin_model->store_bank_account_infos($data))
            {
                $this->session->set_flashdata('feedback_successfull', 'Added New Bank Account Successfully');
                return redirect('admin/bankAccounts');
            }
            else {
                $this->session->set_flashdata('feedback_failed', 'Add New Bank Account Failed!');
                return redirect('admin/addBankAccount');
            }
        }
    }
    public function viewBankAccount($account_id){
        $data=$this->admin_model->get_bank_account_details_by_id($account_id);
		$bankTransections=$this->admin_model->get_bank_account_transection_by_id($account_id);
        $this->load->view('admin/viewBankAccount',['info'=>$data,'bankTransections'=>$bankTransections]);
    }
    public function updateBankAccount(){
        $data=$this->input->post();
        $bankAccountId=$data['id'];
        $this->load->library('form_validation');
        if ($this->form_validation->run('viewBankAccount') == FALSE)
            {
              $data=$this->admin_model->get_bank_account_details_by_id($bankAccountId);
              $this->load->view('admin/viewBankAccount',['info'=>$data]);
            }
        else
            {
            unset($data['id']);
            //print_r($data);exit;
            if($this->admin_model->update_bank_account_infos($data,$bankAccountId)){
                $this->session->set_flashdata('feedback_successfull', 'Updated Bank Account Successfully');
                //return redirect('admin/bankAccounts');
                return redirect('admin/viewBankAccount/'.$bankAccountId);
            }
            else{
                $info=$this->admin_model->get_bank_account_details_by_id($account_id);
                $this->session->set_flashdata('feedback_failed', 'Update Bank Account Failed!');
                return redirect('admin/viewBankAccount/'.$bankAccountId);
            }
        }
    }

	public function addNewParticular(){
        $this->load->view('admin/addNewParticular');
    }    
    public function allparticular(){
        $data=$this->admin_model->get_all_particular_details();

        $this->load->view('admin/allparticular',['infos'=>$data]);
    }    
    public function viewParticular($particularId){
          //print_r($particularId); exit();

         $data=$this->admin_model->get_all_info_by_particularId($particularId);
         //$data['particularId']=$particularId;
        // print_r($data); exit();
        $this->load->view('admin/viewParticular',['info'=>$data]);
    }    

    public function storeParticular(){
        $data=$this->input->post();
        if($this->admin_model->store_particular($data)){
            $this->session->set_flashdata('feedback_successfull', 'Added New Particular Successfully');
            return redirect('admin/addNewParticular');
        }
        else{
            $this->session->set_flashdata('feedback_failed', 'please Try Again!');
            return redirect('admin/addNewParticular');
        }
       // $this->load->view('admin/addNewParticular');
    }   
    public function update_particular()
    {
        $data=$this->input->post();
         
         if($this->admin_model->update_particular($data)){
            $this->session->set_flashdata('feedback_successfull', 'Updated Particular Successfully');
            return redirect('admin/allparticular');
        }
        else{
            $this->session->set_flashdata('feedback_failed', 'please Try Again!');
            $this->load->view('admin/viewParticular',['info'=>$data]);
        }
    }
    public function bankAccountFundAdjustmentType(){
        $this->load->view('admin/bankAccountFundAdjustmentType');
    }
    public function bankAccountFundAdjustments(){
		$bank_infos=$this->admin_model->get_all_bank_account_id_name();
        $this->load->view('admin/bankAccountFundAdjustments',['bank_infos'=>$bank_infos]);
    }
    public function storeBankAdjustment(){
		$data=$this->input->post();
         $this->load->library('form_validation');
         //print_r($data);exit();
        if ($this->form_validation->run('bankAccounAdjustment') == FALSE)
            {
              $bank_infos=$this->admin_model->get_all_bank_account_id_name();
              $this->load->view('admin/bankAccountFundAdjustments',['bank_infos'=>$bank_infos]);
            }
        else
            {
        		if($this->admin_model->store_bank_adjustment($data)){
        			$this->session->set_flashdata('feedback_successfull', 'Bank Account Adjustment has Done Successfully');
                    return redirect('admin/bankAccounts');
        		}
        		else{
        			$this->session->set_flashdata('feedback_failed', 'please Try Again!');
                    return redirect('admin/bankAccounts');
        		}
            }
		/*$bank_infos=$this->admin_model->get_all_bank_account_id_name();
        $this->load->view('admin/bankAccountFundAdjustments',['bank_infos'=>$bank_infos]);*/
    }
	
	public function ajax_bankAccountBalance(){
        
        $bankId=$this->input->post('bankId');
        $bankAccountLastBalance=$this->admin_model->get_bank_account_balance_by_id($bankId);
        if($bankAccountLastBalance){
            $data = array(
            'bankAccountLastBalance'=>$bankAccountLastBalance,
            'status'=>true,
            );
        }
        else{
            $data = array(
            'status'=>false
            );
        }
        echo json_encode($data);
    }
	
	
    /* customers */
    public function customers(){
        $data  = $this->admin_model->get_all_customer_details();
        $this->load->view('admin/customers',['allCustomer'=>$data]);
    }
    public function viewCustomerAccount($customer_id){
        $data=$this->admin_model->get_customer_details_by_customer_id($customer_id);

        $this->load->view('admin/viewCustomerAccount',["customerDetails"=> $data]);
    }
    public function addCustomerAccount(){
        $this->load->view('admin/addCustomerAccount');
    }
    
    public function storeCustomerAccount(){
        $data=$this->input->post();
        $this->load->library('form_validation');
        if ($this->form_validation->run('addCustomerAccount') == FALSE){
               $this->load->view('admin/addCustomerAccount');
        }
        else{

            $dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
            $date= $dt->format('Y-m-d H:i:s');
            $data['customerAddedDate']=$date;
            if($this->admin_model->add_customer($data)){
                $this->session->set_flashdata('feedback_successfull', 'Added Client Successfully');
                return redirect('admin/addCustomerAccount');
            }
            else {
                $this->session->set_flashdata('feedback_failed', 'Add Client Failed!');
                return redirect('admin/addCustomerAccount');
            }
        }
    }

    public function updateCustomerAccount(){
        $data=$this->input->post();
        $customer_id=$data['customerId'];
        $this->load->library('form_validation');
        if ($this->form_validation->run('updateCustomerAccount')== FALSE){
            $data=$this->admin_model->get_customer_details_by_customer_id($customer_id);
            $this->load->view('admin/viewCustomerAccount',["customerDetails"=> $data]);
        }
        else{
             $result = $this->admin_model->updateCustomerAccount($data);
            if($result){
                $this->session->set_flashdata('feedback_successfull', 'Edit Account Successfully');
                return redirect('admin/customers');
             }
            else {
                $this->session->set_flashdata('feedback_failed', 'Edit Account Failed!');
                return redirect('admin/customers');
            }
        }
    }
    
    
    /* Rent Management */
    public function rentManagement(){
        $this->load->view('admin/rentManagement');
    }
    public function rentHistory($customer_id){
        $data=$this->admin_model->get_customer_all_rent_bills_by_id($customer_id);
        //print_r($data);exit;
        $this->load->view('admin/rentHistory',['bills'=>$data,'customerId'=>$customer_id]);
    }

    public function generateRentInvoice(){
        $this->load->view('admin/generaterentinvoice');
    }
    
    /* Billing */
    public function billing(){
        $this->load->view('admin/billing');
    }
    public function allBillingAccount(){
        $this->cart->destroy();
        $infos=$this->admin_model->get_all_customer_meter_info();
        //$services=$this->admin_model->get_all_common_service_name_id();
        $this->load->view('admin/allBillingAccount',['infos'=>$infos]);
    }
    
    public function addBillingAccount(){
        $this->cart->destroy();
        $organization=$this->admin_model->get_all_organization_name_id();
        $services=$this->admin_model->get_all_common_service_name_id();
        $additionalServices=$this->admin_model->get_all_additional_service_name_id();
        $this->load->view('admin/addBillingAccount',['organizations'=>$organization,'services'=>$services,'additionalServices'=>$additionalServices]);
    }
    
    public function storeCustomerBillingAccount(){
        $data=$this->input->post();
        if($this->admin_model->add_customer_billing_account($data)){
            $this->cart->destroy();
            $this->session->set_flashdata('feedback_successfull', 'Added Client Billing Account Successfully');
            return redirect('admin/addBillingAccount');
        }
        else {
            $this->session->set_flashdata('feedback_failed', 'Add Client Billing Account Failed!');
            return redirect('admin/addBillingAccount');
        }
        
    }
    
    public function billingAccountDetails($meter_id){
        $meterInfo=$this->admin_model->get_meter_info_by_id($meter_id);
        $meterServices=$this->admin_model->get_meter_all_common_services_by_id($meter_id);
        $meterServicesId=$this->admin_model->get_meter_all_common_services_id_by_id($meter_id);
        $additionalMeterServicesId=$this->admin_model->get_meter_all_additional_services_id_by_id($meter_id);
       // print_r($additionalMeterServicesId);exit;
        $meterAdditionalServices=$this->admin_model->get_meter_all_additional_services_by_id($meter_id);
        $services=$this->admin_model->get_all_common_service_name_id();
        $additionalServices=$this->admin_model->get_all_additional_service_name_id();
        $this->load->view('admin/billingAccountDetails',['meterInfo'=>$meterInfo,'services'=>$services,'meterAdditionalServices'=>$meterAdditionalServices,'meterServicesId'=>$meterServicesId,'meterServices'=>$meterServices,'additionalServices'=>$additionalServices,'additionalMeterServicesId'=>$additionalMeterServicesId]);
    }
    
    public function updateCustomerBillingAccount(){
        $data=$this->input->post();
        $meter_id=$data['meterId'];
        //print_r($data);exit;
        
        if($this->admin_model->update_customer_billing_account($data)){
            $this->cart->destroy();
            $this->session->set_flashdata('feedback_successfull', 'Updated Client Billing Account Successfully');
            return redirect('admin/billingAccountDetails/'.$meter_id);
        }
        else {
            $this->session->set_flashdata('feedback_failed', 'Update Client Billing Account Failed!');
            return redirect('admin/billingAccountDetails/'.$meter_id);
        }
        
    }
    public function removeServiceCharge($chargeId,$meter_id){
        if($this->admin_model->remove_service_charge($chargeId)){
            $this->session->set_flashdata('feedback_successfull', 'Removed Client Service Charge Successfully');
            return redirect('admin/billingAccountDetails/'.$meter_id);
        }
        else {
            $this->session->set_flashdata('feedback_failed', 'Remove Client Service Charge Failed!');
            return redirect('admin/billingAccountDetails/'.$meter_id);
        }
        
    }
    
    public function removeAdditionalServiceCharge($chargeId,$meter_id,$serviceId){
        if($this->admin_model->remove_additional_service_charge($chargeId,$serviceId)){
            $this->session->set_flashdata('feedback_successfull', 'Removed Client Additional Service Charge Successfully');
            return redirect('admin/billingAccountDetails/'.$meter_id);
        }
        else {
            $this->session->set_flashdata('feedback_failed', 'Remove Client Additional Service Charge Failed!');
            return redirect('admin/billingAccountDetails/'.$meter_id);
        }
        
    }
    
    public function removeBillingAccount($meter_id){
        if($this->admin_model->remove_billing_account($meter_id)){
            $this->session->set_flashdata('feedback_successfull', 'Removed Client Billing Account Successfully');
            return redirect('admin/allBillingAccount');
        }
        else {
            $this->session->set_flashdata('feedback_failed', 'Remove Client Billing Account Failed!');
            return redirect('admin/allBillingAccount');
        }
        
    }
    public function ajax_organizationSpaceTitle(){
        
        $customerId=$this->input->post('customerId');
        $spaceTitle=$this->admin_model->get_space_title_by_id($customerId);
        if($spaceTitle){
            $data = array(
            'spaceTitle'=>$spaceTitle,
            'status'=>true,
            );
        }
        else{
            $data = array(
            'status'=>false
            );
        }
        echo json_encode($data);
    }
    
    public function ajax_addAdditionalCharge(){
        
        $data=$this->input->post();
        $serviceName=$this->admin_model->get_additional_service_charge_name_by_id($data['serviceId']);
        //print_r($serviceName);// exit;
        $pastBuy=0;
        $cartContent=$this->cart->contents();
        if($cartContent !=null){
            foreach($cartContent as $content){
                if($content['id'] ==$data['serviceId']){
                    $pastBuy = $content['qty'];
                }
            }
        }
        $data['serviceType']=2;
        $insert=array(
                'id'=>$data['serviceId'],
                'qty'=>1,
                'price'=>$data['serviceAmount'],
                'name'=>$serviceName,
                'customer'=>$data['customerId'],
            );
            
        $this->cart->insert($insert);   
        $cart=$this->cart->contents();
        //$data['status'] = true; //Set response  
        $numItems = count($this->cart->contents());
        $i = 0;
        foreach($this->cart->contents() as $key) { 
        if($i+1 == $numItems) { 
        $saved_rowid = $key['rowid']; 
        } 
        $i++; 
        }
        if($saved_rowid){
            $data = array(
                'rowid'=>$saved_rowid,
                'serviceName'=>$serviceName,
                'pastBuy'=>$pastBuy,
                'status'=>true,
            );
        }
        else{
            $data = array(
                'status'=>false
            );
        }
        echo json_encode($data);
    }
    
    public function ajaxRemoveAdditional(){
    //print_r($rowid);exit;
    //echo $this->input->post('row_id');exit;
            $this->cart->update(array(
                'rowid'=>$this->input->post('row_id'),
                'qty'=>0
            )
        );
        
        $data = array(
                'status'=>true, 
                //'redirect'=>$this->agent->referrer()
                );
        echo json_encode($data);
            /*$this->cart->update(array(
                'rowid'=>$rowid,
                'qty'=>0
            )
        );*/
        //$this->load->library('user_agent');
        //redirect($this->agent->referrer());
    }
    public function destroy(){
        $this->cart->destroy();
        $data = array(
                'status'=>true, 
                //'redirect'=>$this->agent->referrer()
                );
        echo json_encode($data);
        //$this->load->library('user_agent');
        //redirect($this->agent->referrer());
    }
    public function allBills(){
        $data=$this->admin_model->get_all_bills();
        //print_r()
        $this->load->view('admin/allBills',['bills'=>$data]);
    }
    public function setupBills(){
        $this->load->view('admin/setupBills');
    }
    public function setupOfficeBills(){
        $bills= $this->admin_model->get_all_office_bills();
        $this->load->view('admin/setupOfficeBills',['bills'=>$bills]);
        
    }
    
    public function createOfficeBill(){
        $data=$this->input->post();
       // print_r($data);exit;
        $this->load->library('form_validation');
        if ($this->form_validation->run('setupOfficeBills') == FALSE){
            //print_r($data); exit();
            $bills= $this->admin_model->get_all_office_bills();
            $this->load->view('admin/setupOfficeBills',['bills'=>$bills]);
        }
        else{
			//print_r($data); exit();
            $data['billingMonthYear']= date('Y-m-d', strtotime($data['billingMonthYear']));
			$monthName=date('Y-m', strtotime($data['billingMonthYear']));
			$monthStart=$monthName."-1";
			$monthEnd=$monthName."-31";
			//print_r($monthStart);print_r($monthEnd);exit;
			$month=$this->admin_model->get_office_billing_month_by_date($monthStart,$monthEnd);
			if($month){
				$this->session->set_flashdata('feedback_failed', 'Office Bill has Already Created for This Month ');
				return redirect('admin/setupOfficeBills');
			}
			else{
				$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
				$date= $dt->format('Y-m-d H:i:s');
				$data['monthlyBillAddedDate']=$date;
                $data['currentReadingDate']= date('Y-m-d', strtotime($data['currentReadingDate']));
                $data['lastPaymentDate']= date('Y-m-d', strtotime($data['lastPaymentDate']));
				//print_r($data); exit;
				if($this->admin_model->create_office_bill($data)){
					$this->session->set_flashdata('feedback_successfull', 'Created New Bill Successfully');
					return redirect('admin/setupOfficeBills');
				}
				else{
					$this->session->set_flashdata('feedback_failed', 'Create New Bill Failed!');
					return redirect('admin/setupOfficeBills');
				}
			}
			
      }
    }

    
    public function pendingOfficeSetupBills($bill_id){
        $currentBills=$this->admin_model->get_all_current_bill_by_id_for_office($bill_id);
        $billInfo=$this->admin_model->get_bill_info_by_id($bill_id);
        //print_r($currentBills);exit;
        $this->load->view('admin/pendingOfficeSetupBills',['currentBills'=>$currentBills,'billInfo'=>$billInfo]);
    }
    
    public function ajax_updateOfficeBill(){
        $data=$this->input->post('values');
        $reading=$data['currentReading'];
        //print_r($data['currentReading']);//exit;
        if($this->admin_model->update_office_bill($data)){
            $data = array(
            'status'=>true,
            'currentReading'=>$reading
            );
        }
        else{
            $data = array(
            'status'=>false
            );
        }
        echo json_encode($data);
        
    }
    
    public function makeOfficeBillsFinal($bill_id){
        if($this->admin_model->make_bills_final($bill_id)){
            $this->session->set_flashdata('feedback_successfull', 'Made Bill Final Successfully');
            return redirect('admin/setupOfficeBills');
        }
        else{
            $this->session->set_flashdata('feedback_failed', 'please Try Again!');
            return redirect('admin/setupOfficeBills');
        }
        
    }

    
    public function setupShopBills(){
        $serviceList  = $this->admin_model->get_active_common_services();
        $bills= $this->admin_model->get_all_shop_bills();
        $this->load->view('admin/setupShopBills',['charges'=>$serviceList,'bills'=>$bills]);
    }

    public function createShopBill(){
        $data=$this->input->post();
		$monthName=date('Y-m', strtotime($data['billingMonthYear']));
		$monthStart=$monthName."-1";
		$monthEnd=$monthName."-31";
		//print_r($monthStart);print_r($monthEnd);exit;
		$month=$this->admin_model->get_shop_billing_month_by_date($monthStart,$monthEnd);
		if($month){
			$this->session->set_flashdata('feedback_failed', 'Shop Bill has Already Created for This Month ');
			return redirect('admin/setupShopBills');
		}
		else{
			$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
			$date= $dt->format('Y-m-d H:i:s');
			$data['monthlyBillAddedDate']=$date;
			$data['billingMonthYear']= date('Y-m-d', strtotime($data['billingMonthYear']));
			$data['currentReadingDate']= date('Y-m-d', strtotime($data['currentReadingDate']));
            $data['lastPaymentDate']= date('Y-m-d', strtotime($data['lastPaymentDate']));
			if($this->admin_model->create_shop_bill($data)){
				$this->session->set_flashdata('feedback_successfull', 'Created New Bill Successfully');
				return redirect('admin/setupShopBills');
			}
			else{
				$this->session->set_flashdata('feedback_failed', 'Create New Bill Failed!');
				return redirect('admin/setupShopBills');
			}
		}
    }

    
    public function pendingShopSetupBills($bill_id){
        $currentBills=$this->admin_model->get_all_current_bill_by_id($bill_id);
        $billInfo=$this->admin_model->get_bill_info_by_id($bill_id);
        //print_r($currentBills);exit;
        $this->load->view('admin/pendingShopSetupBills',['currentBills'=>$currentBills,'billInfo'=>$billInfo]);
    }
    
    public function ajax_updateCurrentReading(){
        $data=$this->input->post();
        if($this->admin_model->updateCurrentReading($data)){
            $data = array(
            'status'=>true,
            );
        }
        else{
            $data = array(
            'status'=>false
            );
        }
        echo json_encode($data);
        
    }

    
    public function makeBillsFinal($bill_id){
        if($this->admin_model->make_bills_final($bill_id)){
            $this->session->set_flashdata('feedback_successfull', 'Made Bill Final Successfully');
            return redirect('admin/setupShopBills');
        }
        else{
            $this->session->set_flashdata('feedback_failed', 'please Try Again!');
            return redirect('admin/setupShopBills');
        }
        
    }

    public function finalSetupBills(){
        $this->load->view('admin/finalSetupBills');
    }
    /*End Billings*/
    /* Fixed Assets */
    public function fixedAssets(){
        $data=$this->admin_model->get_all_asset();
        $this->load->view('admin/fixedAssets',["fixedAssets"=>$data]);
    }
    public function viewAsset($assetId){
        $data=$this->admin_model->get_asset_details_by_id($assetId);
        $this->load->view('admin/viewAsset',['assetDetails'=> $data]);
    }
    public function storeAsset()
    {
        $data=$this->input->post();
        //print_r($data);
        //exit();

        $this->load->library('form_validation');
        if ($this->form_validation->run('addAsset') == FALSE){
               $this->load->view('admin/addAsset');
        }
        else{            
            $data['assetAccruingDate']=date('Y-m-d', strtotime($data['assetAccruingDate']));

            $dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
            $date= $dt->format('Y-m-d H:i:s');
            $data['assetAddedDate'] = $date;
            if($this->admin_model->storeAsset($data)){
                $this->session->set_flashdata('feedback_successfull', 'Added New Asset Successfully');
                return redirect('admin/fixedAssets');
            }
            else {
                $this->session->set_flashdata('feedback_failed', 'Add New Asset Failed!');
                return redirect('admin/addAsset');
            }
        }
    }
    public function updateAsset()
    {
         $data=$this->input->post();
       //print_r($data);exit();
        $asset_id=$data['assetId'];
        
        $this->load->library('form_validation');
        
        if ($this->form_validation->run('viewAsset') ==FALSE)
            {
            
              $user=$this->admin_model->get_asset_details_by_id($asset_id);
              $this->load->view('admin/viewAsset',['assetDetails'=> $user]);
              // print_r($data);exit();
              print_r($data);exit();
            }

        else
            {
                unset($data['assetId']);
                //print_r($data);exit();
            $result =$this->admin_model->updateAsset($data,$asset_id);
            //$user=$this->admin_model->get_user_details_by_id($companyUser_id);

            if($result){
                $this->session->set_flashdata('feedback_successfull', 'Edit Asset Successfully');
                return redirect('admin/viewAsset/'.$asset_id);
            }
            else {
                $this->session->set_flashdata('feedback_failed', 'Edit Asset Failed!');
                return redirect('admin/viewAsset/'.$asset_id);
            }
       }    
    }
    
    /* Statements */
    public function incomeStatements(){
        $this->load->view('admin/incomeStatements');
    }
    public function expenseStatements(){
        $this->load->view('admin/expenseStatements');
    }
    public function generateBalanceSheet(){
        $this->load->view('admin/generateBalanceSheet');
    }
    public function assetList(){
        $this->load->view('admin/assetList');
    }
    
    /* Received Payment */
    public function receivedPayment(){
        $payment_types = $this->admin_model->get_all_payment_types();
        $organizations= $this->admin_model->get_all_organization_name_id();
        $this->load->view('admin/receivedPayment',["payment_types"=>$payment_types,"organizations"=>$organizations]);
    }

    public function storeReceivedPayment(){
        $data = $this->input->post();
		//print_r($data);exit;
        $this->load->library('form_validation');
        if ($this->form_validation->run('receivedPayment') == FALSE){
            $payment_types = $this->admin_model->get_all_payment_types();
			$organizations= $this->admin_model->get_all_organization_name_id();
			$this->load->view('admin/receivedPayment',["payment_types"=>$payment_types,"organizations"=>$organizations]);
        }
        else{
            if($this->admin_model->store_received_payment($data)){
                $this->session->set_flashdata('feedback_successfull', 'Payment Done Successfully');
                return redirect('admin/receivedPayment');
            }
            else{
                $this->session->set_flashdata('feedback_failed', 'please Try Again!');
                return redirect('admin/receivedPayment');
            }
         }
    }
    public function allReceivedPayment(){
        $data=$this->admin_model->get_all_received_payment_info();
        $this->load->view('admin/allReceivedPayment',['infos'=>$data]);
    }
    public function viewReceivedPayment($paymentId, $type){
        $data=$this->admin_model->get_received_payment_info_by_id($paymentId);
        $this->load->view('admin/viewReceivedPayment',['info'=>$data]);
    }
    
    /* Income Statement */
    public function billCollections(){
        $this->load->view('admin/billCollections');
    }
    
    
    /* Company User */
    public function companyUser(){

        $data  = $this->admin_model->get_all_user();
        $this->load->view('admin/companyUser',['allcompanyUser'=>$data]);

        //$this->load->view('admin/companyUser');
    }
    public function addCompanyUser(){
        $this->load->view('admin/addCompanyUser');
    }
    public function viewCompanyUser($companyUser_id){
        $data=$this->admin_model->get_user_details_by_id($companyUser_id);
        //print_r($data);exit;

        $this->load->view('admin/viewCompanyUser',['companyUserDetails'=> $data]);
        //$this->load->view('admin/viewCompanyUser');
    }
    public function storeCompanyUserInfo(){
        
        $this->load->library('form_validation');
        if ($this->form_validation->run('addCompanyUser') == FALSE){
            $this->load->view('admin/addCompanyUser');
        }
        else{
            $data = $this->input->post();
            $dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
            $date= $dt->format('Y-m-d H:i:s');
            $data['adminJoinDate']=$date;
            unset($data['companyUserConfirmPassword']);
            if($this->admin_model->add_User($data)){
                $this->session->set_flashdata('feedback_successfull', 'Added Company User Successfully');
                return redirect('admin/addCompanyUser');
            }
            else {
                $this->session->set_flashdata('feedback_failed', 'Add Company User Failed!');
                return redirect('admin/addCompanyUser');
            }
        }
    }
    
    public function updateCompanyUser()
    {
       $data=$this->input->post();
       //print_r($data);exit();
        $companyUser_id=$data['adminId'];
        //print_r($companyUser_id);
        $this->load->library('form_validation');
        
        if ($this->form_validation->run('viewCompanyUser') == FALSE)
            {
               //$user=$this->admin_model->get_user_details_by_id($companyUser_id);
               //print_r($user);exit;
              $user=$this->admin_model->get_user_details_by_id($companyUser_id);

             $this->load->view('admin/viewCompanyUser',['companyUserDetails'=> $user]);
            }
        else
            {
                unset($data['adminId']);
            $result = $this->admin_model->updateCompanyUser($data,$companyUser_id);
            //$user=$this->admin_model->get_user_details_by_id($companyUser_id);

            if($result){
                $this->session->set_flashdata('feedback_successfull', 'Edit Account Successfully');
                return redirect('admin/viewCompanyUser/'.$companyUser_id);
            }
            else {
                $this->session->set_flashdata('feedback_failed', 'Edit Account Failed!');
                return redirect('admin/viewCompanyUser/'.$companyUser_id);
            }
       }    
    }
    /* Prints */
    public function printBill($bill_id,$current_bill){
        $current_bill_info=$this->admin_model->get_print_current_bill_info_by_id($current_bill);
        $service_bill_info=$this->admin_model->get_print_survice_bill_info_by_id($bill_id,$current_bill);
        $common_bill_info=$this->admin_model->get_print_common_bill_info_by_id($bill_id,$current_bill);
        $govt_service_info=$this->admin_model->get_print_govt_service_info_by_id($bill_id,$current_bill);
        /*$energyCharge=($current_bill_info->currentBillCurrentReading-$current_bill_info->currentBillPrevReading)*$current_bill_info->perUnitCharge;
        $total_bill=($energyCharge+$current_bill_info->currentBillDemandCharge+$current_bill_info->currentBillServiceCharge+$common_bill_info->serviceAmount);
        if($services){
            foreach($service_bill_info as $service){
                $total_bill=$total_bill+$service->serviceAmount;
            }
        }*/
        //if($this->Admin_model->update_customer_bill_deu($total_bill,$current_bill_info->customer_info_customerId)){
            $this->load->view('admin/printBill',['bill'=>$current_bill_info,'services'=>$service_bill_info,'common_bill'=>$common_bill_info,'govt_bill'=>$govt_service_info]);
        //}
    }
    public function printClientAccountStatement(){
        $this->load->view('admin/printClientAccountStatement');
    }
    public function printPaymentReceipt(){
        $this->load->view('admin/printPaymentReceipt');
    }
    public function printAll($bill_id){
		
		$currentBills=$this->admin_model->get_all_current_bill_id_for_office_by_id($bill_id);
		
		foreach($currentBills as $currentBill){
			$current_bill_info[$currentBill->currentBillId]=$this->admin_model->get_print_current_bill_info_by_id($currentBill->currentBillId);
			$service_bill_info[$currentBill->currentBillId]=$this->admin_model->get_print_survice_bill_info_by_id($bill_id,$currentBill->currentBillId);
			$common_bill_info[$currentBill->currentBillId]=$this->admin_model->get_print_common_bill_info_by_id($bill_id,$currentBill->currentBillId);
			$govt_service_info[$currentBill->currentBillId]=$this->admin_model->get_print_govt_service_info_by_id($bill_id,$currentBill->currentBillId);
		}
        $this->load->view('admin/printAll',['currentBills'=>$currentBills,'bill'=>$current_bill_info,'services'=>$service_bill_info,'common_bill'=>$common_bill_info,'govt_bill'=>$govt_service_info]);
    }
	
	public function ajax_get_client_meter_info(){
		
		$info=$this->admin_model->ajax_get_client_meter_id_name($this->input->post('client_id'));
		if($info){
			$data = array(
				'status'=>true, 
				'infos'=>$info
			);
		}
		else{
			$data = array(
				'status'=>false,
			);
		}
		echo json_encode($data);
	}


	public function ajax_get_client_services(){
		$data=$this->input->post();
		$services=$this->admin_model->ajax_get_client_all_service_infos($data);
		$energyCharge=$this->admin_model->ajax_get_client_energyCharge($data);
		if($services){
			$data = array(
				'status'=>true, 
				'services'=>$services,
				'energyCharge'=>$energyCharge
			);
		}
		else{
			$data = array(
				'status'=>false,
			);
		}
		echo json_encode($data);
	}

}
