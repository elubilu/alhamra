-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 04, 2017 at 03:16 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `alhamra`
--

-- --------------------------------------------------------

--
-- Table structure for table `account_adjustment`
--

CREATE TABLE `account_adjustment` (
  `accountAdjustmentId` int(11) NOT NULL,
  `bank_account_info_bankAccountId` int(11) NOT NULL,
  `accountAdjustmentType` int(2) NOT NULL COMMENT '1=add,2=remove',
  `accountAdjustmentDate` datetime NOT NULL,
  `accountAdjustmentbeforeAmount` double NOT NULL,
  `accountAdjustmentAfterAmount` double NOT NULL,
  `accountAdjustmentAmount` double NOT NULL,
  `accountAdjustmentNote` text COLLATE utf8_bin NOT NULL,
  `accountAdjustmentBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `asset_info`
--

CREATE TABLE `asset_info` (
  `assetId` int(11) NOT NULL,
  `assetTitle` varchar(300) COLLATE utf8_bin NOT NULL,
  `assetTrackingId` varchar(200) COLLATE utf8_bin NOT NULL,
  `assetOriginalValue` double NOT NULL,
  `assetCurrentValue` double NOT NULL,
  `assetAccruingDate` datetime NOT NULL,
  `assetDepreciationRate` double NOT NULL,
  `assetNote` text COLLATE utf8_bin NOT NULL,
  `assetStatus` int(2) NOT NULL COMMENT '1=Active, 0=InActive',
  `assetAddedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `bank_account_info`
--

CREATE TABLE `bank_account_info` (
  `bankAccountId` int(11) NOT NULL,
  `bankAccountTitle` varchar(350) COLLATE utf8_bin NOT NULL,
  `bankName` varchar(400) COLLATE utf8_bin NOT NULL,
  `bankAccountBranchName` varchar(400) COLLATE utf8_bin NOT NULL,
  `bankAccountHolderName` varchar(300) COLLATE utf8_bin NOT NULL,
  `bankAccountNumber` varchar(200) COLLATE utf8_bin NOT NULL,
  `bankAccountRoutingNumber` varchar(200) COLLATE utf8_bin NOT NULL,
  `bankAccountLastBalance` double NOT NULL,
  `bankAccountNote` text COLLATE utf8_bin NOT NULL,
  `bankAccountAddedDate` datetime NOT NULL,
  `bankAccountStatus` int(2) NOT NULL DEFAULT '1' COMMENT '1=active, 0=Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `bank_account_info`
--

INSERT INTO `bank_account_info` (`bankAccountId`, `bankAccountTitle`, `bankName`, `bankAccountBranchName`, `bankAccountHolderName`, `bankAccountNumber`, `bankAccountRoutingNumber`, `bankAccountLastBalance`, `bankAccountNote`, `bankAccountAddedDate`, `bankAccountStatus`) VALUES
(1, 'Bank Account', 'Bangladesh Bank', 'Sylhet, Bangladesh', 'Mr. Account Holder', 'acc-234653', '2408956', 2500, 'First Bank Account ', '2017-08-01 17:49:22', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cash_transfer`
--

CREATE TABLE `cash_transfer` (
  `cashTransferId` int(11) NOT NULL,
  `cashWithdrawnFrom` int(11) NOT NULL,
  `cashTransferredTo` int(11) NOT NULL,
  `cashTransferAmount` double NOT NULL,
  `cashSlipNo` varchar(150) COLLATE utf8_bin NOT NULL,
  `cashTransferDate` datetime NOT NULL,
  `cashTransferNote` text COLLATE utf8_bin NOT NULL,
  `cashTransferStatus` int(2) NOT NULL,
  `cashTransferEntryDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `ledger_details_info`
--

CREATE TABLE `ledger_details_info` (
  `ledgerDetailsId` int(11) NOT NULL,
  `ledgerDetailsType` int(3) NOT NULL COMMENT '1=income, 2=expense',
  `ledger_info_ledgerId` int(11) NOT NULL,
  `ledgerDetails` text COLLATE utf8_bin NOT NULL,
  `ledgerDetailsAmount` double NOT NULL,
  `ledgerDepositeTo` int(11) NOT NULL,
  `ledgerWithdrawnFrom` int(11) NOT NULL,
  `ledgerDetailsNote` text COLLATE utf8_bin NOT NULL,
  `ledgerDetailsStatus` int(2) NOT NULL COMMENT '1=Active, 0=InActive',
  `ledgerDetailsAddedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `ledger_info`
--

CREATE TABLE `ledger_info` (
  `ledgerId` int(11) NOT NULL,
  `ledgerTitle` varchar(500) COLLATE utf8_bin NOT NULL,
  `particular_info_particularid` int(11) NOT NULL,
  `ledgerType` int(3) NOT NULL DEFAULT '1' COMMENT '1=income,2=expense',
  `ledgerBalance` double NOT NULL,
  `ledgerNote` text COLLATE utf8_bin NOT NULL,
  `ledgerStatus` int(2) NOT NULL COMMENT '1=Active, 0=InActive',
  `ledgerAddedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `particular_info`
--

CREATE TABLE `particular_info` (
  `particularId` int(11) NOT NULL,
  `particularTitle` varchar(350) COLLATE utf8_bin NOT NULL,
  `particularType` int(3) NOT NULL DEFAULT '1' COMMENT '1=income,2=expense',
  `particularStatus` int(2) NOT NULL COMMENT '1=Active, 0=InActive',
  `particularNote` text COLLATE utf8_bin NOT NULL,
  `particularAddedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `petty_cash`
--

CREATE TABLE `petty_cash` (
  `pettyCashId` int(11) NOT NULL,
  `pettyCashDate` datetime NOT NULL,
  `pettyCashEntryDate` datetime NOT NULL,
  `pettyCashto` varchar(500) COLLATE utf8_bin NOT NULL,
  `pettyCashType` int(3) NOT NULL,
  `pettyCashAmount` double NOT NULL,
  `pettyCashStatus` int(2) NOT NULL DEFAULT '1' COMMENT '1=Active, 0=InActive',
  `pettyCashReferenceId` int(11) NOT NULL,
  `pettyCashPurpose` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account_adjustment`
--
ALTER TABLE `account_adjustment`
  ADD PRIMARY KEY (`accountAdjustmentId`);

--
-- Indexes for table `asset_info`
--
ALTER TABLE `asset_info`
  ADD PRIMARY KEY (`assetId`);

--
-- Indexes for table `bank_account_info`
--
ALTER TABLE `bank_account_info`
  ADD PRIMARY KEY (`bankAccountId`);

--
-- Indexes for table `cash_transfer`
--
ALTER TABLE `cash_transfer`
  ADD PRIMARY KEY (`cashTransferId`);

--
-- Indexes for table `ledger_details_info`
--
ALTER TABLE `ledger_details_info`
  ADD PRIMARY KEY (`ledgerDetailsId`);

--
-- Indexes for table `ledger_info`
--
ALTER TABLE `ledger_info`
  ADD PRIMARY KEY (`ledgerId`);

--
-- Indexes for table `particular_info`
--
ALTER TABLE `particular_info`
  ADD PRIMARY KEY (`particularId`);

--
-- Indexes for table `petty_cash`
--
ALTER TABLE `petty_cash`
  ADD PRIMARY KEY (`pettyCashId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account_adjustment`
--
ALTER TABLE `account_adjustment`
  MODIFY `accountAdjustmentId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `asset_info`
--
ALTER TABLE `asset_info`
  MODIFY `assetId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bank_account_info`
--
ALTER TABLE `bank_account_info`
  MODIFY `bankAccountId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cash_transfer`
--
ALTER TABLE `cash_transfer`
  MODIFY `cashTransferId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ledger_details_info`
--
ALTER TABLE `ledger_details_info`
  MODIFY `ledgerDetailsId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ledger_info`
--
ALTER TABLE `ledger_info`
  MODIFY `ledgerId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `particular_info`
--
ALTER TABLE `particular_info`
  MODIFY `particularId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `petty_cash`
--
ALTER TABLE `petty_cash`
  MODIFY `pettyCashId` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
