-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 24, 2017 at 10:35 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `alhamra`
--

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `serviceId` int(11) NOT NULL,
  `serviceName` varchar(150) COLLATE utf8_bin NOT NULL,
  `serviceNote` varchar(300) COLLATE utf8_bin NOT NULL,
  `serviceType` int(2) NOT NULL COMMENT '1=common, 2=personal',
  `serviceAmount` double NOT NULL DEFAULT '0',
  `serviceStatus` int(11) NOT NULL DEFAULT '1' COMMENT '1=Active, 0=InActive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`serviceId`, `serviceName`, `serviceNote`, `serviceType`, `serviceAmount`, `serviceStatus`) VALUES
(0, 'Energy Charge', 'Energy Charge Note', 1, 0, 0),
(1, 'Common Bill', 'Employee cost for Common Bill', 1, 0, 1),
(2, 'Government Charges', 'Government Charges', 1, 0, 0),
(3, 'Parking Charge', '', 2, 0, 1),
(4, ' Maintenance (Lift, Escalator & Central AC)', '', 2, 0, 1),
(5, ' Cleaning Bill', '', 2, 0, 1),
(6, ' Security Bill', '', 2, 0, 1),
(7, ' Generator Bill', '', 2, 0, 1),
(13, 'Generator Bill', '', 1, 0, 1),
(14, 'Security Bill', '', 1, 0, 1),
(15, 'Cleaning Bill', '', 1, 0, 1),
(16, 'Maintenance (Lift, Escalator & Central AC)', '', 1, 0, 1),
(17, 'Others', '', 1, 0, 0),
(18, 'Parking', '', 1, 0, 0),
(35, 'Common Bill', '', 2, 0, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`serviceId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `serviceId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
