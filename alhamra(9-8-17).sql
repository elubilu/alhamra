-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 08, 2017 at 11:00 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `alhamra`
--

-- --------------------------------------------------------

--
-- Table structure for table `account_adjustment`
--

CREATE TABLE `account_adjustment` (
  `accountAdjustmentId` int(11) NOT NULL,
  `bank_account_info_bankAccountId` int(11) NOT NULL,
  `accountAdjustmentType` int(2) NOT NULL COMMENT '1=add,2=remove',
  `accountAdjustmentDate` datetime NOT NULL,
  `accountAdjustmentbeforeAmount` double NOT NULL,
  `accountAdjustmentAfterAmount` double NOT NULL,
  `accountAdjustmentAmount` double NOT NULL,
  `accountAdjustmentNote` text COLLATE utf8_bin NOT NULL,
  `accountAdjustmentBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `admin_info`
--

CREATE TABLE `admin_info` (
  `adminID` int(11) NOT NULL,
  `adminName` varchar(40) COLLATE utf8_bin NOT NULL,
  `adminEmail` varchar(50) COLLATE utf8_bin NOT NULL,
  `adminContact` varchar(20) COLLATE utf8_bin NOT NULL,
  `adminAddress` text COLLATE utf8_bin NOT NULL,
  `adminNote` text COLLATE utf8_bin NOT NULL,
  `adminUserID` varchar(30) COLLATE utf8_bin NOT NULL,
  `adminPassword` varchar(400) COLLATE utf8_bin NOT NULL,
  `adminStatus` int(11) NOT NULL COMMENT '0=InActive, 1=Active',
  `admin_role_roleID` int(11) NOT NULL,
  `adminJoinDate` datetime NOT NULL,
  `adminUpdateDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `admin_info`
--

INSERT INTO `admin_info` (`adminID`, `adminName`, `adminEmail`, `adminContact`, `adminAddress`, `adminNote`, `adminUserID`, `adminPassword`, `adminStatus`, `admin_role_roleID`, `adminJoinDate`, `adminUpdateDate`) VALUES
(1, 'SatrlabIT', 'starlabTeam@gmail.com', '01719450855', 'Zindabazar,Sylhet', '', 'superadmin', 'd7ea52fb792bed01df7174c48605cf19', 1, 1, '2017-03-09 00:14:57', '0000-00-00 00:00:00'),
(2, '    Shamsia Sharmin', 'shamsia@gmail.com', '011759721012', 'Lovely Road, Sylhet', 'note for user', 'manager', 'd7ea52fb792bed01df7174c48605cf19', 1, 3, '2017-03-24 22:05:49', '0000-00-00 00:00:00'),
(3, 'Keshob Chakrabory', 'keshob.kc@gmail.com', '01719450855', 'Address', '', 'admin', 'd7ea52fb792bed01df7174c48605cf19', 1, 2, '2017-02-10 21:39:54', '0000-00-00 00:00:00'),
(4, 'Rasel', '', '', '', '', 'admin2', '5dbab745ca7dc1a1abb2e4352bdb0549', 1, 2, '2017-08-04 00:00:00', '2017-08-04 16:17:52');

-- --------------------------------------------------------

--
-- Table structure for table `admin_role`
--

CREATE TABLE `admin_role` (
  `roleID` int(11) NOT NULL,
  `roleName` varchar(100) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `admin_role`
--

INSERT INTO `admin_role` (`roleID`, `roleName`) VALUES
(1, 'Supper Admin'),
(2, 'Admin'),
(3, 'Manager'),
(4, 'Accountent'),
(5, 'Cashier'),
(6, 'Spectator');

-- --------------------------------------------------------

--
-- Table structure for table `all_transaction_info`
--

CREATE TABLE `all_transaction_info` (
  `transactionId` int(11) NOT NULL,
  `transactionType` int(3) NOT NULL COMMENT '1=rent bill, 2=current bill , 3=rent payment, 4=bill Payment',
  `transactionReferenceId` int(11) NOT NULL,
  `transactionAmount` double NOT NULL,
  `transactionPrevDue` double NOT NULL,
  `transactionDetails` text COLLATE utf8_bin NOT NULL,
  `transactionDate` datetime NOT NULL,
  `transactionFrom` varchar(500) COLLATE utf8_bin NOT NULL,
  `transactionTo` varchar(500) COLLATE utf8_bin NOT NULL,
  `transactionEntryDate` datetime NOT NULL,
  `transactionStatus` int(2) NOT NULL DEFAULT '1' COMMENT '1=Active, 0=InActive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `all_transaction_info`
--

INSERT INTO `all_transaction_info` (`transactionId`, `transactionType`, `transactionReferenceId`, `transactionAmount`, `transactionPrevDue`, `transactionDetails`, `transactionDate`, `transactionFrom`, `transactionTo`, `transactionEntryDate`, `transactionStatus`) VALUES
(25, 5, 1, 2000, 0, 'cash transfer from cash in hand', '2017-08-03 00:00:00', 'Cash In Hend', 'Bank Account', '2017-08-04 19:47:00', 1),
(26, 5, 2, 2500, 0, 'cash transfer ', '2017-08-04 00:00:00', 'Cash In Hend', 'Bank Account', '2017-08-05 01:52:55', 1),
(27, 5, 3, 1000, 0, 'cash transfer to  petty cash', '2017-08-05 00:00:00', 'Cash In Hend', 'Petty Cash', '2017-08-05 03:07:52', 1),
(28, 5, 4, 500, 0, 'note for cash transfer from petty cash', '2017-08-05 00:00:00', 'Petty Cash', 'Cash In Hend', '2017-08-05 03:09:56', 1),
(29, 5, 6, 500, 0, 'cash transfer from cash in hand', '2017-08-05 00:00:00', 'Cash In Hend', 'Bank Account', '2017-08-05 14:17:35', 1),
(30, 1, 9, 15000, 0, 'rent payment', '2017-08-05 21:58:03', 'Global', 'Cash In Hand', '2017-08-05 21:58:03', 1),
(31, 5, 7, 2000, 0, 'bank to bank transfer cash', '2017-08-09 00:00:00', 'Customer Account', 'Bank Account', '2017-08-09 01:21:41', 1),
(32, 5, 8, 5000, 0, 'note', '2017-08-09 00:00:00', 'Customer Account', 'Bank Account', '2017-08-09 02:28:37', 1),
(33, 5, 9, 5000, 0, 'note ', '2017-08-09 00:00:00', 'Bank Account', 'Customer Account', '2017-08-09 02:29:38', 1),
(34, 3, 2, 500, 0, 'Details for ledger entry', '2017-08-09 02:36:53', 'Car', 'Customer Account', '2017-08-09 02:36:53', 1),
(35, 3, 3, 1000, 0, 'details', '2017-08-09 02:45:59', 'Car', '0', '2017-08-09 02:45:59', 1);

-- --------------------------------------------------------

--
-- Table structure for table `asset_info`
--

CREATE TABLE `asset_info` (
  `assetId` int(11) NOT NULL,
  `assetTitle` varchar(300) COLLATE utf8_bin NOT NULL,
  `assetTrackingId` varchar(200) COLLATE utf8_bin NOT NULL,
  `assetOriginalValue` double NOT NULL,
  `assetCurrentValue` double NOT NULL,
  `assetAccruingDate` datetime NOT NULL,
  `assetDepreciationRate` double NOT NULL,
  `assetNote` text COLLATE utf8_bin NOT NULL,
  `assetStatus` int(2) NOT NULL DEFAULT '1' COMMENT '1=Active, 0=InActive',
  `assetAddedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `asset_info`
--

INSERT INTO `asset_info` (`assetId`, `assetTitle`, `assetTrackingId`, `assetOriginalValue`, `assetCurrentValue`, `assetAccruingDate`, `assetDepreciationRate`, `assetNote`, `assetStatus`, `assetAddedDate`) VALUES
(1, 'Car', '24574', 50000, 48000, '2017-08-05 00:00:00', 9, 'note', 1, '2017-08-05 22:26:11');

-- --------------------------------------------------------

--
-- Table structure for table `bank_account_details`
--

CREATE TABLE `bank_account_details` (
  `bankDetailsId` int(11) NOT NULL,
  `bank_account_infoBankId` int(11) NOT NULL,
  `bankDetailsReferrenceId` int(11) NOT NULL,
  `bankDetailsEntryDate` datetime NOT NULL,
  `bankDetailsTransactionDate` datetime NOT NULL,
  `bankDetailsNote` text COLLATE utf8_bin NOT NULL,
  `bankDetailsDeposit` int(11) NOT NULL,
  `bankDetailsBalance` double NOT NULL,
  `bankDetailsPurpose` int(11) NOT NULL,
  `bankDetailsType` int(2) NOT NULL COMMENT '1=cashin, 2=cash out',
  `bankDetailsStatus` int(2) NOT NULL DEFAULT '1' COMMENT '1=Active, 2=InActive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `bank_account_details`
--

INSERT INTO `bank_account_details` (`bankDetailsId`, `bank_account_infoBankId`, `bankDetailsReferrenceId`, `bankDetailsEntryDate`, `bankDetailsTransactionDate`, `bankDetailsNote`, `bankDetailsDeposit`, `bankDetailsBalance`, `bankDetailsPurpose`, `bankDetailsType`, `bankDetailsStatus`) VALUES
(2, 2, 7, '2017-08-09 01:21:41', '2017-08-09 00:00:00', 'bank to bank transfer cash', 2, 2000, 5, 2, 1),
(3, 1, 7, '2017-08-09 01:21:41', '2017-08-09 00:00:00', 'bank to bank transfer cash', 1, 2000, 5, 1, 1),
(4, 2, 8, '2017-08-09 02:28:37', '2017-08-09 00:00:00', 'note', 2, 5000, 5, 2, 1),
(5, 1, 8, '2017-08-09 02:28:37', '2017-08-09 00:00:00', 'note', 1, 5000, 5, 1, 1),
(6, 1, 9, '2017-08-09 02:29:38', '2017-08-09 00:00:00', 'note ', 1, 5000, 5, 2, 1),
(7, 2, 9, '2017-08-09 02:29:38', '2017-08-09 00:00:00', 'note ', 2, 5000, 5, 1, 1),
(8, 2, 2, '2017-08-09 02:36:53', '2017-08-09 02:36:53', 'Details for ledger entry', 3, 500, 3, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `bank_account_info`
--

CREATE TABLE `bank_account_info` (
  `bankAccountId` int(11) NOT NULL,
  `bankAccountTitle` varchar(350) COLLATE utf8_bin NOT NULL,
  `bankName` varchar(400) COLLATE utf8_bin NOT NULL,
  `bankAccountBranchName` varchar(400) COLLATE utf8_bin NOT NULL,
  `bankAccountHolderName` varchar(300) COLLATE utf8_bin NOT NULL,
  `bankAccountNumber` varchar(200) COLLATE utf8_bin NOT NULL,
  `bankAccountRoutingNumber` varchar(200) COLLATE utf8_bin NOT NULL,
  `bankAccountLastBalance` double NOT NULL DEFAULT '0',
  `bankAccountNote` text COLLATE utf8_bin NOT NULL,
  `bankAccountAddedDate` datetime NOT NULL,
  `bankAccountStatus` int(2) NOT NULL DEFAULT '1' COMMENT '1=active, 0=Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `bank_account_info`
--

INSERT INTO `bank_account_info` (`bankAccountId`, `bankAccountTitle`, `bankName`, `bankAccountBranchName`, `bankAccountHolderName`, `bankAccountNumber`, `bankAccountRoutingNumber`, `bankAccountLastBalance`, `bankAccountNote`, `bankAccountAddedDate`, `bankAccountStatus`) VALUES
(1, 'Bank Account', 'Bangladesh Bank', 'Sylhet, Bangladesh', 'Mr. Account Holder', 'acc-234653', '2408956', 7500, 'First Bank Account ', '2017-08-01 17:49:22', 1),
(2, 'Customer Account', 'Starlab Bank Ltd.', 'Sylhet', 'kanthi Lal', '654687', '240895784', 20500, 'note', '2017-08-09 01:20:43', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bill_info`
--

CREATE TABLE `bill_info` (
  `billId` int(11) NOT NULL,
  `monthly_bill_info_mothlyBillInfoId` int(11) NOT NULL,
  `customer_info_customerId` int(11) NOT NULL,
  `billingMonth` date NOT NULL,
  `customer_info_rentAmount` double NOT NULL,
  `customer_info_rentDue` double NOT NULL,
  `billStatus` int(2) NOT NULL DEFAULT '1' COMMENT '1=pending, 2=final',
  `referrenceId` int(11) NOT NULL COMMENT 'transection referrence',
  `billAddedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `bill_info`
--

INSERT INTO `bill_info` (`billId`, `monthly_bill_info_mothlyBillInfoId`, `customer_info_customerId`, `billingMonth`, `customer_info_rentAmount`, `customer_info_rentDue`, `billStatus`, `referrenceId`, `billAddedDate`) VALUES
(1, 1, 1, '2017-05-01', 1600, 0, 1, 0, '2017-07-01 22:14:11'),
(2, 2, 2, '2017-05-01', 20000, 2000, 1, 0, '2017-07-01 23:12:50'),
(3, 5, 2, '2017-06-01', 20000, 2000, 1, 3, '2017-07-02 03:04:55'),
(4, 6, 1, '2017-06-01', 1600, 1000, 1, 6, '2017-07-02 03:09:21'),
(5, 7, 2, '2017-07-01', 20000, 22000, 1, 9, '2017-07-02 03:55:33'),
(6, 8, 2, '2017-07-01', 20000, 42000, 1, 13, '2017-07-13 12:52:38'),
(7, 9, 1, '2017-07-01', 1600, 2600, 1, 16, '2017-07-13 12:57:50'),
(8, 10, 1, '2017-08-01', 1600, 4200, 1, 17, '2017-07-27 13:04:24'),
(9, 11, 2, '2017-08-01', 20000, 62000, 1, 18, '2017-07-27 13:05:08'),
(10, 12, 2, '2017-08-01', 20000, 82000, 1, 19, '2017-07-27 13:23:29');

-- --------------------------------------------------------

--
-- Table structure for table `cash_in_hand`
--

CREATE TABLE `cash_in_hand` (
  `cashInHendId` int(11) NOT NULL,
  `cashDate` datetime NOT NULL,
  `cashEntryDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `cashTo` varchar(500) COLLATE utf8_bin NOT NULL,
  `cashInType` int(3) NOT NULL COMMENT '1=cash in, 2=cash out',
  `cashAmount` double NOT NULL,
  `cashInStatus` int(2) NOT NULL DEFAULT '1',
  `cashReferenceId` int(11) NOT NULL,
  `cashPurpose` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `cash_in_hand`
--

INSERT INTO `cash_in_hand` (`cashInHendId`, `cashDate`, `cashEntryDate`, `cashTo`, `cashInType`, `cashAmount`, `cashInStatus`, `cashReferenceId`, `cashPurpose`) VALUES
(2, '2017-07-31 15:02:42', '2017-07-31 20:58:21', 'Starlab IT', 1, 5000, 1, 6, 2),
(3, '2017-07-31 15:08:59', '2017-07-31 20:58:27', 'Global', 1, 3000, 1, 7, 1),
(4, '2017-08-03 00:00:00', '2017-08-04 13:30:07', 'Bank Account', 2, 2000, 1, 1, 5),
(5, '2017-08-04 00:00:00', '2017-08-04 19:51:53', 'Bank Account', 2, 2500, 1, 2, 5),
(6, '2017-08-05 00:00:00', '2017-08-04 21:07:52', 'Petty Cash', 2, 1000, 1, 3, 5),
(7, '2017-08-05 00:00:00', '2017-08-04 21:09:56', 'Petty Cash', 1, 500, 1, 4, 5),
(9, '2017-08-05 00:00:00', '2017-08-05 08:17:35', 'Bank Account', 2, 500, 1, 6, 5),
(11, '2017-08-05 21:58:03', '2017-08-05 15:58:03', 'Global', 1, 15000, 1, 9, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cash_transfer`
--

CREATE TABLE `cash_transfer` (
  `cashTransferId` int(11) NOT NULL,
  `cashWithdrawnFrom` int(11) NOT NULL,
  `cashTransferredTo` int(11) NOT NULL,
  `cashTransferAmount` double NOT NULL,
  `cashSlipNo` varchar(150) COLLATE utf8_bin NOT NULL,
  `cashTransferDate` datetime NOT NULL,
  `cashTransferNote` text COLLATE utf8_bin NOT NULL,
  `cashTransferStatus` int(2) NOT NULL,
  `cashTransferEntryDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `cash_transfer`
--

INSERT INTO `cash_transfer` (`cashTransferId`, `cashWithdrawnFrom`, `cashTransferredTo`, `cashTransferAmount`, `cashSlipNo`, `cashTransferDate`, `cashTransferNote`, `cashTransferStatus`, `cashTransferEntryDate`) VALUES
(1, -1, 1, 2000, '01245', '2017-08-03 00:00:00', 'cash transfer from cash in hand', 0, '2017-08-04 19:30:07'),
(2, -1, 1, 2500, 'sl-05436', '2017-08-04 00:00:00', 'cash transfer ', 0, '2017-08-05 01:51:53'),
(3, -1, 0, 1000, '44565', '2017-08-05 00:00:00', 'cash transfer to  petty cash', 0, '2017-08-05 03:07:52'),
(4, 0, -1, 500, '54689493', '2017-08-05 00:00:00', 'note for cash transfer from petty cash', 0, '2017-08-05 03:09:56'),
(6, -1, 1, 500, 'slip-123', '2017-08-05 00:00:00', 'cash transfer from cash in hand', 0, '2017-08-05 14:17:35'),
(7, 2, 1, 2000, '77658232', '2017-08-09 00:00:00', 'bank to bank transfer cash', 0, '2017-08-09 01:21:41'),
(8, 2, 1, 5000, 'slip-7947', '2017-08-09 00:00:00', 'note', 0, '2017-08-09 02:28:37'),
(9, 1, 2, 5000, 'g6y7576', '2017-08-09 00:00:00', 'note ', 0, '2017-08-09 02:29:38');

-- --------------------------------------------------------

--
-- Table structure for table `current_bill_info`
--

CREATE TABLE `current_bill_info` (
  `currentBillId` int(11) NOT NULL,
  `monthly_bill_info_mothlyBillInfoId` int(11) NOT NULL,
  `bill_info_billId` int(11) NOT NULL,
  `meter_info_meterId` int(11) NOT NULL,
  `currentBillPrevReading` double NOT NULL,
  `currentBillPrevReadingDate` datetime NOT NULL,
  `currentBillCurrentReading` double NOT NULL,
  `currentBillCurrentReadingDate` datetime NOT NULL,
  `perUnitCharge` double NOT NULL,
  `currentBillDemandCharge` double NOT NULL,
  `currentBillServiceCharge` double NOT NULL,
  `totalCurrentBill` double NOT NULL,
  `meter_info_billDue` double NOT NULL,
  `currentBillAddedDate` datetime NOT NULL,
  `referrenceId` int(11) NOT NULL COMMENT 'transection referrence'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `current_bill_info`
--

INSERT INTO `current_bill_info` (`currentBillId`, `monthly_bill_info_mothlyBillInfoId`, `bill_info_billId`, `meter_info_meterId`, `currentBillPrevReading`, `currentBillPrevReadingDate`, `currentBillCurrentReading`, `currentBillCurrentReadingDate`, `perUnitCharge`, `currentBillDemandCharge`, `currentBillServiceCharge`, `totalCurrentBill`, `meter_info_billDue`, `currentBillAddedDate`, `referrenceId`) VALUES
(1, 1, 1, 1, 5, '2017-07-01 22:09:56', 7, '2017-07-01 23:02:47', 10, 200, 100, 0, 500, '2017-07-01 22:14:11', 0),
(2, 1, 1, 2, 6.9, '2017-07-01 22:12:07', 8, '2017-07-01 23:03:04', 10, 150, 100, 0, 0, '2017-07-01 22:14:11', 0),
(3, 2, 2, 3, 5.5, '2017-07-01 22:12:46', 8, '2017-07-01 23:13:24', 100, 200, 100, 0, 900, '2017-07-01 23:12:50', 0),
(4, 2, 2, 4, 5.8, '2017-07-01 22:13:35', 8.9, '2017-07-01 23:13:25', 100, 150, 100, 0, 0, '2017-07-01 23:12:50', 0),
(5, 5, 3, 3, 8, '2017-07-01 23:13:24', 12, '2017-07-02 03:05:14', 120, 200, 100, 1520, 900, '2017-07-02 03:04:55', 4),
(6, 5, 3, 4, 8.9, '2017-07-01 23:13:25', 13, '2017-07-02 03:05:15', 120, 150, 100, 1602, 0, '2017-07-02 03:04:55', 5),
(7, 6, 4, 1, 7, '2017-07-01 23:02:47', 10, '2017-07-02 03:09:50', 200, 200, 100, 2370, 500, '2017-07-02 03:09:21', 7),
(8, 6, 4, 2, 8, '2017-07-01 23:03:04', 12, '2017-07-02 03:10:16', 200, 150, 100, 1920, 0, '2017-07-02 03:09:21', 8),
(9, 7, 5, 3, 12, '2017-07-02 03:05:14', 15, '2017-07-02 03:57:34', 250, 200, 100, 2050, 900, '2017-07-02 03:55:33', 10),
(10, 7, 5, 4, 13, '2017-07-02 03:05:15', 15.5, '2017-07-02 03:57:30', 250, 150, 100, 1825, 0, '2017-07-02 03:55:33', 11),
(11, 8, 6, 3, 15, '2017-07-02 03:57:34', 20, '2017-07-13 12:53:03', 5, 200, 100, 1375, 900, '2017-07-13 12:52:38', 14),
(12, 8, 6, 4, 15.5, '2017-07-02 03:57:30', 20, '2017-07-13 12:53:14', 5, 150, 100, 1372.5, 0, '2017-07-13 12:52:38', 15),
(13, 9, 7, 1, 10, '2017-07-02 03:09:50', 600, '2017-07-26 14:57:57', 5, 200, 100, 0, 500, '2017-07-13 12:57:50', 0),
(14, 9, 7, 2, 12, '2017-07-02 03:10:16', 100, '2017-07-26 14:58:15', 5, 150, 100, 0, 0, '2017-07-13 12:57:50', 0),
(15, 10, 8, 1, 600, '2017-07-26 14:57:57', 0, '0000-00-00 00:00:00', 5.8, 200, 100, 0, 500, '2017-07-27 13:04:24', 0),
(16, 11, 9, 4, 20, '2017-07-13 12:53:14', 0, '0000-00-00 00:00:00', 5.8, 150, 100, 0, 0, '2017-07-27 13:05:08', 0),
(17, 12, 10, 3, 20, '2017-07-13 12:53:03', 30, '2017-07-27 13:24:01', 5.8, 200, 100, 1208, 900, '2017-07-27 13:23:29', 20),
(18, 12, 10, 4, 20, '2017-07-13 12:53:14', 30, '2017-07-27 13:24:02', 5.8, 150, 100, 908, 0, '2017-07-27 13:23:29', 21),
(19, 12, 10, 5, 200, '2017-07-27 13:22:42', 250, '2017-07-27 13:24:03', 5.8, 160, 180, 1630, 0, '2017-07-27 13:23:29', 22);

-- --------------------------------------------------------

--
-- Table structure for table `customer_balance_adjustment`
--

CREATE TABLE `customer_balance_adjustment` (
  `customerAdjustmentId` int(11) NOT NULL,
  `customer_info_customerId` int(11) NOT NULL,
  `customerAdjustmentType` int(2) NOT NULL COMMENT '1=add,2=remove',
  `customerAdjustmentDate` datetime NOT NULL,
  `customerAdjustmentbeforeAmount` double NOT NULL,
  `customerAdjustmentAfterAmount` double NOT NULL,
  `customerAdjustmentAmount` double NOT NULL,
  `customerAdjustmentNote` text COLLATE utf8_bin NOT NULL,
  `customerAdjustmentBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `customer_info`
--

CREATE TABLE `customer_info` (
  `customerId` int(11) NOT NULL,
  `customerName` varchar(200) COLLATE utf8_bin NOT NULL,
  `customerPhone` varchar(15) COLLATE utf8_bin NOT NULL,
  `organizationName` varchar(250) COLLATE utf8_bin NOT NULL,
  `spaceType` int(11) NOT NULL,
  `spaceTitle` varchar(50) COLLATE utf8_bin NOT NULL,
  `leasingMethodId` int(11) NOT NULL,
  `rentAmount` double NOT NULL,
  `rentDue` double NOT NULL DEFAULT '0',
  `billDue` double NOT NULL DEFAULT '0',
  `customerStatus` int(1) NOT NULL DEFAULT '1',
  `customerAddedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `customer_info`
--

INSERT INTO `customer_info` (`customerId`, `customerName`, `customerPhone`, `organizationName`, `spaceType`, `spaceTitle`, `leasingMethodId`, `rentAmount`, `rentDue`, `billDue`, `customerStatus`, `customerAddedDate`) VALUES
(1, 'Kanthi Lal', '017123455546', 'Starlab IT', 2, 's-708', 1, 1600, 5800, 1321, 1, '2017-07-01 22:04:43'),
(2, 'Mr. Someone ', '93748976579', 'Global', 1, 's-707', 1, 20000, 84000, 10000.5, 1, '2017-07-01 22:07:20');

-- --------------------------------------------------------

--
-- Table structure for table `customer_services`
--

CREATE TABLE `customer_services` (
  `customerServiceId` int(11) NOT NULL,
  `customer_info_customerId` int(11) NOT NULL,
  `services_serviceId` int(11) NOT NULL,
  `meter_info_meterId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `customer_services`
--

INSERT INTO `customer_services` (`customerServiceId`, `customer_info_customerId`, `services_serviceId`, `meter_info_meterId`) VALUES
(1, 1, 1, 1),
(2, 1, 2, 1),
(6, 1, 1, 2),
(7, 1, 2, 2),
(8, 1, 7, 2),
(9, 2, 1, 3),
(10, 2, 8, 3),
(11, 2, 3, 3),
(12, 2, 1, 4),
(13, 2, 2, 4),
(14, 2, 8, 4),
(15, 2, 1, 5),
(16, 2, 3, 5),
(17, 2, 4, 5),
(18, 2, 9, 5),
(19, 1, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `leasing_method`
--

CREATE TABLE `leasing_method` (
  `leasingId` int(11) NOT NULL,
  `leasingName` varchar(50) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `leasing_method`
--

INSERT INTO `leasing_method` (`leasingId`, `leasingName`) VALUES
(1, 'Rent'),
(2, 'Jomidari'),
(3, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `ledger_details_info`
--

CREATE TABLE `ledger_details_info` (
  `ledgerDetailsId` int(11) NOT NULL,
  `ledgerDetailsType` int(3) NOT NULL COMMENT '1=income, 2=expense',
  `ledger_info_ledgerId` int(11) NOT NULL,
  `ledgerDetails` text COLLATE utf8_bin NOT NULL,
  `ledgerDetailsAmount` double NOT NULL,
  `ledgerDepositeTo` int(11) NOT NULL,
  `ledgerWithdrawnFrom` int(11) NOT NULL,
  `ledgerDetailsNote` text COLLATE utf8_bin NOT NULL,
  `ledgerDetailsStatus` int(2) NOT NULL DEFAULT '1' COMMENT '1=Active, 0=InActive',
  `ledgerDetailsAddedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `ledger_details_info`
--

INSERT INTO `ledger_details_info` (`ledgerDetailsId`, `ledgerDetailsType`, `ledger_info_ledgerId`, `ledgerDetails`, `ledgerDetailsAmount`, `ledgerDepositeTo`, `ledgerWithdrawnFrom`, `ledgerDetailsNote`, `ledgerDetailsStatus`, `ledgerDetailsAddedDate`) VALUES
(2, 1, 3, 'Details for ledger entry', 500, 2, 0, 'note for ledger entry', 1, '2017-08-09 02:36:53'),
(3, 1, 3, 'details', 1000, 0, 0, 'note ', 1, '2017-08-09 02:45:59');

-- --------------------------------------------------------

--
-- Table structure for table `ledger_info`
--

CREATE TABLE `ledger_info` (
  `ledgerId` int(11) NOT NULL,
  `ledgerTitle` varchar(500) COLLATE utf8_bin NOT NULL,
  `particular_info_particularid` int(11) NOT NULL,
  `ledgerType` int(3) NOT NULL DEFAULT '1' COMMENT '1=income,2=expense',
  `ledgerBalance` double NOT NULL,
  `ledgerNote` text COLLATE utf8_bin NOT NULL,
  `ledgerStatus` int(2) NOT NULL DEFAULT '1' COMMENT '1=Active, 0=InActive',
  `ledgerAddedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `ledger_info`
--

INSERT INTO `ledger_info` (`ledgerId`, `ledgerTitle`, `particular_info_particularid`, `ledgerType`, `ledgerBalance`, `ledgerNote`, `ledgerStatus`, `ledgerAddedDate`) VALUES
(1, 'Electric Bill', 1, 2, 2000, 'Electric bill', 1, '2017-08-06 01:56:33'),
(2, 'Gas Bill', 1, 2, 3000, 'Gas Bill Ledger', 1, '2017-08-06 02:00:21'),
(3, 'Car', 2, 1, 5000, 'income for cars', 1, '2017-08-06 02:14:39');

-- --------------------------------------------------------

--
-- Table structure for table `meter_info`
--

CREATE TABLE `meter_info` (
  `meterId` int(11) NOT NULL,
  `customer_info_customerId` int(11) NOT NULL,
  `meterCustomerId` varchar(100) COLLATE utf8_bin NOT NULL COMMENT 'admin will give a id/ name',
  `previousReading` double NOT NULL,
  `previousReadingDate` datetime NOT NULL,
  `currentReading` double NOT NULL,
  `meterNo` varchar(60) COLLATE utf8_bin NOT NULL,
  `demandCharge` double NOT NULL,
  `meterServiceCharge` double NOT NULL,
  `billDue` double NOT NULL,
  `meterStatus` int(11) NOT NULL DEFAULT '1' COMMENT '1=Active, 0=InActive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `meter_info`
--

INSERT INTO `meter_info` (`meterId`, `customer_info_customerId`, `meterCustomerId`, `previousReading`, `previousReadingDate`, `currentReading`, `meterNo`, `demandCharge`, `meterServiceCharge`, `billDue`, `meterStatus`) VALUES
(1, 1, 'Starlab', 600, '2017-07-26 14:57:57', 0, 'M-702', 200, 100, 500, 1),
(2, 1, 'Starlab', 100, '2017-07-26 14:58:15', 0, 'M-708', 150, 100, 0, 1),
(3, 2, 'global', 30, '2017-07-27 13:24:01', 0, 'M-707', 200, 100, 900, 1),
(4, 2, 'global', 30, '2017-07-27 13:24:02', 0, 'M-706', 150, 100, 0, 1),
(5, 2, 'global', 250, '2017-07-27 13:24:03', 0, 's-710', 160, 180, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `monthly_bill_info`
--

CREATE TABLE `monthly_bill_info` (
  `mothlyBillInfoId` int(11) NOT NULL,
  `monthlyBillingStatus` int(2) NOT NULL COMMENT '1=pending, 2=final',
  `monthlyBillTypeFor` int(2) NOT NULL COMMENT '1=shop,2=office,3=others, 4=Rent Payment, 5=Bill Payment',
  `billingMonthYear` date NOT NULL,
  `monthlyBillAddedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `monthly_bill_info`
--

INSERT INTO `monthly_bill_info` (`mothlyBillInfoId`, `monthlyBillingStatus`, `monthlyBillTypeFor`, `billingMonthYear`, `monthlyBillAddedDate`) VALUES
(1, 2, 2, '2017-05-01', '2017-07-01 22:14:11'),
(2, 2, 1, '2017-05-01', '2017-07-01 23:12:50'),
(5, 2, 1, '2017-06-01', '2017-07-02 03:04:55'),
(6, 2, 2, '2017-06-01', '2017-07-02 03:09:21'),
(7, 2, 1, '2017-07-01', '2017-07-02 03:55:33'),
(8, 2, 1, '2017-07-01', '2017-07-13 12:52:38'),
(9, 1, 2, '2017-07-01', '2017-07-13 12:57:50'),
(10, 1, 2, '2017-08-01', '2017-07-27 13:04:24'),
(11, 1, 1, '2017-08-01', '2017-07-27 13:05:08'),
(12, 2, 1, '2017-08-01', '2017-07-27 13:23:29');

-- --------------------------------------------------------

--
-- Table structure for table `particular_info`
--

CREATE TABLE `particular_info` (
  `particularId` int(11) NOT NULL,
  `particularTitle` varchar(350) COLLATE utf8_bin NOT NULL,
  `particularType` int(3) NOT NULL DEFAULT '1' COMMENT '1=income,2=expense',
  `particularStatus` int(2) NOT NULL DEFAULT '1' COMMENT '1=Active, 0=InActive',
  `particularNote` text COLLATE utf8_bin NOT NULL,
  `particularAddedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `particular_info`
--

INSERT INTO `particular_info` (`particularId`, `particularTitle`, `particularType`, `particularStatus`, `particularNote`, `particularAddedDate`) VALUES
(1, 'Office Bill', 2, 1, 'office electric bill, gas bill etc.', '2017-08-05 13:37:10'),
(2, 'Income Particular', 1, 1, 'All Office Income', '2017-08-05 13:39:56'),
(3, 'Office Bill', 2, 2, 'office electric bill, gas bill etc.', '2017-08-09 02:48:14'),
(4, 'Office Bill', 2, 1, 'office electric bill, gas bill etc.', '2017-08-09 02:49:42');

-- --------------------------------------------------------

--
-- Table structure for table `payment_history`
--

CREATE TABLE `payment_history` (
  `paymentId` int(11) NOT NULL,
  `customer_info_customerId` int(11) NOT NULL,
  `paymentAmount` double NOT NULL,
  `payment_types_paymentTypeId` int(11) NOT NULL COMMENT '1=Bill, 2=Rent',
  `paymentDetails` text COLLATE utf8_bin NOT NULL,
  `paymentDate` datetime NOT NULL,
  `referrenceId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `payment_history`
--

INSERT INTO `payment_history` (`paymentId`, `customer_info_customerId`, `paymentAmount`, `payment_types_paymentTypeId`, `paymentDetails`, `paymentDate`, `referrenceId`) VALUES
(1, 2, 2000, 1, 'Bill Payment', '2017-07-02 01:30:18', 0),
(2, 1, 600, 2, 'Rent Payment', '2017-07-02 02:37:15', 1),
(3, 2, 20000, 2, 'Rent Payment', '2017-07-02 03:01:48', 2),
(4, 2, 5000, 1, 'Monthly Bill Payment', '2017-07-02 03:58:40', 12),
(6, 1, 5000, 1, 'bill payments', '2017-07-31 15:02:42', 0),
(7, 2, 3000, 2, 'Rent payment', '2017-07-31 15:08:59', 0),
(9, 2, 15000, 2, 'rent payment', '2017-08-05 21:58:03', 0);

-- --------------------------------------------------------

--
-- Table structure for table `payment_types`
--

CREATE TABLE `payment_types` (
  `paymentTypeId` int(11) NOT NULL,
  `paymentTypeName` varchar(60) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `payment_types`
--

INSERT INTO `payment_types` (`paymentTypeId`, `paymentTypeName`) VALUES
(1, 'Bill Payment'),
(2, 'Rent Payment');

-- --------------------------------------------------------

--
-- Table structure for table `petty_cash`
--

CREATE TABLE `petty_cash` (
  `pettyCashId` int(11) NOT NULL,
  `pettyCashDate` datetime NOT NULL,
  `pettyCashEntryDate` datetime NOT NULL,
  `pettyCashto` varchar(500) COLLATE utf8_bin NOT NULL,
  `pettyCashType` int(3) NOT NULL,
  `pettyCashAmount` double NOT NULL,
  `pettyCashStatus` int(2) NOT NULL DEFAULT '1' COMMENT '1=Active, 0=InActive',
  `pettyCashReferenceId` int(11) NOT NULL,
  `pettyCashPurpose` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `petty_cash`
--

INSERT INTO `petty_cash` (`pettyCashId`, `pettyCashDate`, `pettyCashEntryDate`, `pettyCashto`, `pettyCashType`, `pettyCashAmount`, `pettyCashStatus`, `pettyCashReferenceId`, `pettyCashPurpose`) VALUES
(1, '2017-08-05 00:00:00', '2017-08-05 03:07:52', 'Cash In Hend', 1, 1000, 1, 3, 5),
(2, '2017-08-05 00:00:00', '2017-08-05 03:09:56', 'Cash In Hend', 2, 500, 1, 4, 5),
(3, '2017-08-09 02:45:59', '2017-08-09 02:45:59', 'Car', 1, 1000, 1, 3, 3);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `serviceId` int(11) NOT NULL,
  `serviceName` varchar(150) COLLATE utf8_bin NOT NULL,
  `serviceNote` varchar(300) COLLATE utf8_bin NOT NULL,
  `serviceType` int(2) NOT NULL COMMENT '1=common, 2=personal',
  `serviceAmount` double NOT NULL DEFAULT '0',
  `serviceStatus` int(11) NOT NULL DEFAULT '1' COMMENT '1=Active, 0=InActive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`serviceId`, `serviceName`, `serviceNote`, `serviceType`, `serviceAmount`, `serviceStatus`) VALUES
(1, 'Common Bill', 'Employee cost for Common Bill', 1, 0, 1),
(2, 'Government Charges', 'Government Charges', 1, 0, 1),
(3, 'Cleaning Charge', 'Employee cost for cleaning', 1, 0, 1),
(4, 'Staff', 'cost for staffs', 1, 0, 1),
(7, 'Additional charge', '', 2, 200, 1),
(8, 'Security Charges', '', 2, 300, 1),
(9, 'Parking', '', 2, 300, 1);

-- --------------------------------------------------------

--
-- Table structure for table `services_bill_info`
--

CREATE TABLE `services_bill_info` (
  `serviceBillId` int(11) NOT NULL,
  `services_serviceId` int(11) NOT NULL,
  `bill_info_billId` int(11) NOT NULL,
  `current_bill_info_currentBillId` int(11) NOT NULL,
  `monthly_bill_info_mothlyBillInfoId` int(11) NOT NULL,
  `serviceAmount` double NOT NULL,
  `serviceBillingMonth` date NOT NULL,
  `serviceBillAddedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `services_bill_info`
--

INSERT INTO `services_bill_info` (`serviceBillId`, `services_serviceId`, `bill_info_billId`, `current_bill_info_currentBillId`, `monthly_bill_info_mothlyBillInfoId`, `serviceAmount`, `serviceBillingMonth`, `serviceBillAddedDate`) VALUES
(1, 1, 1, 1, 1, 10, '2017-05-01', '2017-07-01 22:14:11'),
(2, 2, 1, 1, 1, 10, '2017-05-01', '2017-07-01 22:14:11'),
(3, 3, 1, 1, 1, 10, '2017-05-01', '2017-07-01 22:14:11'),
(4, 5, 1, 1, 1, 500, '2017-05-01', '2017-07-01 22:14:11'),
(5, 6, 1, 1, 1, 200, '2017-05-01', '2017-07-01 22:14:11'),
(6, 1, 1, 2, 1, 10, '2017-05-01', '2017-07-01 22:14:11'),
(7, 2, 1, 2, 1, 10, '2017-05-01', '2017-07-01 22:14:11'),
(8, 7, 1, 2, 1, 200, '2017-05-01', '2017-07-01 22:14:11'),
(9, 1, 2, 3, 2, 200, '2017-05-01', '2017-07-01 23:12:50'),
(10, 2, 2, 3, 2, 250, '2017-05-01', '2017-07-01 23:12:50'),
(11, 3, 2, 3, 2, 300, '2017-05-01', '2017-07-01 23:12:50'),
(12, 1, 2, 4, 2, 200, '2017-05-01', '2017-07-01 23:12:50'),
(13, 2, 2, 4, 2, 250, '2017-05-01', '2017-07-01 23:12:50'),
(14, 8, 2, 4, 2, 300, '2017-05-01', '2017-07-01 23:12:50'),
(15, 1, 3, 5, 5, 300, '2017-06-01', '2017-07-02 03:04:55'),
(16, 2, 3, 5, 5, 260, '2017-06-01', '2017-07-02 03:04:55'),
(17, 3, 3, 5, 5, 180, '2017-06-01', '2017-07-02 03:04:55'),
(18, 1, 3, 6, 5, 300, '2017-06-01', '2017-07-02 03:04:55'),
(19, 2, 3, 6, 5, 260, '2017-06-01', '2017-07-02 03:04:55'),
(20, 8, 3, 6, 5, 300, '2017-06-01', '2017-07-02 03:04:55'),
(21, 1, 4, 7, 6, 300, '2017-06-01', '2017-07-02 03:09:21'),
(22, 2, 4, 7, 6, 270, '2017-06-01', '2017-07-02 03:09:21'),
(23, 3, 4, 7, 6, 200, '2017-06-01', '2017-07-02 03:09:21'),
(24, 5, 4, 7, 6, 500, '2017-06-01', '2017-07-02 03:09:21'),
(25, 6, 4, 7, 6, 200, '2017-06-01', '2017-07-02 03:09:21'),
(26, 1, 4, 8, 6, 340, '2017-06-01', '2017-07-02 03:09:21'),
(27, 2, 4, 8, 6, 330, '2017-06-01', '2017-07-02 03:09:21'),
(28, 7, 4, 8, 6, 200, '2017-06-01', '2017-07-02 03:09:21'),
(29, 1, 5, 9, 7, 400, '2017-07-01', '2017-07-02 03:55:33'),
(30, 2, 5, 9, 7, 250, '2017-07-01', '2017-07-02 03:55:33'),
(31, 3, 5, 9, 7, 350, '2017-07-01', '2017-07-02 03:55:33'),
(32, 1, 5, 10, 7, 400, '2017-07-01', '2017-07-02 03:55:33'),
(33, 2, 5, 10, 7, 250, '2017-07-01', '2017-07-02 03:55:33'),
(34, 8, 5, 10, 7, 300, '2017-07-01', '2017-07-02 03:55:33'),
(35, 1, 6, 11, 8, 300, '2017-07-01', '2017-07-13 12:52:38'),
(36, 2, 6, 11, 8, 500, '2017-07-01', '2017-07-13 12:52:38'),
(37, 3, 6, 11, 8, 250, '2017-07-01', '2017-07-13 12:52:38'),
(38, 1, 6, 12, 8, 300, '2017-07-01', '2017-07-13 12:52:38'),
(39, 2, 6, 12, 8, 500, '2017-07-01', '2017-07-13 12:52:38'),
(40, 8, 6, 12, 8, 300, '2017-07-01', '2017-07-13 12:52:38'),
(41, 1, 7, 13, 9, 100, '2017-07-01', '2017-07-13 12:57:50'),
(42, 2, 7, 13, 9, 50, '2017-07-01', '2017-07-13 12:57:50'),
(43, 3, 7, 13, 9, 123, '2017-07-01', '2017-07-13 12:57:50'),
(44, 5, 7, 13, 9, 500, '2017-07-01', '2017-07-13 12:57:50'),
(45, 6, 7, 13, 9, 200, '2017-07-01', '2017-07-13 12:57:50'),
(46, 1, 7, 14, 9, 0, '2017-07-01', '2017-07-13 12:57:50'),
(47, 2, 7, 14, 9, 0, '2017-07-01', '2017-07-13 12:57:50'),
(48, 7, 7, 14, 9, 200, '2017-07-01', '2017-07-13 12:57:50'),
(49, 1, 8, 15, 10, 0, '2017-08-01', '2017-07-27 13:04:24'),
(50, 2, 8, 15, 10, 0, '2017-08-01', '2017-07-27 13:04:24'),
(51, 3, 8, 15, 10, 0, '2017-08-01', '2017-07-27 13:04:24'),
(52, 5, 8, 15, 10, 500, '2017-08-01', '2017-07-27 13:04:24'),
(53, 6, 8, 15, 10, 200, '2017-08-01', '2017-07-27 13:04:24'),
(54, 1, 9, 16, 11, 250, '2017-08-01', '2017-07-27 13:05:08'),
(55, 2, 9, 16, 11, 300, '2017-08-01', '2017-07-27 13:05:08'),
(56, 8, 9, 16, 11, 300, '2017-08-01', '2017-07-27 13:05:08'),
(57, 1, 10, 17, 12, 300, '2017-08-01', '2017-07-27 13:23:29'),
(58, 8, 10, 17, 12, 300, '2017-08-01', '2017-07-27 13:23:29'),
(59, 3, 10, 17, 12, 250, '2017-08-01', '2017-07-27 13:23:29'),
(60, 1, 10, 18, 12, 300, '2017-08-01', '2017-07-27 13:23:29'),
(61, 2, 10, 18, 12, 0, '2017-08-01', '2017-07-27 13:23:29'),
(62, 8, 10, 18, 12, 300, '2017-08-01', '2017-07-27 13:23:29'),
(63, 1, 10, 19, 12, 300, '2017-08-01', '2017-07-27 13:23:29'),
(64, 3, 10, 19, 12, 250, '2017-08-01', '2017-07-27 13:23:29'),
(65, 4, 10, 19, 12, 150, '2017-08-01', '2017-07-27 13:23:29'),
(66, 9, 10, 19, 12, 300, '2017-08-01', '2017-07-27 13:23:29');

-- --------------------------------------------------------

--
-- Table structure for table `space_type`
--

CREATE TABLE `space_type` (
  `spaceTypeId` int(11) NOT NULL,
  `spaceTypeName` varchar(50) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `space_type`
--

INSERT INTO `space_type` (`spaceTypeId`, `spaceTypeName`) VALUES
(1, 'Shop'),
(2, 'Office'),
(3, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `transaction_type_info`
--

CREATE TABLE `transaction_type_info` (
  `transactionId` int(11) NOT NULL,
  `transactionName` varchar(50) COLLATE utf8_bin NOT NULL,
  `transactionDetailslink` varchar(500) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `transaction_type_info`
--

INSERT INTO `transaction_type_info` (`transactionId`, `transactionName`, `transactionDetailslink`) VALUES
(1, 'Rent Payment', 'viewReceivedPayment'),
(2, 'Bill Payment', 'viewReceivedPayment'),
(3, 'Income', 'viewLedgerEntry'),
(4, 'Expense', 'viewLedgerEntry'),
(5, 'Cash Transfer', 'viewTransaction');

-- --------------------------------------------------------

--
-- Table structure for table `transection_info`
--

CREATE TABLE `transection_info` (
  `transectionId` int(11) NOT NULL,
  `TransectionType` int(2) NOT NULL COMMENT '1=rent bill, 2=current bill , 3=rent payment, 4=bill Payment',
  `customer_info_customerId` int(11) NOT NULL,
  `transectionAmount` double NOT NULL,
  `transectionPrevDue` double NOT NULL,
  `transectionDetails` text COLLATE utf8_bin NOT NULL,
  `transectionDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `transection_info`
--

INSERT INTO `transection_info` (`transectionId`, `TransectionType`, `customer_info_customerId`, `transectionAmount`, `transectionPrevDue`, `transectionDetails`, `transectionDate`) VALUES
(1, 3, 1, 600, 1600, 'Rent Payment', '2017-07-02 02:37:15'),
(2, 3, 2, 20000, 22000, 'Rent Payment', '2017-07-02 03:01:48'),
(3, 1, 2, 20000, 2000, 'Monthly Rent', '2017-07-02 03:04:55'),
(4, 2, 2, 1520, 1000, 'Monthly Bill', '2017-07-02 03:08:18'),
(5, 2, 2, 1602, 2000, 'Monthly Bill', '2017-07-02 03:08:18'),
(6, 1, 1, 1600, 1000, 'Monthly Rent', '2017-07-02 03:09:21'),
(7, 2, 1, 2370, 2600, 'Monthly Bill', '2017-07-02 03:10:22'),
(8, 2, 1, 1920, 2600, 'Monthly Bill', '2017-07-02 03:10:22'),
(9, 1, 2, 20000, 22000, 'Monthly Rent', '2017-07-02 03:55:33'),
(10, 2, 2, 2050, 4632, 'Monthly Bill', '2017-07-02 03:57:38'),
(11, 2, 2, 1825, 6682, 'Monthly Bill', '2017-07-02 03:57:38'),
(12, 4, 2, 5000, 8507, 'Monthly Bill Payment', '2017-07-02 03:58:40'),
(13, 1, 2, 20000, 42000, 'Monthly Rent', '2017-07-13 12:52:38'),
(14, 2, 2, 1375, 3507, 'Monthly Bill', '2017-07-13 12:53:16'),
(15, 2, 2, 1372.5, 4882, 'Monthly Bill', '2017-07-13 12:53:16'),
(16, 1, 1, 1600, 2600, 'Monthly Rent', '2017-07-13 12:57:50'),
(0, 4, 1, 5000, 6321, 'bill payments', '2017-07-31 15:02:42'),
(0, 3, 2, 3000, 102000, 'Rent payment', '2017-07-31 15:08:59'),
(0, 3, 2, 15000, 99000, 'rent payment', '2017-08-05 21:58:03');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account_adjustment`
--
ALTER TABLE `account_adjustment`
  ADD PRIMARY KEY (`accountAdjustmentId`);

--
-- Indexes for table `admin_info`
--
ALTER TABLE `admin_info`
  ADD PRIMARY KEY (`adminID`),
  ADD UNIQUE KEY `adminUserID` (`adminUserID`);

--
-- Indexes for table `admin_role`
--
ALTER TABLE `admin_role`
  ADD PRIMARY KEY (`roleID`);

--
-- Indexes for table `all_transaction_info`
--
ALTER TABLE `all_transaction_info`
  ADD PRIMARY KEY (`transactionId`);

--
-- Indexes for table `asset_info`
--
ALTER TABLE `asset_info`
  ADD PRIMARY KEY (`assetId`);

--
-- Indexes for table `bank_account_details`
--
ALTER TABLE `bank_account_details`
  ADD PRIMARY KEY (`bankDetailsId`);

--
-- Indexes for table `bank_account_info`
--
ALTER TABLE `bank_account_info`
  ADD PRIMARY KEY (`bankAccountId`);

--
-- Indexes for table `bill_info`
--
ALTER TABLE `bill_info`
  ADD PRIMARY KEY (`billId`);

--
-- Indexes for table `cash_in_hand`
--
ALTER TABLE `cash_in_hand`
  ADD PRIMARY KEY (`cashInHendId`);

--
-- Indexes for table `cash_transfer`
--
ALTER TABLE `cash_transfer`
  ADD PRIMARY KEY (`cashTransferId`);

--
-- Indexes for table `current_bill_info`
--
ALTER TABLE `current_bill_info`
  ADD PRIMARY KEY (`currentBillId`);

--
-- Indexes for table `customer_balance_adjustment`
--
ALTER TABLE `customer_balance_adjustment`
  ADD PRIMARY KEY (`customerAdjustmentId`);

--
-- Indexes for table `customer_info`
--
ALTER TABLE `customer_info`
  ADD PRIMARY KEY (`customerId`);

--
-- Indexes for table `customer_services`
--
ALTER TABLE `customer_services`
  ADD PRIMARY KEY (`customerServiceId`);

--
-- Indexes for table `leasing_method`
--
ALTER TABLE `leasing_method`
  ADD PRIMARY KEY (`leasingId`);

--
-- Indexes for table `ledger_details_info`
--
ALTER TABLE `ledger_details_info`
  ADD PRIMARY KEY (`ledgerDetailsId`);

--
-- Indexes for table `ledger_info`
--
ALTER TABLE `ledger_info`
  ADD PRIMARY KEY (`ledgerId`);

--
-- Indexes for table `meter_info`
--
ALTER TABLE `meter_info`
  ADD PRIMARY KEY (`meterId`);

--
-- Indexes for table `monthly_bill_info`
--
ALTER TABLE `monthly_bill_info`
  ADD PRIMARY KEY (`mothlyBillInfoId`);

--
-- Indexes for table `particular_info`
--
ALTER TABLE `particular_info`
  ADD PRIMARY KEY (`particularId`);

--
-- Indexes for table `payment_history`
--
ALTER TABLE `payment_history`
  ADD PRIMARY KEY (`paymentId`);

--
-- Indexes for table `payment_types`
--
ALTER TABLE `payment_types`
  ADD PRIMARY KEY (`paymentTypeId`);

--
-- Indexes for table `petty_cash`
--
ALTER TABLE `petty_cash`
  ADD PRIMARY KEY (`pettyCashId`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`serviceId`);

--
-- Indexes for table `services_bill_info`
--
ALTER TABLE `services_bill_info`
  ADD PRIMARY KEY (`serviceBillId`);

--
-- Indexes for table `space_type`
--
ALTER TABLE `space_type`
  ADD PRIMARY KEY (`spaceTypeId`);

--
-- Indexes for table `transaction_type_info`
--
ALTER TABLE `transaction_type_info`
  ADD PRIMARY KEY (`transactionId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account_adjustment`
--
ALTER TABLE `account_adjustment`
  MODIFY `accountAdjustmentId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `admin_info`
--
ALTER TABLE `admin_info`
  MODIFY `adminID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `admin_role`
--
ALTER TABLE `admin_role`
  MODIFY `roleID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `all_transaction_info`
--
ALTER TABLE `all_transaction_info`
  MODIFY `transactionId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `asset_info`
--
ALTER TABLE `asset_info`
  MODIFY `assetId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `bank_account_details`
--
ALTER TABLE `bank_account_details`
  MODIFY `bankDetailsId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `bank_account_info`
--
ALTER TABLE `bank_account_info`
  MODIFY `bankAccountId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `bill_info`
--
ALTER TABLE `bill_info`
  MODIFY `billId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `cash_in_hand`
--
ALTER TABLE `cash_in_hand`
  MODIFY `cashInHendId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `cash_transfer`
--
ALTER TABLE `cash_transfer`
  MODIFY `cashTransferId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `current_bill_info`
--
ALTER TABLE `current_bill_info`
  MODIFY `currentBillId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `customer_balance_adjustment`
--
ALTER TABLE `customer_balance_adjustment`
  MODIFY `customerAdjustmentId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customer_info`
--
ALTER TABLE `customer_info`
  MODIFY `customerId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `customer_services`
--
ALTER TABLE `customer_services`
  MODIFY `customerServiceId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `leasing_method`
--
ALTER TABLE `leasing_method`
  MODIFY `leasingId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ledger_details_info`
--
ALTER TABLE `ledger_details_info`
  MODIFY `ledgerDetailsId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ledger_info`
--
ALTER TABLE `ledger_info`
  MODIFY `ledgerId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `meter_info`
--
ALTER TABLE `meter_info`
  MODIFY `meterId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `monthly_bill_info`
--
ALTER TABLE `monthly_bill_info`
  MODIFY `mothlyBillInfoId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `particular_info`
--
ALTER TABLE `particular_info`
  MODIFY `particularId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `payment_history`
--
ALTER TABLE `payment_history`
  MODIFY `paymentId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `payment_types`
--
ALTER TABLE `payment_types`
  MODIFY `paymentTypeId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `petty_cash`
--
ALTER TABLE `petty_cash`
  MODIFY `pettyCashId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `serviceId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `services_bill_info`
--
ALTER TABLE `services_bill_info`
  MODIFY `serviceBillId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;
--
-- AUTO_INCREMENT for table `space_type`
--
ALTER TABLE `space_type`
  MODIFY `spaceTypeId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `transaction_type_info`
--
ALTER TABLE `transaction_type_info`
  MODIFY `transactionId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
