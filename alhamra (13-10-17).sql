-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 12, 2017 at 11:04 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `alhamra`
--

-- --------------------------------------------------------

--
-- Table structure for table `account_adjustment`
--

CREATE TABLE `account_adjustment` (
  `accountAdjustmentId` int(11) NOT NULL,
  `bank_account_info_bankAccountId` int(11) NOT NULL,
  `accountAdjustmentType` int(2) NOT NULL COMMENT '1=add,2=remove',
  `accountAdjustmentDate` datetime NOT NULL,
  `accountAdjustmentbeforeAmount` double NOT NULL,
  `accountAdjustmentAfterAmount` double NOT NULL,
  `accountAdjustmentAmount` double NOT NULL,
  `accountAdjustmentNote` text COLLATE utf8_bin NOT NULL,
  `accountAdjustmentBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `admin_info`
--

CREATE TABLE `admin_info` (
  `adminID` int(11) NOT NULL,
  `adminName` varchar(40) COLLATE utf8_bin NOT NULL,
  `adminEmail` varchar(50) COLLATE utf8_bin NOT NULL,
  `adminContact` varchar(20) COLLATE utf8_bin NOT NULL,
  `adminAddress` text COLLATE utf8_bin NOT NULL,
  `adminNote` text COLLATE utf8_bin NOT NULL,
  `adminUserID` varchar(30) COLLATE utf8_bin NOT NULL,
  `adminPassword` varchar(400) COLLATE utf8_bin NOT NULL,
  `adminStatus` int(11) NOT NULL COMMENT '0=InActive, 1=Active',
  `admin_role_roleID` int(11) NOT NULL,
  `adminJoinDate` datetime NOT NULL,
  `adminUpdateDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `admin_info`
--

INSERT INTO `admin_info` (`adminID`, `adminName`, `adminEmail`, `adminContact`, `adminAddress`, `adminNote`, `adminUserID`, `adminPassword`, `adminStatus`, `admin_role_roleID`, `adminJoinDate`, `adminUpdateDate`) VALUES
(1, 'SatrlabIT', 'starlabTeam@gmail.com', '01719450855', 'Zindabazar,Sylhet', '', 'superadmin', 'd7ea52fb792bed01df7174c48605cf19', 1, 1, '2017-03-09 00:14:57', '0000-00-00 00:00:00'),
(2, 'Shamsia Sharmin', 'shamsia@gmail.com', '011759721012', 'Lovely Road, Sylhet', 'note for user', 'manager', 'd7ea52fb792bed01df7174c48605cf19', 1, 3, '2017-03-24 22:05:49', '0000-00-00 00:00:00'),
(3, 'Keshob Chakrabory', 'keshob.kc@gmail.com', '01719450855', 'Address', '', 'admin', 'd7ea52fb792bed01df7174c48605cf19', 1, 2, '2017-02-10 21:39:54', '0000-00-00 00:00:00'),
(4, 'Rasel', '', '', '', '', 'admin2', '5dbab745ca7dc1a1abb2e4352bdb0549', 1, 2, '2017-08-04 00:00:00', '2017-08-04 16:17:52');

-- --------------------------------------------------------

--
-- Table structure for table `admin_role`
--

CREATE TABLE `admin_role` (
  `roleID` int(11) NOT NULL,
  `roleName` varchar(100) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `admin_role`
--

INSERT INTO `admin_role` (`roleID`, `roleName`) VALUES
(1, 'Supper Admin'),
(2, 'Admin'),
(3, 'Manager'),
(4, 'Accountent'),
(5, 'Cashier'),
(6, 'Spectator');

-- --------------------------------------------------------

--
-- Table structure for table `all_transaction_info`
--

CREATE TABLE `all_transaction_info` (
  `transactionId` int(11) NOT NULL,
  `transactionType` int(3) NOT NULL COMMENT '1=rent bill, 2=current bill , 3=rent payment, 4=bill Payment',
  `transactionReferenceId` int(11) NOT NULL,
  `transactionAmount` double NOT NULL,
  `transactionPrevDue` double NOT NULL,
  `transactionDetails` text COLLATE utf8_bin NOT NULL,
  `transactionDate` datetime NOT NULL,
  `transactionFrom` varchar(500) COLLATE utf8_bin NOT NULL,
  `transactionTo` varchar(500) COLLATE utf8_bin NOT NULL,
  `transactionEntryDate` datetime NOT NULL,
  `transactionStatus` int(2) NOT NULL DEFAULT '1' COMMENT '1=Active, 0=InActive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `all_transaction_info`
--

INSERT INTO `all_transaction_info` (`transactionId`, `transactionType`, `transactionReferenceId`, `transactionAmount`, `transactionPrevDue`, `transactionDetails`, `transactionDate`, `transactionFrom`, `transactionTo`, `transactionEntryDate`, `transactionStatus`) VALUES
(1, 5, 1, 500, 0, 'note', '2017-09-25 00:00:00', 'Cash In Hand', 'Petty Cash', '2017-09-25 14:35:47', 1),
(2, 2, 3, 400, 0, 'bill payment', '2017-10-12 00:50:33', 'Starlab IT', 'Cash In Hand', '2017-10-12 00:50:33', 1),
(3, 2, 4, 700, 0, 'shop bill Payment', '2017-10-12 01:38:37', 'Samsung Show Room', 'Cash In Hand', '2017-10-12 01:38:37', 1);

-- --------------------------------------------------------

--
-- Table structure for table `asset_info`
--

CREATE TABLE `asset_info` (
  `assetId` int(11) NOT NULL,
  `assetTitle` varchar(300) COLLATE utf8_bin NOT NULL,
  `assetTrackingId` varchar(200) COLLATE utf8_bin NOT NULL,
  `assetOriginalValue` double NOT NULL,
  `assetCurrentValue` double NOT NULL,
  `assetAccruingDate` datetime NOT NULL,
  `assetDepreciationRate` double NOT NULL,
  `assetNote` text COLLATE utf8_bin NOT NULL,
  `assetStatus` int(2) NOT NULL DEFAULT '1' COMMENT '1=Active, 0=InActive',
  `assetAddedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `bank_account_details`
--

CREATE TABLE `bank_account_details` (
  `bankDetailsId` int(11) NOT NULL,
  `bank_account_infoBankId` int(11) NOT NULL,
  `bankDetailsReferrenceId` int(11) NOT NULL,
  `bankDetailsEntryDate` datetime NOT NULL,
  `bankDetailsTransactionDate` datetime NOT NULL,
  `bankDetailsNote` text COLLATE utf8_bin NOT NULL,
  `bankDetailsDeposit` int(11) NOT NULL,
  `bankDetailsBalance` double NOT NULL,
  `bankDetailsPurpose` int(11) NOT NULL,
  `bankDetailsType` int(2) NOT NULL COMMENT '1=cashin, 2=cash out',
  `bankDetailsStatus` int(2) NOT NULL DEFAULT '1' COMMENT '1=Active, 2=InActive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `bank_account_info`
--

CREATE TABLE `bank_account_info` (
  `bankAccountId` int(11) NOT NULL,
  `bankAccountTitle` varchar(350) COLLATE utf8_bin NOT NULL,
  `bankName` varchar(400) COLLATE utf8_bin NOT NULL,
  `bankAccountBranchName` varchar(400) COLLATE utf8_bin NOT NULL,
  `bankAccountHolderName` varchar(300) COLLATE utf8_bin NOT NULL,
  `bankAccountNumber` varchar(200) COLLATE utf8_bin NOT NULL,
  `bankAccountRoutingNumber` varchar(200) COLLATE utf8_bin NOT NULL,
  `bankAccountLastBalance` double NOT NULL DEFAULT '0',
  `bankAccountNote` text COLLATE utf8_bin NOT NULL,
  `bankAccountAddedDate` datetime NOT NULL,
  `bankAccountStatus` int(2) NOT NULL DEFAULT '1' COMMENT '1=active, 0=Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `bill_info`
--

CREATE TABLE `bill_info` (
  `billId` int(11) NOT NULL,
  `monthly_bill_info_mothlyBillInfoId` int(11) NOT NULL,
  `customer_info_customerId` int(11) NOT NULL,
  `billingMonth` date NOT NULL,
  `customer_info_rentAmount` double NOT NULL,
  `customer_info_rentDue` double NOT NULL,
  `billStatus` int(2) NOT NULL DEFAULT '1' COMMENT '1=pending, 2=final',
  `referrenceId` int(11) NOT NULL COMMENT 'transection referrence',
  `billAddedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `bill_info`
--

INSERT INTO `bill_info` (`billId`, `monthly_bill_info_mothlyBillInfoId`, `customer_info_customerId`, `billingMonth`, `customer_info_rentAmount`, `customer_info_rentDue`, `billStatus`, `referrenceId`, `billAddedDate`) VALUES
(1, 1, 1, '2017-09-01', 15000, 0, 1, 0, '2017-09-17 17:07:54'),
(2, 2, 1, '2017-08-01', 15000, 15000, 1, 0, '2017-09-17 17:10:08'),
(3, 3, 1, '2017-10-01', 15000, 30000, 1, 0, '2017-09-24 14:00:23'),
(4, 4, 1, '2017-11-01', 15000, 45000, 1, 0, '2017-09-24 14:02:05'),
(5, 5, 2, '2017-09-24', 25000, 0, 1, 0, '2017-09-24 14:21:05'),
(6, 6, 1, '2017-07-07', 15000, 60000, 1, 0, '2017-10-09 12:39:46'),
(9, 9, 1, '2017-12-08', 15000, 75000, 1, 0, '2017-10-09 16:59:20'),
(10, 10, 1, '2018-01-01', 15000, 90000, 1, 0, '2017-10-09 17:11:35'),
(11, 11, 1, '2018-02-07', 15000, 105000, 1, 0, '2017-10-10 22:32:34'),
(12, 12, 2, '2017-10-04', 25000, 25000, 1, 0, '2017-10-12 01:07:42'),
(13, 13, 1, '2018-03-02', 15000, 120000, 1, 0, '2017-10-13 02:22:17');

-- --------------------------------------------------------

--
-- Table structure for table `cash_in_hand`
--

CREATE TABLE `cash_in_hand` (
  `cashInHendId` int(11) NOT NULL,
  `cashDate` datetime NOT NULL,
  `cashEntryDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `cashTo` varchar(500) COLLATE utf8_bin NOT NULL,
  `cashInType` int(3) NOT NULL COMMENT '1=cash in, 2=cash out',
  `cashAmount` double NOT NULL,
  `cashInStatus` int(2) NOT NULL DEFAULT '1',
  `cashReferenceId` int(11) NOT NULL,
  `cashPurpose` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `cash_in_hand`
--

INSERT INTO `cash_in_hand` (`cashInHendId`, `cashDate`, `cashEntryDate`, `cashTo`, `cashInType`, `cashAmount`, `cashInStatus`, `cashReferenceId`, `cashPurpose`) VALUES
(1, '2017-09-25 00:00:00', '2017-09-25 08:35:47', 'Petty Cash', 2, 500, 1, 1, 5),
(3, '2017-10-12 00:50:33', '2017-10-11 18:50:33', 'Starlab IT', 1, 400, 1, 3, 2),
(4, '2017-10-12 01:38:37', '2017-10-11 19:38:37', 'Samsung Show Room', 1, 700, 1, 4, 2);

-- --------------------------------------------------------

--
-- Table structure for table `cash_transfer`
--

CREATE TABLE `cash_transfer` (
  `cashTransferId` int(11) NOT NULL,
  `cashWithdrawnFrom` int(11) NOT NULL,
  `cashTransferredTo` int(11) NOT NULL,
  `cashTransferAmount` double NOT NULL,
  `cashSlipNo` varchar(150) COLLATE utf8_bin NOT NULL,
  `cashTransferDate` datetime NOT NULL,
  `cashTransferNote` text COLLATE utf8_bin NOT NULL,
  `cashTransferStatus` int(2) NOT NULL,
  `cashTransferEntryDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `cash_transfer`
--

INSERT INTO `cash_transfer` (`cashTransferId`, `cashWithdrawnFrom`, `cashTransferredTo`, `cashTransferAmount`, `cashSlipNo`, `cashTransferDate`, `cashTransferNote`, `cashTransferStatus`, `cashTransferEntryDate`) VALUES
(1, -1, 0, 500, '123456', '2017-09-25 00:00:00', 'note', 0, '2017-09-25 14:35:47');

-- --------------------------------------------------------

--
-- Table structure for table `current_bill_info`
--

CREATE TABLE `current_bill_info` (
  `currentBillId` int(11) NOT NULL,
  `monthly_bill_info_mothlyBillInfoId` int(11) NOT NULL,
  `bill_info_billId` int(11) NOT NULL,
  `meter_info_meterId` int(11) NOT NULL,
  `custome_info_current_customerId` int(11) NOT NULL,
  `currentBillPrevReading` double NOT NULL,
  `currentBillPrevReadingDate` datetime NOT NULL,
  `currentBillCurrentReading` double NOT NULL,
  `currentBillCurrentReadingDate` datetime NOT NULL,
  `perUnitCharge` double NOT NULL,
  `currentBillDemandCharge` double NOT NULL,
  `currentBillServiceCharge` double NOT NULL,
  `totalEnergyCharge` double NOT NULL,
  `totalCurrentBill` double NOT NULL,
  `meter_info_billDue` double NOT NULL,
  `currentBillAddedDate` datetime NOT NULL,
  `referrenceId` int(11) NOT NULL COMMENT 'transection referrence',
  `currentBillLastDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `current_bill_info`
--

INSERT INTO `current_bill_info` (`currentBillId`, `monthly_bill_info_mothlyBillInfoId`, `bill_info_billId`, `meter_info_meterId`, `custome_info_current_customerId`, `currentBillPrevReading`, `currentBillPrevReadingDate`, `currentBillCurrentReading`, `currentBillCurrentReadingDate`, `perUnitCharge`, `currentBillDemandCharge`, `currentBillServiceCharge`, `totalEnergyCharge`, `totalCurrentBill`, `meter_info_billDue`, `currentBillAddedDate`, `referrenceId`, `currentBillLastDate`) VALUES
(1, 2, 2, 1, 1, 845, '2017-09-17 17:09:18', 0, '0000-00-00 00:00:00', 5.8, 50, 50, 366.8, -4801, 0, '2017-09-17 17:10:08', 0, '0000-00-00 00:00:00'),
(2, 3, 3, 1, 1, 845, '2017-09-17 17:09:18', 945, '2017-09-24 14:01:04', 5, 50, 50, 600, 750, 0, '2017-09-24 14:00:23', 0, '0000-00-00 00:00:00'),
(3, 4, 4, 1, 1, 945, '2017-09-24 14:01:04', 1045, '2017-09-24 14:02:24', 10, 50, 50, 1100, 1100, 0, '2017-09-24 14:02:05', 0, '0000-00-00 00:00:00'),
(4, 5, 5, 2, 2, 500, '2017-09-24 14:19:00', 700, '2017-09-24 14:21:38', 10, 0, 0, 2000, 2500, 0, '2017-09-24 14:21:05', 0, '0000-00-00 00:00:00'),
(5, 5, 5, 3, 2, 200, '2017-09-24 14:19:56', 220, '2017-09-24 14:21:40', 10, 50, 0, 510, 280, 100, '2017-09-24 14:21:05', 0, '0000-00-00 00:00:00'),
(6, 6, 6, 1, 1, 1045, '2017-09-24 14:02:24', 0, '0000-00-00 00:00:00', 5.6, 50, 50, 357.6, 0, 0, '2017-10-09 12:39:46', 0, '0000-00-00 00:00:00'),
(9, 9, 9, 1, 1, 1045, '2017-09-24 14:02:24', 1200.56, '2017-10-09 17:00:28', 5.61, 50, 50, 972.6916, 1572.6916, 0, '2017-10-09 16:59:20', 0, '2018-01-12 00:00:00'),
(10, 10, 10, 1, 1, 1200.56, '2017-10-09 17:00:28', 1350.65, '2018-02-02 00:00:00', 5.63, 50, 50, 945.0067, 1695.0067, 0, '2017-10-09 17:11:35', 0, '2018-02-14 00:00:00'),
(11, 11, 11, 1, 1, 1350.65, '2018-02-02 00:00:00', 1480, '2018-02-02 00:00:00', 5.33, 50, 50, 792.9, 1545.4355, 0, '2017-10-10 22:32:34', 0, '2018-02-14 00:00:00'),
(12, 12, 12, 2, 2, 700, '2017-09-24 14:21:38', 961, '2017-11-02 00:00:00', 5.69, 0, 0, 1485, 1985.09, 0, '2017-10-12 01:07:42', 0, '2017-11-07 00:00:00'),
(13, 12, 12, 3, 2, 220, '2017-09-24 14:21:40', 326, '2017-11-02 00:00:00', 5.69, 50, 0, 653.14, 982.14, 100, '2017-10-12 01:07:42', 0, '2017-11-07 00:00:00'),
(14, 13, 13, 1, 1, 1480, '2018-02-02 00:00:00', 1630, '2018-04-01 00:00:00', 5.39, 50, 50, 908.5, 1538.5, 589.4355, '2017-10-13 02:22:17', 0, '2018-04-07 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `customer_balance_adjustment`
--

CREATE TABLE `customer_balance_adjustment` (
  `customerAdjustmentId` int(11) NOT NULL,
  `customer_info_customerId` int(11) NOT NULL,
  `customerAdjustmentType` int(2) NOT NULL COMMENT '1=add,2=remove',
  `customerAdjustmentDate` datetime NOT NULL,
  `customerAdjustmentbeforeAmount` double NOT NULL,
  `customerAdjustmentAfterAmount` double NOT NULL,
  `customerAdjustmentAmount` double NOT NULL,
  `customerAdjustmentNote` text COLLATE utf8_bin NOT NULL,
  `customerAdjustmentBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `customer_info`
--

CREATE TABLE `customer_info` (
  `customerId` int(11) NOT NULL,
  `customerName` varchar(200) COLLATE utf8_bin NOT NULL,
  `customerPhone` varchar(15) COLLATE utf8_bin NOT NULL,
  `organizationName` varchar(250) COLLATE utf8_bin NOT NULL,
  `spaceType` int(11) NOT NULL,
  `spaceTitle` varchar(50) COLLATE utf8_bin NOT NULL,
  `leasingMethodId` int(11) NOT NULL,
  `rentAmount` double NOT NULL,
  `rentDue` double NOT NULL DEFAULT '0',
  `billDue` double NOT NULL DEFAULT '0',
  `energyChargeDueAmount` double NOT NULL DEFAULT '0',
  `customerStatus` int(1) NOT NULL DEFAULT '1',
  `customerAddedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `customer_info`
--

INSERT INTO `customer_info` (`customerId`, `customerName`, `customerPhone`, `organizationName`, `spaceType`, `spaceTitle`, `leasingMethodId`, `rentAmount`, `rentDue`, `billDue`, `energyChargeDueAmount`, `customerStatus`, `customerAddedDate`) VALUES
(1, 'Kanthi Lal', '01254632896', 'Starlab IT', 2, 'C985', 1, 15000, 135000, 3000.6338, 0, 1, '2017-08-23 14:19:16'),
(2, 'Samsung', '01718946872', 'Samsung Show Room', 1, '5A', 1, 25000, 50000, 5147.2300000000005, 0, 1, '2017-09-24 14:17:40');

-- --------------------------------------------------------

--
-- Table structure for table `customer_services`
--

CREATE TABLE `customer_services` (
  `customerServiceId` int(11) NOT NULL,
  `customer_info_customerId` int(11) NOT NULL,
  `services_serviceId` int(11) NOT NULL,
  `meter_info_meterId` int(11) NOT NULL,
  `serviceTotalAmount` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `customer_services`
--

INSERT INTO `customer_services` (`customerServiceId`, `customer_info_customerId`, `services_serviceId`, `meter_info_meterId`, `serviceTotalAmount`) VALUES
(1, 1, 1, 1, 613),
(2, 1, 2, 1, 573),
(3, 2, 3, 2, 200),
(4, 2, 1, 3, 329);

-- --------------------------------------------------------

--
-- Table structure for table `leasing_method`
--

CREATE TABLE `leasing_method` (
  `leasingId` int(11) NOT NULL,
  `leasingName` varchar(50) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `leasing_method`
--

INSERT INTO `leasing_method` (`leasingId`, `leasingName`) VALUES
(1, 'Rent'),
(2, 'Jomidari'),
(3, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `ledger_details_info`
--

CREATE TABLE `ledger_details_info` (
  `ledgerDetailsId` int(11) NOT NULL,
  `ledgerDetailsType` int(3) NOT NULL COMMENT '1=income, 2=expense',
  `ledger_info_ledgerId` int(11) NOT NULL,
  `ledgerDetails` text COLLATE utf8_bin NOT NULL,
  `ledgerDetailsAmount` double NOT NULL,
  `ledgerDepositeTo` int(11) NOT NULL,
  `ledgerWithdrawnFrom` int(11) NOT NULL,
  `ledgerDetailsNote` text COLLATE utf8_bin NOT NULL,
  `ledgerDetailsStatus` int(2) NOT NULL DEFAULT '1' COMMENT '1=Active, 0=InActive',
  `ledgerDetailsAddedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `ledger_info`
--

CREATE TABLE `ledger_info` (
  `ledgerId` int(11) NOT NULL,
  `ledgerTitle` varchar(500) COLLATE utf8_bin NOT NULL,
  `particular_info_particularid` int(11) NOT NULL,
  `ledgerType` int(3) NOT NULL DEFAULT '1' COMMENT '1=income,2=expense',
  `ledgerBalance` double NOT NULL,
  `ledgerNote` text COLLATE utf8_bin NOT NULL,
  `ledgerStatus` int(2) NOT NULL DEFAULT '1' COMMENT '1=Active, 0=InActive',
  `ledgerAddedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `meter_info`
--

CREATE TABLE `meter_info` (
  `meterId` int(11) NOT NULL,
  `customer_info_customerId` int(11) NOT NULL,
  `meterCustomerId` varchar(100) COLLATE utf8_bin NOT NULL COMMENT 'admin will give a id/ name',
  `previousReading` double NOT NULL,
  `previousReadingDate` datetime NOT NULL,
  `currentReading` double NOT NULL,
  `meterNo` varchar(60) COLLATE utf8_bin NOT NULL,
  `demandCharge` double NOT NULL,
  `meterServiceCharge` double NOT NULL,
  `billDue` double NOT NULL DEFAULT '0',
  `meterStatus` int(11) NOT NULL DEFAULT '1' COMMENT '1=Active, 0=InActive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `meter_info`
--

INSERT INTO `meter_info` (`meterId`, `customer_info_customerId`, `meterCustomerId`, `previousReading`, `previousReadingDate`, `currentReading`, `meterNo`, `demandCharge`, `meterServiceCharge`, `billDue`, `meterStatus`) VALUES
(1, 1, '154', 1630, '2018-04-01 00:00:00', 0, '512', 50, 50, 1497.9355, 1),
(2, 2, 'ASVB', 961, '2017-11-02 00:00:00', 0, '00', 0, 0, 1085.09, 1),
(3, 2, 'ASDC', 326, '2017-11-02 00:00:00', 0, '000', 50, 0, 753.14, 1);

-- --------------------------------------------------------

--
-- Table structure for table `monthly_bill_info`
--

CREATE TABLE `monthly_bill_info` (
  `mothlyBillInfoId` int(11) NOT NULL,
  `monthlyBillingStatus` int(2) NOT NULL COMMENT '1=pending, 2=final',
  `monthlyBillTypeFor` int(2) NOT NULL COMMENT '1=shop,2=office,3=others, 4=Rent Payment, 5=Bill Payment',
  `billingMonthYear` date NOT NULL,
  `monthlyBillAddedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `monthly_bill_info`
--

INSERT INTO `monthly_bill_info` (`mothlyBillInfoId`, `monthlyBillingStatus`, `monthlyBillTypeFor`, `billingMonthYear`, `monthlyBillAddedDate`) VALUES
(1, 2, 2, '2017-09-01', '2017-09-17 17:07:54'),
(2, 2, 2, '2017-08-01', '2017-09-17 17:10:08'),
(3, 2, 2, '2017-10-01', '2017-09-24 14:00:23'),
(4, 2, 2, '2017-11-01', '2017-09-24 14:02:05'),
(5, 2, 1, '2017-09-24', '2017-09-24 14:21:05'),
(6, 1, 2, '2017-07-07', '2017-10-09 12:39:46'),
(9, 2, 2, '2017-12-08', '2017-10-09 16:59:20'),
(10, 2, 2, '2018-01-01', '2017-10-09 17:11:35'),
(11, 2, 2, '2018-02-07', '2017-10-10 22:32:34'),
(12, 2, 1, '2017-10-04', '2017-10-12 01:07:42'),
(13, 2, 2, '2018-03-02', '2017-10-13 02:22:17');

-- --------------------------------------------------------

--
-- Table structure for table `particular_info`
--

CREATE TABLE `particular_info` (
  `particularId` int(11) NOT NULL,
  `particularTitle` varchar(350) COLLATE utf8_bin NOT NULL,
  `particularType` int(3) NOT NULL DEFAULT '1' COMMENT '1=income,2=expense',
  `particularStatus` int(2) NOT NULL DEFAULT '1' COMMENT '1=Active, 0=InActive',
  `particularNote` text COLLATE utf8_bin NOT NULL,
  `particularAddedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `payment_history`
--

CREATE TABLE `payment_history` (
  `paymentId` int(11) NOT NULL,
  `customer_info_customerId` int(11) NOT NULL,
  `paymentAmount` double NOT NULL,
  `payment_types_paymentTypeId` int(11) NOT NULL COMMENT '1=Bill, 2=Rent',
  `paymentDetails` text COLLATE utf8_bin NOT NULL,
  `paymentDate` datetime NOT NULL,
  `referrenceId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `payment_history`
--

INSERT INTO `payment_history` (`paymentId`, `customer_info_customerId`, `paymentAmount`, `payment_types_paymentTypeId`, `paymentDetails`, `paymentDate`, `referrenceId`) VALUES
(3, 1, 400, 1, 'bill payment', '2017-10-12 00:50:33', 0),
(4, 2, 700, 1, 'shop bill Payment', '2017-10-12 01:38:37', 0);

-- --------------------------------------------------------

--
-- Table structure for table `payment_types`
--

CREATE TABLE `payment_types` (
  `paymentTypeId` int(11) NOT NULL,
  `paymentTypeName` varchar(60) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `payment_types`
--

INSERT INTO `payment_types` (`paymentTypeId`, `paymentTypeName`) VALUES
(1, 'Bill Payment'),
(2, 'Rent Payment');

-- --------------------------------------------------------

--
-- Table structure for table `petty_cash`
--

CREATE TABLE `petty_cash` (
  `pettyCashId` int(11) NOT NULL,
  `pettyCashDate` datetime NOT NULL,
  `pettyCashEntryDate` datetime NOT NULL,
  `pettyCashto` varchar(500) COLLATE utf8_bin NOT NULL,
  `pettyCashType` int(3) NOT NULL,
  `pettyCashAmount` double NOT NULL,
  `pettyCashStatus` int(2) NOT NULL DEFAULT '1' COMMENT '1=Active, 0=InActive',
  `pettyCashReferenceId` int(11) NOT NULL,
  `pettyCashPurpose` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `petty_cash`
--

INSERT INTO `petty_cash` (`pettyCashId`, `pettyCashDate`, `pettyCashEntryDate`, `pettyCashto`, `pettyCashType`, `pettyCashAmount`, `pettyCashStatus`, `pettyCashReferenceId`, `pettyCashPurpose`) VALUES
(1, '2017-09-25 00:00:00', '2017-09-25 14:35:47', 'Cash In Hand', 1, 500, 1, 1, 5);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `serviceId` int(11) NOT NULL,
  `serviceName` varchar(150) COLLATE utf8_bin NOT NULL,
  `serviceNote` varchar(300) COLLATE utf8_bin NOT NULL,
  `serviceType` int(2) NOT NULL COMMENT '1=common, 2=personal',
  `serviceAmount` double NOT NULL DEFAULT '0',
  `serviceStatus` int(11) NOT NULL DEFAULT '1' COMMENT '1=Active, 0=InActive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`serviceId`, `serviceName`, `serviceNote`, `serviceType`, `serviceAmount`, `serviceStatus`) VALUES
(0, 'Energy Charge', 'no note', 1, 0, 0),
(1, 'Common Bill', 'Employee cost for Common Bill', 1, 0, 1),
(2, 'Government Charges', 'Government Charges', 1, 0, 1),
(3, 'Car parking', '', 2, 500, 1);

-- --------------------------------------------------------

--
-- Table structure for table `services_bill_info`
--

CREATE TABLE `services_bill_info` (
  `serviceBillId` int(11) NOT NULL,
  `services_serviceId` int(11) NOT NULL,
  `bill_info_billId` int(11) NOT NULL,
  `current_bill_info_currentBillId` int(11) NOT NULL,
  `monthly_bill_info_mothlyBillInfoId` int(11) NOT NULL,
  `serviceAmount` double NOT NULL,
  `serviceBillingMonth` date NOT NULL,
  `serviceBillAddedDate` datetime NOT NULL,
  `customer_info_service_customerId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `services_bill_info`
--

INSERT INTO `services_bill_info` (`serviceBillId`, `services_serviceId`, `bill_info_billId`, `current_bill_info_currentBillId`, `monthly_bill_info_mothlyBillInfoId`, `serviceAmount`, `serviceBillingMonth`, `serviceBillAddedDate`, `customer_info_service_customerId`) VALUES
(1, 1, 2, 1, 2, 0, '2017-08-01', '2017-09-17 17:10:08', 0),
(2, 2, 2, 1, 2, 0, '2017-08-01', '2017-09-17 17:10:08', 0),
(3, 1, 3, 2, 3, 50, '2017-10-01', '2017-09-24 14:00:23', 0),
(4, 2, 3, 2, 3, 100, '2017-10-01', '2017-09-24 14:00:23', 0),
(5, 1, 4, 3, 4, 0, '2017-11-01', '2017-09-24 14:02:05', 0),
(6, 2, 4, 3, 4, 0, '2017-11-01', '2017-09-24 14:02:05', 0),
(7, 3, 5, 4, 5, 500, '2017-09-24', '2017-09-24 14:21:05', 2),
(8, 1, 5, 5, 5, 30, '2017-09-24', '2017-09-24 14:21:05', 2),
(9, 1, 6, 6, 6, 0, '2017-07-07', '2017-10-09 12:39:46', 1),
(10, 2, 6, 6, 6, 0, '2017-07-07', '2017-10-09 12:39:46', 1),
(15, 1, 9, 9, 9, 500, '2017-12-08', '2017-10-09 16:59:20', 1),
(16, 2, 9, 9, 9, 100, '2017-12-08', '2017-10-09 16:59:20', 1),
(17, 1, 10, 10, 10, 500, '2018-01-01', '2017-10-09 17:11:35', 1),
(18, 2, 10, 10, 10, 250, '2018-01-01', '2017-10-09 17:11:35', 1),
(19, 1, 11, 11, 11, 423, '2018-02-07', '2017-10-10 22:32:34', 1),
(20, 2, 11, 11, 11, 333, '2018-02-07', '2017-10-10 22:32:34', 1),
(21, 3, 12, 12, 12, 500, '2017-10-04', '2017-10-12 01:07:42', 2),
(22, 1, 12, 13, 12, 329, '2017-10-04', '2017-10-12 01:07:42', 2),
(23, 1, 13, 14, 13, 290, '2018-03-02', '2017-10-13 02:22:17', 1),
(24, 2, 13, 14, 13, 340, '2018-03-02', '2017-10-13 02:22:17', 1);

-- --------------------------------------------------------

--
-- Table structure for table `service_charge_payment_info`
--

CREATE TABLE `service_charge_payment_info` (
  `serviceChargePaymentInfo` int(11) NOT NULL,
  `serviceChargePaymentAmount` double NOT NULL,
  `serviceChargePaidAmount` double NOT NULL,
  `payment_history_paymentID` int(11) NOT NULL,
  `service_info_serviceId` int(11) NOT NULL,
  `meter_info_meterId` int(11) NOT NULL,
  `serviceChargePaymentDate` datetime NOT NULL,
  `client_info_clientId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `service_charge_payment_info`
--

INSERT INTO `service_charge_payment_info` (`serviceChargePaymentInfo`, `serviceChargePaymentAmount`, `serviceChargePaidAmount`, `payment_history_paymentID`, `service_info_serviceId`, `meter_info_meterId`, `serviceChargePaymentDate`, `client_info_clientId`) VALUES
(4, 0, 200, 3, 0, 1, '2017-10-12 00:50:33', 1),
(5, 0, 100, 3, 1, 1, '2017-10-12 00:50:33', 1),
(6, 0, 100, 3, 2, 1, '2017-10-12 00:50:33', 1),
(7, 1485.09, 400, 4, 0, 2, '2017-10-12 01:38:37', 2),
(8, 500, 300, 4, 3, 2, '2017-10-12 01:38:37', 2);

-- --------------------------------------------------------

--
-- Table structure for table `space_type`
--

CREATE TABLE `space_type` (
  `spaceTypeId` int(11) NOT NULL,
  `spaceTypeName` varchar(50) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `space_type`
--

INSERT INTO `space_type` (`spaceTypeId`, `spaceTypeName`) VALUES
(1, 'Shop'),
(2, 'Office'),
(3, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `transaction_type_info`
--

CREATE TABLE `transaction_type_info` (
  `transactionId` int(11) NOT NULL,
  `transactionName` varchar(50) COLLATE utf8_bin NOT NULL,
  `transactionDetailslink` varchar(500) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `transaction_type_info`
--

INSERT INTO `transaction_type_info` (`transactionId`, `transactionName`, `transactionDetailslink`) VALUES
(1, 'Rent Payment', 'viewReceivedPayment'),
(2, 'Bill Payment', 'viewReceivedPayment'),
(3, 'Income', 'viewLedgerEntry'),
(4, 'Expense', 'viewLedgerEntry'),
(5, 'Cash Transfer', 'viewTransaction');

-- --------------------------------------------------------

--
-- Table structure for table `transection_info`
--

CREATE TABLE `transection_info` (
  `transectionId` int(11) NOT NULL,
  `TransectionType` int(2) NOT NULL COMMENT '1=rent bill, 2=current bill , 3=rent payment, 4=bill Payment',
  `customer_info_customerId` int(11) NOT NULL,
  `transectionAmount` double NOT NULL,
  `transectionPrevDue` double NOT NULL,
  `transectionDetails` text COLLATE utf8_bin NOT NULL,
  `transectionDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `transection_info`
--

INSERT INTO `transection_info` (`transectionId`, `TransectionType`, `customer_info_customerId`, `transectionAmount`, `transectionPrevDue`, `transectionDetails`, `transectionDate`) VALUES
(0, 1, 1, 15000, 0, 'Monthly Rent', '2017-09-17 17:07:54'),
(0, 1, 1, 15000, 15000, 'Monthly Rent', '2017-09-17 17:10:08'),
(0, 2, 1, -4801, 0, 'Monthly Bill', '2017-09-18 14:16:57'),
(0, 1, 1, 15000, 30000, 'Monthly Rent', '2017-09-24 14:00:23'),
(0, 2, 1, 750, -4801, 'Monthly Bill', '2017-09-24 14:01:08'),
(0, 1, 1, 15000, 45000, 'Monthly Rent', '2017-09-24 14:02:05'),
(0, 2, 1, 1100, -4051, 'Monthly Bill', '2017-09-24 14:02:26'),
(0, 1, 2, 25000, 0, 'Monthly Rent', '2017-09-24 14:21:05'),
(0, 2, 2, 2500, 100, 'Monthly Bill', '2017-09-24 14:21:44'),
(0, 2, 2, 280, 2600, 'Monthly Bill', '2017-09-24 14:21:44'),
(0, 1, 1, 15000, 60000, 'Monthly Rent', '2017-10-09 12:39:46'),
(0, 1, 1, 15000, 75000, 'Monthly Rent', '2017-10-09 16:59:20'),
(0, 2, 1, 1572.6916, -2951, 'Monthly Bill', '2017-10-09 17:00:32'),
(0, 1, 1, 15000, 90000, 'Monthly Rent', '2017-10-09 17:11:35'),
(0, 2, 1, 1695.0067, -1378.3084, 'Monthly Bill', '2017-10-09 17:12:17'),
(0, 1, 1, 15000, 105000, 'Monthly Rent', '2017-10-10 22:32:34'),
(0, 2, 1, 1545.4355, 316.6983, 'Monthly Bill', '2017-10-11 00:59:16'),
(0, 4, 1, 400, 1862.1338, 'bill payment', '2017-10-12 00:50:33'),
(0, 1, 2, 25000, 25000, 'Monthly Rent', '2017-10-12 01:07:42'),
(0, 2, 2, 1985.09, 2880, 'Monthly Bill', '2017-10-12 01:08:57'),
(0, 2, 2, 982.14, 4865.09, 'Monthly Bill', '2017-10-12 01:08:57'),
(0, 4, 2, 700, 5847.2300000000005, 'shop bill Payment', '2017-10-12 01:38:37'),
(0, 1, 1, 15000, 120000, 'Monthly Rent', '2017-10-13 02:22:17'),
(0, 2, 1, 1538.5, 1462.1338, 'Monthly Bill', '2017-10-13 02:23:06');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account_adjustment`
--
ALTER TABLE `account_adjustment`
  ADD PRIMARY KEY (`accountAdjustmentId`);

--
-- Indexes for table `admin_info`
--
ALTER TABLE `admin_info`
  ADD PRIMARY KEY (`adminID`),
  ADD UNIQUE KEY `adminUserID` (`adminUserID`);

--
-- Indexes for table `admin_role`
--
ALTER TABLE `admin_role`
  ADD PRIMARY KEY (`roleID`);

--
-- Indexes for table `all_transaction_info`
--
ALTER TABLE `all_transaction_info`
  ADD PRIMARY KEY (`transactionId`);

--
-- Indexes for table `asset_info`
--
ALTER TABLE `asset_info`
  ADD PRIMARY KEY (`assetId`);

--
-- Indexes for table `bank_account_details`
--
ALTER TABLE `bank_account_details`
  ADD PRIMARY KEY (`bankDetailsId`);

--
-- Indexes for table `bank_account_info`
--
ALTER TABLE `bank_account_info`
  ADD PRIMARY KEY (`bankAccountId`);

--
-- Indexes for table `bill_info`
--
ALTER TABLE `bill_info`
  ADD PRIMARY KEY (`billId`);

--
-- Indexes for table `cash_in_hand`
--
ALTER TABLE `cash_in_hand`
  ADD PRIMARY KEY (`cashInHendId`);

--
-- Indexes for table `cash_transfer`
--
ALTER TABLE `cash_transfer`
  ADD PRIMARY KEY (`cashTransferId`);

--
-- Indexes for table `current_bill_info`
--
ALTER TABLE `current_bill_info`
  ADD PRIMARY KEY (`currentBillId`);

--
-- Indexes for table `customer_balance_adjustment`
--
ALTER TABLE `customer_balance_adjustment`
  ADD PRIMARY KEY (`customerAdjustmentId`);

--
-- Indexes for table `customer_info`
--
ALTER TABLE `customer_info`
  ADD PRIMARY KEY (`customerId`);

--
-- Indexes for table `customer_services`
--
ALTER TABLE `customer_services`
  ADD PRIMARY KEY (`customerServiceId`);

--
-- Indexes for table `leasing_method`
--
ALTER TABLE `leasing_method`
  ADD PRIMARY KEY (`leasingId`);

--
-- Indexes for table `ledger_details_info`
--
ALTER TABLE `ledger_details_info`
  ADD PRIMARY KEY (`ledgerDetailsId`);

--
-- Indexes for table `ledger_info`
--
ALTER TABLE `ledger_info`
  ADD PRIMARY KEY (`ledgerId`);

--
-- Indexes for table `meter_info`
--
ALTER TABLE `meter_info`
  ADD PRIMARY KEY (`meterId`);

--
-- Indexes for table `monthly_bill_info`
--
ALTER TABLE `monthly_bill_info`
  ADD PRIMARY KEY (`mothlyBillInfoId`);

--
-- Indexes for table `particular_info`
--
ALTER TABLE `particular_info`
  ADD PRIMARY KEY (`particularId`);

--
-- Indexes for table `payment_history`
--
ALTER TABLE `payment_history`
  ADD PRIMARY KEY (`paymentId`);

--
-- Indexes for table `payment_types`
--
ALTER TABLE `payment_types`
  ADD PRIMARY KEY (`paymentTypeId`);

--
-- Indexes for table `petty_cash`
--
ALTER TABLE `petty_cash`
  ADD PRIMARY KEY (`pettyCashId`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`serviceId`);

--
-- Indexes for table `services_bill_info`
--
ALTER TABLE `services_bill_info`
  ADD PRIMARY KEY (`serviceBillId`);

--
-- Indexes for table `service_charge_payment_info`
--
ALTER TABLE `service_charge_payment_info`
  ADD PRIMARY KEY (`serviceChargePaymentInfo`);

--
-- Indexes for table `space_type`
--
ALTER TABLE `space_type`
  ADD PRIMARY KEY (`spaceTypeId`);

--
-- Indexes for table `transaction_type_info`
--
ALTER TABLE `transaction_type_info`
  ADD PRIMARY KEY (`transactionId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account_adjustment`
--
ALTER TABLE `account_adjustment`
  MODIFY `accountAdjustmentId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `admin_info`
--
ALTER TABLE `admin_info`
  MODIFY `adminID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `admin_role`
--
ALTER TABLE `admin_role`
  MODIFY `roleID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `all_transaction_info`
--
ALTER TABLE `all_transaction_info`
  MODIFY `transactionId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `asset_info`
--
ALTER TABLE `asset_info`
  MODIFY `assetId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bank_account_details`
--
ALTER TABLE `bank_account_details`
  MODIFY `bankDetailsId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bank_account_info`
--
ALTER TABLE `bank_account_info`
  MODIFY `bankAccountId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bill_info`
--
ALTER TABLE `bill_info`
  MODIFY `billId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `cash_in_hand`
--
ALTER TABLE `cash_in_hand`
  MODIFY `cashInHendId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `cash_transfer`
--
ALTER TABLE `cash_transfer`
  MODIFY `cashTransferId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `current_bill_info`
--
ALTER TABLE `current_bill_info`
  MODIFY `currentBillId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `customer_balance_adjustment`
--
ALTER TABLE `customer_balance_adjustment`
  MODIFY `customerAdjustmentId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customer_info`
--
ALTER TABLE `customer_info`
  MODIFY `customerId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `customer_services`
--
ALTER TABLE `customer_services`
  MODIFY `customerServiceId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `leasing_method`
--
ALTER TABLE `leasing_method`
  MODIFY `leasingId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ledger_details_info`
--
ALTER TABLE `ledger_details_info`
  MODIFY `ledgerDetailsId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ledger_info`
--
ALTER TABLE `ledger_info`
  MODIFY `ledgerId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `meter_info`
--
ALTER TABLE `meter_info`
  MODIFY `meterId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `monthly_bill_info`
--
ALTER TABLE `monthly_bill_info`
  MODIFY `mothlyBillInfoId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `particular_info`
--
ALTER TABLE `particular_info`
  MODIFY `particularId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `payment_history`
--
ALTER TABLE `payment_history`
  MODIFY `paymentId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `payment_types`
--
ALTER TABLE `payment_types`
  MODIFY `paymentTypeId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `petty_cash`
--
ALTER TABLE `petty_cash`
  MODIFY `pettyCashId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `serviceId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `services_bill_info`
--
ALTER TABLE `services_bill_info`
  MODIFY `serviceBillId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `service_charge_payment_info`
--
ALTER TABLE `service_charge_payment_info`
  MODIFY `serviceChargePaymentInfo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `space_type`
--
ALTER TABLE `space_type`
  MODIFY `spaceTypeId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `transaction_type_info`
--
ALTER TABLE `transaction_type_info`
  MODIFY `transactionId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
