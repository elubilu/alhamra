-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 12, 2017 at 10:26 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `alhamra`
--

-- --------------------------------------------------------

--
-- Table structure for table `service_charge_payment_info`
--

CREATE TABLE `service_charge_payment_info` (
  `serviceChargePaymentInfo` int(11) NOT NULL,
  `serviceChargePaymentAmount` double NOT NULL,
  `serviceChargePaidAmount` double NOT NULL,
  `payment_history_paymentID` int(11) NOT NULL,
  `service_info_serviceId` int(11) NOT NULL,
  `meter_info_meterId` int(11) NOT NULL,
  `serviceChargePaymentDate` datetime NOT NULL,
  `client_info_clientId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `service_charge_payment_info`
--

--
-- Indexes for dumped tables
--

--
-- Indexes for table `service_charge_payment_info`
--
ALTER TABLE `service_charge_payment_info`
  ADD PRIMARY KEY (`serviceChargePaymentInfo`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `service_charge_payment_info`
--
ALTER TABLE `service_charge_payment_info`
  MODIFY `serviceChargePaymentInfo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
