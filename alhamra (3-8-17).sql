-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 03, 2017 at 12:57 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `alhamra`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_info`
--

CREATE TABLE `admin_info` (
  `adminID` int(11) NOT NULL,
  `adminName` varchar(40) COLLATE utf8_bin NOT NULL,
  `adminEmail` varchar(50) COLLATE utf8_bin NOT NULL,
  `adminContact` varchar(20) COLLATE utf8_bin NOT NULL,
  `adminAddress` text COLLATE utf8_bin NOT NULL,
  `adminNote` text COLLATE utf8_bin NOT NULL,
  `adminUserID` varchar(30) COLLATE utf8_bin NOT NULL,
  `adminPassword` varchar(400) COLLATE utf8_bin NOT NULL,
  `adminStatus` int(11) NOT NULL COMMENT '0=InActive, 1=Active',
  `admin_role_roleID` int(11) NOT NULL,
  `adminJoinDate` datetime NOT NULL,
  `adminUpdateDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `admin_info`
--

INSERT INTO `admin_info` (`adminID`, `adminName`, `adminEmail`, `adminContact`, `adminAddress`, `adminNote`, `adminUserID`, `adminPassword`, `adminStatus`, `admin_role_roleID`, `adminJoinDate`, `adminUpdateDate`) VALUES
(1, 'SatrlabIT', 'starlabTeam@gmail.com', '01719450855', 'Zindabazar,Sylhet', '', 'superadmin', 'd7ea52fb792bed01df7174c48605cf19', 1, 1, '2017-03-09 00:14:57', '0000-00-00 00:00:00'),
(2, 'Shamsia Sharmin', 'shamsia@gmail.com', '011759721012', 'Lovely Road, Sylhet', '', 'manager', 'd7ea52fb792bed01df7174c48605cf19', 1, 3, '2017-03-24 22:05:49', '0000-00-00 00:00:00'),
(3, 'Keshob Chakrabory', 'keshob.kc@gmail.com', '01719450855', 'Address', '', 'admin', 'd7ea52fb792bed01df7174c48605cf19', 1, 2, '2017-02-10 21:39:54', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `admin_role`
--

CREATE TABLE `admin_role` (
  `roleID` int(11) NOT NULL,
  `roleName` varchar(100) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `admin_role`
--

INSERT INTO `admin_role` (`roleID`, `roleName`) VALUES
(1, 'Supper Admin'),
(2, 'Admin'),
(3, 'Manager'),
(4, 'Accountent'),
(5, 'Cashier'),
(6, 'Spectator');

-- --------------------------------------------------------

--
-- Table structure for table `all_transaction_info`
--

CREATE TABLE `all_transaction_info` (
  `transactionId` int(11) NOT NULL,
  `transactionType` int(3) NOT NULL COMMENT '1=rent bill, 2=current bill , 3=rent payment, 4=bill Payment',
  `transactionReferenceId` int(11) NOT NULL,
  `transactionAmount` double NOT NULL,
  `transactionPrevDue` double NOT NULL,
  `transactionDetails` text COLLATE utf8_bin NOT NULL,
  `transactionDate` datetime NOT NULL,
  `transactionFrom` varchar(500) COLLATE utf8_bin NOT NULL,
  `transactionTo` varchar(500) COLLATE utf8_bin NOT NULL,
  `transactionEntryDate` int(11) NOT NULL,
  `transactionStatus` int(2) NOT NULL DEFAULT '1' COMMENT '1=Active, 0=InActive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `bank_account_info`
--

CREATE TABLE `bank_account_info` (
  `bankAccountId` int(11) NOT NULL,
  `bankAccountTitle` varchar(350) COLLATE utf8_bin NOT NULL,
  `bankName` varchar(400) COLLATE utf8_bin NOT NULL,
  `bankAccountBranchName` varchar(400) COLLATE utf8_bin NOT NULL,
  `bankAccountHolderName` varchar(300) COLLATE utf8_bin NOT NULL,
  `bankAccountNumber` varchar(200) COLLATE utf8_bin NOT NULL,
  `bankAccountRoutingNumber` varchar(200) COLLATE utf8_bin NOT NULL,
  `bankAccountLastBalance` double NOT NULL,
  `bankAccountNote` text COLLATE utf8_bin NOT NULL,
  `bankAccountAddedDate` datetime NOT NULL,
  `bankAccountStatus` int(2) NOT NULL DEFAULT '1' COMMENT '1=active, 0=Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `bank_account_info`
--

INSERT INTO `bank_account_info` (`bankAccountId`, `bankAccountTitle`, `bankName`, `bankAccountBranchName`, `bankAccountHolderName`, `bankAccountNumber`, `bankAccountRoutingNumber`, `bankAccountLastBalance`, `bankAccountNote`, `bankAccountAddedDate`, `bankAccountStatus`) VALUES
(1, 'Bank Account', 'Bangladesh Bank', 'Sylhet, Bangladesh', 'Mr. Account Holder', 'acc-234653', '2408956', 2500, 'First Bank Account ', '2017-08-01 17:49:22', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bill_info`
--

CREATE TABLE `bill_info` (
  `billId` int(11) NOT NULL,
  `monthly_bill_info_mothlyBillInfoId` int(11) NOT NULL,
  `customer_info_customerId` int(11) NOT NULL,
  `billingMonth` date NOT NULL,
  `customer_info_rentAmount` double NOT NULL,
  `customer_info_rentDue` double NOT NULL,
  `billStatus` int(2) NOT NULL DEFAULT '1' COMMENT '1=pending, 2=final',
  `referrenceId` int(11) NOT NULL COMMENT 'transection referrence',
  `billAddedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `cash_in_hand`
--

CREATE TABLE `cash_in_hand` (
  `cashInHendId` int(11) NOT NULL,
  `cashDate` datetime NOT NULL,
  `cashEntryDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `cashTo` varchar(500) COLLATE utf8_bin NOT NULL,
  `cashInType` int(3) NOT NULL COMMENT '1=cash in, 2=cash out',
  `cashAmount` double NOT NULL,
  `cashInStatus` int(2) NOT NULL DEFAULT '1',
  `cashReferenceId` int(11) NOT NULL,
  `cashPurpose` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `cash_transfer`
--

CREATE TABLE `cash_transfer` (
  `cashTransferId` int(11) NOT NULL,
  `cashWithdrawnFrom` int(11) NOT NULL,
  `cashTransferredTo` int(11) NOT NULL,
  `cashTransferAmount` double NOT NULL,
  `cashSlipNo` varchar(150) COLLATE utf8_bin NOT NULL,
  `cashTransferDate` datetime NOT NULL,
  `cashTransferNote` text COLLATE utf8_bin NOT NULL,
  `cashTransferStatus` int(2) NOT NULL,
  `cashTransferEntryDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `current_bill_info`
--

CREATE TABLE `current_bill_info` (
  `currentBillId` int(11) NOT NULL,
  `monthly_bill_info_mothlyBillInfoId` int(11) NOT NULL,
  `bill_info_billId` int(11) NOT NULL,
  `meter_info_meterId` int(11) NOT NULL,
  `currentBillPrevReading` double NOT NULL,
  `currentBillPrevReadingDate` datetime NOT NULL,
  `currentBillCurrentReading` double NOT NULL,
  `currentBillCurrentReadingDate` datetime NOT NULL,
  `perUnitCharge` double NOT NULL,
  `currentBillDemandCharge` double NOT NULL,
  `currentBillServiceCharge` double NOT NULL,
  `totalCurrentBill` double NOT NULL,
  `meter_info_billDue` double NOT NULL,
  `currentBillAddedDate` datetime NOT NULL,
  `referrenceId` int(11) NOT NULL COMMENT 'transection referrence'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `customer_info`
--

CREATE TABLE `customer_info` (
  `customerId` int(11) NOT NULL,
  `customerName` varchar(200) COLLATE utf8_bin NOT NULL,
  `customerPhone` varchar(15) COLLATE utf8_bin NOT NULL,
  `organizationName` varchar(250) COLLATE utf8_bin NOT NULL,
  `spaceType` int(11) NOT NULL,
  `spaceTitle` varchar(50) COLLATE utf8_bin NOT NULL,
  `leasingMethodId` int(11) NOT NULL,
  `rentAmount` double NOT NULL,
  `rentDue` double NOT NULL DEFAULT '0',
  `billDue` double NOT NULL DEFAULT '0',
  `customerStatus` int(1) NOT NULL DEFAULT '1',
  `customerAddedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `customer_services`
--

CREATE TABLE `customer_services` (
  `customerServiceId` int(11) NOT NULL,
  `customer_info_customerId` int(11) NOT NULL,
  `services_serviceId` int(11) NOT NULL,
  `meter_info_meterId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `leasing_method`
--

CREATE TABLE `leasing_method` (
  `leasingId` int(11) NOT NULL,
  `leasingName` varchar(50) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `leasing_method`
--

INSERT INTO `leasing_method` (`leasingId`, `leasingName`) VALUES
(1, 'Rent'),
(2, 'Jomidari'),
(3, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `meter_info`
--

CREATE TABLE `meter_info` (
  `meterId` int(11) NOT NULL,
  `customer_info_customerId` int(11) NOT NULL,
  `meterCustomerId` varchar(100) COLLATE utf8_bin NOT NULL COMMENT 'admin will give a id/ name',
  `previousReading` double NOT NULL,
  `previousReadingDate` datetime NOT NULL,
  `currentReading` double NOT NULL,
  `meterNo` varchar(60) COLLATE utf8_bin NOT NULL,
  `demandCharge` double NOT NULL,
  `meterServiceCharge` double NOT NULL,
  `billDue` double NOT NULL,
  `meterStatus` int(11) NOT NULL DEFAULT '1' COMMENT '1=Active, 0=InActive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `monthly_bill_info`
--

CREATE TABLE `monthly_bill_info` (
  `mothlyBillInfoId` int(11) NOT NULL,
  `monthlyBillingStatus` int(2) NOT NULL COMMENT '1=pending, 2=final',
  `monthlyBillTypeFor` int(2) NOT NULL COMMENT '1=shop,2=office,3=others, 4=Rent Payment, 5=Bill Payment',
  `billingMonthYear` date NOT NULL,
  `monthlyBillAddedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `payment_history`
--

CREATE TABLE `payment_history` (
  `paymentId` int(11) NOT NULL,
  `customer_info_customerId` int(11) NOT NULL,
  `paymentAmount` double NOT NULL,
  `payment_types_paymentTypeId` int(11) NOT NULL COMMENT '1=Bill, 2=Rent',
  `paymentDetails` text COLLATE utf8_bin NOT NULL,
  `paymentDate` datetime NOT NULL,
  `referrenceId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `payment_types`
--

CREATE TABLE `payment_types` (
  `paymentTypeId` int(11) NOT NULL,
  `paymentTypeName` varchar(60) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `payment_types`
--

INSERT INTO `payment_types` (`paymentTypeId`, `paymentTypeName`) VALUES
(1, 'Bill Payment'),
(2, 'Rent Payment');

-- --------------------------------------------------------

--
-- Table structure for table `petty_cash`
--

CREATE TABLE `petty_cash` (
  `pettyCashId` int(11) NOT NULL,
  `pettyCashDate` datetime NOT NULL,
  `pettyCashEntryDate` datetime NOT NULL,
  `pettyCashto` varchar(500) COLLATE utf8_bin NOT NULL,
  `pettyCashType` int(3) NOT NULL,
  `pettyCashAmount` double NOT NULL,
  `pettyCashStatus` int(2) NOT NULL,
  `pettyCashReferenceId` int(11) NOT NULL,
  `pettyCashPurpose` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `serviceId` int(11) NOT NULL,
  `serviceName` varchar(150) COLLATE utf8_bin NOT NULL,
  `serviceNote` varchar(300) COLLATE utf8_bin NOT NULL,
  `serviceType` int(2) NOT NULL COMMENT '1=common, 2=personal',
  `serviceAmount` double NOT NULL DEFAULT '0',
  `serviceStatus` int(11) NOT NULL DEFAULT '1' COMMENT '1=Active, 0=InActive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`serviceId`, `serviceName`, `serviceNote`, `serviceType`, `serviceAmount`, `serviceStatus`) VALUES
(1, 'Common Bill', 'Employee cost for Common Bill', 1, 0, 1),
(2, 'Government Charges', 'Government Charges', 1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `services_bill_info`
--

CREATE TABLE `services_bill_info` (
  `serviceBillId` int(11) NOT NULL,
  `services_serviceId` int(11) NOT NULL,
  `bill_info_billId` int(11) NOT NULL,
  `current_bill_info_currentBillId` int(11) NOT NULL,
  `monthly_bill_info_mothlyBillInfoId` int(11) NOT NULL,
  `serviceAmount` double NOT NULL,
  `serviceBillingMonth` date NOT NULL,
  `serviceBillAddedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `space_type`
--

CREATE TABLE `space_type` (
  `spaceTypeId` int(11) NOT NULL,
  `spaceTypeName` varchar(50) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `space_type`
--

INSERT INTO `space_type` (`spaceTypeId`, `spaceTypeName`) VALUES
(1, 'Shop'),
(2, 'Office'),
(3, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `transaction_type_info`
--

CREATE TABLE `transaction_type_info` (
  `transactionId` int(11) NOT NULL,
  `transactionName` varchar(50) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `transaction_type_info`
--

INSERT INTO `transaction_type_info` (`transactionId`, `transactionName`) VALUES
(1, 'Rent Payment'),
(2, 'Bill Payment'),
(3, 'Cash In'),
(4, 'Cash Out'),
(5, 'Bank Transactions');

-- --------------------------------------------------------

--
-- Table structure for table `transection_info`
--

CREATE TABLE `transection_info` (
  `transectionId` int(11) NOT NULL,
  `TransectionType` int(2) NOT NULL COMMENT '1=rent bill, 2=current bill , 3=rent payment, 4=bill Payment',
  `customer_info_customerId` int(11) NOT NULL,
  `transectionAmount` double NOT NULL,
  `transectionPrevDue` double NOT NULL,
  `transectionDetails` text COLLATE utf8_bin NOT NULL,
  `transectionDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_info`
--
ALTER TABLE `admin_info`
  ADD PRIMARY KEY (`adminID`),
  ADD UNIQUE KEY `adminUserID` (`adminUserID`);

--
-- Indexes for table `admin_role`
--
ALTER TABLE `admin_role`
  ADD PRIMARY KEY (`roleID`);

--
-- Indexes for table `all_transaction_info`
--
ALTER TABLE `all_transaction_info`
  ADD PRIMARY KEY (`transactionId`);

--
-- Indexes for table `bank_account_info`
--
ALTER TABLE `bank_account_info`
  ADD PRIMARY KEY (`bankAccountId`);

--
-- Indexes for table `bill_info`
--
ALTER TABLE `bill_info`
  ADD PRIMARY KEY (`billId`);

--
-- Indexes for table `cash_in_hand`
--
ALTER TABLE `cash_in_hand`
  ADD PRIMARY KEY (`cashInHendId`);

--
-- Indexes for table `cash_transfer`
--
ALTER TABLE `cash_transfer`
  ADD PRIMARY KEY (`cashTransferId`);

--
-- Indexes for table `current_bill_info`
--
ALTER TABLE `current_bill_info`
  ADD PRIMARY KEY (`currentBillId`);

--
-- Indexes for table `customer_info`
--
ALTER TABLE `customer_info`
  ADD PRIMARY KEY (`customerId`);

--
-- Indexes for table `customer_services`
--
ALTER TABLE `customer_services`
  ADD PRIMARY KEY (`customerServiceId`);

--
-- Indexes for table `leasing_method`
--
ALTER TABLE `leasing_method`
  ADD PRIMARY KEY (`leasingId`);

--
-- Indexes for table `meter_info`
--
ALTER TABLE `meter_info`
  ADD PRIMARY KEY (`meterId`);

--
-- Indexes for table `monthly_bill_info`
--
ALTER TABLE `monthly_bill_info`
  ADD PRIMARY KEY (`mothlyBillInfoId`);

--
-- Indexes for table `payment_history`
--
ALTER TABLE `payment_history`
  ADD PRIMARY KEY (`paymentId`);

--
-- Indexes for table `payment_types`
--
ALTER TABLE `payment_types`
  ADD PRIMARY KEY (`paymentTypeId`);

--
-- Indexes for table `petty_cash`
--
ALTER TABLE `petty_cash`
  ADD PRIMARY KEY (`pettyCashId`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`serviceId`);

--
-- Indexes for table `services_bill_info`
--
ALTER TABLE `services_bill_info`
  ADD PRIMARY KEY (`serviceBillId`);

--
-- Indexes for table `space_type`
--
ALTER TABLE `space_type`
  ADD PRIMARY KEY (`spaceTypeId`);

--
-- Indexes for table `transaction_type_info`
--
ALTER TABLE `transaction_type_info`
  ADD PRIMARY KEY (`transactionId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_info`
--
ALTER TABLE `admin_info`
  MODIFY `adminID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `admin_role`
--
ALTER TABLE `admin_role`
  MODIFY `roleID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `all_transaction_info`
--
ALTER TABLE `all_transaction_info`
  MODIFY `transactionId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bank_account_info`
--
ALTER TABLE `bank_account_info`
  MODIFY `bankAccountId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `bill_info`
--
ALTER TABLE `bill_info`
  MODIFY `billId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cash_in_hand`
--
ALTER TABLE `cash_in_hand`
  MODIFY `cashInHendId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cash_transfer`
--
ALTER TABLE `cash_transfer`
  MODIFY `cashTransferId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `current_bill_info`
--
ALTER TABLE `current_bill_info`
  MODIFY `currentBillId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customer_info`
--
ALTER TABLE `customer_info`
  MODIFY `customerId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customer_services`
--
ALTER TABLE `customer_services`
  MODIFY `customerServiceId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `leasing_method`
--
ALTER TABLE `leasing_method`
  MODIFY `leasingId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `meter_info`
--
ALTER TABLE `meter_info`
  MODIFY `meterId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `monthly_bill_info`
--
ALTER TABLE `monthly_bill_info`
  MODIFY `mothlyBillInfoId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `payment_history`
--
ALTER TABLE `payment_history`
  MODIFY `paymentId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `payment_types`
--
ALTER TABLE `payment_types`
  MODIFY `paymentTypeId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `petty_cash`
--
ALTER TABLE `petty_cash`
  MODIFY `pettyCashId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `serviceId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `services_bill_info`
--
ALTER TABLE `services_bill_info`
  MODIFY `serviceBillId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `space_type`
--
ALTER TABLE `space_type`
  MODIFY `spaceTypeId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `transaction_type_info`
--
ALTER TABLE `transaction_type_info`
  MODIFY `transactionId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
