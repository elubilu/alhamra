-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 27, 2017 at 12:43 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `alhamra`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_info`
--

CREATE TABLE `admin_info` (
  `adminID` int(11) NOT NULL,
  `adminName` varchar(40) COLLATE utf8_bin NOT NULL,
  `adminEmail` varchar(50) COLLATE utf8_bin NOT NULL,
  `adminContact` varchar(20) COLLATE utf8_bin NOT NULL,
  `adminAddress` text COLLATE utf8_bin NOT NULL,
  `adminNote` text COLLATE utf8_bin NOT NULL,
  `adminUserID` varchar(30) COLLATE utf8_bin NOT NULL,
  `adminPassword` varchar(400) COLLATE utf8_bin NOT NULL,
  `adminStatus` int(11) NOT NULL COMMENT '0=InActive, 1=Active',
  `admin_role_roleID` int(11) NOT NULL,
  `adminJoinDate` datetime NOT NULL,
  `adminUpdateDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `admin_info`
--

INSERT INTO `admin_info` (`adminID`, `adminName`, `adminEmail`, `adminContact`, `adminAddress`, `adminNote`, `adminUserID`, `adminPassword`, `adminStatus`, `admin_role_roleID`, `adminJoinDate`, `adminUpdateDate`) VALUES
(1, 'SatrlabIT', 'starlabTeam@gmail.com', '01719450855', 'Zindabazar,Sylhet', '', 'superadmin', 'd7ea52fb792bed01df7174c48605cf19', 1, 1, '2017-03-09 00:14:57', '0000-00-00 00:00:00'),
(2, 'Shamsia Sharmin', 'shamsia@gmail.com', '011759721012', 'Lovely Road, Sylhet', '', 'manager', 'd7ea52fb792bed01df7174c48605cf19', 1, 3, '2017-03-24 22:05:49', '0000-00-00 00:00:00'),
(3, 'Keshob Chakrabory', 'keshob.kc@gmail.com', '01719450855', 'Address', '', 'admin', 'd7ea52fb792bed01df7174c48605cf19', 1, 2, '2017-02-10 21:39:54', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `admin_role`
--

CREATE TABLE `admin_role` (
  `roleID` int(11) NOT NULL,
  `roleName` varchar(100) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `admin_role`
--

INSERT INTO `admin_role` (`roleID`, `roleName`) VALUES
(1, 'Supper Admin'),
(2, 'Admin'),
(3, 'Manager'),
(4, 'Accountent'),
(5, 'Cashier'),
(6, 'Spectator');

-- --------------------------------------------------------

--
-- Table structure for table `all_transaction_info`
--

CREATE TABLE `all_transaction_info` (
  `transactionId` int(11) NOT NULL,
  `transactionType` int(3) NOT NULL COMMENT '1=rent bill, 2=current bill , 3=rent payment, 4=bill Payment',
  `transactionReferenceId` int(11) NOT NULL,
  `transactionAmount` double NOT NULL,
  `transactionPrevDue` double NOT NULL,
  `transactionDetails` text COLLATE utf8_bin NOT NULL,
  `transactionDate` datetime NOT NULL,
  `transactionFrom` int(11) NOT NULL,
  `transactionTo` int(11) NOT NULL,
  `transactionEntryDate` int(11) NOT NULL,
  `transactionStatus` int(2) NOT NULL COMMENT '1=Active, 0=InActive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `all_transaction_info`
--

INSERT INTO `all_transaction_info` (`transactionId`, `transactionType`, `transactionReferenceId`, `transactionAmount`, `transactionPrevDue`, `transactionDetails`, `transactionDate`, `transactionFrom`, `transactionTo`, `transactionEntryDate`, `transactionStatus`) VALUES
(1, 3, 1, 600, 1600, 'Rent Payment', '2017-07-02 02:37:15', 0, 0, 0, 0),
(2, 3, 2, 20000, 22000, 'Rent Payment', '2017-07-02 03:01:48', 0, 0, 0, 0),
(3, 1, 2, 20000, 2000, 'Monthly Rent', '2017-07-02 03:04:55', 0, 0, 0, 0),
(4, 2, 2, 1520, 1000, 'Monthly Bill', '2017-07-02 03:08:18', 0, 0, 0, 0),
(5, 2, 2, 1602, 2000, 'Monthly Bill', '2017-07-02 03:08:18', 0, 0, 0, 0),
(6, 1, 1, 1600, 1000, 'Monthly Rent', '2017-07-02 03:09:21', 0, 0, 0, 0),
(7, 2, 1, 2370, 2600, 'Monthly Bill', '2017-07-02 03:10:22', 0, 0, 0, 0),
(8, 2, 1, 1920, 2600, 'Monthly Bill', '2017-07-02 03:10:22', 0, 0, 0, 0),
(9, 1, 2, 20000, 22000, 'Monthly Rent', '2017-07-02 03:55:33', 0, 0, 0, 0),
(10, 2, 2, 2050, 4632, 'Monthly Bill', '2017-07-02 03:57:38', 0, 0, 0, 0),
(11, 2, 2, 1825, 6682, 'Monthly Bill', '2017-07-02 03:57:38', 0, 0, 0, 0),
(12, 4, 2, 5000, 8507, 'Monthly Bill Payment', '2017-07-02 03:58:40', 0, 0, 0, 0),
(13, 1, 2, 20000, 42000, 'Monthly Rent', '2017-07-13 12:52:38', 0, 0, 0, 0),
(14, 2, 2, 1375, 3507, 'Monthly Bill', '2017-07-13 12:53:16', 0, 0, 0, 0),
(15, 2, 2, 1372.5, 4882, 'Monthly Bill', '2017-07-13 12:53:16', 0, 0, 0, 0),
(16, 1, 1, 1600, 2600, 'Monthly Rent', '2017-07-13 12:57:50', 0, 0, 0, 0),
(17, 1, 1, 1600, 4200, 'Monthly Rent', '2017-07-27 13:04:24', 0, 0, 0, 0),
(18, 1, 2, 20000, 62000, 'Monthly Rent', '2017-07-27 13:05:08', 0, 0, 0, 0),
(19, 1, 2, 20000, 82000, 'Monthly Rent', '2017-07-27 13:23:29', 0, 0, 0, 0),
(20, 2, 2, 1208, 6254.5, 'Monthly Bill', '2017-07-27 13:24:05', 0, 0, 0, 0),
(21, 2, 2, 908, 7462.5, 'Monthly Bill', '2017-07-27 13:24:05', 0, 0, 0, 0),
(22, 2, 2, 1630, 8370.5, 'Monthly Bill', '2017-07-27 13:24:05', 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `bill_info`
--

CREATE TABLE `bill_info` (
  `billId` int(11) NOT NULL,
  `monthly_bill_info_mothlyBillInfoId` int(11) NOT NULL,
  `customer_info_customerId` int(11) NOT NULL,
  `billingMonth` date NOT NULL,
  `customer_info_rentAmount` double NOT NULL,
  `customer_info_rentDue` double NOT NULL,
  `billStatus` int(2) NOT NULL DEFAULT '1' COMMENT '1=pending, 2=final',
  `referrenceId` int(11) NOT NULL COMMENT 'transection referrence',
  `billAddedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `bill_info`
--

INSERT INTO `bill_info` (`billId`, `monthly_bill_info_mothlyBillInfoId`, `customer_info_customerId`, `billingMonth`, `customer_info_rentAmount`, `customer_info_rentDue`, `billStatus`, `referrenceId`, `billAddedDate`) VALUES
(1, 1, 1, '2017-05-01', 1600, 0, 1, 0, '2017-07-01 22:14:11'),
(2, 2, 2, '2017-05-01', 20000, 2000, 1, 0, '2017-07-01 23:12:50'),
(3, 5, 2, '2017-06-01', 20000, 2000, 1, 3, '2017-07-02 03:04:55'),
(4, 6, 1, '2017-06-01', 1600, 1000, 1, 6, '2017-07-02 03:09:21'),
(5, 7, 2, '2017-07-01', 20000, 22000, 1, 9, '2017-07-02 03:55:33'),
(6, 8, 2, '2017-07-01', 20000, 42000, 1, 13, '2017-07-13 12:52:38'),
(7, 9, 1, '2017-07-01', 1600, 2600, 1, 16, '2017-07-13 12:57:50'),
(8, 10, 1, '2017-08-01', 1600, 4200, 1, 17, '2017-07-27 13:04:24'),
(9, 11, 2, '2017-08-01', 20000, 62000, 1, 18, '2017-07-27 13:05:08'),
(10, 12, 2, '2017-08-01', 20000, 82000, 1, 19, '2017-07-27 13:23:29');

-- --------------------------------------------------------

--
-- Table structure for table `cash_in_hend`
--

CREATE TABLE `cash_in_hend` (
  `cashInHendId` int(11) NOT NULL,
  `cashDate` datetime NOT NULL,
  `cashEntryDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `cashTo` int(11) NOT NULL,
  `cashInType` int(3) NOT NULL COMMENT '1=cash in, 2=cash out',
  `cashAmount` double NOT NULL,
  `cashInStatus` int(2) NOT NULL DEFAULT '1',
  `cashReferenceId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `current_bill_info`
--

CREATE TABLE `current_bill_info` (
  `currentBillId` int(11) NOT NULL,
  `monthly_bill_info_mothlyBillInfoId` int(11) NOT NULL,
  `bill_info_billId` int(11) NOT NULL,
  `meter_info_meterId` int(11) NOT NULL,
  `currentBillPrevReading` double NOT NULL,
  `currentBillPrevReadingDate` datetime NOT NULL,
  `currentBillCurrentReading` double NOT NULL,
  `currentBillCurrentReadingDate` datetime NOT NULL,
  `perUnitCharge` double NOT NULL,
  `currentBillDemandCharge` double NOT NULL,
  `currentBillServiceCharge` double NOT NULL,
  `totalCurrentBill` double NOT NULL,
  `meter_info_billDue` double NOT NULL,
  `currentBillAddedDate` datetime NOT NULL,
  `referrenceId` int(11) NOT NULL COMMENT 'transection referrence'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `current_bill_info`
--

INSERT INTO `current_bill_info` (`currentBillId`, `monthly_bill_info_mothlyBillInfoId`, `bill_info_billId`, `meter_info_meterId`, `currentBillPrevReading`, `currentBillPrevReadingDate`, `currentBillCurrentReading`, `currentBillCurrentReadingDate`, `perUnitCharge`, `currentBillDemandCharge`, `currentBillServiceCharge`, `totalCurrentBill`, `meter_info_billDue`, `currentBillAddedDate`, `referrenceId`) VALUES
(1, 1, 1, 1, 5, '2017-07-01 22:09:56', 7, '2017-07-01 23:02:47', 10, 200, 100, 0, 500, '2017-07-01 22:14:11', 0),
(2, 1, 1, 2, 6.9, '2017-07-01 22:12:07', 8, '2017-07-01 23:03:04', 10, 150, 100, 0, 0, '2017-07-01 22:14:11', 0),
(3, 2, 2, 3, 5.5, '2017-07-01 22:12:46', 8, '2017-07-01 23:13:24', 100, 200, 100, 0, 900, '2017-07-01 23:12:50', 0),
(4, 2, 2, 4, 5.8, '2017-07-01 22:13:35', 8.9, '2017-07-01 23:13:25', 100, 150, 100, 0, 0, '2017-07-01 23:12:50', 0),
(5, 5, 3, 3, 8, '2017-07-01 23:13:24', 12, '2017-07-02 03:05:14', 120, 200, 100, 1520, 900, '2017-07-02 03:04:55', 4),
(6, 5, 3, 4, 8.9, '2017-07-01 23:13:25', 13, '2017-07-02 03:05:15', 120, 150, 100, 1602, 0, '2017-07-02 03:04:55', 5),
(7, 6, 4, 1, 7, '2017-07-01 23:02:47', 10, '2017-07-02 03:09:50', 200, 200, 100, 2370, 500, '2017-07-02 03:09:21', 7),
(8, 6, 4, 2, 8, '2017-07-01 23:03:04', 12, '2017-07-02 03:10:16', 200, 150, 100, 1920, 0, '2017-07-02 03:09:21', 8),
(9, 7, 5, 3, 12, '2017-07-02 03:05:14', 15, '2017-07-02 03:57:34', 250, 200, 100, 2050, 900, '2017-07-02 03:55:33', 10),
(10, 7, 5, 4, 13, '2017-07-02 03:05:15', 15.5, '2017-07-02 03:57:30', 250, 150, 100, 1825, 0, '2017-07-02 03:55:33', 11),
(11, 8, 6, 3, 15, '2017-07-02 03:57:34', 20, '2017-07-13 12:53:03', 5, 200, 100, 1375, 900, '2017-07-13 12:52:38', 14),
(12, 8, 6, 4, 15.5, '2017-07-02 03:57:30', 20, '2017-07-13 12:53:14', 5, 150, 100, 1372.5, 0, '2017-07-13 12:52:38', 15),
(13, 9, 7, 1, 10, '2017-07-02 03:09:50', 600, '2017-07-26 14:57:57', 5, 200, 100, 0, 500, '2017-07-13 12:57:50', 0),
(14, 9, 7, 2, 12, '2017-07-02 03:10:16', 100, '2017-07-26 14:58:15', 5, 150, 100, 0, 0, '2017-07-13 12:57:50', 0),
(15, 10, 8, 1, 600, '2017-07-26 14:57:57', 0, '0000-00-00 00:00:00', 5.8, 200, 100, 0, 500, '2017-07-27 13:04:24', 0),
(16, 11, 9, 4, 20, '2017-07-13 12:53:14', 0, '0000-00-00 00:00:00', 5.8, 150, 100, 0, 0, '2017-07-27 13:05:08', 0),
(17, 12, 10, 3, 20, '2017-07-13 12:53:03', 30, '2017-07-27 13:24:01', 5.8, 200, 100, 1208, 900, '2017-07-27 13:23:29', 20),
(18, 12, 10, 4, 20, '2017-07-13 12:53:14', 30, '2017-07-27 13:24:02', 5.8, 150, 100, 908, 0, '2017-07-27 13:23:29', 21),
(19, 12, 10, 5, 200, '2017-07-27 13:22:42', 250, '2017-07-27 13:24:03', 5.8, 160, 180, 1630, 0, '2017-07-27 13:23:29', 22);

-- --------------------------------------------------------

--
-- Table structure for table `customer_info`
--

CREATE TABLE `customer_info` (
  `customerId` int(11) NOT NULL,
  `customerName` varchar(200) COLLATE utf8_bin NOT NULL,
  `customerPhone` varchar(15) COLLATE utf8_bin NOT NULL,
  `organizationName` varchar(250) COLLATE utf8_bin NOT NULL,
  `spaceType` int(11) NOT NULL,
  `spaceTitle` varchar(50) COLLATE utf8_bin NOT NULL,
  `leasingMethodId` int(11) NOT NULL,
  `rentAmount` double NOT NULL,
  `rentDue` double NOT NULL DEFAULT '0',
  `billDue` double NOT NULL DEFAULT '0',
  `customerStatus` int(1) NOT NULL DEFAULT '1',
  `customerAddedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `customer_info`
--

INSERT INTO `customer_info` (`customerId`, `customerName`, `customerPhone`, `organizationName`, `spaceType`, `spaceTitle`, `leasingMethodId`, `rentAmount`, `rentDue`, `billDue`, `customerStatus`, `customerAddedDate`) VALUES
(1, 'Kanthi Lal', '017123455546', 'Starlab IT', 2, 's-708', 1, 1600, 5800, 6321, 1, '2017-07-01 22:04:43'),
(2, 'Mr. Someone ', '93748976579', 'Global', 1, 's-707', 1, 20000, 102000, 10000.5, 1, '2017-07-01 22:07:20');

-- --------------------------------------------------------

--
-- Table structure for table `customer_services`
--

CREATE TABLE `customer_services` (
  `customerServiceId` int(11) NOT NULL,
  `customer_info_customerId` int(11) NOT NULL,
  `services_serviceId` int(11) NOT NULL,
  `meter_info_meterId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `customer_services`
--

INSERT INTO `customer_services` (`customerServiceId`, `customer_info_customerId`, `services_serviceId`, `meter_info_meterId`) VALUES
(1, 1, 1, 1),
(2, 1, 2, 1),
(3, 1, 3, 1),
(4, 1, 5, 1),
(5, 1, 6, 1),
(6, 1, 1, 2),
(7, 1, 2, 2),
(8, 1, 7, 2),
(9, 2, 1, 3),
(10, 2, 8, 3),
(11, 2, 3, 3),
(12, 2, 1, 4),
(13, 2, 2, 4),
(14, 2, 8, 4),
(15, 2, 1, 5),
(16, 2, 3, 5),
(17, 2, 4, 5),
(18, 2, 9, 5);

-- --------------------------------------------------------

--
-- Table structure for table `leasing_method`
--

CREATE TABLE `leasing_method` (
  `leasingId` int(11) NOT NULL,
  `leasingName` varchar(50) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `leasing_method`
--

INSERT INTO `leasing_method` (`leasingId`, `leasingName`) VALUES
(1, 'Rent'),
(2, 'Jomidari'),
(3, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `meter_info`
--

CREATE TABLE `meter_info` (
  `meterId` int(11) NOT NULL,
  `customer_info_customerId` int(11) NOT NULL,
  `meterCustomerId` varchar(100) COLLATE utf8_bin NOT NULL COMMENT 'admin will give a id/ name',
  `previousReading` double NOT NULL,
  `previousReadingDate` datetime NOT NULL,
  `currentReading` double NOT NULL,
  `meterNo` varchar(60) COLLATE utf8_bin NOT NULL,
  `demandCharge` double NOT NULL,
  `meterServiceCharge` double NOT NULL,
  `billDue` double NOT NULL,
  `meterStatus` int(11) NOT NULL DEFAULT '1' COMMENT '1=Active, 0=InActive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `meter_info`
--

INSERT INTO `meter_info` (`meterId`, `customer_info_customerId`, `meterCustomerId`, `previousReading`, `previousReadingDate`, `currentReading`, `meterNo`, `demandCharge`, `meterServiceCharge`, `billDue`, `meterStatus`) VALUES
(1, 1, 'Starlab', 600, '2017-07-26 14:57:57', 0, 'M-702', 200, 100, 500, 1),
(2, 1, 'Starlab', 100, '2017-07-26 14:58:15', 0, 'M-708', 150, 100, 0, 1),
(3, 2, 'global', 30, '2017-07-27 13:24:01', 0, 'M-707', 200, 100, 900, 1),
(4, 2, 'global', 30, '2017-07-27 13:24:02', 0, 'M-706', 150, 100, 0, 1),
(5, 2, 'global', 250, '2017-07-27 13:24:03', 0, 's-710', 160, 180, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `monthly_bill_info`
--

CREATE TABLE `monthly_bill_info` (
  `mothlyBillInfoId` int(11) NOT NULL,
  `monthlyBillingStatus` int(2) NOT NULL COMMENT '1=pending, 2=final',
  `monthlyBillTypeFor` int(2) NOT NULL COMMENT '1=shop,2=office,3=others, 4=Rent Payment, 5=Bill Payment',
  `billingMonthYear` date NOT NULL,
  `monthlyBillAddedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `monthly_bill_info`
--

INSERT INTO `monthly_bill_info` (`mothlyBillInfoId`, `monthlyBillingStatus`, `monthlyBillTypeFor`, `billingMonthYear`, `monthlyBillAddedDate`) VALUES
(1, 2, 2, '2017-05-01', '2017-07-01 22:14:11'),
(2, 2, 1, '2017-05-01', '2017-07-01 23:12:50'),
(5, 2, 1, '2017-06-01', '2017-07-02 03:04:55'),
(6, 2, 2, '2017-06-01', '2017-07-02 03:09:21'),
(7, 2, 1, '2017-07-01', '2017-07-02 03:55:33'),
(8, 2, 1, '2017-07-01', '2017-07-13 12:52:38'),
(9, 1, 2, '2017-07-01', '2017-07-13 12:57:50'),
(10, 1, 2, '2017-08-01', '2017-07-27 13:04:24'),
(11, 1, 1, '2017-08-01', '2017-07-27 13:05:08'),
(12, 2, 1, '2017-08-01', '2017-07-27 13:23:29');

-- --------------------------------------------------------

--
-- Table structure for table `payment_history`
--

CREATE TABLE `payment_history` (
  `paymentId` int(11) NOT NULL,
  `customer_info_customerId` int(11) NOT NULL,
  `paymentAmount` double NOT NULL,
  `payment_types_paymentTypeId` int(11) NOT NULL COMMENT '1=Bill, 2=Rent',
  `paymentDetails` text COLLATE utf8_bin NOT NULL,
  `paymentDate` datetime NOT NULL,
  `referrenceId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `payment_history`
--

INSERT INTO `payment_history` (`paymentId`, `customer_info_customerId`, `paymentAmount`, `payment_types_paymentTypeId`, `paymentDetails`, `paymentDate`, `referrenceId`) VALUES
(1, 2, 2000, 1, 'Bill Payment', '2017-07-02 01:30:18', 0),
(2, 1, 600, 2, 'Rent Payment', '2017-07-02 02:37:15', 1),
(3, 2, 20000, 2, 'Rent Payment', '2017-07-02 03:01:48', 2),
(4, 2, 5000, 1, 'Monthly Bill Payment', '2017-07-02 03:58:40', 12);

-- --------------------------------------------------------

--
-- Table structure for table `payment_types`
--

CREATE TABLE `payment_types` (
  `paymentTypeId` int(11) NOT NULL,
  `paymentTypeName` varchar(60) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `payment_types`
--

INSERT INTO `payment_types` (`paymentTypeId`, `paymentTypeName`) VALUES
(1, 'Bill Payment'),
(2, 'Rent Payment');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `serviceId` int(11) NOT NULL,
  `serviceName` varchar(150) COLLATE utf8_bin NOT NULL,
  `serviceNote` varchar(300) COLLATE utf8_bin NOT NULL,
  `serviceType` int(2) NOT NULL COMMENT '1=common, 2=personal',
  `serviceAmount` double NOT NULL DEFAULT '0',
  `serviceStatus` int(11) NOT NULL DEFAULT '1' COMMENT '1=Active, 0=InActive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`serviceId`, `serviceName`, `serviceNote`, `serviceType`, `serviceAmount`, `serviceStatus`) VALUES
(1, 'Common Bill', 'Employee cost for Common Bill', 1, 0, 1),
(2, 'Government Charges', 'Government Charges', 1, 0, 1),
(3, 'Cleaning Charge', 'Employee cost for cleaning', 1, 0, 1),
(4, 'Staff', 'cost for staffs', 1, 0, 1),
(5, 'Additional Charge', '', 2, 500, 1),
(6, ' Parking', '', 2, 200, 1),
(7, 'Additional charge', '', 2, 200, 1),
(8, 'Security Charges', '', 2, 300, 1),
(9, 'Parking', '', 2, 300, 1);

-- --------------------------------------------------------

--
-- Table structure for table `services_bill_info`
--

CREATE TABLE `services_bill_info` (
  `serviceBillId` int(11) NOT NULL,
  `services_serviceId` int(11) NOT NULL,
  `bill_info_billId` int(11) NOT NULL,
  `current_bill_info_currentBillId` int(11) NOT NULL,
  `monthly_bill_info_mothlyBillInfoId` int(11) NOT NULL,
  `serviceAmount` double NOT NULL,
  `serviceBillingMonth` date NOT NULL,
  `serviceBillAddedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `services_bill_info`
--

INSERT INTO `services_bill_info` (`serviceBillId`, `services_serviceId`, `bill_info_billId`, `current_bill_info_currentBillId`, `monthly_bill_info_mothlyBillInfoId`, `serviceAmount`, `serviceBillingMonth`, `serviceBillAddedDate`) VALUES
(1, 1, 1, 1, 1, 10, '2017-05-01', '2017-07-01 22:14:11'),
(2, 2, 1, 1, 1, 10, '2017-05-01', '2017-07-01 22:14:11'),
(3, 3, 1, 1, 1, 10, '2017-05-01', '2017-07-01 22:14:11'),
(4, 5, 1, 1, 1, 500, '2017-05-01', '2017-07-01 22:14:11'),
(5, 6, 1, 1, 1, 200, '2017-05-01', '2017-07-01 22:14:11'),
(6, 1, 1, 2, 1, 10, '2017-05-01', '2017-07-01 22:14:11'),
(7, 2, 1, 2, 1, 10, '2017-05-01', '2017-07-01 22:14:11'),
(8, 7, 1, 2, 1, 200, '2017-05-01', '2017-07-01 22:14:11'),
(9, 1, 2, 3, 2, 200, '2017-05-01', '2017-07-01 23:12:50'),
(10, 2, 2, 3, 2, 250, '2017-05-01', '2017-07-01 23:12:50'),
(11, 3, 2, 3, 2, 300, '2017-05-01', '2017-07-01 23:12:50'),
(12, 1, 2, 4, 2, 200, '2017-05-01', '2017-07-01 23:12:50'),
(13, 2, 2, 4, 2, 250, '2017-05-01', '2017-07-01 23:12:50'),
(14, 8, 2, 4, 2, 300, '2017-05-01', '2017-07-01 23:12:50'),
(15, 1, 3, 5, 5, 300, '2017-06-01', '2017-07-02 03:04:55'),
(16, 2, 3, 5, 5, 260, '2017-06-01', '2017-07-02 03:04:55'),
(17, 3, 3, 5, 5, 180, '2017-06-01', '2017-07-02 03:04:55'),
(18, 1, 3, 6, 5, 300, '2017-06-01', '2017-07-02 03:04:55'),
(19, 2, 3, 6, 5, 260, '2017-06-01', '2017-07-02 03:04:55'),
(20, 8, 3, 6, 5, 300, '2017-06-01', '2017-07-02 03:04:55'),
(21, 1, 4, 7, 6, 300, '2017-06-01', '2017-07-02 03:09:21'),
(22, 2, 4, 7, 6, 270, '2017-06-01', '2017-07-02 03:09:21'),
(23, 3, 4, 7, 6, 200, '2017-06-01', '2017-07-02 03:09:21'),
(24, 5, 4, 7, 6, 500, '2017-06-01', '2017-07-02 03:09:21'),
(25, 6, 4, 7, 6, 200, '2017-06-01', '2017-07-02 03:09:21'),
(26, 1, 4, 8, 6, 340, '2017-06-01', '2017-07-02 03:09:21'),
(27, 2, 4, 8, 6, 330, '2017-06-01', '2017-07-02 03:09:21'),
(28, 7, 4, 8, 6, 200, '2017-06-01', '2017-07-02 03:09:21'),
(29, 1, 5, 9, 7, 400, '2017-07-01', '2017-07-02 03:55:33'),
(30, 2, 5, 9, 7, 250, '2017-07-01', '2017-07-02 03:55:33'),
(31, 3, 5, 9, 7, 350, '2017-07-01', '2017-07-02 03:55:33'),
(32, 1, 5, 10, 7, 400, '2017-07-01', '2017-07-02 03:55:33'),
(33, 2, 5, 10, 7, 250, '2017-07-01', '2017-07-02 03:55:33'),
(34, 8, 5, 10, 7, 300, '2017-07-01', '2017-07-02 03:55:33'),
(35, 1, 6, 11, 8, 300, '2017-07-01', '2017-07-13 12:52:38'),
(36, 2, 6, 11, 8, 500, '2017-07-01', '2017-07-13 12:52:38'),
(37, 3, 6, 11, 8, 250, '2017-07-01', '2017-07-13 12:52:38'),
(38, 1, 6, 12, 8, 300, '2017-07-01', '2017-07-13 12:52:38'),
(39, 2, 6, 12, 8, 500, '2017-07-01', '2017-07-13 12:52:38'),
(40, 8, 6, 12, 8, 300, '2017-07-01', '2017-07-13 12:52:38'),
(41, 1, 7, 13, 9, 100, '2017-07-01', '2017-07-13 12:57:50'),
(42, 2, 7, 13, 9, 50, '2017-07-01', '2017-07-13 12:57:50'),
(43, 3, 7, 13, 9, 123, '2017-07-01', '2017-07-13 12:57:50'),
(44, 5, 7, 13, 9, 500, '2017-07-01', '2017-07-13 12:57:50'),
(45, 6, 7, 13, 9, 200, '2017-07-01', '2017-07-13 12:57:50'),
(46, 1, 7, 14, 9, 0, '2017-07-01', '2017-07-13 12:57:50'),
(47, 2, 7, 14, 9, 0, '2017-07-01', '2017-07-13 12:57:50'),
(48, 7, 7, 14, 9, 200, '2017-07-01', '2017-07-13 12:57:50'),
(49, 1, 8, 15, 10, 0, '2017-08-01', '2017-07-27 13:04:24'),
(50, 2, 8, 15, 10, 0, '2017-08-01', '2017-07-27 13:04:24'),
(51, 3, 8, 15, 10, 0, '2017-08-01', '2017-07-27 13:04:24'),
(52, 5, 8, 15, 10, 500, '2017-08-01', '2017-07-27 13:04:24'),
(53, 6, 8, 15, 10, 200, '2017-08-01', '2017-07-27 13:04:24'),
(54, 1, 9, 16, 11, 250, '2017-08-01', '2017-07-27 13:05:08'),
(55, 2, 9, 16, 11, 300, '2017-08-01', '2017-07-27 13:05:08'),
(56, 8, 9, 16, 11, 300, '2017-08-01', '2017-07-27 13:05:08'),
(57, 1, 10, 17, 12, 300, '2017-08-01', '2017-07-27 13:23:29'),
(58, 8, 10, 17, 12, 300, '2017-08-01', '2017-07-27 13:23:29'),
(59, 3, 10, 17, 12, 250, '2017-08-01', '2017-07-27 13:23:29'),
(60, 1, 10, 18, 12, 300, '2017-08-01', '2017-07-27 13:23:29'),
(61, 2, 10, 18, 12, 0, '2017-08-01', '2017-07-27 13:23:29'),
(62, 8, 10, 18, 12, 300, '2017-08-01', '2017-07-27 13:23:29'),
(63, 1, 10, 19, 12, 300, '2017-08-01', '2017-07-27 13:23:29'),
(64, 3, 10, 19, 12, 250, '2017-08-01', '2017-07-27 13:23:29'),
(65, 4, 10, 19, 12, 150, '2017-08-01', '2017-07-27 13:23:29'),
(66, 9, 10, 19, 12, 300, '2017-08-01', '2017-07-27 13:23:29');

-- --------------------------------------------------------

--
-- Table structure for table `space_type`
--

CREATE TABLE `space_type` (
  `spaceTypeId` int(11) NOT NULL,
  `spaceTypeName` varchar(50) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `space_type`
--

INSERT INTO `space_type` (`spaceTypeId`, `spaceTypeName`) VALUES
(1, 'Shop'),
(2, 'Office'),
(3, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `transaction_type_info`
--

CREATE TABLE `transaction_type_info` (
  `transactionId` int(11) NOT NULL,
  `transactionName` varchar(50) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `transaction_type_info`
--

INSERT INTO `transaction_type_info` (`transactionId`, `transactionName`) VALUES
(1, 'Rent Payment'),
(2, 'Bill Payment'),
(3, 'Cash In'),
(4, 'Cash Out'),
(5, 'Bank Transactions');

-- --------------------------------------------------------

--
-- Table structure for table `transection_info`
--

CREATE TABLE `transection_info` (
  `transectionId` int(11) NOT NULL,
  `TransectionType` int(2) NOT NULL COMMENT '1=rent bill, 2=current bill , 3=rent payment, 4=bill Payment',
  `customer_info_customerId` int(11) NOT NULL,
  `transectionAmount` double NOT NULL,
  `transectionPrevDue` double NOT NULL,
  `transectionDetails` text COLLATE utf8_bin NOT NULL,
  `transectionDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `transection_info`
--

INSERT INTO `transection_info` (`transectionId`, `TransectionType`, `customer_info_customerId`, `transectionAmount`, `transectionPrevDue`, `transectionDetails`, `transectionDate`) VALUES
(1, 3, 1, 600, 1600, 'Rent Payment', '2017-07-02 02:37:15'),
(2, 3, 2, 20000, 22000, 'Rent Payment', '2017-07-02 03:01:48'),
(3, 1, 2, 20000, 2000, 'Monthly Rent', '2017-07-02 03:04:55'),
(4, 2, 2, 1520, 1000, 'Monthly Bill', '2017-07-02 03:08:18'),
(5, 2, 2, 1602, 2000, 'Monthly Bill', '2017-07-02 03:08:18'),
(6, 1, 1, 1600, 1000, 'Monthly Rent', '2017-07-02 03:09:21'),
(7, 2, 1, 2370, 2600, 'Monthly Bill', '2017-07-02 03:10:22'),
(8, 2, 1, 1920, 2600, 'Monthly Bill', '2017-07-02 03:10:22'),
(9, 1, 2, 20000, 22000, 'Monthly Rent', '2017-07-02 03:55:33'),
(10, 2, 2, 2050, 4632, 'Monthly Bill', '2017-07-02 03:57:38'),
(11, 2, 2, 1825, 6682, 'Monthly Bill', '2017-07-02 03:57:38'),
(12, 4, 2, 5000, 8507, 'Monthly Bill Payment', '2017-07-02 03:58:40'),
(13, 1, 2, 20000, 42000, 'Monthly Rent', '2017-07-13 12:52:38'),
(14, 2, 2, 1375, 3507, 'Monthly Bill', '2017-07-13 12:53:16'),
(15, 2, 2, 1372.5, 4882, 'Monthly Bill', '2017-07-13 12:53:16'),
(16, 1, 1, 1600, 2600, 'Monthly Rent', '2017-07-13 12:57:50');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_info`
--
ALTER TABLE `admin_info`
  ADD PRIMARY KEY (`adminID`),
  ADD UNIQUE KEY `adminUserID` (`adminUserID`);

--
-- Indexes for table `admin_role`
--
ALTER TABLE `admin_role`
  ADD PRIMARY KEY (`roleID`);

--
-- Indexes for table `all_transaction_info`
--
ALTER TABLE `all_transaction_info`
  ADD PRIMARY KEY (`transactionId`);

--
-- Indexes for table `bill_info`
--
ALTER TABLE `bill_info`
  ADD PRIMARY KEY (`billId`);

--
-- Indexes for table `cash_in_hend`
--
ALTER TABLE `cash_in_hend`
  ADD PRIMARY KEY (`cashInHendId`);

--
-- Indexes for table `current_bill_info`
--
ALTER TABLE `current_bill_info`
  ADD PRIMARY KEY (`currentBillId`);

--
-- Indexes for table `customer_info`
--
ALTER TABLE `customer_info`
  ADD PRIMARY KEY (`customerId`);

--
-- Indexes for table `customer_services`
--
ALTER TABLE `customer_services`
  ADD PRIMARY KEY (`customerServiceId`);

--
-- Indexes for table `leasing_method`
--
ALTER TABLE `leasing_method`
  ADD PRIMARY KEY (`leasingId`);

--
-- Indexes for table `meter_info`
--
ALTER TABLE `meter_info`
  ADD PRIMARY KEY (`meterId`);

--
-- Indexes for table `monthly_bill_info`
--
ALTER TABLE `monthly_bill_info`
  ADD PRIMARY KEY (`mothlyBillInfoId`);

--
-- Indexes for table `payment_history`
--
ALTER TABLE `payment_history`
  ADD PRIMARY KEY (`paymentId`);

--
-- Indexes for table `payment_types`
--
ALTER TABLE `payment_types`
  ADD PRIMARY KEY (`paymentTypeId`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`serviceId`);

--
-- Indexes for table `services_bill_info`
--
ALTER TABLE `services_bill_info`
  ADD PRIMARY KEY (`serviceBillId`);

--
-- Indexes for table `space_type`
--
ALTER TABLE `space_type`
  ADD PRIMARY KEY (`spaceTypeId`);

--
-- Indexes for table `transaction_type_info`
--
ALTER TABLE `transaction_type_info`
  ADD PRIMARY KEY (`transactionId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_info`
--
ALTER TABLE `admin_info`
  MODIFY `adminID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `admin_role`
--
ALTER TABLE `admin_role`
  MODIFY `roleID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `all_transaction_info`
--
ALTER TABLE `all_transaction_info`
  MODIFY `transactionId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `bill_info`
--
ALTER TABLE `bill_info`
  MODIFY `billId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `cash_in_hend`
--
ALTER TABLE `cash_in_hend`
  MODIFY `cashInHendId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `current_bill_info`
--
ALTER TABLE `current_bill_info`
  MODIFY `currentBillId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `customer_info`
--
ALTER TABLE `customer_info`
  MODIFY `customerId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `customer_services`
--
ALTER TABLE `customer_services`
  MODIFY `customerServiceId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `leasing_method`
--
ALTER TABLE `leasing_method`
  MODIFY `leasingId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `meter_info`
--
ALTER TABLE `meter_info`
  MODIFY `meterId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `monthly_bill_info`
--
ALTER TABLE `monthly_bill_info`
  MODIFY `mothlyBillInfoId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `payment_history`
--
ALTER TABLE `payment_history`
  MODIFY `paymentId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `payment_types`
--
ALTER TABLE `payment_types`
  MODIFY `paymentTypeId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `serviceId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `services_bill_info`
--
ALTER TABLE `services_bill_info`
  MODIFY `serviceBillId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;
--
-- AUTO_INCREMENT for table `space_type`
--
ALTER TABLE `space_type`
  MODIFY `spaceTypeId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `transaction_type_info`
--
ALTER TABLE `transaction_type_info`
  MODIFY `transactionId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
